﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Events.EventArgs`1
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using System;

namespace GDRomExplorer.Events
{
  public class EventArgs<T> : EventArgs, IEventArgs<T>
  {
    public EventArgs(T value) => this.Value = value;

    public T Value { get; private set; }
  }
}
