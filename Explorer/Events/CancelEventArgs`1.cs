﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Events.CancelEventArgs`1
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using System.ComponentModel;

namespace GDRomExplorer.Events
{
  public class CancelEventArgs<T> : CancelEventArgs, IEventArgs<T>
  {
    public CancelEventArgs(T value)
      : this(value, false)
    {
    }

    public CancelEventArgs(T value, bool cancel)
      : base(cancel)
      => this.Value = value;

    public T Value { get; private set; }
  }
}
