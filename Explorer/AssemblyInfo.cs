﻿using GDRomExplorer;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyGitBuildSHA1("fd2eb45")]
[assembly: AssemblyGitBuildBranch("release_1.6.3")]
[assembly: AssemblyFileVersion("1.6.3")]
[assembly: AssemblyTitle("GD-ROM Explorer")]
[assembly: AssemblyDescription("GD-ROM Explorer for Dreamcast and Naomi disc images")]
[assembly: ComVisible(true)]
[assembly: AssemblyCompany("Japanese Cake")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyConfiguration("")]
[assembly: Guid("028a0854-6263-4b97-8b78-7512902f19d5")]
[assembly: AssemblyProduct("GD-ROM Explorer")]
[assembly: AssemblyCopyright("2010 - 2016")]
[assembly: AssemblyVersion("1.6.3.0")]
//[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.config",Watch = true)]
