﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Others.ListViewColumnSorter
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using ImageReader.ISO9660.DirectoryRecords;
using System;
using System.Collections;
using System.Windows.Forms;

namespace GDRomExplorer.Others
{
  public class ListViewColumnSorter : IComparer
  {
    public int SortColumn;
    public SortOrder Order;
    private CaseInsensitiveComparer ObjectCompare;

    public ListViewColumnSorter()
    {
      this.SortColumn = 0;
      this.Order = SortOrder.None;
      this.ObjectCompare = new CaseInsensitiveComparer();
    }

    public int Compare(object x, object y)
    {
      ListViewItem listViewItem1 = (ListViewItem) x;
      ListViewItem listViewItem2 = (ListViewItem) y;
      int num;
      if (listViewItem1.Name.Equals(DirectoryRecord.PARENT_DIRECTORY_NAME))
        num = this.Order == SortOrder.Ascending ? -1 : 1;
      else if (listViewItem1.Tag != null && listViewItem2.Tag != null)
      {
        DirectoryRecord tag1 = (DirectoryRecord) listViewItem1.Tag;
        DirectoryRecord tag2 = (DirectoryRecord) listViewItem2.Tag;
        switch (this.SortColumn)
        {
          case 0:
            num = this.CompareFilename(tag1, tag2);
            break;
          case 1:
          case 2:
            num = this.ObjectCompare.Compare((object) tag1.ExtentSize, (object) tag2.ExtentSize);
            break;
          case 3:
            num = this.ObjectCompare.Compare((object) tag1.Extent, (object) tag2.Extent);
            break;
          case 4:
            num = this.CompareDateTime(tag1.RecordingDateTime, tag2.RecordingDateTime);
            break;
          default:
            num = this.ObjectCompare.Compare((object) listViewItem1.SubItems[this.SortColumn].Text, (object) listViewItem2.SubItems[this.SortColumn].Text);
            break;
        }
      }
      else
        num = this.ObjectCompare.Compare((object) listViewItem1.SubItems[this.SortColumn].Text, (object) listViewItem2.SubItems[this.SortColumn].Text);
      if (this.Order == SortOrder.Ascending)
        return num;
      return this.Order == SortOrder.Descending ? -num : 0;
    }

    private int CompareFilename(DirectoryRecord x, DirectoryRecord y) => !x.IsDirectory || y.IsDirectory ? (!y.IsDirectory || x.IsDirectory ? this.ObjectCompare.Compare((object) x.Name, (object) y.Name) : 1) : -1;

    private int CompareDateTime(DateTime? x, DateTime? y)
    {
      if (!x.HasValue && !y.HasValue)
        return 0;
      if (!x.HasValue)
        return 1;
      return !y.HasValue ? -1 : DateTime.Compare(x.Value, y.Value);
    }
  }
}
