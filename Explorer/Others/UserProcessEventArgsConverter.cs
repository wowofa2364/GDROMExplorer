﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Others.UserProcessEventArgsConverter
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using SEGATools.UserProcess;
using System;
using System.Resources;
using System.Text;

namespace GDRomExplorer.Others
{
  public class UserProcessEventArgsConverter
  {
    public static int MaximumNumberOfLinesToDisplay = 10;

    public static string ToFormatedString(
      UserProcessWaitingForUserConsentEventArgs eventArgs,
      ResourceManager resourceManager)
    {
      return !(eventArgs is UserProcessWaitingForUserConsentFileConflictEventArgs) ? UserProcessEventArgsConverter.DefaultWaitForUserConsentEventArgs.ToFormatedString(eventArgs, resourceManager) : UserProcessEventArgsConverter.FileConflictEventArgs.ToFormatedString(eventArgs, resourceManager);
    }

    private static class DefaultWaitForUserConsentEventArgs
    {
      public static string ToFormatedString(
        UserProcessWaitingForUserConsentEventArgs eventArgs,
        ResourceManager resourceManager)
      {
        string format = resourceManager.GetString(eventArgs.QuestionContentResourceName);
        if (eventArgs.HasStringArguments)
          format = string.Format(format, (object[]) eventArgs.QuestionContentArgs);
        return format;
      }
    }

    private static class FileConflictEventArgs
    {
      public static string ToFormatedString(
        UserProcessWaitingForUserConsentEventArgs eventArgs,
        ResourceManager resourceManager)
      {
        return string.Format(resourceManager.GetString(eventArgs.QuestionContentResourceName), (object) UserProcessEventArgsConverter.FileConflictEventArgs.CreateFileList(eventArgs.QuestionContentArgs, UserProcessEventArgsConverter.MaximumNumberOfLinesToDisplay));
      }

      private static string CreateFileList(string[] files, int MaximumNumberOfFilesToDisplay)
      {
        StringBuilder stringBuilder = new StringBuilder();
        int num = Math.Min(MaximumNumberOfFilesToDisplay, files.Length);
        for (int index = 0; index < num; ++index)
        {
          string file = files[index];
          stringBuilder.Append("- ").Append(file).Append("\n");
        }
        if (num < files.Length)
          stringBuilder.Append("- etc.").Append("\n");
        return stringBuilder.ToString();
      }
    }
  }
}
