﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.UserControls.NaomiEncryptDecryptTool
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.Forms;
using GDRomExplorer.Resources;
using SEGATools.Encrypt;
using SEGATools.VirtualFile;
using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace GDRomExplorer.UserControls
{
  public class NaomiEncryptDecryptTool : Component
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private IWin32Window owner;
    public string InitialDirectory;
    private IContainer components;
    private OpenFileDialog openFileDialog;
    private SaveFileDialog saveFileDialog;

    public NaomiEncryptDecryptTool() => this.InitializeComponent();

    public NaomiEncryptDecryptTool(IContainer container)
    {
      container.Add((IComponent) this);
      this.InitializeComponent();
    }

    public void OpenAndEncryptNaomiBinary(IWin32Window owner)
    {
      this.owner = owner;
      this.ChangeFileDialogsForEncryption();
      NaomiEncryptDecryptTool.InputOutputFilesAndKeyHolder filesAndKeyHolder = this.OpenDialogsForInputAndOutputFiles(true);
      if (filesAndKeyHolder == null)
        return;
      DesEncryptDecryptTool desDecryptor = new DesEncryptDecryptTool();
      Guid TaskId = Guid.NewGuid();
      FormProcess.createForDesEncryptor(desDecryptor, TaskId);
      desDecryptor.EncryptAsync(filesAndKeyHolder.InputFile, filesAndKeyHolder.OutputFile, filesAndKeyHolder.DESKey, (object) TaskId);
    }

    public void OpenAndDecryptNaomiBinary(IWin32Window owner)
    {
      this.owner = owner;
      this.ChangeFileDialogsForDecryption();
      NaomiEncryptDecryptTool.InputOutputFilesAndKeyHolder filesAndKeyHolder = this.OpenDialogsForInputAndOutputFiles(false);
      if (filesAndKeyHolder == null)
        return;
      DesEncryptDecryptTool desDecryptor = new DesEncryptDecryptTool();
      Guid TaskId = Guid.NewGuid();
      FormProcess.createForDesDecryptor(desDecryptor, TaskId);
      desDecryptor.DecryptAsync(filesAndKeyHolder.InputFile, filesAndKeyHolder.OutputFile, filesAndKeyHolder.DESKey, (object) TaskId);
    }

    private NaomiEncryptDecryptTool.InputOutputFilesAndKeyHolder OpenDialogsForInputAndOutputFiles(
      bool isForEncryption)
    {
      if (this.openFileDialog.ShowDialog(this.owner) != DialogResult.OK)
        return (NaomiEncryptDecryptTool.InputOutputFilesAndKeyHolder) null;
      IVirtualFile virtualFile1 = (IVirtualFile) VirtualFileFactory.createVirtualFile(this.openFileDialog.FileName);
      this.openFileDialog.InitialDirectory = Path.GetDirectoryName(this.openFileDialog.FileName);
      this.openFileDialog.FileName = Path.GetFileName(this.openFileDialog.FileName);
      FormGetDESKey formGetDesKey = !isForEncryption ? FormGetDESKey.aGetDESKeyFormWithDecryptLabel() : FormGetDESKey.aGetDESKeyFormWithEncryptLabel();
      if (formGetDesKey.ShowDialog(this.owner) != DialogResult.OK)
      {
        formGetDesKey.Dispose();
        return (NaomiEncryptDecryptTool.InputOutputFilesAndKeyHolder) null;
      }
      formGetDesKey.Dispose();
      DESKey desKey = formGetDesKey.DESKey;
      this.saveFileDialog.FileName = Path.GetFileNameWithoutExtension(this.openFileDialog.FileName);
      if (this.saveFileDialog.ShowDialog(this.owner) != DialogResult.OK)
        return (NaomiEncryptDecryptTool.InputOutputFilesAndKeyHolder) null;
      IVirtualFile virtualFile2 = (IVirtualFile) VirtualFileFactory.createVirtualFile(this.saveFileDialog.FileName);
      this.saveFileDialog.InitialDirectory = Path.GetDirectoryName(this.saveFileDialog.FileName);
      this.saveFileDialog.FileName = Path.GetFileName(this.saveFileDialog.FileName);
      return new NaomiEncryptDecryptTool.InputOutputFilesAndKeyHolder()
      {
        InputFile = virtualFile1,
        OutputFile = virtualFile2,
        DESKey = desKey
      };
    }

    private void ChangeFileDialogsForEncryption()
    {
      this.openFileDialog.Title = Strings.NaomiEncryptDecryptToolOfdEncTitle;
      this.openFileDialog.Filter = Strings.NaomiEncryptDecryptToolOfdEncFilter;
      this.openFileDialog.InitialDirectory = this.InitialDirectory;
      this.saveFileDialog.Title = Strings.NaomiEncryptDecryptToolSfdEncTitle;
      this.saveFileDialog.Filter = Strings.NaomiEncryptDecryptToolSfdEncFilter;
      this.saveFileDialog.InitialDirectory = this.InitialDirectory;
    }

    private void ChangeFileDialogsForDecryption()
    {
      this.openFileDialog.Title = Strings.NaomiEncryptDecryptToolOfdDecTitle;
      this.openFileDialog.Filter = Strings.NaomiEncryptDecryptToolOfdDecFilter;
      this.openFileDialog.InitialDirectory = this.InitialDirectory;
      this.saveFileDialog.Title = Strings.NaomiEncryptDecryptToolSfdDecTitle;
      this.saveFileDialog.Filter = Strings.NaomiEncryptDecryptToolSfdDecFilter;
      this.saveFileDialog.InitialDirectory = this.InitialDirectory;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.openFileDialog = new OpenFileDialog();
      this.saveFileDialog = new SaveFileDialog();
    }

    private class InputOutputFilesAndKeyHolder
    {
      public IVirtualFile InputFile;
      public IVirtualFile OutputFile;
      public DESKey DESKey;
    }
  }
}
