﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.UserControls.GDEmuExportSettings
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.Events;
using SEGATools.DiscFileSystem;
using SEGATools.GDEmu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace GDRomExplorer.UserControls
{
  public class GDEmuExportSettings : UserControl
  {
    private IContainer components;
    private FolderBrowserDialog folderBrowserDialog;
    private GroupBox gbOutputDiscImage;
    private TextBox tbAppOutputFolder;
    private Button btSelectOutputFolder;
    private DiscViewOpener discViewOpener;
    private GroupBox gbExportOptions;
    private FlowLayoutPanel flpExportOptions;
    private CheckBox cbRegionFree;
    private CheckBox cbForceVGA;

    public string InitialDirectory
    {
      get => this.folderBrowserDialog.SelectedPath;
      set
      {
        this.folderBrowserDialog.SelectedPath = value;
        this.discViewOpener.InitialDirectory = value;
      }
    }

    public IDiscFileSystem InputDiscImage { get; private set; }

    public GDEmuExportOptions ExportOptions { get; private set; }

    private static GDEmuExportOptions GetDefaultOptions() => new GDEmuExportOptions()
    {
      ForceVGA = true,
      RegionFree = true
    };

    public GDEmuExportSettings()
    {
      this.InitializeComponent();
      this.ExportOptions = GDEmuExportSettings.GetDefaultOptions();
      this.cbRegionFree.Checked = this.ExportOptions.RegionFree;
      this.cbForceVGA.Checked = this.ExportOptions.ForceVGA;
      this.InitializeUI();
    }

    public void SetLoadedDiscImage(IDiscFileSystem discFileSystem)
    {
      this.InputDiscImage = discFileSystem;
      this.discViewOpener.SetLoadedDiscImage(discFileSystem);
      this.InitialDirectory = Path.GetDirectoryName(discFileSystem.FileName);
    }

    private void InitializeUI()
    {
      this.discViewOpener.ImageLoaded += new EventHandler<EventArgs<IDiscFileSystem>>(this.discViewOpener_ImageLoaded);
      this.discViewOpener.ImageNotLoaded += new EventHandler(this.discViewOpener_ImageNotLoaded);
      this.cbRegionFree.CheckedChanged += new EventHandler(this.cbRegionFree_CheckedChanged);
      this.cbForceVGA.CheckedChanged += new EventHandler(this.cbForceVGA_CheckedChanged);
    }

    private void discViewOpener_ImageNotLoaded(object sender, EventArgs e) => this.InputDiscImage = (IDiscFileSystem) null;

    private void discViewOpener_ImageLoaded(object sender, EventArgs<IDiscFileSystem> e)
    {
      this.InputDiscImage = e.Value;
      this.InitialDirectory = Path.GetDirectoryName(this.InputDiscImage.FileName);
    }

    private void btSelectOutputFolder_Click(object sender, EventArgs e)
    {
      if (this.folderBrowserDialog.ShowDialog() != DialogResult.OK)
        return;
      this.tbAppOutputFolder.Text = Path.Combine(this.folderBrowserDialog.SelectedPath, this.ExportOptions.GDIFileName);
      this.ExportOptions.OutputPath = this.folderBrowserDialog.SelectedPath;
    }

    private void cbForceVGA_CheckedChanged(object sender, EventArgs e) => this.ExportOptions.ForceVGA = this.cbForceVGA.Checked;

    private void cbRegionFree_CheckedChanged(object sender, EventArgs e) => this.ExportOptions.RegionFree = this.cbRegionFree.Checked;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (GDEmuExportSettings));
      this.folderBrowserDialog = new FolderBrowserDialog();
      this.gbOutputDiscImage = new GroupBox();
      this.btSelectOutputFolder = new Button();
      this.tbAppOutputFolder = new TextBox();
      this.gbExportOptions = new GroupBox();
      this.flpExportOptions = new FlowLayoutPanel();
      this.cbRegionFree = new CheckBox();
      this.cbForceVGA = new CheckBox();
      this.discViewOpener = new DiscViewOpener();
      this.gbOutputDiscImage.SuspendLayout();
      this.gbExportOptions.SuspendLayout();
      this.flpExportOptions.SuspendLayout();
      this.SuspendLayout();
      this.gbOutputDiscImage.BackColor = SystemColors.Window;
      this.gbOutputDiscImage.Controls.Add((Control) this.btSelectOutputFolder);
      this.gbOutputDiscImage.Controls.Add((Control) this.tbAppOutputFolder);
      this.gbOutputDiscImage.Location = new Point(3, 60);
      this.gbOutputDiscImage.Name = "gbOutputDiscImage";
      this.gbOutputDiscImage.Padding = new Padding(0);
      this.gbOutputDiscImage.Size = new Size(473, 46);
      this.gbOutputDiscImage.TabIndex = 1;
      this.gbOutputDiscImage.TabStop = false;
      this.gbOutputDiscImage.Text = " Output Disc Image:";
      this.btSelectOutputFolder.CausesValidation = false;
      this.btSelectOutputFolder.FlatStyle = FlatStyle.Popup;
      this.btSelectOutputFolder.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btSelectOutputFolder.ForeColor = Color.FromArgb(248, 48, 0);
      this.btSelectOutputFolder.Location = new Point(388, 14);
      this.btSelectOutputFolder.Name = "btSelectOutputFolder";
      this.btSelectOutputFolder.Size = new Size(79, 24);
      this.btSelectOutputFolder.TabIndex = 2;
      this.btSelectOutputFolder.Text = "...";
      this.btSelectOutputFolder.UseVisualStyleBackColor = false;
      this.btSelectOutputFolder.Click += new EventHandler(this.btSelectOutputFolder_Click);
      this.tbAppOutputFolder.BackColor = SystemColors.Window;
      this.tbAppOutputFolder.Location = new Point(6, 17);
      this.tbAppOutputFolder.Name = "tbAppOutputFolder";
      this.tbAppOutputFolder.ReadOnly = true;
      this.tbAppOutputFolder.Size = new Size(376, 20);
      this.tbAppOutputFolder.TabIndex = 3;
      this.tbAppOutputFolder.TabStop = false;
      this.gbExportOptions.BackColor = SystemColors.Window;
      this.gbExportOptions.Controls.Add((Control) this.flpExportOptions);
      this.gbExportOptions.Location = new Point(4, 112);
      this.gbExportOptions.Name = "gbExportOptions";
      this.gbExportOptions.Padding = new Padding(3, 0, 3, 3);
      this.gbExportOptions.Size = new Size(473, 42);
      this.gbExportOptions.TabIndex = 4;
      this.gbExportOptions.TabStop = false;
      this.gbExportOptions.Text = "Export Options:";
      this.flpExportOptions.AutoSize = true;
      this.flpExportOptions.Controls.Add((Control) this.cbRegionFree);
      this.flpExportOptions.Controls.Add((Control) this.cbForceVGA);
      this.flpExportOptions.Dock = DockStyle.Fill;
      this.flpExportOptions.Location = new Point(3, 13);
      this.flpExportOptions.Name = "flpExportOptions";
      this.flpExportOptions.Padding = new Padding(3);
      this.flpExportOptions.Size = new Size(467, 26);
      this.flpExportOptions.TabIndex = 0;
      this.cbRegionFree.AutoSize = true;
      this.cbRegionFree.Location = new Point(6, 6);
      this.cbRegionFree.Name = "cbRegionFree";
      this.cbRegionFree.Size = new Size(84, 17);
      this.cbRegionFree.TabIndex = 0;
      this.cbRegionFree.Text = "Region Free";
      this.cbRegionFree.UseVisualStyleBackColor = true;
      this.cbForceVGA.AutoSize = true;
      this.cbForceVGA.Location = new Point(96, 6);
      this.cbForceVGA.Name = "cbForceVGA";
      this.cbForceVGA.Size = new Size(78, 17);
      this.cbForceVGA.TabIndex = 1;
      this.cbForceVGA.Text = "Force VGA";
      this.cbForceVGA.UseVisualStyleBackColor = true;
      this.discViewOpener.AutoSize = true;
      this.discViewOpener.BackColor = SystemColors.Window;
      this.discViewOpener.ButtonText = "&Open";
      this.discViewOpener.FileDialogFilters = (List<string>) componentResourceManager.GetObject("discViewOpener.FileDialogFilters");
      this.discViewOpener.InitialDirectory = "";
      this.discViewOpener.Location = new Point(0, 0);
      this.discViewOpener.Margin = new Padding(0);
      this.discViewOpener.Name = "discViewOpener";
      this.discViewOpener.NotifyStatusEvents = false;
      this.discViewOpener.Padding = new Padding(3);
      this.discViewOpener.Size = new Size(479, 57);
      this.discViewOpener.TabIndex = 0;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoSize = true;
      this.BackColor = SystemColors.Window;
      this.Controls.Add((Control) this.gbExportOptions);
      this.Controls.Add((Control) this.gbOutputDiscImage);
      this.Controls.Add((Control) this.discViewOpener);
      this.Name = nameof (GDEmuExportSettings);
      this.Size = new Size(480, 157);
      this.gbOutputDiscImage.ResumeLayout(false);
      this.gbOutputDiscImage.PerformLayout();
      this.gbExportOptions.ResumeLayout(false);
      this.gbExportOptions.PerformLayout();
      this.flpExportOptions.ResumeLayout(false);
      this.flpExportOptions.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
