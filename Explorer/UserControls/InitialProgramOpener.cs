﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.UserControls.InitialProgramOpener
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.Forms;
using GDRomExplorer.Resources;
using SEGATools.Binary;
using SEGATools.Scanner;
using SEGATools.Security;
using SEGATools.VirtualFile;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace GDRomExplorer.UserControls
{
  public class InitialProgramOpener : Component
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private IWin32Window owner;
    private FormLoading formLoading;
    private IContainer components;
    private BackgroundWorker backgroundWorker;
    private OpenFileDialog openFileDialogForInitialProgram;

    public InitialProgramOpener()
    {
      this.InitializeComponent();
      this.Initialize();
    }

    private void Initialize()
    {
      this.openFileDialogForInitialProgram.Title = Strings.OfdInitialProgramTitle;
      this.openFileDialogForInitialProgram.Filter = Strings.OfdInitialProgramFilter;
      this.backgroundWorker.DoWork += new DoWorkEventHandler(this.bwOpenInitialProgramFile_DoWork);
      this.backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.bwOpenInitialProgramFile_RunWorkerCompleted);
    }

    public void OpenAndViewInitialProgram(IWin32Window owner)
    {
      this.owner = owner;
      if (this.openFileDialogForInitialProgram.ShowDialog(owner) != DialogResult.OK)
        return;
      this.backgroundWorker.RunWorkerAsync((object) VirtualFileFactory.createVirtualFile(this.openFileDialogForInitialProgram.FileName));
      using (this.formLoading = new FormLoading(Strings.InitialProgramLoadingTitle, Strings.InitialProgramLoadingMessage))
      {
        int num = (int) this.formLoading.ShowDialog(owner);
      }
    }

    private void bwOpenInitialProgramFile_DoWork(object sender, DoWorkEventArgs e)
    {
      IVirtualFile virtualFile1 = (IVirtualFile) e.Argument;
      InitialProgram initialProgram;
      using (Stream fileInputStream = virtualFile1.FileInputStream)
      {
        byte[] buffer = new byte[InitialProgramConverter.INITIAL_PROGRAM_SIZE];
        fileInputStream.Read(buffer, 0, buffer.Length);
        initialProgram = InitialProgramConverter.ToInitialProgram(buffer, 0);
      }
      List<SEGALibrary> libraries;
      using (FileScanner fileScanner = new FileScanner())
      {
        IVirtualFile virtualFile2 = (IVirtualFile) VirtualFileFactory.createVirtualFile(initialProgram.Stream, virtualFile1.OriginalFileName);
        libraries = fileScanner.ScanFile<SEGALibrary>(virtualFile2, FileScannerPattern.aPatternForSEGALibraries(), (IFileScannerResultConverter<SEGALibrary>) new FileScannerResultConverterForSEGALibrary());
      }
      e.Result = (object) InitialProgramExtended.create(initialProgram, libraries);
    }

    private void bwOpenInitialProgramFile_RunWorkerCompleted(
      object sender,
      RunWorkerCompletedEventArgs e)
    {
      if (!this.formLoading.IsDisposed)
        this.formLoading.Dispose();
      if (e.Error != null)
      {
        string str = string.Format(Strings.MsgBoxInitialProgramOpenerErrorWithFormat, (object) e.Error.Message);
        InitialProgramOpener.logger.ErrorFormat("Unable to open ip: {0}", (object) e.Error);
        int num = (int) MessageBox.Show(this.owner, str, Strings.MsgBoxInitialProgramOpenerTitle, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        AppStatus.NotifyNewAppStatus(str);
      }
      else
      {
        string fileName = this.openFileDialogForInitialProgram.FileName;
        this.openFileDialogForInitialProgram.FileName = Path.GetFileName(fileName);
        string message = string.Format(Strings.MsgBoxInitialProgramOpenerSuccessWithFormat, (object) fileName);
        InitialProgramOpener.logger.InfoFormat("File {0} successfully opened", (object) fileName);
        AppStatus.NotifyNewAppStatus(message);
        using (FormInitialProgram formInitialProgram = new FormInitialProgram((InitialProgramExtended) e.Result, Path.GetDirectoryName(fileName)))
        {
          int num = (int) formInitialProgram.ShowDialog(this.owner);
        }
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.backgroundWorker = new BackgroundWorker();
      this.openFileDialogForInitialProgram = new OpenFileDialog();
      this.openFileDialogForInitialProgram.Multiselect = true;
    }
  }
}
