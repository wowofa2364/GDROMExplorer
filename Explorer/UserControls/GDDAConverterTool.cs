﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.UserControls.GDDAConverterTool
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.Forms;
using SEGATools.Audio;
using SEGATools.DiscFileSystem;
using SEGATools.VirtualFile;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace GDRomExplorer.UserControls
{
  public class GDDAConverterTool : Component
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    public string InitialDirectory;
    private IWin32Window owner;
    private Raw2WavConverter raw2wavConverter;
    private IContainer components;
    private OpenFileDialog openFileDialog;
    private SaveFileDialog saveFileDialog;
    private FolderBrowserDialog folderBrowserDialog;

    public GDDAConverterTool()
    {
      this.InitializeComponent();
      this.InitializeDialogs();
      this.raw2wavConverter = new Raw2WavConverter();
    }

    public GDDAConverterTool(IContainer container)
    {
      container.Add((IComponent) this);
      this.InitializeComponent();
      this.InitializeDialogs();
      this.raw2wavConverter = new Raw2WavConverter(container);
    }

    public void OpenAndConvertGDDAFiles(
      IWin32Window owner,
      AudioConversionSettings AudioConversionSettings)
    {
      this.owner = owner;
      this.UpdateInitialDirectory(this.InitialDirectory);
      IList<IVirtualFile> inputFiles;
      string outputLocation;
      if (this.AskUserForInputFiles(out inputFiles) != DialogResult.OK || this.AskUserForSaveLocation(inputFiles, out outputLocation) != DialogResult.OK)
        return;
      this.StartAudioConversion(inputFiles, outputLocation, AudioConversionSettings);
    }

    public void ConvertGDDATracks(
      IDiscTrack AudioTrack,
      AudioConversionSettings AudioConversionSettings)
    {
      this.ConvertGDDATracks((IList<IDiscTrack>) new List<IDiscTrack>()
      {
        AudioTrack
      }, AudioConversionSettings);
    }

    public void ConvertGDDATracks(
      IList<IDiscTrack> AudioTracks,
      AudioConversionSettings AudioConversionSettings)
    {
      this.CheckTrackListIsValid(AudioTracks);
      this.UpdateInitialDirectory(this.InitialDirectory);
      if (AudioTracks.Count == 0)
      {
        this.ShowNoTrackWarning();
      }
      else
      {
        IList<IVirtualFile> list = (IList<IVirtualFile>) AudioTracks.Select<IDiscTrack, IVirtualFile>((Func<IDiscTrack, IVirtualFile>) (track => (IVirtualFile) track)).ToList<IVirtualFile>();
        string outputLocation;
        if (this.AskUserForSaveLocation(list, out outputLocation) != DialogResult.OK)
          return;
        this.StartAudioConversion(list, outputLocation, AudioConversionSettings);
      }
    }

    private void StartAudioConversion(
      IList<IVirtualFile> files,
      string outputPath,
      AudioConversionSettings AudioConversionSettings)
    {
      Guid TaskId = Guid.NewGuid();
      this.raw2wavConverter.AudioConversionSettings = AudioConversionSettings;
      FormProcess.createForRawToWavConverter(this.raw2wavConverter, TaskId);
      this.raw2wavConverter.ConvertAsync(files, outputPath, (object) TaskId);
    }

    private void InitializeDialogs()
    {
      this.openFileDialog.Title = GDRomExplorer.Resources.Strings.OfdConvertGddaTitle;
      this.openFileDialog.Filter = GDRomExplorer.Resources.Strings.OfdConvertGddaFilter;
      this.saveFileDialog.Title = GDRomExplorer.Resources.Strings.SfdCddaTitle;
      this.saveFileDialog.Filter = GDRomExplorer.Resources.Strings.SfdCddaFilter;
      this.folderBrowserDialog.Description = GDRomExplorer.Resources.Strings.FbdConvertGddaTitle;
      this.folderBrowserDialog.RootFolder = Environment.SpecialFolder.Desktop;
    }

    private void UpdateInitialDirectory(string newInitialDirectory)
    {
      this.openFileDialog.InitialDirectory = newInitialDirectory;
      this.saveFileDialog.InitialDirectory = newInitialDirectory;
      this.folderBrowserDialog.SelectedPath = newInitialDirectory;
    }

    private DialogResult AskUserForInputFiles(out IList<IVirtualFile> inputFiles)
    {
      inputFiles = (IList<IVirtualFile>) null;
      if (this.openFileDialog.ShowDialog(this.owner) != DialogResult.OK)
        return DialogResult.Cancel;
      inputFiles = (IList<IVirtualFile>) ((IEnumerable<string>) this.openFileDialog.FileNames).Select<string, IVirtualFile>((Func<string, IVirtualFile>) (inputFilename => (IVirtualFile) VirtualFileFactory.createVirtualFile(inputFilename))).ToList<IVirtualFile>();
      this.openFileDialog.InitialDirectory = Path.GetDirectoryName(this.openFileDialog.FileName);
      this.openFileDialog.FileName = Path.GetFileName(this.openFileDialog.FileName);
      return DialogResult.OK;
    }

    private DialogResult AskUserForSaveLocation(
      IList<IVirtualFile> AudioTracks,
      out string outputLocation)
    {
      outputLocation = (string) null;
      if (AudioTracks.Count == 1)
      {
        if (this.AskUserForSaveFileLocation(AudioTracks[0], out outputLocation) != DialogResult.OK)
          return DialogResult.Cancel;
      }
      else if (this.AskUserForSaveDirectoryLocation(out outputLocation) != DialogResult.OK)
        return DialogResult.Cancel;
      return DialogResult.OK;
    }

    private DialogResult AskUserForSaveFileLocation(
      IVirtualFile suggestedFileName,
      out string outputPath)
    {
      outputPath = (string) null;
      this.saveFileDialog.FileName = Path.GetFileNameWithoutExtension(suggestedFileName.VirtualName);
      if (this.saveFileDialog.ShowDialog(this.owner) != DialogResult.OK)
        return DialogResult.Cancel;
      outputPath = this.saveFileDialog.FileName;
      this.UpdateInitialDirectory(Path.GetDirectoryName(outputPath));
      this.saveFileDialog.FileName = Path.GetFileName(outputPath);
      return DialogResult.OK;
    }

    private DialogResult AskUserForSaveDirectoryLocation(out string outputDirectory)
    {
      outputDirectory = (string) null;
      if (this.folderBrowserDialog.ShowDialog(this.owner) != DialogResult.OK)
        return DialogResult.Cancel;
      outputDirectory = this.folderBrowserDialog.SelectedPath;
      this.UpdateInitialDirectory(outputDirectory);
      return DialogResult.OK;
    }

    private void CheckTrackListIsValid(IList<IDiscTrack> tracks)
    {
      if (tracks == null)
        throw new ArgumentNullException();
      foreach (IDiscTrack track in (IEnumerable<IDiscTrack>) tracks)
      {
        if (track.TrackData != TrackModeType.Audio)
        {
          GDDAConverterTool.logger.ErrorFormat("The track {0} is not an audio track!", (object) track);
          throw new ArgumentException("must be an audio track");
        }
      }
    }

    private void ShowNoTrackWarning()
    {
      GDDAConverterTool.logger.Warn((object) "No audio track to convert!");
      int num = (int) MessageBox.Show(this.owner, GDRomExplorer.Resources.Strings.MsgBoxAudioConverterNoTrackWarning, GDRomExplorer.Resources.Strings.MsgBoxAudioConverterTitle, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
      AppStatus.NotifyNewAppStatus(GDRomExplorer.Resources.Strings.MsgBoxAudioConverterNoTrackWarning);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.openFileDialog = new OpenFileDialog();
      this.saveFileDialog = new SaveFileDialog();
      this.folderBrowserDialog = new FolderBrowserDialog();
      this.openFileDialog.Multiselect = true;
    }
  }
}
