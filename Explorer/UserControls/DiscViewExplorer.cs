﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.UserControls.DiscViewExplorer
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.DiscView;
using GDRomExplorer.Events;
using GDRomExplorer.Forms;
using GDRomExplorer.Others;
using GDRomExplorer.ShellDataTransfert;
using ImageReader.ISO9660.DirectoryRecords;
using ImageReader.ISO9660.VolumeDescriptors;
using SEGATools.CueSheet;
using SEGATools.Disc;
using SEGATools.DiscFileSystem;
using SEGATools.Encrypt;
using SEGATools.Security;
using SEGATools.SortFile;
using SEGATools.VirtualFile;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace GDRomExplorer.UserControls
{
  public class DiscViewExplorer : UserControl
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private IDiscFileSystem discFileSystem;
    private TreeNode treeNodeInListView;
    private Control lastControlWithFocus;
    private MenuItemFactory menuItemFactory;
    private ContextMenuManager contextMenuManager;
    private UserActions userActions;
    private ToolStripItem[] discMenuItems;
    private DiscSelectionHandlerFactory treeViewSelectionHandlerFactory;
    private DiscSelectionHandlerFactory listViewSelectionHandlerFactory;
    private IContainer components;
    private GroupBox groupBox;
    private SplitContainer splitContainer;
    private TreeView treeView;
    private ListView listView;
    private DiscExtractor discExtractor;
    private SaveFileDialog saveFileDialogForInitialProgram;
    private SaveFileDialog saveFileDialogForExtraction;
    private SaveFileDialog saveFileDialogForCueSheet;
    private SaveFileDialog saveFileDialogForSortFile;
    private FolderBrowserDialog folderBrowserDialogForExtraction;
    private SaveFileDialog saveFileDialogForDesDecrypt;
    private GDDAConverterTool gddaConverterTool;
    private ImageList treeViewImageList;
    private ImageList listViewImageList;
    private ImageList contextMenuImageList;

    public event EventHandler<EventArgs<DiscViewExplorer.ActionsForEditMenuItem>> SelectionChanged;

    public string Title
    {
      get => this.groupBox.Text;
      private set => this.groupBox.Text = string.Format(GDRomExplorer.Resources.Strings.DiscViewExplorerGroupLabelWithFormat, (object) value);
    }

    public string Filename => this.discFileSystem != null ? this.discFileSystem.FileName : string.Empty;

    public DiscViewExplorer()
    {
      this.InitializeComponent();
      this.userActions = new UserActions();
      this.userActions.SetHandler(UserActions.Action.ExportForGDEmu, new EventHandler(this.UserActionExportForGDEmu));
      this.userActions.SetHandler(UserActions.Action.CreateCueSheet, new EventHandler(this.UserActionCreateCueSheet));
      this.userActions.SetHandler(UserActions.Action.ShowGDDAConversion, new EventHandler(this.UserActionShowGDDAConversionForm));
      this.userActions.SetHandler(UserActions.Action.ShowBootSector, new EventHandler(this.UserActionShowBootSector));
      this.userActions.SetHandler(UserActions.Action.ExtractBootSector, new EventHandler(this.UserActionExtractInitialProgram));
      this.userActions.SetHandler(UserActions.Action.ExtractItem, new EventHandler(this.UserActionExtractItem));
      this.userActions.SetHandler(UserActions.Action.CreateSortFile, new EventHandler(this.UserActionGenerateSortFile));
      this.userActions.SetHandler(UserActions.Action.ShowPrimaryVolumeDescriptor, new EventHandler(this.UserActionShowPrimaryVolumeDescriptor));
      this.userActions.SetHandler(UserActions.Action.ConvertGDDA, new EventHandler(this.UserActionConvertGDDA));
      this.userActions.SetHandler(UserActions.Action.DecryptNaomiBinary, new EventHandler(this.UserActionDecryptBinaryFromDisc));
      this.treeViewSelectionHandlerFactory = (DiscSelectionHandlerFactory) new DiscTreeViewSelectionHandlerFactory(this, this.treeView);
      this.listViewSelectionHandlerFactory = (DiscSelectionHandlerFactory) new DiscListViewSelectionHandlerFactory(this, this.listView);
      this.menuItemFactory = new MenuItemFactory(this.userActions, this.contextMenuImageList);
      this.contextMenuManager = new ContextMenuManager(this.menuItemFactory);
      this.Resize += new EventHandler(this.DiscViewExplorer_Resize);
      this.Leave += new EventHandler(this.DiscViewExplorer_Leave);
      this.listView.Columns.Add(GDRomExplorer.Resources.Strings.DiscViewListColumnFileName, -2, HorizontalAlignment.Left);
      this.listView.Columns.Add(GDRomExplorer.Resources.Strings.DiscViewListColumnSize, -2, HorizontalAlignment.Right);
      this.listView.Columns.Add(GDRomExplorer.Resources.Strings.DiscViewListColumnSizeInBytes, -2, HorizontalAlignment.Right);
      this.listView.Columns.Add(GDRomExplorer.Resources.Strings.DiscViewListColumnLBA, -2, HorizontalAlignment.Right);
      this.listView.Columns.Add(GDRomExplorer.Resources.Strings.DiscViewListColumnModifiedDate, -2, HorizontalAlignment.Right);
      this.listView.MouseClick += new MouseEventHandler(this.listView_MouseClick);
      this.listView.MouseDoubleClick += new MouseEventHandler(this.listView_MouseClick);
      this.listView.MouseUp += new MouseEventHandler(this.listView_MouseClick);
      this.listView.ColumnClick += new ColumnClickEventHandler(this.listView_ColumnClick);
      this.listView.KeyDown += new KeyEventHandler(this.listView_KeyDown);
      this.listView.KeyUp += new KeyEventHandler(this.listView_KeyUp);
      this.listView.ItemDrag += new ItemDragEventHandler(this.DiscViewExplorer_ItemDrag);
      this.listView.GotFocus += new EventHandler(this.listView_GotFocus);
      this.listView.LostFocus += new EventHandler(this.DiscViewExplorer_Leave);
      this.listView.ListViewItemSorter = (IComparer) new ListViewColumnSorter()
      {
        Order = SortOrder.Ascending
      };
      this.treeView.AfterSelect += new TreeViewEventHandler(this.treeView_AfterSelect);
      this.treeView.MouseClick += new MouseEventHandler(this.treeView_MouseClick);
      this.treeView.MouseDoubleClick += new MouseEventHandler(this.treeView_MouseClick);
      this.treeView.ItemDrag += new ItemDragEventHandler(this.DiscViewExplorer_ItemDrag);
      this.treeView.GotFocus += new EventHandler(this.treeView_GotFocus);
      this.treeView.Leave += new EventHandler(this.DiscViewExplorer_Leave);
      this.saveFileDialogForInitialProgram.Title = GDRomExplorer.Resources.Strings.SfdInitialProgramTitle;
      this.saveFileDialogForInitialProgram.Filter = GDRomExplorer.Resources.Strings.SfdInitialProgramFilter;
      this.saveFileDialogForExtraction.Title = GDRomExplorer.Resources.Strings.SfdExtractTitle;
      this.saveFileDialogForExtraction.Filter = GDRomExplorer.Resources.Strings.SfdExtractFilter;
      this.saveFileDialogForCueSheet.Title = GDRomExplorer.Resources.Strings.SfdCueSheetTitle;
      this.saveFileDialogForCueSheet.Filter = GDRomExplorer.Resources.Strings.SfdCueSheetFilter;
      this.saveFileDialogForSortFile.Title = GDRomExplorer.Resources.Strings.SfdSortFileTitle;
      this.saveFileDialogForSortFile.Filter = GDRomExplorer.Resources.Strings.SfdSortFileFilter;
      this.saveFileDialogForDesDecrypt.Title = GDRomExplorer.Resources.Strings.NaomiEncryptDecryptToolSfdDecTitle;
      this.saveFileDialogForDesDecrypt.Filter = GDRomExplorer.Resources.Strings.NaomiEncryptDecryptToolSfdDecFilter;
      this.folderBrowserDialogForExtraction.Description = GDRomExplorer.Resources.Strings.FbdExtractFilesTitle;
      this.folderBrowserDialogForExtraction.RootFolder = Environment.SpecialFolder.Desktop;
    }

    private void DiscViewExplorer_ItemDrag(object sender, ItemDragEventArgs e)
    {
      if (e.Button != MouseButtons.Left)
        return;
      if (sender == this.listView)
      {
        this.HandleDragAndDropFrom(this.listView);
      }
      else
      {
        if (sender != this.treeView)
          throw new NotSupportedException();
        this.treeView.SelectedNode = e.Item as TreeNode;
        this.HandleDragAndDropFrom(this.treeView);
      }
    }

    private void DiscViewExplorer_Load(object sender, EventArgs e) => this.CloseDisc();

    private void DiscViewExplorer_Resize(object sender, EventArgs e) => this.ResizeListView();

    private void DiscViewExplorer_Leave(object sender, EventArgs e) => this.contextMenuManager.CloseContextMenuIfAny();

    public void LoadDisc(IDiscFileSystem discFileSystem)
    {
      this.EnableUIElements();
      this.discFileSystem = discFileSystem;
      this.discExtractor.DiscFileSystem = discFileSystem;
      this.Title = discFileSystem.DiscName;
      this.discMenuItems = this.menuItemFactory.CreateDiscWithSessionTopMenuItem(discFileSystem);
      this.UpdateDialogsPath(Path.GetDirectoryName(discFileSystem.FileName));
      TreeNode tree = ExplorerTreeNodeFactory.CreateTree(discFileSystem);
      this.treeView.Nodes.Add(tree);
      this.UpdateTreeViewSelectedNode(this.FindFileSystemTreeNode(discFileSystem, tree));
    }

    public void CloseDisc()
    {
      this.DisableUIElements();
      this.treeNodeInListView = (TreeNode) null;
      this.discFileSystem = (IDiscFileSystem) null;
      this.DisableSelectionAndNotify();
    }

    private TreeNode FindFileSystemTreeNode(IDiscFileSystem disc, TreeNode rootNode)
    {
      TreeNode treeNode1 = rootNode;
      foreach (IDiscSession discSession in (IEnumerable<IDiscSession>) disc.Sessions.OrderByDescending<IDiscSession, int>((Func<IDiscSession, int>) (session => session.Index)))
      {
        TreeNode[] treeNodeArray = rootNode.Nodes.Find(discSession.Name, false);
        if (treeNodeArray.Length != 0)
        {
          TreeNode treeNode2 = treeNodeArray[0];
          treeNode1 = treeNode2;
          if (treeNode2.FirstNode != null)
          {
            TreeNode firstNode = treeNode2.FirstNode;
            if (firstNode.Tag is IDiscTrack tag && tag.TrackData == TrackModeType.Data && (firstNode.LastNode != null && firstNode.LastNode.Tag is DirectoryRecord))
            {
              treeNode1 = firstNode.LastNode;
              break;
            }
          }
        }
      }
      return treeNode1;
    }

    private void DisableUIElements()
    {
      this.Title = GDRomExplorer.Resources.Strings.DiscViewNoDiscTitle;
      this.treeView.Enabled = false;
      this.treeView.Nodes.Clear();
      this.treeView.BackColor = SystemColors.Control;
      this.listView.Enabled = false;
      this.listView.Items.Clear();
      this.listView.Scrollable = false;
    }

    private void EnableUIElements()
    {
      this.treeView.BackColor = SystemColors.Window;
      this.treeView.Nodes.Clear();
      this.treeView.Enabled = true;
      this.listView.Items.Clear();
      this.listView.Scrollable = true;
      this.listView.Enabled = true;
    }

    private void ShowBootSector(InitialProgramExtended ip)
    {
      using (FormInitialProgram formInitialProgram = new FormInitialProgram(ip, Path.GetDirectoryName(this.discFileSystem.FileName)))
      {
        int num = (int) formInitialProgram.ShowDialog((IWin32Window) this);
      }
    }

    private void ShowPVD(PrimaryVolumeDescriptor PrimVolDesc)
    {
      using (FormPrimaryVolumeDescriptor volumeDescriptor = new FormPrimaryVolumeDescriptor(PrimVolDesc))
      {
        int num = (int) volumeDescriptor.ShowDialog((IWin32Window) this);
      }
    }

    private void FillListView(DirectoryRecord directoryRecordToRead, TreeNode origin)
    {
      this.listView.Cursor = Cursors.AppStarting;
      this.listView.BeginUpdate();
      this.listView.Items.Clear();
      ListViewItem listViewItem1 = (ListViewItem) null;
      if (!directoryRecordToRead.IsRoot)
      {
        ListViewItem parentDirectoryItem = ExplorerListItemFactory.CreateParentDirectoryItem(directoryRecordToRead);
        this.listView.Items.Add(parentDirectoryItem);
        listViewItem1 = parentDirectoryItem;
      }
      ListViewItem[] items = new ListViewItem[directoryRecordToRead.SubDirectories.Count];
      int num = 0;
      foreach (DirectoryRecord subDirectory in directoryRecordToRead.SubDirectories)
      {
        ListViewItem listViewItem2 = ExplorerListItemFactory.CreateItem(subDirectory, this.discFileSystem);
        items[num++] = listViewItem2;
        if (origin != null && listViewItem2.Tag == origin.Tag)
          listViewItem1 = listViewItem2;
      }
      this.listView.Items.AddRange(items);
      this.listView.FocusedItem = listViewItem1;
      this.listView.Cursor = Cursors.Default;
      this.listView.EndUpdate();
      this.ResizeListView();
    }

    private void ConvertGDDA(IDiscTrack discTrack)
    {
      using (FormAudioConversionSettings conversionSettings = new FormAudioConversionSettings())
      {
        if (conversionSettings.ShowDialog((IWin32Window) this) != DialogResult.OK)
          return;
        this.gddaConverterTool.ConvertGDDATracks(discTrack, conversionSettings.AudioConversionSettings);
      }
    }

    private void ExtractAndDecryptFile(DirectoryRecord directoryRecord)
    {
      if (directoryRecord == null || directoryRecord.IsDirectory)
        return;
      DESKey desKey;
      using (FormGetDESKey extractAndDecryptLabel = FormGetDESKey.aGetDESKeyFormWithExtractAndDecryptLabel(this.discFileSystem.NaomiDESKey))
      {
        if (extractAndDecryptLabel.ShowDialog((IWin32Window) this) != DialogResult.OK)
          return;
        desKey = extractAndDecryptLabel.DESKey;
      }
      this.saveFileDialogForDesDecrypt.FileName = Path.GetFileNameWithoutExtension(directoryRecord.Name);
      if (this.saveFileDialogForDesDecrypt.ShowDialog((IWin32Window) this) != DialogResult.OK)
        return;
      string fileName = this.saveFileDialogForDesDecrypt.FileName;
      this.UpdateDialogsPath(Path.GetDirectoryName(fileName));
      this.saveFileDialogForDesDecrypt.FileName = Path.GetFileNameWithoutExtension(fileName);
      IVirtualFile<DirectoryRecord> virtualFile1 = VirtualFileFactory.createVirtualFile(directoryRecord, this.discFileSystem);
      IVirtualFile<string> virtualFile2 = VirtualFileFactory.createVirtualFile(fileName);
      DesEncryptDecryptTool desDecryptor = new DesEncryptDecryptTool();
      Guid TaskId = Guid.NewGuid();
      FormProcess.createForDesDecryptor(desDecryptor, TaskId);
      desDecryptor.DecryptAsync((IVirtualFile) virtualFile1, (IVirtualFile) virtualFile2, desKey, (object) TaskId);
    }

    private void ExtractInitialProgram(InitialProgramExtended initialProgramExtended)
    {
      this.saveFileDialogForInitialProgram.FileName = initialProgramExtended.FileName;
      if (this.saveFileDialogForInitialProgram.ShowDialog((IWin32Window) this) != DialogResult.OK)
        return;
      string fileName = this.saveFileDialogForInitialProgram.FileName;
      this.UpdateDialogsPath(Path.GetDirectoryName(fileName));
      try
      {
        this.discExtractor.ExtractBootStrap((InitialProgram) initialProgramExtended, fileName);
        AppStatus.NotifyNewAppStatus(string.Format(GDRomExplorer.Resources.Strings.MsgBoxInitialProgramExtractionSuccessWithFormat, (object) fileName));
      }
      catch (Exception ex)
      {
        DiscViewExplorer.logger.ErrorFormat("Unable to extract the boot file: {0}", (object) ex);
        int num = (int) MessageBox.Show((IWin32Window) this, GDRomExplorer.Resources.Strings.MsgBoxInitialProgramExtractionErrorWithFormat, GDRomExplorer.Resources.Strings.MsgBoxInitialProgramExtractionTitle, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        AppStatus.NotifyNewAppStatus(string.Format(GDRomExplorer.Resources.Strings.MsgBoxInitialProgramExtractionErrorWithFormat, (object) fileName));
      }
    }

    private void ExportForGDEmu(IDiscFileSystem discFileSystem)
    {
      using (FormGDEmuExportSettings emuExportSettings = new FormGDEmuExportSettings(discFileSystem))
      {
        int num = (int) emuExportSettings.ShowDialog((IWin32Window) this);
      }
    }

    private void CreateCueSheet(IDiscSession discSession)
    {
      string Title = string.Format("{0} ({1})", (object) this.discFileSystem.DiscName, (object) discSession.Name);
      this.saveFileDialogForCueSheet.FileName = Title;
      if (this.saveFileDialogForCueSheet.ShowDialog((IWin32Window) this) != DialogResult.OK)
        return;
      string fileName = this.saveFileDialogForCueSheet.FileName;
      this.UpdateDialogsPath(Path.GetDirectoryName(fileName));
      this.saveFileDialogForCueSheet.FileName = Path.GetFileNameWithoutExtension(fileName);
      try
      {
        CueSheetCreator.CreateFromDiscSession(discSession, fileName, Title);
        AppStatus.NotifyNewAppStatus(GDRomExplorer.Resources.Strings.MsgBoxCueCreatorSuccess);
      }
      catch (Exception ex)
      {
        DiscViewExplorer.logger.ErrorFormat("Unable to create the sort file: {0}", (object) ex);
        int num = (int) MessageBox.Show((IWin32Window) this, GDRomExplorer.Resources.Strings.MsgBoxCueCreatorError, GDRomExplorer.Resources.Strings.MsgBoxCueCreatorTitle, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        AppStatus.NotifyNewAppStatus(GDRomExplorer.Resources.Strings.MsgBoxCueCreatorError);
      }
    }

    private void CreateSortFile(IDiscSession discSession)
    {
      string pathPrefix;
      int lowestFileWeight;
      using (FormSortFileOptions formSortFileOptions = new FormSortFileOptions())
      {
        if (formSortFileOptions.ShowDialog((IWin32Window) this) != DialogResult.OK)
          return;
        pathPrefix = formSortFileOptions.PathPrefix;
        lowestFileWeight = formSortFileOptions.LowestFileWeight;
      }
      if (string.IsNullOrEmpty(Path.GetFileName(this.saveFileDialogForSortFile.FileName)))
        this.saveFileDialogForSortFile.FileName = SortFileCreator.DefaultFileName;
      if (this.saveFileDialogForSortFile.ShowDialog((IWin32Window) this) != DialogResult.OK)
        return;
      string fileName = this.saveFileDialogForSortFile.FileName;
      this.saveFileDialogForSortFile.FileName = Path.GetFileNameWithoutExtension(fileName);
      this.UpdateDialogsPath(Path.GetDirectoryName(fileName));
      try
      {
        SortFileCreator.CreateSortFile(discSession, pathPrefix, lowestFileWeight, fileName);
        AppStatus.NotifyNewAppStatus(GDRomExplorer.Resources.Strings.MsgBoxSortFileCreationSuccess);
      }
      catch (Exception ex)
      {
        DiscViewExplorer.logger.ErrorFormat("Unable to create the sort file: {0}", (object) ex);
        int num = (int) MessageBox.Show((IWin32Window) this, GDRomExplorer.Resources.Strings.MsgBoxSortFileCreationError, GDRomExplorer.Resources.Strings.MsgBoxSortFileCreationTitle, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        AppStatus.NotifyNewAppStatus(GDRomExplorer.Resources.Strings.MsgBoxSortFileCreationError);
      }
    }

    private void OpenExtractNodesDialog(List<DirectoryRecord> DirectoryRecords)
    {
      if (DirectoryRecords == null || DirectoryRecords.Count == 0)
      {
        DiscViewExplorer.logger.Debug((object) "Nothing to extract!");
      }
      else
      {
        string str;
        if (DirectoryRecords.Count == 1 && !DirectoryRecords[0].IsDirectory)
        {
          this.saveFileDialogForExtraction.FileName = DirectoryRecords[0].Name;
          if (this.saveFileDialogForExtraction.ShowDialog((IWin32Window) this) != DialogResult.OK)
            return;
          str = this.saveFileDialogForExtraction.FileName;
          this.UpdateDialogsPath(Path.GetDirectoryName(str));
        }
        else
        {
          if (this.folderBrowserDialogForExtraction.ShowDialog((IWin32Window) this) != DialogResult.OK)
            return;
          str = this.folderBrowserDialogForExtraction.SelectedPath;
          this.UpdateDialogsPath(str);
        }
        Guid TaskId = Guid.NewGuid();
        FormProcess.createForDiscExtractor(this.discExtractor, TaskId);
        this.discExtractor.ExtractPathsAsync(DirectoryRecords, str, (object) TaskId);
      }
    }

    private void OpenExtractNodesDialogForDiscTrack(IDiscTrack discTrack)
    {
      this.saveFileDialogForExtraction.FileName = Path.GetFileName(discTrack.VirtualName);
      if (this.saveFileDialogForExtraction.ShowDialog((IWin32Window) this) != DialogResult.OK)
        return;
      string fileName = this.saveFileDialogForExtraction.FileName;
      this.UpdateDialogsPath(Path.GetDirectoryName(fileName));
      Guid TaskId = Guid.NewGuid();
      FormProcess.createForDiscExtractor(this.discExtractor, TaskId);
      this.discExtractor.ExtractDiscTracksAsync(discTrack, fileName, (object) TaskId);
    }

    private void UpdateDialogsPath(string outputPath) => this.saveFileDialogForInitialProgram.InitialDirectory = this.saveFileDialogForExtraction.InitialDirectory = this.saveFileDialogForCueSheet.InitialDirectory = this.saveFileDialogForSortFile.InitialDirectory = this.saveFileDialogForDesDecrypt.InitialDirectory = this.folderBrowserDialogForExtraction.SelectedPath = outputPath;

    private void ResizeListView()
    {
      this.listView.SuspendLayout();
      for (int index = 0; index < this.listView.Columns.Count; ++index)
        this.listView.Columns[index].Width = -2;
      this.listView.ResumeLayout(false);
    }

    private void DisableSelectionAndNotify()
    {
      if (this.SelectionChanged == null)
        return;
      this.SelectionChanged((object) this, new EventArgs<DiscViewExplorer.ActionsForEditMenuItem>(DiscViewExplorer.ActionsForEditMenuItem.NoEditActions()));
    }

    private void RemoveSelectionAndNotify()
    {
      if (this.SelectionChanged == null)
        return;
      this.SelectionChanged((object) this, this.CreateNewSelectionEvent(this.menuItemFactory.CreateSelectionDisabledTopMenuItem()));
    }

    private void NotifySelectionChanged(ToolStripItem[] NewSelectionMenuItems)
    {
      if (this.SelectionChanged == null)
        return;
      this.SelectionChanged((object) this, this.CreateNewSelectionEvent(NewSelectionMenuItems));
    }

    private EventArgs<DiscViewExplorer.ActionsForEditMenuItem> CreateNewSelectionEvent(
      ToolStripItem[] NewSelectionMenuItems)
    {
      return new EventArgs<DiscViewExplorer.ActionsForEditMenuItem>(new DiscViewExplorer.ActionsForEditMenuItem(this.discMenuItems, NewSelectionMenuItems));
    }

    private void UpdateTreeViewSelectedNode(TreeNode newSelection)
    {
      this.treeView.SelectedNode = newSelection;
      this.treeView.SelectedNode.EnsureVisible();
      this.treeView.SelectedNode.Expand();
    }

    private void NagivateUp()
    {
      if (this.treeNodeInListView.Parent == null || !(this.treeNodeInListView.Parent.Tag is DirectoryRecord))
        return;
      this.treeView.SelectedNode = this.treeNodeInListView.Parent;
    }

    private void NavigateDown()
    {
      if (this.listView.FocusedItem == null || !(this.listView.FocusedItem.Tag is DirectoryRecord) || !(this.listView.FocusedItem.Tag as DirectoryRecord).IsDirectory)
        return;
      if (this.treeNodeInListView.Tag == this.listView.FocusedItem.Tag)
      {
        this.NagivateUp();
      }
      else
      {
        TreeNode[] treeNodeArray = this.treeNodeInListView.Nodes.Find(this.listView.FocusedItem.Name, false);
        if (treeNodeArray.Length != 1)
          return;
        this.NotifySelectionChanged(this.menuItemFactory.CreateSelectionMenuItemsForTreeView(treeNodeArray[0].Tag));
      }
    }

    private void SelectAllListViewItems()
    {
      foreach (ListViewItem listViewItem in this.listView.Items)
      {
        if (!DirectoryRecord.PARENT_DIRECTORY_NAME.Equals(listViewItem.Text))
          listViewItem.Selected = true;
      }
    }

    private void UserActionExportForGDEmu(object sender, EventArgs e)
    {
      if (!(sender is ToolStripItem))
        return;
      ToolStripItem toolStripItem = sender as ToolStripItem;
      if (!(toolStripItem.Tag is IDiscFileSystem))
        return;
      this.ExportForGDEmu(toolStripItem.Tag as IDiscFileSystem);
    }

    private void UserActionCreateCueSheet(object sender, EventArgs e)
    {
      if (!(sender is ToolStripItem))
        return;
      ToolStripItem toolStripItem = sender as ToolStripItem;
      if (!(toolStripItem.Tag is IDiscSession))
        return;
      this.CreateCueSheet(toolStripItem.Tag as IDiscSession);
    }

    private void UserActionShowGDDAConversionForm(object sender, EventArgs e)
    {
      if (!(sender is ToolStripItem))
        return;
      ToolStripItem toolStripItem = sender as ToolStripItem;
      if (!(toolStripItem.Tag is IDiscSession))
        return;
      using (FormGdda formGdda = new FormGdda(toolStripItem.Tag as IDiscSession))
      {
        if (formGdda.ShowDialog((IWin32Window) this) != DialogResult.OK)
          return;
        this.gddaConverterTool.ConvertGDDATracks((IList<IDiscTrack>) formGdda.SelectedDiscTracks, formGdda.AudioConversionSettings);
      }
    }

    private void UserActionShowBootSector(object sender, EventArgs e)
    {
      if (!(sender is ToolStripItem))
        return;
      ToolStripItem toolStripItem = sender as ToolStripItem;
      if (!(toolStripItem.Tag is InitialProgramExtended))
        return;
      this.ShowBootSector(toolStripItem.Tag as InitialProgramExtended);
    }

    private void UserActionExtractInitialProgram(object sender, EventArgs eventArgs)
    {
      if (!(sender is ToolStripItem))
        return;
      ToolStripItem toolStripItem = sender as ToolStripItem;
      if (!(toolStripItem.Tag is InitialProgramExtended))
        return;
      this.ExtractInitialProgram(toolStripItem.Tag as InitialProgramExtended);
    }

    private void UserActionExtractItem(object sender, EventArgs e)
    {
      if (!(sender is ToolStripItem))
        return;
      ToolStripItem toolStripItem = sender as ToolStripItem;
      List<DirectoryRecord> DirectoryRecords = new List<DirectoryRecord>();
      if (toolStripItem.Tag is List<DirectoryRecord>)
        this.OpenExtractNodesDialog(toolStripItem.Tag as List<DirectoryRecord>);
      else if (toolStripItem.Tag is DirectoryRecord)
      {
        DirectoryRecords.Add(toolStripItem.Tag as DirectoryRecord);
        this.OpenExtractNodesDialog(DirectoryRecords);
      }
      else if (toolStripItem.Tag is IDiscTrack)
        this.OpenExtractNodesDialogForDiscTrack(toolStripItem.Tag as IDiscTrack);
      else
        DiscViewExplorer.logger.ErrorFormat("Unhandled tag object {0} for ToolStripItem {1}", toolStripItem.Tag, (object) toolStripItem);
    }

    private void UserActionGenerateSortFile(object sender, EventArgs e)
    {
      if (!(sender is ToolStripItem))
        return;
      ToolStripItem toolStripItem = sender as ToolStripItem;
      if (!(toolStripItem.Tag is IDiscSession))
        return;
      this.CreateSortFile(toolStripItem.Tag as IDiscSession);
    }

    private void UserActionShowPrimaryVolumeDescriptor(object sender, EventArgs e)
    {
      if (!(sender is ToolStripItem))
        return;
      ToolStripItem toolStripItem = sender as ToolStripItem;
      if (!(toolStripItem.Tag is IDiscTrack))
        return;
      this.ShowPVD((toolStripItem.Tag as IDiscTrack).Session.PrimaryVolumeDescriptor);
    }

    private void UserActionConvertGDDA(object sender, EventArgs e)
    {
      if (!(sender is ToolStripItem))
        return;
      ToolStripItem toolStripItem = sender as ToolStripItem;
      if (!(toolStripItem.Tag is IDiscTrack))
        return;
      this.ConvertGDDA(toolStripItem.Tag as IDiscTrack);
    }

    private void UserActionDecryptBinaryFromDisc(object sender, EventArgs e)
    {
      if (this.listView.SelectedItems.Count != 1 || !(this.listView.SelectedItems[0].Tag is DirectoryRecord))
        return;
      this.ExtractAndDecryptFile(this.listView.SelectedItems[0].Tag as DirectoryRecord);
    }

    private void HandleDragAndDropFrom(ListView ListView)
    {
      List<DirectoryRecord> list = ListView.SelectedItems.Cast<ListViewItem>().Select<ListViewItem, DirectoryRecord>((Func<ListViewItem, DirectoryRecord>) (item => (DirectoryRecord) item.Tag)).ToList<DirectoryRecord>();
      DataObject dataObject = FileDescriptorFactory.createDataObject((object) list, this.discFileSystem);
      if (dataObject != null)
      {
        int num = (int) this.DoDragDrop((object) dataObject, DragDropEffects.Copy);
      }
      else
        DiscViewExplorer.logger.WarnFormat("Drag'n Drop canceled: FileDescriptorFactory does not support {0}", (object) list);
    }

    private void HandleDragAndDropFrom(TreeView TreeView)
    {
      object tag = TreeView.SelectedNode.Tag;
      DataObject dataObject = FileDescriptorFactory.createDataObject(tag, this.discFileSystem);
      if (dataObject != null)
      {
        int num = (int) this.DoDragDrop((object) dataObject, DragDropEffects.Copy);
      }
      else
        DiscViewExplorer.logger.WarnFormat("Drag'n Drop canceled: FileDescriptorFactory does not support {0}", tag);
    }

    public void Disc_AfterSelectHandler(IDiscFileSystem disc) => this.NotifySelectionChanged(this.menuItemFactory.CreateSelectionMenuItemsForTreeView((object) disc));

    public void Disc_MouseRightClickHandler(IDiscFileSystem disc)
    {
      ToolStripItem[] itemsForTreeView = this.menuItemFactory.CreateSelectionMenuItemsForTreeView((object) disc);
      this.contextMenuManager.CreateAndShowContextMenu(itemsForTreeView);
      this.NotifySelectionChanged(itemsForTreeView);
    }

    public void DirectoryRecord_AfterSelectClickHandler(DirectoryRecord DirectoryRecord)
    {
      DiscViewExplorer.logger.InfoFormat("Opening path \"{0}\"", (object) DirectoryRecord.FullPath);
      ToolStripItem[] itemsForTreeView = this.menuItemFactory.CreateSelectionMenuItemsForTreeView((object) DirectoryRecord);
      TreeNode treeNodeInListView = this.treeNodeInListView;
      this.treeNodeInListView = this.treeView.SelectedNode;
      this.FillListView(DirectoryRecord, treeNodeInListView);
      this.NotifySelectionChanged(itemsForTreeView);
    }

    public void DirectoryRecord_MouseRightClickHandler(DirectoryRecord DirectoryRecord)
    {
      ToolStripItem[] itemsForTreeView = this.menuItemFactory.CreateSelectionMenuItemsForTreeView((object) DirectoryRecord);
      this.contextMenuManager.CreateAndShowContextMenu(itemsForTreeView);
      this.NotifySelectionChanged(itemsForTreeView);
    }

    public void DirectoryRecords_MouseLeftClickHandler(IList<DirectoryRecord> DirectoryRecords)
    {
      DirectoryRecord firstDirectoryRecord = DirectoryRecords[0];
      if (!DirectoryRecords.All<DirectoryRecord>((Func<DirectoryRecord, bool>) (directoryRecord => directoryRecord.ParentDirectory == firstDirectoryRecord.ParentDirectory)))
        this.RemoveSelectionAndNotify();
      else
        this.NotifySelectionChanged(this.menuItemFactory.CreateSelectionMenuItemsForListView((object) DirectoryRecords, this.discFileSystem));
    }

    public void DirectoryRecords_MouseDoubleLeftClicksHandler(
      IList<DirectoryRecord> DirectoryRecords)
    {
      DirectoryRecord directoryRecord = DirectoryRecords[0];
      if (!directoryRecord.IsDirectory)
        return;
      this.UpdateTreeViewSelectedNode(this.treeNodeInListView.Tag as DirectoryRecord != directoryRecord || this.treeNodeInListView.Parent == null ? this.treeNodeInListView.Nodes[directoryRecord.Name] : this.treeNodeInListView.Parent);
    }

    public void DirectoryRecords_MouseRightClickHandler(IList<DirectoryRecord> DirectoryRecords)
    {
      DirectoryRecord firstDirectoryRecord = DirectoryRecords[0];
      if (!DirectoryRecords.All<DirectoryRecord>((Func<DirectoryRecord, bool>) (directoryRecord => directoryRecord.ParentDirectory == firstDirectoryRecord.ParentDirectory)))
      {
        this.RemoveSelectionAndNotify();
      }
      else
      {
        ToolStripItem[] itemsForListView = this.menuItemFactory.CreateSelectionMenuItemsForListView((object) DirectoryRecords, this.discFileSystem);
        this.contextMenuManager.CreateAndShowContextMenu(itemsForListView);
        this.NotifySelectionChanged(itemsForListView);
      }
    }

    public void InitialProgram_AfterSelectHandler(InitialProgramExtended InitialProgram) => this.NotifySelectionChanged(this.menuItemFactory.CreateSelectionMenuItemsForTreeView((object) InitialProgram));

    public void InitialProgram_MouseDoubleLeftClicksHandler(InitialProgramExtended InitialProgram)
    {
      ToolStripItem[] itemsForTreeView = this.menuItemFactory.CreateSelectionMenuItemsForTreeView((object) InitialProgram);
      this.ShowBootSector(InitialProgram);
      this.NotifySelectionChanged(itemsForTreeView);
    }

    public void InitialProgram_MouseRightClickHandler(InitialProgramExtended InitialProgram) => this.contextMenuManager.CreateAndShowContextMenu(this.menuItemFactory.CreateSelectionMenuItemsForTreeView((object) InitialProgram));

    public void DiscSession_AfterSelectHandler(IDiscSession DiscSession) => this.NotifySelectionChanged(this.menuItemFactory.CreateSelectionMenuItemsForTreeView((object) DiscSession));

    public void DiscSession_MouseRightClickHandler(IDiscSession DiscSession)
    {
      ToolStripItem[] itemsForTreeView = this.menuItemFactory.CreateSelectionMenuItemsForTreeView((object) DiscSession);
      this.contextMenuManager.CreateAndShowContextMenu(itemsForTreeView);
      this.NotifySelectionChanged(itemsForTreeView);
    }

    public void DiscTrack_AfterSelectHandler(IDiscTrack DiscTrack) => this.NotifySelectionChanged(this.menuItemFactory.CreateSelectionMenuItemsForTreeView((object) DiscTrack));

    public void DiscTrack_MouseRightClickHandler(IDiscTrack DiscTrack)
    {
      ToolStripItem[] itemsForTreeView = this.menuItemFactory.CreateSelectionMenuItemsForTreeView((object) DiscTrack);
      this.contextMenuManager.CreateAndShowContextMenu(itemsForTreeView);
      this.NotifySelectionChanged(itemsForTreeView);
    }

    private void treeView_MouseClick(object sender, MouseEventArgs e)
    {
      TreeNode nodeAt = this.treeView.GetNodeAt(e.Location);
      if (nodeAt == null)
        return;
      this.treeView.SelectedNode = nodeAt;
      IDiscViewSelection discViewSelection = this.treeViewSelectionHandlerFactory.newSelectionHandlers(this.treeView.SelectedNode.Tag);
      if (discViewSelection != null)
        discViewSelection.HandleMouseEvent(e);
      else
        DiscViewExplorer.logger.ErrorFormat("Unhandled object {0}", (object) this.Handle);
    }

    private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
    {
      TreeNode node = e.Node;
      if (node == null)
        return;
      this.treeViewSelectionHandlerFactory.newSelectionHandlers(node.Tag)?.HandleAfterSelect();
    }

    private void treeView_GotFocus(object sender, EventArgs e)
    {
      if (this.lastControlWithFocus == this.treeView)
        return;
      this.lastControlWithFocus = (Control) this.treeView;
      if (this.treeView.SelectedNode != null)
        this.NotifySelectionChanged(this.menuItemFactory.CreateSelectionMenuItemsForTreeView(this.treeView.SelectedNode.Tag));
      else
        this.RemoveSelectionAndNotify();
    }

    private void listView_MouseClick(object sender, MouseEventArgs mouseEvent)
    {
      if (this.listView.SelectedItems.Count == 0)
        this.RemoveSelectionAndNotify();
      else
        this.listViewSelectionHandlerFactory.newSelectionHandlers((object) this.listView.SelectedItems.Cast<ListViewItem>().Select<ListViewItem, DirectoryRecord>((Func<ListViewItem, DirectoryRecord>) (item => (DirectoryRecord) item.Tag)).ToList<DirectoryRecord>()).HandleMouseEvent(mouseEvent);
    }

    private void listView_ColumnClick(object sender, ColumnClickEventArgs e)
    {
      this.listView.BeginUpdate();
      this.listView.Sorting = this.listView.Sorting != SortOrder.Ascending ? (((ListViewColumnSorter) this.listView.ListViewItemSorter).Order = SortOrder.Ascending) : (((ListViewColumnSorter) this.listView.ListViewItemSorter).Order = SortOrder.Descending);
      ((ListViewColumnSorter) this.listView.ListViewItemSorter).SortColumn = e.Column;
      this.listView.Sort();
      this.listView.EndUpdate();
    }

    private void listView_KeyUp(object sender, KeyEventArgs e)
    {
      if (this.listView.SelectedItems.Count == 0)
        this.RemoveSelectionAndNotify();
      else
        this.listViewSelectionHandlerFactory.newSelectionHandlers((object) this.listView.SelectedItems.Cast<ListViewItem>().Select<ListViewItem, DirectoryRecord>((Func<ListViewItem, DirectoryRecord>) (item => (DirectoryRecord) item.Tag)).ToList<DirectoryRecord>()).HandleLeftClick();
    }

    private void listView_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Control)
      {
        if (e.KeyCode != Keys.A)
          return;
        this.SelectAllListViewItems();
      }
      else if (e.KeyCode == Keys.Return)
      {
        this.NavigateDown();
      }
      else
      {
        if (e.KeyCode != Keys.Back)
          return;
        this.NagivateUp();
      }
    }

    private void listView_GotFocus(object sender, EventArgs e)
    {
      if (this.lastControlWithFocus == this.listView)
        return;
      this.lastControlWithFocus = (Control) this.listView;
      if (this.listView.SelectedItems.Count > 0)
        this.NotifySelectionChanged(this.menuItemFactory.CreateSelectionMenuItemsForListView((object) this.listView.SelectedItems.Cast<ListViewItem>().Select<ListViewItem, DirectoryRecord>((Func<ListViewItem, DirectoryRecord>) (item => (DirectoryRecord) item.Tag)).ToList<DirectoryRecord>(), this.discFileSystem));
      else
        this.RemoveSelectionAndNotify();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (DiscViewExplorer));
      this.groupBox = new GroupBox();
      this.splitContainer = new SplitContainer();
      this.treeView = new TreeView();
      this.treeViewImageList = new ImageList(this.components);
      this.listView = new ListView();
      this.listViewImageList = new ImageList(this.components);
      this.saveFileDialogForInitialProgram = new SaveFileDialog();
      this.saveFileDialogForExtraction = new SaveFileDialog();
      this.saveFileDialogForCueSheet = new SaveFileDialog();
      this.saveFileDialogForSortFile = new SaveFileDialog();
      this.folderBrowserDialogForExtraction = new FolderBrowserDialog();
      this.saveFileDialogForDesDecrypt = new SaveFileDialog();
      this.discExtractor = new DiscExtractor(this.components);
      this.contextMenuImageList = new ImageList(this.components);
      this.gddaConverterTool = new GDDAConverterTool(this.components);
      this.groupBox.SuspendLayout();
      this.splitContainer.Panel1.SuspendLayout();
      this.splitContainer.Panel2.SuspendLayout();
      this.splitContainer.SuspendLayout();
      this.SuspendLayout();
      this.groupBox.AutoSize = true;
      this.groupBox.BackColor = SystemColors.Window;
      this.groupBox.Controls.Add((Control) this.splitContainer);
      this.groupBox.Dock = DockStyle.Fill;
      this.groupBox.Location = new Point(4, 4);
      this.groupBox.Name = "groupBox";
      this.groupBox.Size = new Size(632, 472);
      this.groupBox.TabIndex = 0;
      this.groupBox.TabStop = false;
      this.groupBox.Text = "Disc title";
      this.splitContainer.Dock = DockStyle.Fill;
      this.splitContainer.Location = new Point(3, 16);
      this.splitContainer.Name = "splitContainer";
      this.splitContainer.Panel1.Controls.Add((Control) this.treeView);
      this.splitContainer.Panel2.Controls.Add((Control) this.listView);
      this.splitContainer.Size = new Size(626, 453);
      this.splitContainer.SplitterDistance = 207;
      this.splitContainer.TabIndex = 0;
      this.splitContainer.TabStop = false;
      this.treeView.BackColor = SystemColors.Window;
      this.treeView.Dock = DockStyle.Fill;
      this.treeView.HideSelection = false;
      this.treeView.HotTracking = true;
      this.treeView.ImageIndex = 0;
      this.treeView.ImageList = this.treeViewImageList;
      this.treeView.Indent = 18;
      this.treeView.ItemHeight = 18;
      this.treeView.Location = new Point(0, 0);
      this.treeView.Name = "treeView";
      this.treeView.SelectedImageIndex = 0;
      this.treeView.ShowLines = false;
      this.treeView.ShowNodeToolTips = true;
      this.treeView.Size = new Size(207, 453);
      this.treeView.TabIndex = 1;
      this.treeViewImageList.ImageStream = (ImageListStreamer) componentResourceManager.GetObject("treeViewImageList.ImageStream");
      this.treeViewImageList.TransparentColor = Color.Transparent;
      this.treeViewImageList.Images.SetKeyName(0, "gd_icon.png");
      this.treeViewImageList.Images.SetKeyName(1, "dclogo_black.png");
      this.treeViewImageList.Images.SetKeyName(2, "cdrom_data.png");
      this.treeViewImageList.Images.SetKeyName(3, "cdrom_audio.png");
      this.treeViewImageList.Images.SetKeyName(4, "file_binary.png");
      this.treeViewImageList.Images.SetKeyName(5, "iso9660.png");
      this.treeViewImageList.Images.SetKeyName(6, "folder_closed.ico");
      this.treeViewImageList.Images.SetKeyName(7, "folder_open.ico");
      this.listView.AllowColumnReorder = true;
      this.listView.Dock = DockStyle.Fill;
      this.listView.Font = new Font("Microsoft Sans Serif", 8.25f);
      this.listView.FullRowSelect = true;
      this.listView.HideSelection = false;
      this.listView.LabelWrap = false;
      this.listView.Location = new Point(0, 0);
      this.listView.Name = "listView";
      this.listView.ShowGroups = false;
      this.listView.ShowItemToolTips = true;
      this.listView.Size = new Size(415, 453);
      this.listView.SmallImageList = this.listViewImageList;
      this.listView.Sorting = SortOrder.Ascending;
      this.listView.TabIndex = 2;
      this.listView.UseCompatibleStateImageBehavior = false;
      this.listView.View = View.Details;
      this.listViewImageList.ImageStream = (ImageListStreamer) componentResourceManager.GetObject("listViewImageList.ImageStream");
      this.listViewImageList.TransparentColor = Color.Transparent;
      this.listViewImageList.Images.SetKeyName(0, "file.ico");
      this.listViewImageList.Images.SetKeyName(1, "folder_closed.ico");
      this.listViewImageList.Images.SetKeyName(2, "folder_up.ico");
      this.discExtractor.DiscFileSystem = (IDiscFileSystem) null;
      this.contextMenuImageList.ImageStream = (ImageListStreamer) componentResourceManager.GetObject("contextMenuImageList.ImageStream");
      this.contextMenuImageList.TransparentColor = Color.Transparent;
      this.contextMenuImageList.Images.SetKeyName(0, "extract_icon.ico");
      this.contextMenuImageList.Images.SetKeyName(1, "file.ico");
      this.contextMenuImageList.Images.SetKeyName(2, "zoom.ico");
      this.contextMenuImageList.Images.SetKeyName(3, "help.ico");
      this.contextMenuImageList.Images.SetKeyName(4, "folder_open.ico");
      this.contextMenuImageList.Images.SetKeyName(5, "folder_up.ico");
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoSize = true;
      this.Controls.Add((Control) this.groupBox);
      this.DoubleBuffered = true;
      this.Name = nameof (DiscViewExplorer);
      this.Padding = new Padding(4);
      this.Size = new Size(640, 480);
      this.Load += new EventHandler(this.DiscViewExplorer_Load);
      this.Leave += new EventHandler(this.DiscViewExplorer_Leave);
      this.groupBox.ResumeLayout(false);
      this.splitContainer.Panel1.ResumeLayout(false);
      this.splitContainer.Panel2.ResumeLayout(false);
      this.splitContainer.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public sealed class ActionsForEditMenuItem
    {
      public bool AnyAvailable { get; private set; }

      public bool SelectionMenuAvailable { get; private set; }

      public ToolStripItem[] DiscMenuItems { get; private set; }

      public ToolStripItem[] SelectionMenuItems { get; private set; }

      internal ActionsForEditMenuItem(
        ToolStripItem[] DiscMenuItems,
        ToolStripItem[] SelectionMenuItems)
      {
        this.DiscMenuItems = DiscMenuItems;
        this.SelectionMenuItems = SelectionMenuItems;
        this.SelectionMenuAvailable = SelectionMenuItems != null && SelectionMenuItems.Length > 0;
        this.AnyAvailable = this.SelectionMenuAvailable || DiscMenuItems != null && DiscMenuItems.Length > 0;
      }

      internal static DiscViewExplorer.ActionsForEditMenuItem NoEditActions() => new DiscViewExplorer.ActionsForEditMenuItem((ToolStripItem[]) null, (ToolStripItem[]) null);
    }
  }
}
