﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.UserControls.DiscViewOpener
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.Events;
using GDRomExplorer.Forms;
using GDRomExplorer.Properties;
using GDRomExplorer.Resources;
using ImageReader.DiscFileSystem;
using SEGATools.Disc;
using SEGATools.DiscFileSystem;
using SEGATools.FileFormat;
using SEGATools.Security;
using SEGATools.UserProcess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace GDRomExplorer.UserControls
{
  public class DiscViewOpener : UserControl
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private FormLoading formLoading;
    private List<string> fileDialogIncludeFilters = new List<string>();
    private string SelectedDiscImage;
    private bool notifyStatusEvents;
    private IContainer components;
    private TableLayoutPanel tableLayoutPanel;
    private Button btOpen;
    private TextBox tbDiscImageFile;
    private GroupBox groupBox;
    private DiscOpener discOpener;
    private DiscFormatProvider discFormatProvider;
    private OpenFileDialog openFileDialog;

    public event EventHandler<EventArgs<IDiscFileSystem>> ImageLoaded;

    public event EventHandler ImageNotLoaded;

    [Description("Specifies the text of the group box.")]
    [Category("Appearance")]
    [Browsable(true)]
    public override string Text
    {
      get => this.groupBox.Text;
      set => this.groupBox.Text = value;
    }

    [Description("Specifies the text of the open button.")]
    [Category("Appearance")]
    [Browsable(true)]
    public string ButtonText
    {
      get => this.btOpen.Text;
      set => this.btOpen.Text = value;
    }

    [Browsable(true)]
    [Description("Indicates the initial directory for the OpenFileDialog.")]
    [Category("Behavior")]
    public string InitialDirectory
    {
      get => this.openFileDialog.InitialDirectory;
      set => this.openFileDialog.InitialDirectory = value;
    }

    [Description("Indicates whether the control should notify the application status bar control.")]
    [Category("Behavior")]
    [Browsable(true)]
    public bool NotifyStatusEvents
    {
      get => this.notifyStatusEvents;
      set => this.notifyStatusEvents = value;
    }

    [Category("Behavior")]
    [Description("Restrict the list of valid file extensions.")]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
    [EditorBrowsable(EditorBrowsableState.Always)]
    [Browsable(true)]
    [Editor("System.Windows.Forms.Design.StringCollectionEditor, System.Design", "System.Drawing.Design.UITypeEditor, System.Drawing")]
    public List<string> FileDialogFilters
    {
      get => this.fileDialogIncludeFilters;
      set => this.fileDialogIncludeFilters = value;
    }

    public DiscViewOpener()
    {
      this.InitializeComponent();
      this.btOpen.Text = Strings.DiscViewOpenerButtonOpen;
      this.groupBox.Text = Strings.DiscViewOpenerLabel + (object) ':';
      this.openFileDialog.Title = Strings.DiscViewOpenerFileDialogTitle;
      this.discOpener = new DiscOpener();
    }

    public void SetLoadedDiscImage(IDiscFileSystem discFileSystem)
    {
      string message = string.Format(Strings.StatusLabelImageLoaded, (object) discFileSystem.FileName);
      DiscViewOpener.logger.InfoFormat("Image successfully opened: {0}", (object) discFileSystem.FileName);
      this.tbDiscImageFile.Text = discFileSystem.FileName;
      this.tbDiscImageFile.BackColor = SystemColors.Window;
      if (this.ImageLoaded != null)
        this.ImageLoaded((object) this, new EventArgs<IDiscFileSystem>(discFileSystem));
      this.NotifyNewAppStatus(message);
    }

    public void Open()
    {
      this.openFileDialog.Filter = this.discFormatProvider.GetFileDialogFilters(this.FileDialogFilters != null ? this.FileDialogFilters.ToArray() : (string[]) null);
      if (this.openFileDialog.ShowDialog() != DialogResult.OK)
        return;
      this.Open(this.openFileDialog.FileName);
    }

    public void Open(string FileName)
    {
      if (!File.Exists(FileName))
      {
        DiscViewOpener.logger.ErrorFormat("File not found {0}", (object) FileName);
        string str = string.Format(Strings.MsgBoxImageOpenerFileNotFoundErrorWithFormat, (object) Path.GetFullPath(FileName));
        int num = (int) MessageBox.Show((IWin32Window) this, str, Strings.MsgBoxImageOpenerTitle, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        this.NotifyNewAppStatus(str);
        this.Close();
      }
      else
      {
        string extension = Path.GetExtension(FileName);
        if (!this.discFormatProvider.IsFileExtensionSupported(extension))
        {
          DiscViewOpener.logger.ErrorFormat("Not supported file extension for file {0}", (object) FileName);
          string str = string.Format(Strings.MsgBoxImageOpenerBadFileExtensionErrorWithFormat, (object) Path.GetExtension(FileName));
          int num = (int) MessageBox.Show((IWin32Window) this, str, Strings.MsgBoxImageOpenerTitle, MessageBoxButtons.OK, MessageBoxIcon.Hand);
          this.NotifyNewAppStatus(str);
          this.Close();
        }
        else
        {
          this.SelectedDiscImage = FileName;
          IImageFileFormat forFileExtension = this.discFormatProvider.FindImageFileFormatForFileExtension(extension);
          this.openFileDialog.InitialDirectory = Path.GetDirectoryName(this.SelectedDiscImage);
          this.openFileDialog.FileName = Path.GetFileName(this.SelectedDiscImage);
          if (this.openFileDialog.FilterIndex > 1)
            this.openFileDialog.FilterIndex = this.discFormatProvider.GetFileFormatIndex(forFileExtension);
          this.formLoading = new FormLoading(Strings.DiscViewOpenerImageLoadingTitle, Strings.DiscViewOpenerImageLoadingMessage);
          this.discOpener.FileOpenerProgressChanged += new AsyncOperationProgressChangedEventHandler(this.GDRomFileSystem_GDRomImageOpenProgressChanged);
          this.discOpener.FileOpenerCompleted += new AsyncOperationCompletedEventHandler(this.GDRomFileSystem_GDRomImageOpenCompleted);
          this.discOpener.OpenImageAsync(this.SelectedDiscImage, forFileExtension.ImageFileConverter, Settings.Default.ImageReaderComputePathTable);
        }
      }
    }

    public void Close()
    {
      this.tbDiscImageFile.Text = string.Empty;
      this.tbDiscImageFile.BackColor = SystemColors.Menu;
      this.SelectedDiscImage = string.Empty;
      this.discOpener.CloseImage();
      if (this.ImageNotLoaded == null)
        return;
      this.ImageNotLoaded((object) this, new EventArgs());
    }

    private void btOpen_Click(object sender, EventArgs e) => this.Open();

    private void NotifyNewAppStatus(string message)
    {
      if (!this.NotifyStatusEvents)
        return;
      AppStatus.NotifyNewAppStatus(message);
    }

    private string GetErrorMessage(Exception e)
    {
      switch (e)
      {
        case DiscFileSystemException _:
          return string.Format(Strings.StatusLabelImageError, (object) Strings.DiscViewOpenerNoFileSystemError);
        case InitialProgramException _:
          return string.Format(Strings.StatusLabelImageError, (object) string.Format(Strings.DiscViewOpenerInvalidInitialProgram, (object) e.Message));
        case DiscFormatException _:
          return string.Format(Strings.StatusLabelImageError, (object) e.Message);
        default:
          return string.Format(Strings.StatusLabelImageError, (object) Strings.DiscViewOpenerUnknownError);
      }
    }

    private void GDRomFileSystem_GDRomImageOpenCompleted(
      object sender,
      UserProcessCompletedEventArgs e)
    {
      this.formLoading.Close();
      this.formLoading = (FormLoading) null;
      this.discOpener.FileOpenerProgressChanged -= new AsyncOperationProgressChangedEventHandler(this.GDRomFileSystem_GDRomImageOpenProgressChanged);
      this.discOpener.FileOpenerCompleted -= new AsyncOperationCompletedEventHandler(this.GDRomFileSystem_GDRomImageOpenCompleted);
      if (e.Error != null)
      {
        string errorMessage = this.GetErrorMessage(e.Error);
        DiscViewOpener.logger.ErrorFormat("Unable to open image {0}: {1}", (object) e.ResourceName, (object) e.Error);
        int num = (int) MessageBox.Show((IWin32Window) this, errorMessage, Strings.MsgBoxImageOpenerTitle, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        this.NotifyNewAppStatus(errorMessage);
        this.Close();
      }
      else
        this.SetLoadedDiscImage(this.discOpener.DiscFileSystem);
    }

    private void GDRomFileSystem_GDRomImageOpenProgressChanged(UserProcessProgressChangedEventArgs e)
    {
      if (this.formLoading.Visible)
        return;
      int num = (int) this.formLoading.ShowDialog((IWin32Window) this);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      this.tableLayoutPanel = new TableLayoutPanel();
      this.tbDiscImageFile = new TextBox();
      this.btOpen = new Button();
      this.groupBox = new GroupBox();
      this.openFileDialog = new OpenFileDialog();
      this.discOpener = new DiscOpener(this.components);
      this.discFormatProvider = new DiscFormatProvider(this.components);
      this.tableLayoutPanel.SuspendLayout();
      this.groupBox.SuspendLayout();
      this.SuspendLayout();
      this.tableLayoutPanel.AutoSize = true;
      this.tableLayoutPanel.ColumnCount = 2;
      this.tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100f));
      this.tableLayoutPanel.ColumnStyles.Add(new ColumnStyle());
      this.tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanel.Controls.Add((Control) this.tbDiscImageFile, 0, 0);
      this.tableLayoutPanel.Controls.Add((Control) this.btOpen, 1, 0);
      this.tableLayoutPanel.Dock = DockStyle.Fill;
      this.tableLayoutPanel.Location = new Point(3, 16);
      this.tableLayoutPanel.Margin = new Padding(0);
      this.tableLayoutPanel.Name = "tableLayoutPanel";
      this.tableLayoutPanel.RowCount = 1;
      this.tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100f));
      this.tableLayoutPanel.Size = new Size(762, 31);
      this.tableLayoutPanel.TabIndex = 0;
      this.tbDiscImageFile.BackColor = Color.White;
      this.tbDiscImageFile.Dock = DockStyle.Fill;
      this.tbDiscImageFile.HideSelection = false;
      this.tbDiscImageFile.Location = new Point(3, 3);
      this.tbDiscImageFile.Name = "tbDiscImageFile";
      this.tbDiscImageFile.ReadOnly = true;
      this.tbDiscImageFile.Size = new Size(670, 20);
      this.tbDiscImageFile.TabIndex = 1;
      this.tbDiscImageFile.TabStop = false;
      this.tbDiscImageFile.WordWrap = false;
      this.btOpen.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.btOpen.AutoSize = true;
      this.btOpen.FlatStyle = FlatStyle.Popup;
      this.btOpen.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btOpen.ForeColor = Color.FromArgb(248, 48, 0);
      this.btOpen.Location = new Point(679, 1);
      this.btOpen.Margin = new Padding(3, 1, 3, 3);
      this.btOpen.Name = "btOpen";
      this.btOpen.Size = new Size(80, 24);
      this.btOpen.TabIndex = 2;
      this.btOpen.Text = "&Open";
      this.btOpen.UseVisualStyleBackColor = false;
      this.btOpen.Click += new EventHandler(this.btOpen_Click);
      this.groupBox.BackColor = SystemColors.Window;
      this.groupBox.Controls.Add((Control) this.tableLayoutPanel);
      this.groupBox.Dock = DockStyle.Fill;
      this.groupBox.Location = new Point(4, 4);
      this.groupBox.Margin = new Padding(0);
      this.groupBox.MaximumSize = new Size(0, 50);
      this.groupBox.MinimumSize = new Size(0, 50);
      this.groupBox.Name = "groupBox";
      this.groupBox.Size = new Size(768, 50);
      this.groupBox.TabIndex = 0;
      this.groupBox.TabStop = false;
      this.groupBox.Text = "disc image";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoSize = true;
      this.Controls.Add((Control) this.groupBox);
      this.Name = nameof (DiscViewOpener);
      this.Padding = new Padding(4);
      this.Size = new Size(776, 55);
      this.tableLayoutPanel.ResumeLayout(false);
      this.tableLayoutPanel.PerformLayout();
      this.groupBox.ResumeLayout(false);
      this.groupBox.PerformLayout();
      this.ResumeLayout(false);
    }
  }
}
