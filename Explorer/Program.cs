﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Program
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.Forms;
using System;
using System.Reflection;
using System.Security;
using System.Security.Permissions;
using System.Security.Policy;
using System.Threading;
using System.Windows.Forms;

namespace GDRomExplorer
{
  public class Program
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private static readonly string THRUSTED_DOMAIN_NAME = "GDROMExplorer Thrusted Domain";
    private static string inputImageFile = string.Empty;

    public static string GetProgamArgument() => Program.inputImageFile;

    [STAThread]
    private static void Main(string[] args)
    {
      if (args.Length > 0)
        Program.inputImageFile = args[0];
      PermissionSet grantSet = new PermissionSet(PermissionState.Unrestricted);
      AppDomain.CreateDomain(Program.THRUSTED_DOMAIN_NAME, (Evidence) null, new AppDomainSetup()
      {
        ApplicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase
      }, grantSet).CreateInstance(Assembly.GetExecutingAssembly().FullName, typeof (Program).FullName);
    }

    public Program()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.SetUnhandledExceptionMode(UnhandledExceptionMode.Automatic);
      Application.ThreadException += new ThreadExceptionEventHandler(Program.Application_ThreadException);
      if (!Program.LoadAssembly("ImageReader", "ImageReader.AssemblyTest") || !Program.LoadAssembly("SEGATools", "SEGATools.AssemblyTest"))
        return;
      Application.Run((Form) new GDRomExplorerForm());
    }

    private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
    {
      Program.logger.ErrorFormat("Unhandled exception: {0}", (object) e.Exception);
      int num = (int) MessageBox.Show("An unexpected error occurred.", "Error in Application", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
    }

    private static bool LoadAssembly(string assemblyName, string typeName)
    {
      try
      {
        AppDomain.CurrentDomain.CreateInstance(assemblyName, typeName);
      }
      catch (Exception ex)
      {
        Program.logger.Error((object) ex);
        int num = (int) MessageBox.Show(assemblyName + ".dll is corrupted or missing!", Application.ProductName + ": Error!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        return false;
      }
      return true;
    }
  }
}
