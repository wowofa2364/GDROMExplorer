﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.DiscView.ExplorerToolTipFactory
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.Resources;
using ImageReader.ISO9660.DirectoryRecords;
using SEGATools.Formater;
using System.Text;

namespace GDRomExplorer.DiscView
{
  public class ExplorerToolTipFactory
  {
    public static string CreateToolTipText(DirectoryRecord directoryRecord)
    {
      StringBuilder stringBuilder = new StringBuilder();
      if (directoryRecord.IsDirectory)
      {
        stringBuilder.AppendLine(string.Format(Strings.ToolTipFileSizeWithFormat, (object) SizeFormater.ToHumanReadableSize((long) directoryRecord.UsedSpace)));
        int count1 = directoryRecord.GetAllSubFiles().Count;
        int count2 = directoryRecord.GetAllSubFolder().Count;
        stringBuilder.AppendLine(string.Format(Strings.ToolTipDirectoryContentWithFormat, (object) count1, count1 > 1 ? (object) Strings.ToolTipFiles : (object) Strings.ToolTipFile, (object) count2, count2 > 1 ? (object) Strings.ToolTipDirectories : (object) Strings.ToolTipDirectory));
      }
      return stringBuilder.ToString();
    }
  }
}
