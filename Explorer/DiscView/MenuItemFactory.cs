﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.DiscView.MenuItemFactory
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using ImageReader.ISO9660.DirectoryRecords;
using SEGATools.DiscFileSystem;
using SEGATools.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace GDRomExplorer.DiscView
{
  public class MenuItemFactory
  {
    private UserActions Context;
    private ImageList ContextMenuImageList;

    public MenuItemFactory(UserActions Context, ImageList ImageList)
    {
      this.Context = Context;
      this.ContextMenuImageList = ImageList;
    }

    private ToolStripItem CreateMenuItem(
      string Text,
      int ImageIndex,
      EventHandler Handler)
    {
      return (ToolStripItem) new ToolStripMenuItem(Text, this.ContextMenuImageList.Images[ImageIndex], Handler);
    }

    public ToolStripItem[] CreateDiscMenuItems(IDiscFileSystem Disc)
    {
      ToolStripItem menuItem = this.CreateMenuItem(GDRomExplorer.Resources.Strings.ContextMenuItemExportForGDEmu, 0, this.Context.GetHandler(UserActions.Action.ExportForGDEmu));
      menuItem.Enabled = Disc.CanBeExportedToGdi;
      menuItem.Tag = (object) Disc;
      return new ToolStripItem[1]{ menuItem };
    }

    public ToolStripItem[] CreateDiscSessionMenuItems(IDiscSession Session)
    {
      ToolStripItem[] toolStripItemArray = new ToolStripItem[2]
      {
        this.CreateMenuItem(GDRomExplorer.Resources.Strings.ContextMenuItemCreateCUE, 0, this.Context.GetHandler(UserActions.Action.CreateCueSheet)),
        null
      };
      toolStripItemArray[0].Enabled = Session.Disc.CanBeExportedToCueSheet && Session.Tracks.Count > 0;
      toolStripItemArray[0].Tag = (object) Session;
      toolStripItemArray[1] = this.CreateMenuItem(GDRomExplorer.Resources.Strings.ContextMenuItemConvertGDDA, 0, this.Context.GetHandler(UserActions.Action.ShowGDDAConversion));
      toolStripItemArray[1].Enabled = Session.Disc.CanExtractData && Session.AudioTracks.Count > 0;
      toolStripItemArray[1].Tag = (object) Session;
      return toolStripItemArray;
    }

    public ToolStripItem[] CreateDiscTrackMenuItems(IDiscTrack Track)
    {
      IList<ToolStripItem> source = (IList<ToolStripItem>) new List<ToolStripItem>();
      string fileName = Path.GetFileName(Track.Name);
      DirectoryRecord rootDirectoryRecord = Track.Session.RootDirectoryRecord;
      ToolStripItem menuItem1 = this.CreateMenuItem(string.Format(GDRomExplorer.Resources.Strings.ContextMenuItemExtract, (object) fileName), 0, this.Context.GetHandler(UserActions.Action.ExtractItem));
      menuItem1.Enabled = Track.Session.Disc.CanExtractData;
      menuItem1.Tag = (object) Track;
      source.Add(menuItem1);
      if (Track.TrackData == TrackModeType.Data && Track == Track.Session.FirstDataTrack)
      {
        ToolStripItem menuItem2 = this.CreateMenuItem(GDRomExplorer.Resources.Strings.ContextMenuItemGenerateSortFile, 1, this.Context.GetHandler(UserActions.Action.CreateSortFile));
        menuItem2.Tag = (object) Track.Session;
        source.Add(menuItem2);
        ToolStripItem menuItem3 = this.CreateMenuItem(GDRomExplorer.Resources.Strings.ContextMenuItemViewPvd, 3, this.Context.GetHandler(UserActions.Action.ShowPrimaryVolumeDescriptor));
        menuItem3.Tag = (object) Track;
        source.Add(menuItem3);
      }
      else if (Track.TrackData == TrackModeType.Audio)
      {
        ToolStripItem menuItem2 = this.CreateMenuItem(string.Format(GDRomExplorer.Resources.Strings.ContextMenuItemConvertAudio, (object) fileName), 0, this.Context.GetHandler(UserActions.Action.ConvertGDDA));
        menuItem2.Enabled = Track.Session.Disc.CanExtractData;
        menuItem2.Tag = (object) Track;
        source.Add(menuItem2);
      }
      return source.ToArray<ToolStripItem>();
    }

    public ToolStripItem[] CreateInitialProgramMenuItems(
      InitialProgramExtended InitialProgramFile)
    {
      List<ToolStripItem> toolStripItemList = new List<ToolStripItem>();
      ToolStripItem menuItem1 = this.CreateMenuItem(GDRomExplorer.Resources.Strings.ContextMenuItemViewIP, 2, this.Context.GetHandler(UserActions.Action.ShowBootSector));
      menuItem1.Tag = (object) InitialProgramFile;
      toolStripItemList.Add(menuItem1);
      ToolStripItem menuItem2 = this.CreateMenuItem(string.Format(GDRomExplorer.Resources.Strings.ContextMenuItemExtractIP, (object) InitialProgramFile.FileName), 0, this.Context.GetHandler(UserActions.Action.ExtractBootSector));
      menuItem2.Tag = (object) InitialProgramFile;
      toolStripItemList.Add(menuItem2);
      return toolStripItemList.ToArray();
    }

    public ToolStripItem[] CreateFileExtractMenuItems(
      string ItemLabel,
      object Tag,
      bool SupportDecryption)
    {
      string.Format(GDRomExplorer.Resources.Strings.ContextMenuItemExtract, (object) ItemLabel);
      string.Format(GDRomExplorer.Resources.Strings.ContextMenuItemDecryptAndExtract, (object) ItemLabel);
      return this.CreateFileMenuItems(string.Format(GDRomExplorer.Resources.Strings.ContextMenuItemExtract, (object) ItemLabel), string.Format(GDRomExplorer.Resources.Strings.ContextMenuItemDecryptAndExtract, (object) ItemLabel), Tag, SupportDecryption);
    }

    public ToolStripItem[] CreateFileExtractContentMenuItems(
      string ItemLabel,
      object Tag)
    {
      return this.CreateFileMenuItems(string.Format(GDRomExplorer.Resources.Strings.ContextMenuItemExtractContent, (object) ItemLabel), string.Format(GDRomExplorer.Resources.Strings.ContextMenuItemDecryptAndExtract, (object) ItemLabel), Tag, false);
    }

    private ToolStripItem[] CreateFileMenuItems(
      string ExtractText,
      string DecryptExtractText,
      object Tag,
      bool SupportDecryption)
    {
      List<ToolStripItem> toolStripItemList = new List<ToolStripItem>();
      if (SupportDecryption)
      {
        ToolStripItem menuItem = this.CreateMenuItem(DecryptExtractText, 0, this.Context.GetHandler(UserActions.Action.DecryptNaomiBinary));
        menuItem.Tag = Tag;
        toolStripItemList.Add(menuItem);
      }
      ToolStripItem menuItem1 = this.CreateMenuItem(ExtractText, 0, this.Context.GetHandler(UserActions.Action.ExtractItem));
      menuItem1.Tag = Tag;
      toolStripItemList.Add(menuItem1);
      return toolStripItemList.ToArray();
    }

    public ToolStripItem[] CreateDiscWithSessionTopMenuItem(IDiscFileSystem Disc)
    {
      List<ToolStripItem> toolStripItemList = new List<ToolStripItem>();
      toolStripItemList.AddRange((IEnumerable<ToolStripItem>) this.CreateDiscMenuItems(Disc));
      toolStripItemList.Add((ToolStripItem) new ToolStripSeparator());
      foreach (IDiscSession session in Disc.Sessions)
      {
        ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem(session.Name);
        toolStripMenuItem.DropDownItems.AddRange(this.CreateDiscSessionMenuItems(session));
        toolStripItemList.Add((ToolStripItem) toolStripMenuItem);
      }
      return toolStripItemList.ToArray();
    }

    public ToolStripMenuItem CreateSelectionTopMenuItemForListView(
      ToolStripItem[] MenuItems)
    {
      ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem(GDRomExplorer.Resources.Strings.EditSelectionMenuItemName);
      toolStripMenuItem.DropDownItems.AddRange(MenuItems);
      toolStripMenuItem.Enabled = toolStripMenuItem.DropDownItems.Count > 0;
      return toolStripMenuItem;
    }

    public ToolStripItem[] CreateSelectionMenuItemsForListView(
      object NewSelection,
      IDiscFileSystem Disc)
    {
      ToolStripItem[] toolStripItemArray = new ToolStripItem[0];
      List<DirectoryRecord> directoryRecordList = NewSelection is List<DirectoryRecord> ? NewSelection as List<DirectoryRecord> : throw new NotSupportedException();
      if (directoryRecordList.Count != 1)
        return this.CreateFileExtractMenuItems(GDRomExplorer.Resources.Strings.MsgBoxToolStripMenuSelectedFiles, NewSelection, false);
      DirectoryRecord directoryRecord = directoryRecordList[0];
      string ItemLabel = directoryRecord == Disc.MainBinary ? string.Format(GDRomExplorer.Resources.Strings.ToolTipMainBinaryWithFormat, (object) directoryRecord.Name) : directoryRecord.Name;
      bool SupportDecryption = Disc.DiscType == DiscImageType.Naomi && directoryRecord == Disc.MainBinary;
      return this.CreateFileExtractMenuItems(ItemLabel, NewSelection, SupportDecryption);
    }

    public ToolStripItem[] CreateSelectionDisabledTopMenuItem() => new ToolStripItem[0];

    public static ToolStripMenuItem CreateEditSelectionTopMenuItem(
      ToolStripItem[] MenuItems)
    {
      ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem(GDRomExplorer.Resources.Strings.EditSelectionMenuItemName);
      if (MenuItems != null && MenuItems.Length > 0)
        toolStripMenuItem.DropDownItems.AddRange(MenuItems);
      toolStripMenuItem.Enabled = MenuItems != null && MenuItems.Length > 0;
      return toolStripMenuItem;
    }

    public static ToolStripMenuItem CreateEditDiscTopMenuItem(
      ToolStripItem[] MenuItems)
    {
      ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem(GDRomExplorer.Resources.Strings.EditDiscMenuItemName);
      toolStripMenuItem.DropDownItems.AddRange(MenuItems);
      toolStripMenuItem.Enabled = MenuItems != null && MenuItems.Length > 0;
      return toolStripMenuItem;
    }

    public static ToolStripItem[] CreateEditMenuItems(
      ToolStripItem[] DiscMenuItems,
      ToolStripItem[] SelectionMenuItems)
    {
      return new ToolStripItem[3]
      {
        (ToolStripItem) MenuItemFactory.CreateEditSelectionTopMenuItem(SelectionMenuItems),
        (ToolStripItem) new ToolStripSeparator(),
        (ToolStripItem) MenuItemFactory.CreateEditDiscTopMenuItem(DiscMenuItems)
      };
    }

    public ToolStripItem[] CreateSelectionMenuItemsForTreeView(object NewSelection)
    {
      ToolStripItem[] toolStripItemArray = new ToolStripItem[0];
      switch (NewSelection)
      {
        case IDiscFileSystem _:
          return this.CreateDiscMenuItems(NewSelection as IDiscFileSystem);
        case IDiscSession _:
          return this.CreateDiscSessionMenuItems(NewSelection as IDiscSession);
        case IDiscTrack _:
          return this.CreateDiscTrackMenuItems(NewSelection as IDiscTrack);
        case InitialProgramExtended _:
          return this.CreateInitialProgramMenuItems(NewSelection as InitialProgramExtended);
        case DirectoryRecord _:
          DirectoryRecord directoryRecord = NewSelection as DirectoryRecord;
          return this.CreateFileExtractContentMenuItems(directoryRecord.Name, (object) directoryRecord.SubDirectories);
        default:
          throw new NotSupportedException();
      }
    }
  }
}
