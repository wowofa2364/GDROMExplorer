﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.DiscView.ExplorerTreeNodeFactory
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using ImageReader.ISO9660.DirectoryRecords;
using SEGATools.Binary;
using SEGATools.DiscFileSystem;
using SEGATools.Security;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace GDRomExplorer.DiscView
{
  public class ExplorerTreeNodeFactory
  {
    public static TreeNode CreateTree(IDiscFileSystem Disc)
    {
      TreeNode treeNode1 = ExplorerTreeNodeFactory.CreateTreeNode(Disc);
      foreach (IDiscSession session in Disc.Sessions)
      {
        TreeNode treeNode2 = ExplorerTreeNodeFactory.CreateTreeNode(session);
        treeNode1.Nodes.Add(treeNode2);
        foreach (IDiscTrack track in session.Tracks)
        {
          TreeNode treeNode3 = ExplorerTreeNodeFactory.CreateTreeNode(track);
          treeNode2.Nodes.Add(treeNode3);
          if (track == session.FirstDataTrack)
          {
            if (session.BootStrap != null)
            {
              TreeNode treeNode4 = ExplorerTreeNodeFactory.CreateTreeNode(session.BootStrap, Disc);
              treeNode3.Nodes.Add(treeNode4);
            }
            if (session.RootDirectoryRecord != null)
            {
              string identifier = session.PrimaryVolumeDescriptor.Identifier;
              TreeNode discFileSystemTree = ExplorerTreeNodeFactory.CreateDiscFileSystemTree(session.RootDirectoryRecord, identifier);
              treeNode3.Nodes.Add(discFileSystemTree);
            }
          }
        }
      }
      return treeNode1;
    }

    private static TreeNode CreateDiscFileSystemTree(
      DirectoryRecord RootDirectoryRecord,
      string RootDirectoryName)
    {
      TreeNode treeNode = ExplorerTreeNodeFactory.CreateTreeNode(RootDirectoryRecord, RootDirectoryName, 5, 5);
      ExplorerTreeNodeFactory.CreateDiscFileSystemTreeRec(treeNode);
      return treeNode;
    }

    private static void CreateDiscFileSystemTreeRec(TreeNode ParentNode)
    {
      foreach (DirectoryRecord DirectoryRecord in (IEnumerable<DirectoryRecord>) (ParentNode.Tag as DirectoryRecord).SubDirectories.FindAll((Predicate<DirectoryRecord>) (directoryRecord => directoryRecord.IsDirectory)))
      {
        TreeNode treeNode = ExplorerTreeNodeFactory.CreateTreeNode(DirectoryRecord, 6, 7);
        ParentNode.Nodes.Add(treeNode);
        ExplorerTreeNodeFactory.CreateDiscFileSystemTreeRec(treeNode);
      }
    }

    private static TreeNode CreateTreeNode(
      DirectoryRecord DirectoryRecord,
      int ImageIndex,
      int SelectedImageIndex)
    {
      return ExplorerTreeNodeFactory.CreateTreeNode(DirectoryRecord, DirectoryRecord.Name, ImageIndex, SelectedImageIndex);
    }

    private static TreeNode CreateTreeNode(
      DirectoryRecord DirectoryRecord,
      string Name,
      int ImageIndex,
      int SelectedImageIndex)
    {
      return new TreeNode(Name, ImageIndex, SelectedImageIndex)
      {
        Name = Name,
        Tag = (object) DirectoryRecord,
        ToolTipText = ExplorerToolTipFactory.CreateToolTipText(DirectoryRecord)
      };
    }

    private static TreeNode CreateTreeNode(
      InitialProgram InitialProgram,
      IDiscFileSystem Disc)
    {
      List<SEGALibrary> segaLibraries = Disc.GetSEGALibraries((object) InitialProgram);
      InitialProgramExtended initialProgramExtended = InitialProgramExtended.create(InitialProgram, segaLibraries);
      return new TreeNode(initialProgramExtended.FileName, 4, 4)
      {
        Name = initialProgramExtended.FileName,
        Tag = (object) initialProgramExtended
      };
    }

    private static TreeNode CreateTreeNode(IDiscTrack DiscTrack)
    {
      TreeNode treeNode = new TreeNode(DiscTrack.ToString());
      switch (DiscTrack.TrackData)
      {
        case TrackModeType.Audio:
          treeNode.ImageIndex = 3;
          treeNode.ToolTipText = string.Format("CD-DA ({0})", (object) DiscTrack.Name);
          break;
        case TrackModeType.Data:
          treeNode.ImageIndex = 2;
          treeNode.ToolTipText = string.Format("Data ({0})", (object) DiscTrack.Name);
          break;
      }
      treeNode.Name = DiscTrack.Name;
      treeNode.Tag = (object) DiscTrack;
      treeNode.SelectedImageIndex = treeNode.ImageIndex;
      return treeNode;
    }

    private static TreeNode CreateTreeNode(IDiscSession DiscSession) => new TreeNode(DiscSession.Name, 1, 1)
    {
      Name = DiscSession.Name,
      Tag = (object) DiscSession
    };

    private static TreeNode CreateTreeNode(IDiscFileSystem Disc) => new TreeNode(Disc.DiscName, 0, 0)
    {
      Name = Disc.DiscName,
      Tag = (object) Disc
    };
  }
}
