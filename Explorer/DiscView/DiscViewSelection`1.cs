﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.DiscView.DiscViewSelection`1
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using System.Windows.Forms;

namespace GDRomExplorer.DiscView
{
  public class DiscViewSelection<Selection> : IDiscViewSelection
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private DiscViewSelection<Selection>.SelectionHandler<Selection> AfterSelectHandler;
    private DiscViewSelection<Selection>.SelectionHandler<Selection> RightClickHandler;
    private DiscViewSelection<Selection>.SelectionHandler<Selection> LeftClickHandler;
    private DiscViewSelection<Selection>.SelectionHandler<Selection> DoubleLeftClickHandler;
    private Selection Handle;

    private DiscViewSelection(
      DiscViewSelection<Selection>.SelectionHandler<Selection> AfterSelectHandler,
      DiscViewSelection<Selection>.SelectionHandler<Selection> RightClickHandler,
      DiscViewSelection<Selection>.SelectionHandler<Selection> LeftClickHandler,
      DiscViewSelection<Selection>.SelectionHandler<Selection> DoubleLeftClickHandler,
      Selection Handle)
    {
      this.AfterSelectHandler = AfterSelectHandler;
      this.RightClickHandler = RightClickHandler;
      this.LeftClickHandler = LeftClickHandler;
      this.DoubleLeftClickHandler = DoubleLeftClickHandler;
      this.Handle = Handle;
    }

    public static DiscViewSelection<Selection> NoHandler(Selection Handle) => new DiscViewSelection<Selection>((DiscViewSelection<Selection>.SelectionHandler<Selection>) null, (DiscViewSelection<Selection>.SelectionHandler<Selection>) null, (DiscViewSelection<Selection>.SelectionHandler<Selection>) null, (DiscViewSelection<Selection>.SelectionHandler<Selection>) null, Handle);

    public static DiscViewSelection<Selection> NewHandlersForTreeView(
      DiscViewSelection<Selection>.SelectionHandler<Selection> AfterSelectHandler,
      DiscViewSelection<Selection>.SelectionHandler<Selection> RightClickHandler,
      DiscViewSelection<Selection>.SelectionHandler<Selection> DoubleLeftClickHandler,
      Selection Handle)
    {
      return new DiscViewSelection<Selection>(AfterSelectHandler, RightClickHandler, (DiscViewSelection<Selection>.SelectionHandler<Selection>) null, DoubleLeftClickHandler, Handle);
    }

    public static DiscViewSelection<Selection> NewHandlersForListView(
      DiscViewSelection<Selection>.SelectionHandler<Selection> RightClickHandler,
      DiscViewSelection<Selection>.SelectionHandler<Selection> LeftClickHandler,
      DiscViewSelection<Selection>.SelectionHandler<Selection> DoubleLeftClickHandler,
      Selection Handle)
    {
      return new DiscViewSelection<Selection>((DiscViewSelection<Selection>.SelectionHandler<Selection>) null, RightClickHandler, LeftClickHandler, DoubleLeftClickHandler, Handle);
    }

    public void HandleAfterSelect()
    {
      if (this.AfterSelectHandler == null)
        return;
      this.AfterSelectHandler(this.Handle);
    }

    public void HandleLeftClick()
    {
      if (this.LeftClickHandler == null)
        return;
      this.LeftClickHandler(this.Handle);
    }

    public void HandleDoubleLeftClicks()
    {
      if (this.DoubleLeftClickHandler == null)
        return;
      this.DoubleLeftClickHandler(this.Handle);
    }

    public void HandleRightClick()
    {
      if (this.RightClickHandler == null)
        return;
      this.RightClickHandler(this.Handle);
    }

    public void HandleMouseEvent(MouseEventArgs MouseEvent)
    {
      switch (MouseEvent.Button)
      {
        case MouseButtons.Left:
          if (MouseEvent.Clicks == 1)
          {
            this.HandleLeftClick();
            return;
          }
          if (MouseEvent.Clicks == 2)
          {
            this.HandleDoubleLeftClicks();
            return;
          }
          break;
        case MouseButtons.Right:
          this.HandleRightClick();
          return;
      }
      DiscViewSelection<Selection>.logger.DebugFormat("No handler for {0} with {1} click(s) mouse button(s) {2}", (object) this.Handle, (object) MouseEvent.Clicks, (object) MouseEvent.Button);
    }

    public delegate void SelectionHandler<HandleType>(HandleType handle);
  }
}
