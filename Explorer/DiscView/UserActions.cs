﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.DiscView.UserActions
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using System;
using System.Collections.Generic;

namespace GDRomExplorer.DiscView
{
  public class UserActions
  {
    private Dictionary<UserActions.Action, EventHandler> EventHandlers = new Dictionary<UserActions.Action, EventHandler>();

    public void SetHandler(UserActions.Action Action, EventHandler Handler) => this.EventHandlers.Add(Action, Handler);

    public EventHandler GetHandler(UserActions.Action Action) => this.EventHandlers.ContainsKey(Action) ? this.EventHandlers[Action] : throw new NotSupportedException("Action not supported");

    public enum Action
    {
      ExportForGDEmu,
      CreateCueSheet,
      ShowGDDAConversion,
      ShowBootSector,
      ExtractBootSector,
      ExtractItem,
      CreateSortFile,
      ShowPrimaryVolumeDescriptor,
      ConvertGDDA,
      DecryptNaomiBinary,
    }
  }
}
