﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.DiscView.DiscTreeViewSelectionHandlerFactory
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.UserControls;
using ImageReader.ISO9660.DirectoryRecords;
using SEGATools.DiscFileSystem;
using SEGATools.Security;
using System.Windows.Forms;

namespace GDRomExplorer.DiscView
{
  public class DiscTreeViewSelectionHandlerFactory : DiscSelectionHandlerFactory<TreeView>
  {
    public DiscTreeViewSelectionHandlerFactory(DiscViewExplorer Context, TreeView Control)
      : base(Context, Control)
    {
    }

    private DiscViewSelection<IDiscFileSystem> newSelectionHandlers(
      IDiscFileSystem discFileSystem)
    {
      return DiscViewSelection<IDiscFileSystem>.NewHandlersForTreeView(new DiscViewSelection<IDiscFileSystem>.SelectionHandler<IDiscFileSystem>(this.Context.Disc_AfterSelectHandler), new DiscViewSelection<IDiscFileSystem>.SelectionHandler<IDiscFileSystem>(this.Context.Disc_MouseRightClickHandler), (DiscViewSelection<IDiscFileSystem>.SelectionHandler<IDiscFileSystem>) null, discFileSystem);
    }

    private DiscViewSelection<DirectoryRecord> newSelectionHandlers(
      DirectoryRecord DirectoryRecord)
    {
      return DiscViewSelection<DirectoryRecord>.NewHandlersForTreeView(new DiscViewSelection<DirectoryRecord>.SelectionHandler<DirectoryRecord>(this.Context.DirectoryRecord_AfterSelectClickHandler), new DiscViewSelection<DirectoryRecord>.SelectionHandler<DirectoryRecord>(this.Context.DirectoryRecord_MouseRightClickHandler), (DiscViewSelection<DirectoryRecord>.SelectionHandler<DirectoryRecord>) null, DirectoryRecord);
    }

    private DiscViewSelection<InitialProgramExtended> newSelectionHandlers(
      InitialProgramExtended InitialProgram)
    {
      return DiscViewSelection<InitialProgramExtended>.NewHandlersForTreeView(new DiscViewSelection<InitialProgramExtended>.SelectionHandler<InitialProgramExtended>(this.Context.InitialProgram_AfterSelectHandler), new DiscViewSelection<InitialProgramExtended>.SelectionHandler<InitialProgramExtended>(this.Context.InitialProgram_MouseRightClickHandler), new DiscViewSelection<InitialProgramExtended>.SelectionHandler<InitialProgramExtended>(this.Context.InitialProgram_MouseDoubleLeftClicksHandler), InitialProgram);
    }

    private DiscViewSelection<IDiscSession> newSelectionHandlers(
      IDiscSession DiscSession)
    {
      return DiscViewSelection<IDiscSession>.NewHandlersForTreeView(new DiscViewSelection<IDiscSession>.SelectionHandler<IDiscSession>(this.Context.DiscSession_AfterSelectHandler), new DiscViewSelection<IDiscSession>.SelectionHandler<IDiscSession>(this.Context.DiscSession_MouseRightClickHandler), (DiscViewSelection<IDiscSession>.SelectionHandler<IDiscSession>) null, DiscSession);
    }

    private DiscViewSelection<IDiscTrack> newSelectionHandlers(
      IDiscTrack DiscTrack)
    {
      return DiscViewSelection<IDiscTrack>.NewHandlersForTreeView(new DiscViewSelection<IDiscTrack>.SelectionHandler<IDiscTrack>(this.Context.DiscTrack_AfterSelectHandler), new DiscViewSelection<IDiscTrack>.SelectionHandler<IDiscTrack>(this.Context.DiscTrack_MouseRightClickHandler), (DiscViewSelection<IDiscTrack>.SelectionHandler<IDiscTrack>) null, DiscTrack);
    }

    public override IDiscViewSelection newSelectionHandlers(object Handle)
    {
      switch (Handle)
      {
        case IDiscFileSystem _:
          return (IDiscViewSelection) this.newSelectionHandlers(Handle as IDiscFileSystem);
        case IDiscSession _:
          return (IDiscViewSelection) this.newSelectionHandlers(Handle as IDiscSession);
        case IDiscTrack _:
          return (IDiscViewSelection) this.newSelectionHandlers(Handle as IDiscTrack);
        case InitialProgramExtended _:
          return (IDiscViewSelection) this.newSelectionHandlers(Handle as InitialProgramExtended);
        case DirectoryRecord _:
          return (IDiscViewSelection) this.newSelectionHandlers(Handle as DirectoryRecord);
        default:
          return (IDiscViewSelection) null;
      }
    }
  }
}
