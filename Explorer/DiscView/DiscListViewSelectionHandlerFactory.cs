﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.DiscView.DiscListViewSelectionHandlerFactory
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.UserControls;
using ImageReader.ISO9660.DirectoryRecords;
using System.Collections.Generic;
using System.Windows.Forms;

namespace GDRomExplorer.DiscView
{
  public class DiscListViewSelectionHandlerFactory : DiscSelectionHandlerFactory<ListView>
  {
    public DiscListViewSelectionHandlerFactory(DiscViewExplorer Context, ListView Control)
      : base(Context, Control)
    {
    }

    private DiscViewSelection<IList<DirectoryRecord>> newSelectionHandlers(
      IList<DirectoryRecord> DirectoryRecords)
    {
      return DiscViewSelection<IList<DirectoryRecord>>.NewHandlersForListView(new DiscViewSelection<IList<DirectoryRecord>>.SelectionHandler<IList<DirectoryRecord>>(this.Context.DirectoryRecords_MouseRightClickHandler), new DiscViewSelection<IList<DirectoryRecord>>.SelectionHandler<IList<DirectoryRecord>>(this.Context.DirectoryRecords_MouseLeftClickHandler), new DiscViewSelection<IList<DirectoryRecord>>.SelectionHandler<IList<DirectoryRecord>>(this.Context.DirectoryRecords_MouseDoubleLeftClicksHandler), DirectoryRecords);
    }

    public override IDiscViewSelection newSelectionHandlers(object Handle) => Handle is IList<DirectoryRecord> ? (IDiscViewSelection) this.newSelectionHandlers(Handle as IList<DirectoryRecord>) : (IDiscViewSelection) null;
  }
}
