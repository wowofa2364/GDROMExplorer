﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.DiscView.IDiscViewSelection
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using System.Windows.Forms;

namespace GDRomExplorer.DiscView
{
  public interface IDiscViewSelection
  {
    void HandleAfterSelect();

    void HandleDoubleLeftClicks();

    void HandleLeftClick();

    void HandleRightClick();

    void HandleMouseEvent(MouseEventArgs MouseEvent);
  }
}
