﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.DiscView.ContextMenuManager
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using System.Windows.Forms;

namespace GDRomExplorer.DiscView
{
  public class ContextMenuManager
  {
    private MenuItemFactory Context;
    private ContextMenuStrip LastDisplayedContextMenu;

    public ContextMenuManager(MenuItemFactory Context) => this.Context = Context;

    public ContextMenuStrip CreateContextMenu(ToolStripItem[] MenuItems)
    {
      ContextMenuStrip contextMenuStrip = new ContextMenuStrip();
      contextMenuStrip.Items.AddRange(MenuItems);
      return contextMenuStrip;
    }

    public ContextMenuStrip CreateAndShowContextMenu(ToolStripItem[] MenuItems)
    {
      this.CloseContextMenuIfAny();
      this.LastDisplayedContextMenu = this.CreateContextMenu(MenuItems);
      this.LastDisplayedContextMenu.Show(Cursor.Position);
      return this.LastDisplayedContextMenu;
    }

    public void CloseContextMenuIfAny()
    {
      if (this.LastDisplayedContextMenu == null)
        return;
      this.LastDisplayedContextMenu.Close();
    }
  }
}
