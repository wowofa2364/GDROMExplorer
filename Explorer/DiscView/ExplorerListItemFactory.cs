﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.DiscView.ExplorerListItemFactory
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.Resources;
using ImageReader.ISO9660.DirectoryRecords;
using SEGATools.DiscFileSystem;
using SEGATools.Formater;
using System.Drawing;
using System.Windows.Forms;

namespace GDRomExplorer.DiscView
{
  public class ExplorerListItemFactory
  {
    public static ListViewItem CreateItem(
      DirectoryRecord DirectoryRecord,
      IDiscFileSystem Disc)
    {
      ListViewItem listViewItem = new ListViewItem(DirectoryRecord.Name, DirectoryRecord.IsDirectory ? 1 : 0);
      listViewItem.Name = DirectoryRecord.Name;
      listViewItem.Tag = (object) DirectoryRecord;
      listViewItem.UseItemStyleForSubItems = true;
      listViewItem.ToolTipText = ExplorerToolTipFactory.CreateToolTipText(DirectoryRecord);
      listViewItem.SubItems.Add(SizeFormater.ToHumanReadableSize((long) DirectoryRecord.ExtentSize));
      listViewItem.SubItems.Add(DirectoryRecord.ExtentSize.ToString());
      listViewItem.SubItems.Add(DirectoryRecord.Extent.ToString());
      if (DirectoryRecord.RecordingDateTime.HasValue)
        listViewItem.SubItems.Add(string.Format("{0:G}", (object) DirectoryRecord.RecordingDateTime));
      else
        listViewItem.SubItems.Add(Strings.InvalidDateTimeText);
      if (!DirectoryRecord.HasValidFileIdentifier)
      {
        listViewItem.BackColor = Color.OrangeRed;
        listViewItem.ForeColor = Color.White;
        listViewItem.ToolTipText = Strings.ToolTipInvalidFileIdentifier;
      }
      else if (Disc.MainBinary != null && Disc.MainBinary.FullPath.Equals(DirectoryRecord.FullPath))
      {
        listViewItem.Font = new Font(listViewItem.Font, FontStyle.Bold);
        listViewItem.ToolTipText = string.Format(Strings.ToolTipMainBinaryWithFormat, (object) DirectoryRecord.Name);
      }
      return listViewItem;
    }

    public static ListViewItem CreateParentDirectoryItem(DirectoryRecord DirectoryRecord) => new ListViewItem(DirectoryRecord.PARENT_DIRECTORY_NAME, 2)
    {
      Name = DirectoryRecord.PARENT_DIRECTORY_NAME,
      Tag = (object) DirectoryRecord
    };
  }
}
