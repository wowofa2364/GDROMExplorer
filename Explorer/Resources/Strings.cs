﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Resources.Strings
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace GDRomExplorer.Resources
{
  [DebuggerNonUserCode]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [CompilerGenerated]
  internal class Strings
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Strings()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) Strings.resourceMan, (object) null))
          Strings.resourceMan = new ResourceManager("GDRomExplorer.Resources.Strings", typeof (Strings).Assembly);
        return Strings.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get => Strings.resourceCulture;
      set => Strings.resourceCulture = value;
    }

    internal static string ContextMenuItemConvertAudio => Strings.ResourceManager.GetString(nameof (ContextMenuItemConvertAudio), Strings.resourceCulture);

    internal static string ContextMenuItemConvertGDDA => Strings.ResourceManager.GetString(nameof (ContextMenuItemConvertGDDA), Strings.resourceCulture);

    internal static string ContextMenuItemCreateCUE => Strings.ResourceManager.GetString(nameof (ContextMenuItemCreateCUE), Strings.resourceCulture);

    internal static string ContextMenuItemDecryptAndExtract => Strings.ResourceManager.GetString(nameof (ContextMenuItemDecryptAndExtract), Strings.resourceCulture);

    internal static string ContextMenuItemExportForGDEmu => Strings.ResourceManager.GetString(nameof (ContextMenuItemExportForGDEmu), Strings.resourceCulture);

    internal static string ContextMenuItemExtract => Strings.ResourceManager.GetString(nameof (ContextMenuItemExtract), Strings.resourceCulture);

    internal static string ContextMenuItemExtractContent => Strings.ResourceManager.GetString(nameof (ContextMenuItemExtractContent), Strings.resourceCulture);

    internal static string ContextMenuItemExtractIP => Strings.ResourceManager.GetString(nameof (ContextMenuItemExtractIP), Strings.resourceCulture);

    internal static string ContextMenuItemGenerateSortFile => Strings.ResourceManager.GetString(nameof (ContextMenuItemGenerateSortFile), Strings.resourceCulture);

    internal static string ContextMenuItemViewIP => Strings.ResourceManager.GetString(nameof (ContextMenuItemViewIP), Strings.resourceCulture);

    internal static string ContextMenuItemViewPvd => Strings.ResourceManager.GetString(nameof (ContextMenuItemViewPvd), Strings.resourceCulture);

    internal static string DESDecryptorSubTitle => Strings.ResourceManager.GetString(nameof (DESDecryptorSubTitle), Strings.resourceCulture);

    internal static string DESDecryptorTitle => Strings.ResourceManager.GetString(nameof (DESDecryptorTitle), Strings.resourceCulture);

    internal static string DESDescriptorTypeName => Strings.ResourceManager.GetString(nameof (DESDescriptorTypeName), Strings.resourceCulture);

    internal static string DESEncryptorSubTitle => Strings.ResourceManager.GetString(nameof (DESEncryptorSubTitle), Strings.resourceCulture);

    internal static string DESEncryptorTitle => Strings.ResourceManager.GetString(nameof (DESEncryptorTitle), Strings.resourceCulture);

    internal static string DESEncryptorTypeName => Strings.ResourceManager.GetString(nameof (DESEncryptorTypeName), Strings.resourceCulture);

    internal static string DESKeyFormButtonLabelDecrypt => Strings.ResourceManager.GetString(nameof (DESKeyFormButtonLabelDecrypt), Strings.resourceCulture);

    internal static string DESKeyFormButtonLabelEncrypt => Strings.ResourceManager.GetString(nameof (DESKeyFormButtonLabelEncrypt), Strings.resourceCulture);

    internal static string DESKeyFormButtonLabelExtractAndDecrypt => Strings.ResourceManager.GetString(nameof (DESKeyFormButtonLabelExtractAndDecrypt), Strings.resourceCulture);

    internal static string DiscViewExplorerGroupLabelWithFormat => Strings.ResourceManager.GetString(nameof (DiscViewExplorerGroupLabelWithFormat), Strings.resourceCulture);

    internal static string DiscViewListColumnFileName => Strings.ResourceManager.GetString(nameof (DiscViewListColumnFileName), Strings.resourceCulture);

    internal static string DiscViewListColumnLBA => Strings.ResourceManager.GetString(nameof (DiscViewListColumnLBA), Strings.resourceCulture);

    internal static string DiscViewListColumnModifiedDate => Strings.ResourceManager.GetString(nameof (DiscViewListColumnModifiedDate), Strings.resourceCulture);

    internal static string DiscViewListColumnSize => Strings.ResourceManager.GetString(nameof (DiscViewListColumnSize), Strings.resourceCulture);

    internal static string DiscViewListColumnSizeInBytes => Strings.ResourceManager.GetString(nameof (DiscViewListColumnSizeInBytes), Strings.resourceCulture);

    internal static string DiscViewNoDiscTitle => Strings.ResourceManager.GetString(nameof (DiscViewNoDiscTitle), Strings.resourceCulture);

    internal static string DiscViewOpenerButtonClose => Strings.ResourceManager.GetString(nameof (DiscViewOpenerButtonClose), Strings.resourceCulture);

    internal static string DiscViewOpenerButtonOpen => Strings.ResourceManager.GetString(nameof (DiscViewOpenerButtonOpen), Strings.resourceCulture);

    internal static string DiscViewOpenerButtonToolStripText => Strings.ResourceManager.GetString(nameof (DiscViewOpenerButtonToolStripText), Strings.resourceCulture);

    internal static string DiscViewOpenerFileDialogTitle => Strings.ResourceManager.GetString(nameof (DiscViewOpenerFileDialogTitle), Strings.resourceCulture);

    internal static string DiscViewOpenerImageLoadingMessage => Strings.ResourceManager.GetString(nameof (DiscViewOpenerImageLoadingMessage), Strings.resourceCulture);

    internal static string DiscViewOpenerImageLoadingTitle => Strings.ResourceManager.GetString(nameof (DiscViewOpenerImageLoadingTitle), Strings.resourceCulture);

    internal static string DiscViewOpenerInvalidInitialProgram => Strings.ResourceManager.GetString(nameof (DiscViewOpenerInvalidInitialProgram), Strings.resourceCulture);

    internal static string DiscViewOpenerLabel => Strings.ResourceManager.GetString(nameof (DiscViewOpenerLabel), Strings.resourceCulture);

    internal static string DiscViewOpenerNoFileSystemError => Strings.ResourceManager.GetString(nameof (DiscViewOpenerNoFileSystemError), Strings.resourceCulture);

    internal static string DiscViewOpenerUnknownError => Strings.ResourceManager.GetString(nameof (DiscViewOpenerUnknownError), Strings.resourceCulture);

    internal static string EditDiscMenuItemName => Strings.ResourceManager.GetString(nameof (EditDiscMenuItemName), Strings.resourceCulture);

    internal static string EditSelectionMenuItemName => Strings.ResourceManager.GetString(nameof (EditSelectionMenuItemName), Strings.resourceCulture);

    internal static string FbdConvertGddaTitle => Strings.ResourceManager.GetString(nameof (FbdConvertGddaTitle), Strings.resourceCulture);

    internal static string FbdExtractFilesTitle => Strings.ResourceManager.GetString(nameof (FbdExtractFilesTitle), Strings.resourceCulture);

    internal static string FileExtractorSubTitle => Strings.ResourceManager.GetString(nameof (FileExtractorSubTitle), Strings.resourceCulture);

    internal static string FileExtractorTitle => Strings.ResourceManager.GetString(nameof (FileExtractorTitle), Strings.resourceCulture);

    internal static string FileExtractorTypeName => Strings.ResourceManager.GetString(nameof (FileExtractorTypeName), Strings.resourceCulture);

    internal static string FileScannerSubTitle => Strings.ResourceManager.GetString(nameof (FileScannerSubTitle), Strings.resourceCulture);

    internal static string FileScannerTitle => Strings.ResourceManager.GetString(nameof (FileScannerTitle), Strings.resourceCulture);

    internal static string FileScannerTypeName => Strings.ResourceManager.GetString(nameof (FileScannerTypeName), Strings.resourceCulture);

    internal static string GDEmuExporterBinaryPatcherHint => Strings.ResourceManager.GetString(nameof (GDEmuExporterBinaryPatcherHint), Strings.resourceCulture);

    internal static string GDEmuExporterDiscSectorEncoderHint => Strings.ResourceManager.GetString(nameof (GDEmuExporterDiscSectorEncoderHint), Strings.resourceCulture);

    internal static string GDEmuExporterFileConflictQuestionContent => Strings.ResourceManager.GetString(nameof (GDEmuExporterFileConflictQuestionContent), Strings.resourceCulture);

    internal static string GDEmuExporterFileConflictQuestionTitle => Strings.ResourceManager.GetString(nameof (GDEmuExporterFileConflictQuestionTitle), Strings.resourceCulture);

    internal static string GDEmuExporterSubTitle => Strings.ResourceManager.GetString(nameof (GDEmuExporterSubTitle), Strings.resourceCulture);

    internal static string GDEmuExporterTitle => Strings.ResourceManager.GetString(nameof (GDEmuExporterTitle), Strings.resourceCulture);

    internal static string GDEmuExporterTypeName => Strings.ResourceManager.GetString(nameof (GDEmuExporterTypeName), Strings.resourceCulture);

    internal static string GoToBlogLinkTitle => Strings.ResourceManager.GetString(nameof (GoToBlogLinkTitle), Strings.resourceCulture);

    internal static string InitialProgramLoadingMessage => Strings.ResourceManager.GetString(nameof (InitialProgramLoadingMessage), Strings.resourceCulture);

    internal static string InitialProgramLoadingTitle => Strings.ResourceManager.GetString(nameof (InitialProgramLoadingTitle), Strings.resourceCulture);

    internal static string InvalidDateTimeText => Strings.ResourceManager.GetString(nameof (InvalidDateTimeText), Strings.resourceCulture);

    internal static string MsgBoxAudioConverterError => Strings.ResourceManager.GetString(nameof (MsgBoxAudioConverterError), Strings.resourceCulture);

    internal static string MsgBoxAudioConverterNoTrackWarning => Strings.ResourceManager.GetString(nameof (MsgBoxAudioConverterNoTrackWarning), Strings.resourceCulture);

    internal static string MsgBoxAudioConverterTitle => Strings.ResourceManager.GetString(nameof (MsgBoxAudioConverterTitle), Strings.resourceCulture);

    internal static string MsgBoxCueCreatorError => Strings.ResourceManager.GetString(nameof (MsgBoxCueCreatorError), Strings.resourceCulture);

    internal static string MsgBoxCueCreatorSuccess => Strings.ResourceManager.GetString(nameof (MsgBoxCueCreatorSuccess), Strings.resourceCulture);

    internal static string MsgBoxCueCreatorTitle => Strings.ResourceManager.GetString(nameof (MsgBoxCueCreatorTitle), Strings.resourceCulture);

    internal static string MsgBoxGDEmuSettingsImageMissing => Strings.ResourceManager.GetString(nameof (MsgBoxGDEmuSettingsImageMissing), Strings.resourceCulture);

    internal static string MsgBoxGDEmuSettingsOutputPathMissing => Strings.ResourceManager.GetString(nameof (MsgBoxGDEmuSettingsOutputPathMissing), Strings.resourceCulture);

    internal static string MsgBoxImageOpenerBadFileExtensionErrorWithFormat => Strings.ResourceManager.GetString(nameof (MsgBoxImageOpenerBadFileExtensionErrorWithFormat), Strings.resourceCulture);

    internal static string MsgBoxImageOpenerFileNotFoundErrorWithFormat => Strings.ResourceManager.GetString(nameof (MsgBoxImageOpenerFileNotFoundErrorWithFormat), Strings.resourceCulture);

    internal static string MsgBoxImageOpenerTitle => Strings.ResourceManager.GetString(nameof (MsgBoxImageOpenerTitle), Strings.resourceCulture);

    internal static string MsgBoxInitialProgramExtractionErrorWithFormat => Strings.ResourceManager.GetString(nameof (MsgBoxInitialProgramExtractionErrorWithFormat), Strings.resourceCulture);

    internal static string MsgBoxInitialProgramExtractionSuccessWithFormat => Strings.ResourceManager.GetString(nameof (MsgBoxInitialProgramExtractionSuccessWithFormat), Strings.resourceCulture);

    internal static string MsgBoxInitialProgramExtractionTitle => Strings.ResourceManager.GetString(nameof (MsgBoxInitialProgramExtractionTitle), Strings.resourceCulture);

    internal static string MsgBoxInitialProgramOpenerErrorWithFormat => Strings.ResourceManager.GetString(nameof (MsgBoxInitialProgramOpenerErrorWithFormat), Strings.resourceCulture);

    internal static string MsgBoxInitialProgramOpenerSuccessWithFormat => Strings.ResourceManager.GetString(nameof (MsgBoxInitialProgramOpenerSuccessWithFormat), Strings.resourceCulture);

    internal static string MsgBoxInitialProgramOpenerTitle => Strings.ResourceManager.GetString(nameof (MsgBoxInitialProgramOpenerTitle), Strings.resourceCulture);

    internal static string MsgBoxSortFileCreationError => Strings.ResourceManager.GetString(nameof (MsgBoxSortFileCreationError), Strings.resourceCulture);

    internal static string MsgBoxSortFileCreationSuccess => Strings.ResourceManager.GetString(nameof (MsgBoxSortFileCreationSuccess), Strings.resourceCulture);

    internal static string MsgBoxSortFileCreationTitle => Strings.ResourceManager.GetString(nameof (MsgBoxSortFileCreationTitle), Strings.resourceCulture);

    internal static string MsgBoxToolStripMenuSelectedFiles => Strings.ResourceManager.GetString(nameof (MsgBoxToolStripMenuSelectedFiles), Strings.resourceCulture);

    internal static string NaomiEncryptDecryptToolOfdDecFilter => Strings.ResourceManager.GetString(nameof (NaomiEncryptDecryptToolOfdDecFilter), Strings.resourceCulture);

    internal static string NaomiEncryptDecryptToolOfdDecTitle => Strings.ResourceManager.GetString(nameof (NaomiEncryptDecryptToolOfdDecTitle), Strings.resourceCulture);

    internal static string NaomiEncryptDecryptToolOfdEncFilter => Strings.ResourceManager.GetString(nameof (NaomiEncryptDecryptToolOfdEncFilter), Strings.resourceCulture);

    internal static string NaomiEncryptDecryptToolOfdEncTitle => Strings.ResourceManager.GetString(nameof (NaomiEncryptDecryptToolOfdEncTitle), Strings.resourceCulture);

    internal static string NaomiEncryptDecryptToolSfdDecFilter => Strings.ResourceManager.GetString(nameof (NaomiEncryptDecryptToolSfdDecFilter), Strings.resourceCulture);

    internal static string NaomiEncryptDecryptToolSfdDecTitle => Strings.ResourceManager.GetString(nameof (NaomiEncryptDecryptToolSfdDecTitle), Strings.resourceCulture);

    internal static string NaomiEncryptDecryptToolSfdEncFilter => Strings.ResourceManager.GetString(nameof (NaomiEncryptDecryptToolSfdEncFilter), Strings.resourceCulture);

    internal static string NaomiEncryptDecryptToolSfdEncTitle => Strings.ResourceManager.GetString(nameof (NaomiEncryptDecryptToolSfdEncTitle), Strings.resourceCulture);

    internal static string OfdConvertGddaFilter => Strings.ResourceManager.GetString(nameof (OfdConvertGddaFilter), Strings.resourceCulture);

    internal static string OfdConvertGddaTitle => Strings.ResourceManager.GetString(nameof (OfdConvertGddaTitle), Strings.resourceCulture);

    internal static string OfdInitialProgramFilter => Strings.ResourceManager.GetString(nameof (OfdInitialProgramFilter), Strings.resourceCulture);

    internal static string OfdInitialProgramTitle => Strings.ResourceManager.GetString(nameof (OfdInitialProgramTitle), Strings.resourceCulture);

    internal static string Raw2WavConverterSubTitle => Strings.ResourceManager.GetString(nameof (Raw2WavConverterSubTitle), Strings.resourceCulture);

    internal static string Raw2WavConverterTitle => Strings.ResourceManager.GetString(nameof (Raw2WavConverterTitle), Strings.resourceCulture);

    internal static string Raw2WavConverterTypeName => Strings.ResourceManager.GetString(nameof (Raw2WavConverterTypeName), Strings.resourceCulture);

    internal static string SettingsInfoPathTable => Strings.ResourceManager.GetString(nameof (SettingsInfoPathTable), Strings.resourceCulture);

    internal static string SettingsSaved => Strings.ResourceManager.GetString(nameof (SettingsSaved), Strings.resourceCulture);

    internal static string SfdCddaDefaultFileName => Strings.ResourceManager.GetString(nameof (SfdCddaDefaultFileName), Strings.resourceCulture);

    internal static string SfdCddaFilter => Strings.ResourceManager.GetString(nameof (SfdCddaFilter), Strings.resourceCulture);

    internal static string SfdCddaTitle => Strings.ResourceManager.GetString(nameof (SfdCddaTitle), Strings.resourceCulture);

    internal static string SfdCueSheetFilter => Strings.ResourceManager.GetString(nameof (SfdCueSheetFilter), Strings.resourceCulture);

    internal static string SfdCueSheetTitle => Strings.ResourceManager.GetString(nameof (SfdCueSheetTitle), Strings.resourceCulture);

    internal static string SfdExtractFilter => Strings.ResourceManager.GetString(nameof (SfdExtractFilter), Strings.resourceCulture);

    internal static string SfdExtractTitle => Strings.ResourceManager.GetString(nameof (SfdExtractTitle), Strings.resourceCulture);

    internal static string SfdInitialProgramFilter => Strings.ResourceManager.GetString(nameof (SfdInitialProgramFilter), Strings.resourceCulture);

    internal static string SfdInitialProgramTitle => Strings.ResourceManager.GetString(nameof (SfdInitialProgramTitle), Strings.resourceCulture);

    internal static string SfdSortFileFilter => Strings.ResourceManager.GetString(nameof (SfdSortFileFilter), Strings.resourceCulture);

    internal static string SfdSortFileTitle => Strings.ResourceManager.GetString(nameof (SfdSortFileTitle), Strings.resourceCulture);

    internal static string SortFileOptionsHint => Strings.ResourceManager.GetString(nameof (SortFileOptionsHint), Strings.resourceCulture);

    internal static string StatusLabelImageError => Strings.ResourceManager.GetString(nameof (StatusLabelImageError), Strings.resourceCulture);

    internal static string StatusLabelImageLoaded => Strings.ResourceManager.GetString(nameof (StatusLabelImageLoaded), Strings.resourceCulture);

    internal static string StatusLabelImageNotLoaded => Strings.ResourceManager.GetString(nameof (StatusLabelImageNotLoaded), Strings.resourceCulture);

    internal static string ToolTipDirectories => Strings.ResourceManager.GetString(nameof (ToolTipDirectories), Strings.resourceCulture);

    internal static string ToolTipDirectory => Strings.ResourceManager.GetString(nameof (ToolTipDirectory), Strings.resourceCulture);

    internal static string ToolTipDirectoryContentWithFormat => Strings.ResourceManager.GetString(nameof (ToolTipDirectoryContentWithFormat), Strings.resourceCulture);

    internal static string ToolTipFile => Strings.ResourceManager.GetString(nameof (ToolTipFile), Strings.resourceCulture);

    internal static string ToolTipFiles => Strings.ResourceManager.GetString(nameof (ToolTipFiles), Strings.resourceCulture);

    internal static string ToolTipFileSizeWithFormat => Strings.ResourceManager.GetString(nameof (ToolTipFileSizeWithFormat), Strings.resourceCulture);

    internal static string ToolTipInvalidFileIdentifier => Strings.ResourceManager.GetString(nameof (ToolTipInvalidFileIdentifier), Strings.resourceCulture);

    internal static string ToolTipMainBinaryWithFormat => Strings.ResourceManager.GetString(nameof (ToolTipMainBinaryWithFormat), Strings.resourceCulture);
  }
}
