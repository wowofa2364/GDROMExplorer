﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.AppStatus
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

namespace GDRomExplorer
{
  public class AppStatus
  {
    public static void NotifyNewAppStatus(string message)
    {
      if (AppStatus.OnAppStatusUpdate == null)
        return;
      AppStatus.OnAppStatusUpdate(message);
    }

    public static event AppStatus.AppStatusUpdateEventHandler OnAppStatusUpdate;

    public delegate void AppStatusUpdateEventHandler(string message);
  }
}
