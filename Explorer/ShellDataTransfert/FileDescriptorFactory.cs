﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.ShellDataTransfert.FileDescriptorFactory
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using ImageReader.ISO9660.DirectoryRecords;
using ImageReader.Stream;
using SEGATools.DiscFileSystem;
using SEGATools.Security;
using SEGATools.VirtualFile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace GDRomExplorer.ShellDataTransfert
{
  public class FileDescriptorFactory
  {
    private static readonly string DEFAULT_FILENAME = "no_name";

    public static FileDescriptor createFromVirtualFile(IVirtualFile virtualFile)
    {
      FileDescriptor fileDescriptor = new FileDescriptor();
      System.IO.Stream fileInputStream = virtualFile.FileInputStream;
      fileDescriptor.FileName = virtualFile.VirtualName;
      fileDescriptor.WriteTime = DateTime.Now;
      fileDescriptor.IsDirectory = false;
      fileDescriptor.Data = fileInputStream;
      fileDescriptor.FileSize = fileInputStream.Length;
      return fileDescriptor;
    }

    private static FileDescriptor createFromDirectoryRecord(
      DirectoryRecord directoryRecord,
      IDiscFileSystem discFileSystem,
      string directoryRecordFileName)
    {
      string str = directoryRecordFileName;
      if (string.Empty.Equals(directoryRecordFileName))
        str = FileDescriptorFactory.DEFAULT_FILENAME;
      FileDescriptor fileDescriptor = new FileDescriptor();
      DiscSectorStream forDirectoryRecord = discFileSystem.GetDiscStreamForDirectoryRecord(directoryRecord);
      fileDescriptor.FileName = str;
      fileDescriptor.WriteTime = directoryRecord.RecordingDateTime.HasValue ? directoryRecord.RecordingDateTime.Value : DateTime.Now;
      fileDescriptor.IsDirectory = directoryRecord.IsDirectory;
      fileDescriptor.Data = (System.IO.Stream) forDirectoryRecord;
      fileDescriptor.FileSize = directoryRecord.IsDirectory ? 0L : forDirectoryRecord.Length;
      return fileDescriptor;
    }

    public static FileDescriptor[] createFromDirectoryRecordTree(
      DirectoryRecord directoryRecord,
      IDiscFileSystem discFileSystem)
    {
      List<FileDescriptor> fileDescriptorList = new List<FileDescriptor>();
      int count = 1;
      if (!directoryRecord.IsRoot)
      {
        count = directoryRecord.ParentDirectory.FullPath.Length;
        if (!directoryRecord.ParentDirectory.IsRoot)
          ++count;
        fileDescriptorList.Add(FileDescriptorFactory.createFromDirectoryRecord(directoryRecord, discFileSystem, directoryRecord.Name));
      }
      foreach (DirectoryRecord allSubDirectory in directoryRecord.GetAllSubDirectories())
      {
        string directoryRecordFileName = allSubDirectory.FullPath.Remove(0, count);
        fileDescriptorList.Add(FileDescriptorFactory.createFromDirectoryRecord(allSubDirectory, discFileSystem, directoryRecordFileName));
      }
      return fileDescriptorList.ToArray();
    }

    public static DataObject createDataObject(object Handle, IDiscFileSystem Disc)
    {
      DataObjectEx dataObjectEx = new DataObjectEx();
      FileDescriptor[] FileDescriptors;
      switch (Handle)
      {
        case DirectoryRecord _:
          FileDescriptors = FileDescriptorFactory.createFromDirectoryRecordTree(Handle as DirectoryRecord, Disc);
          break;
        case InitialProgramExtended _:
          FileDescriptors = new FileDescriptor[1]
          {
            FileDescriptorFactory.createFromVirtualFile((IVirtualFile) VirtualFileFactory.createVirtualFile(Handle as InitialProgramExtended))
          };
          break;
        case IVirtualFile _:
          FileDescriptors = new FileDescriptor[1]
          {
            FileDescriptorFactory.createFromVirtualFile(Handle as IVirtualFile)
          };
          break;
        case IList<DirectoryRecord> _:
          IList<DirectoryRecord> directoryRecordList = Handle as IList<DirectoryRecord>;
          List<FileDescriptor> fileDescriptorList = new List<FileDescriptor>(directoryRecordList.Count);
          foreach (DirectoryRecord directoryRecord in (IEnumerable<DirectoryRecord>) directoryRecordList)
          {
            FileDescriptor[] directoryRecordTree = FileDescriptorFactory.createFromDirectoryRecordTree(directoryRecord, Disc);
            fileDescriptorList.AddRange((IEnumerable<FileDescriptor>) ((IEnumerable<FileDescriptor>) directoryRecordTree).ToList<FileDescriptor>());
          }
          FileDescriptors = fileDescriptorList.ToArray();
          break;
        default:
          return (DataObject) null;
      }
      dataObjectEx.SetItems(FileDescriptors);
      return (DataObject) dataObjectEx;
    }
  }
}
