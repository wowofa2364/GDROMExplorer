﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.ShellDataTransfert.NativeMethods
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using System;
using System.Runtime.InteropServices;

namespace GDRomExplorer.ShellDataTransfert
{
  public class NativeMethods
  {
    public const string CFSTR_PREFERREDDROPEFFECT = "Preferred DropEffect";
    public const string CFSTR_PERFORMEDDROPEFFECT = "Performed DropEffect";
    public const string CFSTR_FILEDESCRIPTORW = "FileGroupDescriptorW";
    public const string CFSTR_FILECONTENTS = "FileContents";
    public const int FD_CLSID = 1;
    public const int FD_SIZEPOINT = 2;
    public const int FD_ATTRIBUTES = 4;
    public const int FD_CREATETIME = 8;
    public const int FD_ACCESSTIME = 16;
    public const int FD_WRITESTIME = 32;
    public const int FD_FILESIZE = 64;
    public const int FD_PROGRESSUI = 16384;
    public const int FD_LINKUI = 32768;
    public const uint FILE_ATTRIBUTE_READONLY = 1;
    public const uint FILE_ATTRIBUTE_HIDDEN = 2;
    public const uint FILE_ATTRIBUTE_SYSTEM = 4;
    public const uint FILE_ATTRIBUTE_DIRECTORY = 16;
    public const uint FILE_ATTRIBUTE_ARCHIVE = 32;
    public const uint FILE_ATTRIBUTE_NORMAL = 128;
    public const int DV_E_TYMED = -2147221399;

    [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
    public static extern IntPtr GlobalAlloc(int uFlags, int dwBytes);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
    public static extern IntPtr GlobalFree(HandleRef handle);
  }
}
