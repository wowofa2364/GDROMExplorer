﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.ShellDataTransfert.DataObjectEx
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Permissions;
using System.Windows.Forms;

namespace GDRomExplorer.ShellDataTransfert
{
  public class DataObjectEx : DataObject, System.Runtime.InteropServices.ComTypes.IDataObject
  {
    private static readonly TYMED[] ALLOWED_TYMEDS = new TYMED[5]
    {
      TYMED.TYMED_HGLOBAL,
      TYMED.TYMED_ISTREAM,
      TYMED.TYMED_ENHMF,
      TYMED.TYMED_MFPICT,
      TYMED.TYMED_GDI
    };
    private FileDescriptor[] fileDescriptors;
    private int fileDescriptorIndex;

    public void SetItems(FileDescriptor[] FileDescriptors)
    {
      this.fileDescriptors = FileDescriptors;
      this.SetData("FileGroupDescriptorW", (object) null);
      this.SetData("FileContents", (object) null);
      this.SetData("Performed DropEffect", (object) null);
    }

    public override object GetData(string format, bool autoConvert)
    {
      if (string.Compare(format, "FileGroupDescriptorW", StringComparison.OrdinalIgnoreCase) == 0 && this.fileDescriptors != null)
        this.SetData("FileGroupDescriptorW", (object) this.GetFileDescriptor(this.fileDescriptors));
      else if (string.Compare(format, "FileContents", StringComparison.OrdinalIgnoreCase) == 0)
        this.SetData("FileContents", (object) this.GetFileContents(this.fileDescriptors, this.fileDescriptorIndex));
      else
        string.Compare(format, "Performed DropEffect", StringComparison.OrdinalIgnoreCase);
      return base.GetData(format, autoConvert);
    }

    [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
    void System.Runtime.InteropServices.ComTypes.IDataObject.GetData(
      ref FORMATETC formatetc,
      out STGMEDIUM medium)
    {
      if ((int) formatetc.cfFormat == (int) (short) DataFormats.GetFormat("FileContents").Id)
        this.fileDescriptorIndex = formatetc.lindex;
      medium = new STGMEDIUM();
      if (DataObjectEx.GetTymedUseable(formatetc.tymed))
      {
        if ((formatetc.tymed & TYMED.TYMED_ISTREAM) != TYMED.TYMED_NULL)
        {
          medium.tymed = TYMED.TYMED_ISTREAM;
          medium.unionmember = IntPtr.Zero;
          System.Runtime.InteropServices.ComTypes.IStream fileContents = this.GetFileContents(this.fileDescriptors, this.fileDescriptorIndex);
          if (fileContents == null)
            return;
          medium.unionmember = Marshal.GetComInterfaceForObject((object) fileContents, typeof (System.Runtime.InteropServices.ComTypes.IStream));
        }
        else
        {
          medium.tymed = formatetc.tymed;
          ((System.Runtime.InteropServices.ComTypes.IDataObject) this).GetDataHere(ref formatetc, ref medium);
        }
      }
      else
        Marshal.ThrowExceptionForHR(-2147221399);
    }

    private static bool GetTymedUseable(TYMED tymed)
    {
      for (int index = 0; index < DataObjectEx.ALLOWED_TYMEDS.Length; ++index)
      {
        if ((tymed & DataObjectEx.ALLOWED_TYMEDS[index]) != TYMED.TYMED_NULL)
          return true;
      }
      return false;
    }

    private MemoryStream GetFileDescriptor(FileDescriptor[] FileDescriptors)
    {
      MemoryStream memoryStream = new MemoryStream();
      memoryStream.Write(BitConverter.GetBytes(FileDescriptors.Length), 0, 4);
      DataObjectEx.FILEDESCRIPTOR filedescriptor = new DataObjectEx.FILEDESCRIPTOR();
      foreach (FileDescriptor fileDescriptor in FileDescriptors)
      {
        filedescriptor.cFileName = fileDescriptor.FileName;
        filedescriptor.dwFlags = 16384U;
        filedescriptor.dwFlags |= 68U;
        filedescriptor.dwFlags |= 56U;
        filedescriptor.dwFileAttributes = fileDescriptor.IsDirectory ? 16U : 128U;
        long fileTime = fileDescriptor.WriteTime.ToFileTime();
        System.Runtime.InteropServices.ComTypes.FILETIME filetime = new System.Runtime.InteropServices.ComTypes.FILETIME()
        {
          dwLowDateTime = (int) (fileTime & (long) uint.MaxValue),
          dwHighDateTime = (int) (fileTime >> 32)
        };
        filedescriptor.ftLastWriteTime = filetime;
        filedescriptor.ftCreationTime = filetime;
        filedescriptor.nFileSizeHigh = (uint) (fileDescriptor.FileSize >> 32);
        filedescriptor.nFileSizeLow = (uint) ((ulong) fileDescriptor.FileSize & (ulong) uint.MaxValue);
        int length = Marshal.SizeOf((object) filedescriptor);
        IntPtr num = Marshal.AllocHGlobal(length);
        Marshal.StructureToPtr((object) filedescriptor, num, true);
        byte[] numArray = new byte[length];
        Marshal.Copy(num, numArray, 0, length);
        Marshal.FreeHGlobal(num);
        memoryStream.Write(numArray, 0, numArray.Length);
      }
      return memoryStream;
    }

    private System.Runtime.InteropServices.ComTypes.IStream GetFileContents(
      FileDescriptor[] FileDescriptors,
      int FileNumber)
    {
      return FileNumber >= FileDescriptors.Length || FileDescriptors[FileNumber].Data == null ? (System.Runtime.InteropServices.ComTypes.IStream) null : (System.Runtime.InteropServices.ComTypes.IStream) new StreamWrapper(FileDescriptors[FileNumber].Data, FileDescriptors[FileNumber].FileName);
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    private struct FILEDESCRIPTOR
    {
      public uint dwFlags;
      public Guid clsid;
      public Size sizel;
      public Point pointl;
      public uint dwFileAttributes;
      public System.Runtime.InteropServices.ComTypes.FILETIME ftCreationTime;
      public System.Runtime.InteropServices.ComTypes.FILETIME ftLastAccessTime;
      public System.Runtime.InteropServices.ComTypes.FILETIME ftLastWriteTime;
      public uint nFileSizeHigh;
      public uint nFileSizeLow;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
      public string cFileName;
    }
  }
}
