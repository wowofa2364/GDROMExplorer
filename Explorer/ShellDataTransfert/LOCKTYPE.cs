﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.ShellDataTransfert.LOCKTYPE
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

namespace GDRomExplorer.ShellDataTransfert
{
  public enum LOCKTYPE
  {
    LOCK_WRITE = 1,
    LOCK_EXCLUSIVE = 2,
    LOCK_ONLYONCE = 4,
  }
}
