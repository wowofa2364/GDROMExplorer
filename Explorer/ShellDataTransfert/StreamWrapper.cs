﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.ShellDataTransfert.StreamWrapper
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;

namespace GDRomExplorer.ShellDataTransfert
{
  public class StreamWrapper : IStream
  {
    private Stream fileStream;
    private string fileName;

    public StreamWrapper(Stream fileStream, string fileName)
    {
      this.fileStream = fileStream;
      this.fileName = fileName;
    }

    public void Read(byte[] pv, int cb, IntPtr pcbRead) => Marshal.WriteInt32(pcbRead, this.fileStream.Read(pv, 0, cb));

    public void Seek(long dlibMove, int dwOrigin, IntPtr plibNewPosition) => Marshal.WriteInt32(plibNewPosition, (int) this.fileStream.Seek(dlibMove, (SeekOrigin) dwOrigin));

    public void Clone(out IStream ppstm) => throw new NotImplementedException();

    public void Commit(int grfCommitFlags) => throw new NotImplementedException();

    public void CopyTo(IStream pstm, long cb, IntPtr pcbRead, IntPtr pcbWritten) => throw new NotImplementedException();

    public void LockRegion(long libOffset, long cb, int dwLockType) => throw new NotImplementedException();

    public void Revert() => throw new NotImplementedException();

    public void SetSize(long libNewSize) => throw new NotImplementedException();

    public void Stat(out System.Runtime.InteropServices.ComTypes.STATSTG pstatstg, int grfStatFlag)
    {
      pstatstg = new System.Runtime.InteropServices.ComTypes.STATSTG();
      pstatstg.type = 2;
      pstatstg.cbSize = this.fileStream.Length;
      pstatstg.pwcsName = this.fileName;
    }

    public void UnlockRegion(long libOffset, long cb, int dwLockType) => throw new NotImplementedException();

    public void Write(byte[] pv, int cb, IntPtr pcbWritten) => throw new NotImplementedException();
  }
}
