﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Properties.Settings
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace GDRomExplorer.Properties
{
  [CompilerGenerated]
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "11.0.0.0")]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default => Settings.defaultInstance;

    [DebuggerNonUserCode]
    [DefaultSettingValue("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=982C3EY58BF8U")]
    [ApplicationScopedSetting]
    public string PayPalDonateUrl => (string) this[nameof (PayPalDonateUrl)];

    [ApplicationScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("http://japanese-cake.livejournal.com/")]
    public string BlogUrl => (string) this[nameof (BlogUrl)];

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    public bool ImageReaderComputePathTable
    {
      get => (bool) this[nameof (ImageReaderComputePathTable)];
      set => this[nameof (ImageReaderComputePathTable)] = (object) value;
    }
  }
}
