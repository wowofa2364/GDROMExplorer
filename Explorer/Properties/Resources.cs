﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Properties.Resources
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace GDRomExplorer.Properties
{
  [DebuggerNonUserCode]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [CompilerGenerated]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) GDRomExplorer.Properties.Resources.resourceMan, (object) null))
          GDRomExplorer.Properties.Resources.resourceMan = new ResourceManager("GDRomExplorer.Properties.Resources", typeof (GDRomExplorer.Properties.Resources).Assembly);
        return GDRomExplorer.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get => GDRomExplorer.Properties.Resources.resourceCulture;
      set => GDRomExplorer.Properties.Resources.resourceCulture = value;
    }

    internal static Icon extract_icon => (Icon) GDRomExplorer.Properties.Resources.ResourceManager.GetObject(nameof (extract_icon), GDRomExplorer.Properties.Resources.resourceCulture);

    internal static Icon file => (Icon) GDRomExplorer.Properties.Resources.ResourceManager.GetObject(nameof (file), GDRomExplorer.Properties.Resources.resourceCulture);

    internal static Icon folder_open => (Icon) GDRomExplorer.Properties.Resources.ResourceManager.GetObject(nameof (folder_open), GDRomExplorer.Properties.Resources.resourceCulture);

    internal static Icon folder_up => (Icon) GDRomExplorer.Properties.Resources.ResourceManager.GetObject(nameof (folder_up), GDRomExplorer.Properties.Resources.resourceCulture);

    internal static Icon help => (Icon) GDRomExplorer.Properties.Resources.ResourceManager.GetObject(nameof (help), GDRomExplorer.Properties.Resources.resourceCulture);

    internal static Bitmap paypal_donate => (Bitmap) GDRomExplorer.Properties.Resources.ResourceManager.GetObject(nameof (paypal_donate), GDRomExplorer.Properties.Resources.resourceCulture);

    internal static Icon zoom => (Icon) GDRomExplorer.Properties.Resources.ResourceManager.GetObject(nameof (zoom), GDRomExplorer.Properties.Resources.resourceCulture);
  }
}
