﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Forms.FormSortFileOptions
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.Resources;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace GDRomExplorer.Forms
{
  public class FormSortFileOptions : Form
  {
    protected static string DEFAULT_PREFIX = "data\\";
    protected static int DEFAULT_LOWEST_FILE_WEIGHT = 0;
    private IContainer components;
    private TextBox tbPathPrefix;
    private Label lbDESKey;
    private Button btGenerateSortFile;
    private Button btCancel;
    private Label lbLowestFileWeight;
    private Label lbInfoSortFile;
    private NumericUpDown nudFileWeight;

    public string PathPrefix { get; private set; }

    public int LowestFileWeight { get; private set; }

    public FormSortFileOptions()
    {
      this.InitializeComponent();
      this.PathPrefix = FormSortFileOptions.DEFAULT_PREFIX;
      this.LowestFileWeight = FormSortFileOptions.DEFAULT_LOWEST_FILE_WEIGHT;
    }

    private void btCancel_Click(object sender, EventArgs e) => this.Close();

    private void FormGetSortTxtPrefix_Load(object sender, EventArgs e)
    {
      this.lbInfoSortFile.Text = Strings.SortFileOptionsHint;
      this.nudFileWeight.Value = (Decimal) this.LowestFileWeight;
      this.tbPathPrefix.Text = this.PathPrefix;
      this.tbPathPrefix.Focus();
    }

    private void btGenerateSortFile_Click(object sender, EventArgs e)
    {
      this.PathPrefix = this.tbPathPrefix.Text;
      this.LowestFileWeight = Convert.ToInt32(this.nudFileWeight.Value);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.tbPathPrefix = new TextBox();
      this.lbDESKey = new Label();
      this.btGenerateSortFile = new Button();
      this.btCancel = new Button();
      this.lbLowestFileWeight = new Label();
      this.lbInfoSortFile = new Label();
      this.nudFileWeight = new NumericUpDown();
      this.nudFileWeight.BeginInit();
      this.SuspendLayout();
      this.tbPathPrefix.Location = new Point(78, 6);
      this.tbPathPrefix.MaxLength = 0;
      this.tbPathPrefix.Name = "tbPathPrefix";
      this.tbPathPrefix.Size = new Size(130, 20);
      this.tbPathPrefix.TabIndex = 0;
      this.tbPathPrefix.Text = "data\\";
      this.lbDESKey.AutoSize = true;
      this.lbDESKey.Location = new Point(12, 9);
      this.lbDESKey.Name = "lbDESKey";
      this.lbDESKey.Size = new Size(60, 13);
      this.lbDESKey.TabIndex = 2;
      this.lbDESKey.Text = "Path prefix:";
      this.btGenerateSortFile.DialogResult = DialogResult.OK;
      this.btGenerateSortFile.FlatStyle = FlatStyle.Popup;
      this.btGenerateSortFile.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btGenerateSortFile.ForeColor = Color.FromArgb(248, 48, 0);
      this.btGenerateSortFile.Location = new Point(250, 102);
      this.btGenerateSortFile.Name = "btGenerateSortFile";
      this.btGenerateSortFile.Size = new Size((int) sbyte.MaxValue, 22);
      this.btGenerateSortFile.TabIndex = 1;
      this.btGenerateSortFile.Text = "&Generate sort file";
      this.btGenerateSortFile.UseVisualStyleBackColor = false;
      this.btGenerateSortFile.Click += new EventHandler(this.btGenerateSortFile_Click);
      this.btCancel.DialogResult = DialogResult.Cancel;
      this.btCancel.FlatStyle = FlatStyle.Popup;
      this.btCancel.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btCancel.ForeColor = Color.FromArgb(248, 48, 0);
      this.btCancel.Location = new Point(15, 102);
      this.btCancel.Name = "btCancel";
      this.btCancel.Size = new Size((int) sbyte.MaxValue, 22);
      this.btCancel.TabIndex = 2;
      this.btCancel.Text = "&Cancel";
      this.btCancel.UseVisualStyleBackColor = false;
      this.btCancel.Click += new EventHandler(this.btCancel_Click);
      this.lbLowestFileWeight.AutoSize = true;
      this.lbLowestFileWeight.Location = new Point(214, 9);
      this.lbLowestFileWeight.Name = "lbLowestFileWeight";
      this.lbLowestFileWeight.Size = new Size(94, 13);
      this.lbLowestFileWeight.TabIndex = 3;
      this.lbLowestFileWeight.Text = "Lowest file weight:";
      this.lbInfoSortFile.Location = new Point(12, 35);
      this.lbInfoSortFile.Name = "lbInfoSortFile";
      this.lbInfoSortFile.Size = new Size(364, 64);
      this.lbInfoSortFile.TabIndex = 0;
      this.lbInfoSortFile.Text = "Info sort file";
      this.nudFileWeight.Location = new Point(316, 7);
      this.nudFileWeight.Maximum = new Decimal(new int[4]
      {
        int.MaxValue,
        0,
        0,
        0
      });
      this.nudFileWeight.Name = "nudFileWeight";
      this.nudFileWeight.Size = new Size(60, 20);
      this.nudFileWeight.TabIndex = 1;
      this.nudFileWeight.TextAlign = HorizontalAlignment.Center;
      this.nudFileWeight.ThousandsSeparator = true;
      this.AcceptButton = (IButtonControl) this.btGenerateSortFile;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = SystemColors.Window;
      this.CancelButton = (IButtonControl) this.btCancel;
      this.ClientSize = new Size(389, 136);
      this.Controls.Add((Control) this.nudFileWeight);
      this.Controls.Add((Control) this.lbInfoSortFile);
      this.Controls.Add((Control) this.lbLowestFileWeight);
      this.Controls.Add((Control) this.btCancel);
      this.Controls.Add((Control) this.btGenerateSortFile);
      this.Controls.Add((Control) this.tbPathPrefix);
      this.Controls.Add((Control) this.lbDESKey);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (FormSortFileOptions);
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Sort file options:";
      this.Load += new EventHandler(this.FormGetSortTxtPrefix_Load);
      this.nudFileWeight.EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
