﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Forms.FormGetDESKey
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.Resources;
using SEGATools.Encrypt;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace GDRomExplorer.Forms
{
  public class FormGetDESKey : Form
  {
    private IContainer components;
    private TextBox tbDESKey;
    private Label lbDESKey;
    private Button btProceed;
    private Button btCancel;

    public DESKey DESKey { get; private set; }

    public static FormGetDESKey aGetDESKeyFormWithDecryptLabel() => new FormGetDESKey(FormGetDESKey.ButtonLabel.DecryptOnly, (DESKey) null);

    public static FormGetDESKey aGetDESKeyFormWithEncryptLabel() => new FormGetDESKey(FormGetDESKey.ButtonLabel.EncryptOnly, (DESKey) null);

    public static FormGetDESKey aGetDESKeyFormWithExtractAndDecryptLabel(
      DESKey inputDESKey)
    {
      return new FormGetDESKey(FormGetDESKey.ButtonLabel.ExtractAndDecrypt, inputDESKey);
    }

    private FormGetDESKey()
      : this(FormGetDESKey.ButtonLabel.ExtractAndDecrypt, (DESKey) null)
    {
    }

    private FormGetDESKey(FormGetDESKey.ButtonLabel buttonLabel, DESKey suggestedDESKey)
    {
      string extractButtonLabel;
      switch (buttonLabel)
      {
        case FormGetDESKey.ButtonLabel.ExtractAndDecrypt:
          extractButtonLabel = Strings.DESKeyFormButtonLabelExtractAndDecrypt;
          break;
        case FormGetDESKey.ButtonLabel.EncryptOnly:
          extractButtonLabel = Strings.DESKeyFormButtonLabelEncrypt;
          break;
        case FormGetDESKey.ButtonLabel.DecryptOnly:
          extractButtonLabel = Strings.DESKeyFormButtonLabelDecrypt;
          break;
        default:
          extractButtonLabel = Strings.DESKeyFormButtonLabelDecrypt;
          break;
      }
      this.DESKey = suggestedDESKey;
      this.Initialize(extractButtonLabel);
    }

    private void Initialize(string extractButtonLabel)
    {
      this.InitializeComponent();
      this.Activated += new EventHandler(this.FormGetDESKey_Activated);
      this.btProceed.Text = extractButtonLabel;
      this.tbDESKey.CharacterCasing = CharacterCasing.Upper;
      this.tbDESKey.TextChanged += new EventHandler(this.tbDESKey_TextChanged);
    }

    private void ValidateDESKey(string key)
    {
      string key1 = key.Trim();
      if (DESKey.TryParse(key1))
      {
        this.DESKey = DESKey.Parse(key1);
        this.btProceed.Enabled = true;
      }
      else
      {
        this.DESKey = (DESKey) null;
        this.btProceed.Enabled = false;
      }
    }

    private void FormGetDESKey_Load(object sender, EventArgs e)
    {
      this.btProceed.Enabled = false;
      this.tbDESKey.Text = this.DESKey != null ? this.DESKey.KeyString : string.Empty;
      this.tbDESKey.Focus();
    }

    private void FormGetDESKey_Activated(object sender, EventArgs e)
    {
      if (!Clipboard.ContainsText() || !DESKey.TryParse(Clipboard.GetText().Trim()))
        return;
      this.tbDESKey.Text = Clipboard.GetText().Trim();
    }

    private void tbDESKey_TextChanged(object sender, EventArgs e) => this.ValidateDESKey(this.tbDESKey.Text);

    private void btProceed_Click(object sender, EventArgs e) => this.ValidateDESKey(this.tbDESKey.Text);

    private void btCancel_Click(object sender, EventArgs e) => this.Close();

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.tbDESKey = new TextBox();
      this.lbDESKey = new Label();
      this.btProceed = new Button();
      this.btCancel = new Button();
      this.SuspendLayout();
      this.tbDESKey.Location = new Point(71, 6);
      this.tbDESKey.MaxLength = 16;
      this.tbDESKey.Name = "tbDESKey";
      this.tbDESKey.Size = new Size(210, 20);
      this.tbDESKey.TabIndex = 0;
      this.tbDESKey.TextAlign = HorizontalAlignment.Center;
      this.lbDESKey.AutoSize = true;
      this.lbDESKey.Location = new Point(12, 9);
      this.lbDESKey.Name = "lbDESKey";
      this.lbDESKey.Size = new Size(53, 13);
      this.lbDESKey.TabIndex = 2;
      this.lbDESKey.Text = "DES Key:";
      this.btProceed.DialogResult = DialogResult.OK;
      this.btProceed.FlatStyle = FlatStyle.Popup;
      this.btProceed.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btProceed.ForeColor = Color.FromArgb(248, 48, 0);
      this.btProceed.Location = new Point(154, 32);
      this.btProceed.Name = "btProceed";
      this.btProceed.Size = new Size((int) sbyte.MaxValue, 22);
      this.btProceed.TabIndex = 1;
      this.btProceed.Text = "&OK";
      this.btProceed.UseVisualStyleBackColor = false;
      this.btProceed.Click += new EventHandler(this.btProceed_Click);
      this.btCancel.DialogResult = DialogResult.Cancel;
      this.btCancel.FlatStyle = FlatStyle.Popup;
      this.btCancel.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btCancel.ForeColor = Color.FromArgb(248, 48, 0);
      this.btCancel.Location = new Point(15, 32);
      this.btCancel.Name = "btCancel";
      this.btCancel.Size = new Size((int) sbyte.MaxValue, 22);
      this.btCancel.TabIndex = 2;
      this.btCancel.Text = "&Cancel";
      this.btCancel.UseVisualStyleBackColor = false;
      this.btCancel.Click += new EventHandler(this.btCancel_Click);
      this.AcceptButton = (IButtonControl) this.btProceed;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = SystemColors.Window;
      this.CancelButton = (IButtonControl) this.btCancel;
      this.ClientSize = new Size(295, 65);
      this.Controls.Add((Control) this.btCancel);
      this.Controls.Add((Control) this.btProceed);
      this.Controls.Add((Control) this.tbDESKey);
      this.Controls.Add((Control) this.lbDESKey);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (FormGetDESKey);
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Enter the Naomi DES key";
      this.Load += new EventHandler(this.FormGetDESKey_Load);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    private enum ButtonLabel : short
    {
      ExtractAndDecrypt,
      EncryptOnly,
      DecryptOnly,
    }
  }
}
