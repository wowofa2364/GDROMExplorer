﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Forms.FormSettings
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.Properties;
using GDRomExplorer.Resources;
using SEGATools.Disc;
using SEGATools.FileFormat;
using SEGATools.Registry;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace GDRomExplorer.Forms
{
  public class FormSettings : Form
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private readonly List<FormSettings.CheckedListBoxItem> initialFileExtensions;
    private IContainer components;
    private TableLayoutPanel tableLayoutPanel;
    private Button btSaveClose;
    private Button btCancel;
    private TabControl tabControl;
    private TabPage tpAppSettings;
    private TabPage tpImageReaderSettings;
    private DiscFormatProvider discFormatProvider;
    private GroupBox gbFileExtensions;
    private CheckedListBox checkedListBox;
    private FlowLayoutPanel flowLayoutPanel;
    private LinkLabel lbCheckAll;
    private LinkLabel lbSelectNone;
    private TableLayoutPanel tableLayoutPanelImageReading;
    private GroupBox gbImageReaderOptions;
    private CheckBox cbComputePathTable;
    private Label lbInfoPathTable;

    public FormSettings()
    {
      this.InitializeComponent();
      this.initialFileExtensions = new List<FormSettings.CheckedListBoxItem>();
      this.FillFileAssociationList();
      this.cbComputePathTable.Checked = Settings.Default.ImageReaderComputePathTable;
      this.lbInfoPathTable.Text = Strings.SettingsInfoPathTable;
    }

    private string GetFileAssociationInfoName(string FileExtension) => "GDRomExplorer" + FileExtension;

    private void FillFileAssociationList()
    {
      this.checkedListBox.Items.Clear();
      foreach (IImageFileFormat ImageFileFormat in this.discFormatProvider.SupportedFileFormat)
      {
        for (int index = 0; index < ImageFileFormat.FileExtentions.Length; ++index)
        {
          string fileExtention = ImageFileFormat.FileExtentions[index];
          string extentionDescription = ImageFileFormat.FileExtentionDescriptions[index];
          bool application = this.IsExtensionAssociatedToApplication(fileExtention);
          FormSettings.CheckedListBoxItem checkedListBoxItem = new FormSettings.CheckedListBoxItem(fileExtention, extentionDescription, ImageFileFormat);
          if (application)
            this.initialFileExtensions.Add(checkedListBoxItem);
          this.checkedListBox.Items.Add((object) checkedListBoxItem, application);
        }
      }
    }

    private bool IsExtensionAssociatedToApplication(string fileExtension)
    {
      FileAssociationInfo fileAssociationInfo = new FileAssociationInfo(fileExtension);
      return fileAssociationInfo.Exists && fileAssociationInfo.ProgID.Equals(this.GetFileAssociationInfoName(fileExtension));
    }

    private void AddFileAssociation(FormSettings.CheckedListBoxItem item)
    {
      FileAssociationInfo fileAssociationInfo = new FileAssociationInfo(item.FileExtension);
      fileAssociationInfo.Create(this.GetFileAssociationInfoName(item.FileExtension));
      string fullPath = Path.GetFullPath(Environment.GetCommandLineArgs()[0]);
      ProgramAssociationInfo programAssociationInfo = new ProgramAssociationInfo(fileAssociationInfo.ProgID);
      programAssociationInfo.Create(item.FileDescription, new ProgramVerb("Open", fullPath + " \"%1\""));
      string path = fullPath;
      programAssociationInfo.DefaultIcon = new ProgramIcon(path, 1);
    }

    private void RemoveFileAssociation(string fileExtension)
    {
      FileAssociationInfo fileAssociationInfo = new FileAssociationInfo(fileExtension);
      if (!this.IsExtensionAssociatedToApplication(fileExtension))
        return;
      ProgramAssociationInfo programAssociationInfo = new ProgramAssociationInfo(fileAssociationInfo.ProgID);
      if (programAssociationInfo.Exists)
        programAssociationInfo.Delete();
      fileAssociationInfo.Delete();
    }

    private void UpdateFileExtensions()
    {
      FormSettings.logger.Info((object) "Updating file extensions");
      foreach (FormSettings.CheckedListBoxItem checkedListBoxItem in (ListBox.ObjectCollection) this.checkedListBox.Items)
      {
        bool flag1 = this.initialFileExtensions.Contains(checkedListBoxItem);
        bool flag2 = this.checkedListBox.CheckedItems.Contains((object) checkedListBoxItem);
        if (!flag1 && flag2)
          this.AddFileAssociation(checkedListBoxItem);
        if (flag1 && !flag2)
          this.RemoveFileAssociation(checkedListBoxItem.FileExtension);
      }
    }

    private void UpdateImageReaderSettings()
    {
      FormSettings.logger.Info((object) "Updating image reader settings");
      Settings.Default.ImageReaderComputePathTable = this.cbComputePathTable.Checked;
    }

    private void btSaveClose_Click(object sender, EventArgs e)
    {
      this.UpdateFileExtensions();
      this.UpdateImageReaderSettings();
      AppStatus.NotifyNewAppStatus(Strings.SettingsSaved);
    }

    private void lbSelectNone_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      for (int index = 0; index < this.checkedListBox.Items.Count; ++index)
        this.checkedListBox.SetItemChecked(index, false);
    }

    private void lbCheckAll_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      for (int index = 0; index < this.checkedListBox.Items.Count; ++index)
        this.checkedListBox.SetItemChecked(index, true);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.tableLayoutPanel = new TableLayoutPanel();
      this.btCancel = new Button();
      this.btSaveClose = new Button();
      this.tabControl = new TabControl();
      this.tpAppSettings = new TabPage();
      this.gbFileExtensions = new GroupBox();
      this.flowLayoutPanel = new FlowLayoutPanel();
      this.lbCheckAll = new LinkLabel();
      this.lbSelectNone = new LinkLabel();
      this.checkedListBox = new CheckedListBox();
      this.tpImageReaderSettings = new TabPage();
      this.tableLayoutPanelImageReading = new TableLayoutPanel();
      this.gbImageReaderOptions = new GroupBox();
      this.lbInfoPathTable = new Label();
      this.cbComputePathTable = new CheckBox();
      this.discFormatProvider = new DiscFormatProvider();
      this.tableLayoutPanel.SuspendLayout();
      this.tabControl.SuspendLayout();
      this.tpAppSettings.SuspendLayout();
      this.gbFileExtensions.SuspendLayout();
      this.flowLayoutPanel.SuspendLayout();
      this.tpImageReaderSettings.SuspendLayout();
      this.tableLayoutPanelImageReading.SuspendLayout();
      this.gbImageReaderOptions.SuspendLayout();
      this.SuspendLayout();
      this.tableLayoutPanel.ColumnCount = 2;
      this.tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
      this.tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
      this.tableLayoutPanel.Controls.Add((Control) this.btCancel, 0, 1);
      this.tableLayoutPanel.Controls.Add((Control) this.btSaveClose, 1, 1);
      this.tableLayoutPanel.Controls.Add((Control) this.tabControl, 0, 0);
      this.tableLayoutPanel.Dock = DockStyle.Fill;
      this.tableLayoutPanel.Location = new Point(0, 0);
      this.tableLayoutPanel.Name = "tableLayoutPanel";
      this.tableLayoutPanel.RowCount = 2;
      this.tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 89.3617f));
      this.tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 10.6383f));
      this.tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanel.Size = new Size(271, 235);
      this.tableLayoutPanel.TabIndex = 0;
      this.btCancel.DialogResult = DialogResult.Cancel;
      this.btCancel.Dock = DockStyle.Fill;
      this.btCancel.FlatStyle = FlatStyle.Popup;
      this.btCancel.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btCancel.ForeColor = Color.FromArgb(248, 48, 0);
      this.btCancel.Location = new Point(3, 212);
      this.btCancel.Name = "btCancel";
      this.btCancel.Size = new Size(129, 20);
      this.btCancel.TabIndex = 11;
      this.btCancel.Text = "&Cancel";
      this.btCancel.UseVisualStyleBackColor = false;
      this.btSaveClose.DialogResult = DialogResult.Cancel;
      this.btSaveClose.Dock = DockStyle.Fill;
      this.btSaveClose.FlatStyle = FlatStyle.Popup;
      this.btSaveClose.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btSaveClose.ForeColor = Color.FromArgb(248, 48, 0);
      this.btSaveClose.Location = new Point(138, 212);
      this.btSaveClose.Name = "btSaveClose";
      this.btSaveClose.Size = new Size(130, 20);
      this.btSaveClose.TabIndex = 10;
      this.btSaveClose.Text = "&OK";
      this.btSaveClose.UseVisualStyleBackColor = false;
      this.btSaveClose.Click += new EventHandler(this.btSaveClose_Click);
      this.tableLayoutPanel.SetColumnSpan((Control) this.tabControl, 2);
      this.tabControl.Controls.Add((Control) this.tpAppSettings);
      this.tabControl.Controls.Add((Control) this.tpImageReaderSettings);
      this.tabControl.Dock = DockStyle.Fill;
      this.tabControl.Location = new Point(3, 3);
      this.tabControl.Name = "tabControl";
      this.tabControl.SelectedIndex = 0;
      this.tabControl.Size = new Size(265, 203);
      this.tabControl.TabIndex = 12;
      this.tpAppSettings.BackColor = SystemColors.Window;
      this.tpAppSettings.Controls.Add((Control) this.gbFileExtensions);
      this.tpAppSettings.Location = new Point(4, 22);
      this.tpAppSettings.Name = "tpAppSettings";
      this.tpAppSettings.Padding = new Padding(3);
      this.tpAppSettings.Size = new Size(257, 177);
      this.tpAppSettings.TabIndex = 0;
      this.tpAppSettings.Text = "General";
      this.gbFileExtensions.BackColor = SystemColors.Window;
      this.gbFileExtensions.Controls.Add((Control) this.flowLayoutPanel);
      this.gbFileExtensions.Controls.Add((Control) this.checkedListBox);
      this.gbFileExtensions.Dock = DockStyle.Top;
      this.gbFileExtensions.Location = new Point(3, 3);
      this.gbFileExtensions.Name = "gbFileExtensions";
      this.gbFileExtensions.Size = new Size(251, 167);
      this.gbFileExtensions.TabIndex = 1;
      this.gbFileExtensions.TabStop = false;
      this.gbFileExtensions.Text = "File Extensions:";
      this.flowLayoutPanel.Controls.Add((Control) this.lbCheckAll);
      this.flowLayoutPanel.Controls.Add((Control) this.lbSelectNone);
      this.flowLayoutPanel.Dock = DockStyle.Bottom;
      this.flowLayoutPanel.Location = new Point(3, 147);
      this.flowLayoutPanel.Name = "flowLayoutPanel";
      this.flowLayoutPanel.Size = new Size(245, 17);
      this.flowLayoutPanel.TabIndex = 3;
      this.lbCheckAll.AutoSize = true;
      this.lbCheckAll.Dock = DockStyle.Bottom;
      this.lbCheckAll.Location = new Point(3, 0);
      this.lbCheckAll.Name = "lbCheckAll";
      this.lbCheckAll.Size = new Size(48, 13);
      this.lbCheckAll.TabIndex = 6;
      this.lbCheckAll.TabStop = true;
      this.lbCheckAll.Text = "select all";
      this.lbCheckAll.LinkClicked += new LinkLabelLinkClickedEventHandler(this.lbCheckAll_LinkClicked);
      this.lbSelectNone.AutoSize = true;
      this.lbSelectNone.Dock = DockStyle.Bottom;
      this.lbSelectNone.Location = new Point(57, 0);
      this.lbSelectNone.Name = "lbSelectNone";
      this.lbSelectNone.Size = new Size(62, 13);
      this.lbSelectNone.TabIndex = 7;
      this.lbSelectNone.TabStop = true;
      this.lbSelectNone.Text = "select none";
      this.lbSelectNone.LinkClicked += new LinkLabelLinkClickedEventHandler(this.lbSelectNone_LinkClicked);
      this.checkedListBox.CheckOnClick = true;
      this.checkedListBox.Dock = DockStyle.Top;
      this.checkedListBox.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.checkedListBox.FormattingEnabled = true;
      this.checkedListBox.Location = new Point(3, 16);
      this.checkedListBox.Name = "checkedListBox";
      this.checkedListBox.Size = new Size(245, 124);
      this.checkedListBox.Sorted = true;
      this.checkedListBox.TabIndex = 1;
      this.tpImageReaderSettings.BackColor = SystemColors.Window;
      this.tpImageReaderSettings.Controls.Add((Control) this.tableLayoutPanelImageReading);
      this.tpImageReaderSettings.Location = new Point(4, 22);
      this.tpImageReaderSettings.Name = "tpImageReaderSettings";
      this.tpImageReaderSettings.Padding = new Padding(3);
      this.tpImageReaderSettings.Size = new Size(257, 177);
      this.tpImageReaderSettings.TabIndex = 1;
      this.tpImageReaderSettings.Text = "Image Reading";
      this.tableLayoutPanelImageReading.BackColor = Color.Transparent;
      this.tableLayoutPanelImageReading.ColumnCount = 1;
      this.tableLayoutPanelImageReading.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
      this.tableLayoutPanelImageReading.Controls.Add((Control) this.gbImageReaderOptions, 0, 0);
      this.tableLayoutPanelImageReading.Dock = DockStyle.Fill;
      this.tableLayoutPanelImageReading.Location = new Point(3, 3);
      this.tableLayoutPanelImageReading.Name = "tableLayoutPanelImageReading";
      this.tableLayoutPanelImageReading.RowCount = 1;
      this.tableLayoutPanelImageReading.RowStyles.Add(new RowStyle(SizeType.Percent, 50f));
      this.tableLayoutPanelImageReading.Size = new Size(251, 171);
      this.tableLayoutPanelImageReading.TabIndex = 0;
      this.gbImageReaderOptions.Controls.Add((Control) this.lbInfoPathTable);
      this.gbImageReaderOptions.Controls.Add((Control) this.cbComputePathTable);
      this.gbImageReaderOptions.Dock = DockStyle.Top;
      this.gbImageReaderOptions.Location = new Point(3, 3);
      this.gbImageReaderOptions.Name = "gbImageReaderOptions";
      this.gbImageReaderOptions.Size = new Size(245, 90);
      this.gbImageReaderOptions.TabIndex = 0;
      this.gbImageReaderOptions.TabStop = false;
      this.gbImageReaderOptions.Text = "ISO9660 File System:";
      this.lbInfoPathTable.Dock = DockStyle.Bottom;
      this.lbInfoPathTable.Location = new Point(3, 40);
      this.lbInfoPathTable.Name = "lbInfoPathTable";
      this.lbInfoPathTable.Size = new Size(239, 47);
      this.lbInfoPathTable.TabIndex = 1;
      this.lbInfoPathTable.Text = "Info path table";
      this.cbComputePathTable.AutoSize = true;
      this.cbComputePathTable.Location = new Point(7, 20);
      this.cbComputePathTable.Name = "cbComputePathTable";
      this.cbComputePathTable.Size = new Size(118, 17);
      this.cbComputePathTable.TabIndex = 0;
      this.cbComputePathTable.Text = "Compute path table";
      this.cbComputePathTable.UseVisualStyleBackColor = true;
      this.AcceptButton = (IButtonControl) this.btSaveClose;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = SystemColors.Window;
      this.CancelButton = (IButtonControl) this.btCancel;
      this.ClientSize = new Size(271, 235);
      this.Controls.Add((Control) this.tableLayoutPanel);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (FormSettings);
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Settings";
      this.tableLayoutPanel.ResumeLayout(false);
      this.tabControl.ResumeLayout(false);
      this.tpAppSettings.ResumeLayout(false);
      this.gbFileExtensions.ResumeLayout(false);
      this.flowLayoutPanel.ResumeLayout(false);
      this.flowLayoutPanel.PerformLayout();
      this.tpImageReaderSettings.ResumeLayout(false);
      this.tableLayoutPanelImageReading.ResumeLayout(false);
      this.gbImageReaderOptions.ResumeLayout(false);
      this.gbImageReaderOptions.PerformLayout();
      this.ResumeLayout(false);
    }

    private class CheckedListBoxItem
    {
      public string FileExtension { get; private set; }

      public string FileDescription { get; private set; }

      public IImageFileFormat ImageFileFormat { get; private set; }

      internal CheckedListBoxItem(
        string FileExtension,
        string FileDescription,
        IImageFileFormat ImageFileFormat)
      {
        this.FileExtension = FileExtension;
        this.FileDescription = FileDescription;
        this.ImageFileFormat = ImageFileFormat;
      }

      public override string ToString() => string.Format("*{0} - {1}", (object) this.FileExtension.ToUpper(CultureInfo.InvariantCulture), (object) this.FileDescription);
    }
  }
}
