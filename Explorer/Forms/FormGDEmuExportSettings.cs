﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Forms.FormGDEmuExportSettings
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.Resources;
using GDRomExplorer.UserControls;
using SEGATools.DiscFileSystem;
using SEGATools.GDEmu;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace GDRomExplorer.Forms
{
  public class FormGDEmuExportSettings : Form
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private FormProcess formProcess;
    private IContainer components;
    private GDEmuExportSettings gdEmuExportSettings;
    private Button btCancel;
    private Button btOk;
    private GDEmuConverter gdEmuConverter;

    public IDiscFileSystem InputDiscImage => this.gdEmuExportSettings.InputDiscImage;

    public FormGDEmuExportSettings() => this.InitializeComponent();

    public FormGDEmuExportSettings(string InitialDirectory)
      : this()
      => this.gdEmuExportSettings.InitialDirectory = InitialDirectory;

    public FormGDEmuExportSettings(IDiscFileSystem DiscFileSystem)
      : this()
      => this.SetLoadedDiscImage(DiscFileSystem);

    private void SetLoadedDiscImage(IDiscFileSystem discFileSystem) => this.gdEmuExportSettings.SetLoadedDiscImage(discFileSystem);

    private bool AreSettingsValid() => this.gdEmuExportSettings.InputDiscImage != null && !string.IsNullOrEmpty(this.gdEmuExportSettings.ExportOptions.OutputPath);

    private void ShowIncompleteMessagBox()
    {
      string text = this.gdEmuExportSettings.InputDiscImage != null ? Strings.MsgBoxGDEmuSettingsOutputPathMissing : Strings.MsgBoxGDEmuSettingsImageMissing;
      FormGDEmuExportSettings.logger.WarnFormat("Cannot start the conversion: {0}", (object) text);
      int num = (int) MessageBox.Show((IWin32Window) this, text, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
    }

    private void StartConversion()
    {
      Guid TaskId = Guid.NewGuid();
      this.formProcess = FormProcess.createForGDEmuConverter(this.gdEmuConverter, TaskId);
      this.gdEmuConverter.ConvertAsync(this.gdEmuExportSettings.InputDiscImage, this.gdEmuExportSettings.ExportOptions, (object) TaskId);
    }

    private void btOk_Click(object sender, EventArgs e)
    {
      if (this.AreSettingsValid())
        this.StartConversion();
      else
        this.ShowIncompleteMessagBox();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormGDEmuExportSettings));
      this.btCancel = new Button();
      this.btOk = new Button();
      this.gdEmuExportSettings = new GDEmuExportSettings();
      this.gdEmuConverter = new GDEmuConverter(this.components);
      this.SuspendLayout();
      this.btCancel.DialogResult = DialogResult.Cancel;
      this.btCancel.FlatStyle = FlatStyle.Popup;
      this.btCancel.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btCancel.ForeColor = Color.FromArgb(248, 48, 0);
      this.btCancel.Location = new Point(12, 174);
      this.btCancel.Name = "btCancel";
      this.btCancel.Size = new Size((int) sbyte.MaxValue, 22);
      this.btCancel.TabIndex = 2;
      this.btCancel.Text = "&Close";
      this.btCancel.UseVisualStyleBackColor = false;
      this.btOk.CausesValidation = false;
      this.btOk.FlatStyle = FlatStyle.Popup;
      this.btOk.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btOk.ForeColor = Color.FromArgb(248, 48, 0);
      this.btOk.Location = new Point(340, 174);
      this.btOk.Name = "btOk";
      this.btOk.Size = new Size((int) sbyte.MaxValue, 22);
      this.btOk.TabIndex = 1;
      this.btOk.Text = "&OK";
      this.btOk.UseVisualStyleBackColor = false;
      this.btOk.Click += new EventHandler(this.btOk_Click);
      this.gdEmuExportSettings.AutoSize = true;
      this.gdEmuExportSettings.BackColor = SystemColors.Window;
      this.gdEmuExportSettings.InitialDirectory = "";
      this.gdEmuExportSettings.Location = new Point(0, 0);
      this.gdEmuExportSettings.Name = "gdEmuExportSettings";
      this.gdEmuExportSettings.Size = new Size(480, 157);
      this.gdEmuExportSettings.TabIndex = 0;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = SystemColors.Window;
      this.CancelButton = (IButtonControl) this.btCancel;
      this.CausesValidation = false;
      this.ClientSize = new Size(479, 208);
      this.Controls.Add((Control) this.btCancel);
      this.Controls.Add((Control) this.btOk);
      this.Controls.Add((Control) this.gdEmuExportSettings);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (FormGDEmuExportSettings);
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Export GDI for GDEMU";
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
