﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Forms.GDRomExplorerForm
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.DiscView;
using GDRomExplorer.Events;
using GDRomExplorer.Resources;
using GDRomExplorer.UserControls;
using SEGATools.Audio;
using SEGATools.Disc;
using SEGATools.DiscFileSystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Windows.Forms.Layout;

namespace GDRomExplorer.Forms
{
  public class GDRomExplorerForm : Form
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private IDiscFileSystem discFileSystem;
    private DiscViewExplorer.ActionsForEditMenuItem editMenuActions;
    private string initialDirectory;
    private IContainer components;
    private StatusStrip statusStrip;
    private DiscViewOpener discViewOpener;
    private ToolStripStatusLabel toolStripStatusLabel;
    private DiscViewExplorer discView;
    private ToolStripMenuItem fileToolStripMenuItem;
    private ToolStripMenuItem openToolStripMenuItem;
    private ToolStripMenuItem closeToolStripMenuItem;
    private ToolStripMenuItem exitToolStripMenuItem;
    private ToolStripMenuItem toolsToolStripMenuItem;
    private ToolStripMenuItem ConvertToolStripMenuItem;
    private ToolStripMenuItem naomiBinarydecryptionToolStripMenuItem;
    private ToolStripSeparator toolStripSeparator;
    private ToolStripMenuItem aboutToolStripMenuItem;
    private ToolStripMenuItem aboutGDRomExplorerToolStripMenuItem;
    private MenuStrip menuStrip;
    private DiscFormatProvider discFormatProvider;
    private ToolStripMenuItem settingsToolStripMenuItem;
    private ToolStripMenuItem ipViewerToolStripMenuItem;
    private InitialProgramOpener ipOpener;
    private ToolStripMenuItem encryptANaomiBinaryToolStripMenuItem;
    private NaomiEncryptDecryptTool naomiEncryptDecryptTool;
    private ToolStripMenuItem convertGDIForGDEMUToolStripMenuItem;
    private ToolStripSeparator toolStripSeparator1;
    private ToolStripMenuItem editToolStripMenuItem;
    private GDDAConverterTool gddaConverterTool;

    public GDRomExplorerForm()
    {
      this.InitializeComponent();
      this.Text = Assembly.GetExecutingAssembly().GetName().Name;
    }

    private void GDRomExplorerForm_Load(object sender, EventArgs e)
    {
      this.RemoveImageMargin(this.menuStrip);
      this.MenuClose();
      this.DragDrop += new DragEventHandler(this.GDRomExplorerForm_DragDrop);
      this.DragEnter += new DragEventHandler(this.GDRomExplorerForm_DragEnter);
      this.discViewOpener.ImageLoaded += new EventHandler<EventArgs<IDiscFileSystem>>(this.discViewOpener_ImageLoaded);
      this.discViewOpener.ImageNotLoaded += new EventHandler(this.discViewOpener_ImageNotLoaded);
      this.discView.SelectionChanged += new EventHandler<EventArgs<DiscViewExplorer.ActionsForEditMenuItem>>(this.discView_SelectionChanged);
      AppStatus.OnAppStatusUpdate += new AppStatus.AppStatusUpdateEventHandler(this.AppStatus_OnAppStatusUpdate);
      string progamArgument = Program.GetProgamArgument();
      if (!string.IsNullOrEmpty(progamArgument))
        this.UpdateDialogInitialDirectory(progamArgument);
      else
        this.UpdateDialogInitialDirectory(Assembly.GetExecutingAssembly().GetName().CodeBase);
      if (string.IsNullOrEmpty(progamArgument))
        return;
      this.discViewOpener.Open(progamArgument);
    }

    private void AppStatus_OnAppStatusUpdate(string message)
    {
      string str = char.ToUpper(message[0]).ToString() + message.Substring(1);
      this.toolStripStatusLabel.Text = str;
      this.toolStripStatusLabel.ToolTipText = str;
    }

    private void GDRomExplorerForm_DragEnter(object sender, DragEventArgs e)
    {
      if (e.Data.GetDataPresent(DataFormats.FileDrop))
        e.Effect = DragDropEffects.All;
      else
        e.Effect = DragDropEffects.None;
    }

    private void GDRomExplorerForm_DragDrop(object sender, DragEventArgs e)
    {
      string[] data = (string[]) e.Data.GetData(DataFormats.FileDrop, false);
      if (data.Length == 1)
        this.discViewOpener.Open(Path.GetFullPath(data[0]));
      else
        GDRomExplorerForm.logger.Error((object) "Unhandled data for the drag & drop operation!");
    }

    private void discViewOpener_ImageLoaded(object sender, EventArgs<IDiscFileSystem> e)
    {
      this.discFileSystem = e.Value;
      this.MenuOpen();
      this.UpdateDialogInitialDirectory(this.discFileSystem.FileName);
      this.discView.LoadDisc(this.discFileSystem);
    }

    private void discViewOpener_ImageNotLoaded(object sender, EventArgs e)
    {
      this.discFileSystem = (IDiscFileSystem) null;
      this.MenuClose();
    }

    private void discView_SelectionChanged(
      object sender,
      EventArgs<DiscViewExplorer.ActionsForEditMenuItem> e)
    {
      this.editMenuActions = e.Value;
      if (!this.editMenuActions.AnyAvailable)
        this.editToolStripMenuItem.Enabled = false;
      else
        this.editToolStripMenuItem.Enabled = true;
    }

    private void MenuClose()
    {
      this.toolStripStatusLabel.Text = Strings.StatusLabelImageNotLoaded;
      this.closeToolStripMenuItem.Enabled = false;
      this.editToolStripMenuItem.Enabled = false;
      this.discView.CloseDisc();
    }

    private void MenuOpen()
    {
      this.closeToolStripMenuItem.Enabled = true;
      this.editToolStripMenuItem.Enabled = true;
    }

    private void UpdateDialogInitialDirectory(string initialPath)
    {
      this.initialDirectory = Path.GetDirectoryName(initialPath);
      this.gddaConverterTool.InitialDirectory = this.initialDirectory;
      this.naomiEncryptDecryptTool.InitialDirectory = this.initialDirectory;
    }

    private void RemoveImageMargin(MenuStrip Menu)
    {
      foreach (ToolStripItem toolStripItem in (ArrangedElementCollection) Menu.Items)
        this.RemoveItemImageMargin(toolStripItem);
    }

    private void RemoveItemImageMargin(ToolStripItem Item)
    {
      if (!(Item is ToolStripMenuItem))
        return;
      ToolStripMenuItem toolStripMenuItem = Item as ToolStripMenuItem;
      (toolStripMenuItem.DropDown as ToolStripDropDownMenu).ShowImageMargin = false;
      foreach (object dropDownItem in (ArrangedElementCollection) toolStripMenuItem.DropDownItems)
      {
        if (dropDownItem is ToolStripItem)
          this.RemoveItemImageMargin(dropDownItem as ToolStripItem);
      }
    }

    private void ToolStripMenuItemExit_Click(object sender, EventArgs e)
    {
      this.discViewOpener.Close();
      this.discView.CloseDisc();
      this.discFileSystem = (IDiscFileSystem) null;
      Application.Exit();
    }

    private void ToolStripMenuItemClose_Click(object sender, EventArgs e) => this.discViewOpener.Close();

    private void ToolStripMenuItemOpenImageFile_Click(object sender, EventArgs e) => this.discViewOpener.Open();

    private void ToolStripMenuItemShowGDDAConverter_Click(object sender, EventArgs e) => this.gddaConverterTool.OpenAndConvertGDDAFiles((IWin32Window) this, AudioConversionSettings.defaultAudioConvOptions());

    private void ToolStripMenuItemAboutGDRomExplorer_Click(object sender, EventArgs e)
    {
      using (FormAbout formAbout = new FormAbout())
      {
        int num = (int) formAbout.ShowDialog((IWin32Window) this);
      }
    }

    private void ToolStripMenuItemExportGDIForGDEMU_Click(object sender, EventArgs e)
    {
      using (FormGDEmuExportSettings emuExportSettings = new FormGDEmuExportSettings(this.initialDirectory))
      {
        int num = (int) emuExportSettings.ShowDialog((IWin32Window) this);
      }
    }

    private void ToolStripMenuItemEncryptNaomiBinary_Click(object sender, EventArgs e) => this.naomiEncryptDecryptTool.OpenAndEncryptNaomiBinary((IWin32Window) this);

    private void ToolStripMenuItemDecryptNaomiBinary_Click(object sender, EventArgs e) => this.naomiEncryptDecryptTool.OpenAndDecryptNaomiBinary((IWin32Window) this);

    private void ToolStripMenuItemSettings_Click(object sender, EventArgs e)
    {
      using (FormSettings formSettings = new FormSettings())
      {
        int num = (int) formSettings.ShowDialog((IWin32Window) this);
      }
    }

    private void ToolStripMenuItemIPViewer_Click(object sender, EventArgs e) => this.ipOpener.OpenAndViewInitialProgram((IWin32Window) this);

    private void ToolStripMenuItemEdit_DropDownOpening(object sender, EventArgs e)
    {
      this.editToolStripMenuItem.DropDownItems.Clear();
      this.editToolStripMenuItem.DropDownItems.AddRange(MenuItemFactory.CreateEditMenuItems(this.editMenuActions.DiscMenuItems, this.editMenuActions.SelectionMenuItems));
      this.RemoveItemImageMargin((ToolStripItem) this.editToolStripMenuItem);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (GDRomExplorerForm));
      this.statusStrip = new StatusStrip();
      this.toolStripStatusLabel = new ToolStripStatusLabel();
      this.fileToolStripMenuItem = new ToolStripMenuItem();
      this.openToolStripMenuItem = new ToolStripMenuItem();
      this.closeToolStripMenuItem = new ToolStripMenuItem();
      this.exitToolStripMenuItem = new ToolStripMenuItem();
      this.toolsToolStripMenuItem = new ToolStripMenuItem();
      this.ipViewerToolStripMenuItem = new ToolStripMenuItem();
      this.ConvertToolStripMenuItem = new ToolStripMenuItem();
      this.convertGDIForGDEMUToolStripMenuItem = new ToolStripMenuItem();
      this.toolStripSeparator1 = new ToolStripSeparator();
      this.encryptANaomiBinaryToolStripMenuItem = new ToolStripMenuItem();
      this.naomiBinarydecryptionToolStripMenuItem = new ToolStripMenuItem();
      this.toolStripSeparator = new ToolStripSeparator();
      this.settingsToolStripMenuItem = new ToolStripMenuItem();
      this.aboutToolStripMenuItem = new ToolStripMenuItem();
      this.aboutGDRomExplorerToolStripMenuItem = new ToolStripMenuItem();
      this.menuStrip = new MenuStrip();
      this.editToolStripMenuItem = new ToolStripMenuItem();
      this.discFormatProvider = new DiscFormatProvider(this.components);
      this.discView = new DiscViewExplorer();
      this.discViewOpener = new DiscViewOpener();
      this.ipOpener = new InitialProgramOpener();
      this.naomiEncryptDecryptTool = new NaomiEncryptDecryptTool(this.components);
      this.gddaConverterTool = new GDDAConverterTool(this.components);
      this.statusStrip.SuspendLayout();
      this.menuStrip.SuspendLayout();
      this.SuspendLayout();
      this.statusStrip.BackColor = Color.Transparent;
      this.statusStrip.Items.AddRange(new ToolStripItem[1]
      {
        (ToolStripItem) this.toolStripStatusLabel
      });
      this.statusStrip.Location = new Point(0, 540);
      this.statusStrip.Name = "statusStrip";
      this.statusStrip.ShowItemToolTips = true;
      this.statusStrip.Size = new Size(784, 22);
      this.statusStrip.TabIndex = 27;
      this.statusStrip.Text = "statusStrip";
      this.toolStripStatusLabel.AutoToolTip = true;
      this.toolStripStatusLabel.DisplayStyle = ToolStripItemDisplayStyle.Text;
      this.toolStripStatusLabel.Margin = new Padding(3, 3, 0, 2);
      this.toolStripStatusLabel.Name = "toolStripStatusLabel";
      this.toolStripStatusLabel.Size = new Size(766, 17);
      this.toolStripStatusLabel.Spring = true;
      this.toolStripStatusLabel.Text = "AppStatus";
      this.toolStripStatusLabel.TextAlign = ContentAlignment.MiddleLeft;
      this.fileToolStripMenuItem.DisplayStyle = ToolStripItemDisplayStyle.Text;
      this.fileToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[3]
      {
        (ToolStripItem) this.openToolStripMenuItem,
        (ToolStripItem) this.closeToolStripMenuItem,
        (ToolStripItem) this.exitToolStripMenuItem
      });
      this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
      this.fileToolStripMenuItem.Size = new Size(37, 20);
      this.fileToolStripMenuItem.Text = "&File";
      this.openToolStripMenuItem.BackColor = SystemColors.Window;
      this.openToolStripMenuItem.BackgroundImageLayout = ImageLayout.None;
      this.openToolStripMenuItem.DisplayStyle = ToolStripItemDisplayStyle.Text;
      this.openToolStripMenuItem.Name = "openToolStripMenuItem";
      this.openToolStripMenuItem.ShortcutKeys = Keys.O | Keys.Control;
      this.openToolStripMenuItem.Size = new Size(155, 22);
      this.openToolStripMenuItem.Text = "&Open...";
      this.openToolStripMenuItem.TextImageRelation = TextImageRelation.TextAboveImage;
      this.openToolStripMenuItem.Click += new EventHandler(this.ToolStripMenuItemOpenImageFile_Click);
      this.closeToolStripMenuItem.BackColor = SystemColors.Window;
      this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
      this.closeToolStripMenuItem.Size = new Size(155, 22);
      this.closeToolStripMenuItem.Text = "Close";
      this.closeToolStripMenuItem.Click += new EventHandler(this.ToolStripMenuItemClose_Click);
      this.exitToolStripMenuItem.BackColor = SystemColors.Window;
      this.exitToolStripMenuItem.DisplayStyle = ToolStripItemDisplayStyle.Text;
      this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
      this.exitToolStripMenuItem.ShortcutKeys = Keys.F4 | Keys.Alt;
      this.exitToolStripMenuItem.Size = new Size(155, 22);
      this.exitToolStripMenuItem.Text = "Exit";
      this.exitToolStripMenuItem.Click += new EventHandler(this.ToolStripMenuItemExit_Click);
      this.toolsToolStripMenuItem.DisplayStyle = ToolStripItemDisplayStyle.Text;
      this.toolsToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[8]
      {
        (ToolStripItem) this.ipViewerToolStripMenuItem,
        (ToolStripItem) this.ConvertToolStripMenuItem,
        (ToolStripItem) this.convertGDIForGDEMUToolStripMenuItem,
        (ToolStripItem) this.toolStripSeparator1,
        (ToolStripItem) this.encryptANaomiBinaryToolStripMenuItem,
        (ToolStripItem) this.naomiBinarydecryptionToolStripMenuItem,
        (ToolStripItem) this.toolStripSeparator,
        (ToolStripItem) this.settingsToolStripMenuItem
      });
      this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
      this.toolsToolStripMenuItem.Size = new Size(48, 20);
      this.toolsToolStripMenuItem.Text = "&Tools";
      this.ipViewerToolStripMenuItem.Name = "ipViewerToolStripMenuItem";
      this.ipViewerToolStripMenuItem.ShortcutKeys = Keys.I | Keys.Control;
      this.ipViewerToolStripMenuItem.Size = new Size(242, 22);
      this.ipViewerToolStripMenuItem.Text = "IP.BIN Viewer";
      this.ipViewerToolStripMenuItem.Click += new EventHandler(this.ToolStripMenuItemIPViewer_Click);
      this.ConvertToolStripMenuItem.BackColor = SystemColors.Window;
      this.ConvertToolStripMenuItem.Name = "ConvertToolStripMenuItem";
      this.ConvertToolStripMenuItem.ShortcutKeys = Keys.T | Keys.Control;
      this.ConvertToolStripMenuItem.Size = new Size(242, 22);
      this.ConvertToolStripMenuItem.Text = "Convert audio tracks";
      this.ConvertToolStripMenuItem.Click += new EventHandler(this.ToolStripMenuItemShowGDDAConverter_Click);
      this.convertGDIForGDEMUToolStripMenuItem.Name = "convertGDIForGDEMUToolStripMenuItem";
      this.convertGDIForGDEMUToolStripMenuItem.ShortcutKeys = Keys.G | Keys.Control;
      this.convertGDIForGDEMUToolStripMenuItem.Size = new Size(242, 22);
      this.convertGDIForGDEMUToolStripMenuItem.Text = "Convert GDI for GDEMU";
      this.convertGDIForGDEMUToolStripMenuItem.Click += new EventHandler(this.ToolStripMenuItemExportGDIForGDEMU_Click);
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new Size(239, 6);
      this.encryptANaomiBinaryToolStripMenuItem.Name = "encryptANaomiBinaryToolStripMenuItem";
      this.encryptANaomiBinaryToolStripMenuItem.ShortcutKeys = Keys.E | Keys.Control;
      this.encryptANaomiBinaryToolStripMenuItem.Size = new Size(242, 22);
      this.encryptANaomiBinaryToolStripMenuItem.Text = "&Encrypt a Naomi binary";
      this.encryptANaomiBinaryToolStripMenuItem.Click += new EventHandler(this.ToolStripMenuItemEncryptNaomiBinary_Click);
      this.naomiBinarydecryptionToolStripMenuItem.BackColor = SystemColors.Window;
      this.naomiBinarydecryptionToolStripMenuItem.Name = "naomiBinarydecryptionToolStripMenuItem";
      this.naomiBinarydecryptionToolStripMenuItem.ShortcutKeys = Keys.D | Keys.Control;
      this.naomiBinarydecryptionToolStripMenuItem.Size = new Size(242, 22);
      this.naomiBinarydecryptionToolStripMenuItem.Text = "&Decrypt a Naomi binary";
      this.naomiBinarydecryptionToolStripMenuItem.Click += new EventHandler(this.ToolStripMenuItemDecryptNaomiBinary_Click);
      this.toolStripSeparator.BackColor = SystemColors.Window;
      this.toolStripSeparator.ForeColor = SystemColors.ControlText;
      this.toolStripSeparator.Name = "toolStripSeparator";
      this.toolStripSeparator.Size = new Size(239, 6);
      this.settingsToolStripMenuItem.BackColor = SystemColors.Window;
      this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
      this.settingsToolStripMenuItem.Size = new Size(242, 22);
      this.settingsToolStripMenuItem.Text = "Settings";
      this.settingsToolStripMenuItem.Click += new EventHandler(this.ToolStripMenuItemSettings_Click);
      this.aboutToolStripMenuItem.BackColor = SystemColors.Window;
      this.aboutToolStripMenuItem.DisplayStyle = ToolStripItemDisplayStyle.Text;
      this.aboutToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[1]
      {
        (ToolStripItem) this.aboutGDRomExplorerToolStripMenuItem
      });
      this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
      this.aboutToolStripMenuItem.Size = new Size(24, 20);
      this.aboutToolStripMenuItem.Text = "?";
      this.aboutGDRomExplorerToolStripMenuItem.BackColor = SystemColors.Window;
      this.aboutGDRomExplorerToolStripMenuItem.Name = "aboutGDRomExplorerToolStripMenuItem";
      this.aboutGDRomExplorerToolStripMenuItem.Size = new Size(107, 22);
      this.aboutGDRomExplorerToolStripMenuItem.Text = "About";
      this.aboutGDRomExplorerToolStripMenuItem.Click += new EventHandler(this.ToolStripMenuItemAboutGDRomExplorer_Click);
      this.menuStrip.BackColor = SystemColors.Window;
      this.menuStrip.Items.AddRange(new ToolStripItem[4]
      {
        (ToolStripItem) this.fileToolStripMenuItem,
        (ToolStripItem) this.editToolStripMenuItem,
        (ToolStripItem) this.toolsToolStripMenuItem,
        (ToolStripItem) this.aboutToolStripMenuItem
      });
      this.menuStrip.Location = new Point(0, 0);
      this.menuStrip.Name = "menuStrip";
      this.menuStrip.ShowItemToolTips = true;
      this.menuStrip.Size = new Size(784, 24);
      this.menuStrip.TabIndex = 25;
      this.menuStrip.Text = "menuStrip";
      this.editToolStripMenuItem.Name = "editToolStripMenuItem";
      this.editToolStripMenuItem.Size = new Size(39, 20);
      this.editToolStripMenuItem.Text = "&Edit";
      this.editToolStripMenuItem.DropDownOpening += new EventHandler(this.ToolStripMenuItemEdit_DropDownOpening);
      this.discView.AutoSize = true;
      this.discView.BackColor = SystemColors.Window;
      this.discView.Dock = DockStyle.Fill;
      this.discView.Location = new Point(0, 82);
      this.discView.Name = "discView";
      this.discView.Padding = new Padding(4);
      this.discView.Size = new Size(784, 458);
      this.discView.TabIndex = 31;
      this.discViewOpener.AutoSize = true;
      this.discViewOpener.ButtonText = "&Open";
      this.discViewOpener.Dock = DockStyle.Top;
      this.discViewOpener.FileDialogFilters = (List<string>) null;
      this.discViewOpener.InitialDirectory = "";
      this.discViewOpener.Location = new Point(0, 24);
      this.discViewOpener.MinimumSize = new Size(0, 58);
      this.discViewOpener.Name = "discViewOpener";
      this.discViewOpener.NotifyStatusEvents = true;
      this.discViewOpener.Padding = new Padding(4);
      this.discViewOpener.Size = new Size(784, 58);
      this.discViewOpener.TabIndex = 28;
      this.AllowDrop = true;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoScroll = true;
      this.AutoSize = true;
      this.BackColor = SystemColors.Window;
      this.BackgroundImageLayout = ImageLayout.None;
      this.ClientSize = new Size(784, 562);
      this.Controls.Add((Control) this.discView);
      this.Controls.Add((Control) this.discViewOpener);
      this.Controls.Add((Control) this.statusStrip);
      this.Controls.Add((Control) this.menuStrip);
      this.DoubleBuffered = true;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MainMenuStrip = this.menuStrip;
      this.MinimumSize = new Size(800, 600);
      this.Name = nameof (GDRomExplorerForm);
      this.StartPosition = FormStartPosition.CenterScreen;
      this.Text = "GD-ROM Explorer";
      this.Load += new EventHandler(this.GDRomExplorerForm_Load);
      this.statusStrip.ResumeLayout(false);
      this.statusStrip.PerformLayout();
      this.menuStrip.ResumeLayout(false);
      this.menuStrip.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
