﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Forms.FormPrimaryVolumeDescriptor
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.UserControls;
using ImageReader.ISO9660.VolumeDescriptors;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace GDRomExplorer.Forms
{
  public class FormPrimaryVolumeDescriptor : Form
  {
    private PrimaryVolumeDescriptor PrimaryVolumeDescriptor;
    private IContainer components;
    private Button btClose;
    private PrimaryVolumeDescriptorViewer primaryVolumeDescriptorViewer;

    public FormPrimaryVolumeDescriptor(PrimaryVolumeDescriptor PrimaryVolumeDescriptor)
    {
      this.InitializeComponent();
      this.PrimaryVolumeDescriptor = PrimaryVolumeDescriptor;
    }

    private void FormInfo_Load(object sender, EventArgs e) => this.primaryVolumeDescriptorViewer.LoadPrimaryVolumeDescriptor(this.PrimaryVolumeDescriptor);

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormPrimaryVolumeDescriptor));
      this.btClose = new Button();
      this.primaryVolumeDescriptorViewer = new PrimaryVolumeDescriptorViewer();
      this.SuspendLayout();
      this.btClose.DialogResult = DialogResult.Cancel;
      this.btClose.FlatStyle = FlatStyle.Popup;
      this.btClose.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btClose.ForeColor = Color.FromArgb(248, 48, 0);
      this.btClose.Location = new Point(466, 198);
      this.btClose.Name = "btClose";
      this.btClose.Size = new Size(108, 22);
      this.btClose.TabIndex = 1;
      this.btClose.Text = "Close";
      this.btClose.UseVisualStyleBackColor = false;
      this.primaryVolumeDescriptorViewer.AutoSize = true;
      this.primaryVolumeDescriptorViewer.Location = new Point(5, 8);
      this.primaryVolumeDescriptorViewer.Margin = new Padding(1);
      this.primaryVolumeDescriptorViewer.Name = "primaryVolumeDescriptorViewer";
      this.primaryVolumeDescriptorViewer.Size = new Size(571, 181);
      this.primaryVolumeDescriptorViewer.TabIndex = 2;
      this.AcceptButton = (IButtonControl) this.btClose;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = SystemColors.Window;
      this.CancelButton = (IButtonControl) this.btClose;
      this.ClientSize = new Size(584, 226);
      this.Controls.Add((Control) this.primaryVolumeDescriptorViewer);
      this.Controls.Add((Control) this.btClose);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (FormPrimaryVolumeDescriptor);
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Primary Volume Descriptor Viewer";
      this.Load += new EventHandler(this.FormInfo_Load);
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
