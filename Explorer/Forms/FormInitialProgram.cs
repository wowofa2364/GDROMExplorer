﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Forms.FormInitialProgram
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using SEGATools.Security;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace GDRomExplorer.Forms
{
  public class FormInitialProgram : Form
  {
    private InitialProgramExtended ip;
    private string basePath;
    private IContainer components;
    private Button btClose;
    private TabControl tcInitialProgram;
    private TabPage tpGeneralSettings;
    private InitialProgramGeneralSettingsViewer ipGeneralSettingsViewer;
    private TabPage tpPeripheralSettings;
    private InitialProgramPeripheralsViewer ipPeriperalsViewer;
    private TabPage tpMRImages;
    private InitialProgramImagesViewer ipImagesViewer;
    private TabPage tpLibraryReferences;
    private TabPage tpTrackList;
    private InitialProgramLibraryReferences ipLibraryReferences;
    private InitialProgramTocViewer ipTocViewer;

    public FormInitialProgram(InitialProgramExtended ip, string basePath)
    {
      this.InitializeComponent();
      this.StartPosition = FormStartPosition.CenterParent;
      this.ip = ip;
      this.basePath = basePath;
    }

    private void FormIp_Load(object sender, EventArgs e)
    {
      this.Text = "Initial Program Viewer: " + this.ip.FileName;
      this.ipGeneralSettingsViewer.LoadInitialProgram(this.ip);
      this.ipPeriperalsViewer.LoadInitialProgram(this.ip);
      this.ipImagesViewer.LoadInitialProgram(this.ip, this.basePath);
      this.ipLibraryReferences.LoadInitialProgram(this.ip);
      this.ipTocViewer.LoadInitialProgram(this.ip);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.btClose = new Button();
      this.tcInitialProgram = new TabControl();
      this.tpGeneralSettings = new TabPage();
      this.ipGeneralSettingsViewer = new InitialProgramGeneralSettingsViewer();
      this.tpPeripheralSettings = new TabPage();
      this.ipPeriperalsViewer = new InitialProgramPeripheralsViewer();
      this.tpMRImages = new TabPage();
      this.ipImagesViewer = new InitialProgramImagesViewer();
      this.tpLibraryReferences = new TabPage();
      this.ipLibraryReferences = new InitialProgramLibraryReferences();
      this.tpTrackList = new TabPage();
      this.ipTocViewer = new InitialProgramTocViewer();
      this.tcInitialProgram.SuspendLayout();
      this.tpGeneralSettings.SuspendLayout();
      this.tpPeripheralSettings.SuspendLayout();
      this.tpMRImages.SuspendLayout();
      this.tpLibraryReferences.SuspendLayout();
      this.tpTrackList.SuspendLayout();
      this.SuspendLayout();
      this.btClose.DialogResult = DialogResult.Cancel;
      this.btClose.FlatStyle = FlatStyle.Popup;
      this.btClose.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btClose.ForeColor = Color.FromArgb(248, 48, 0);
      this.btClose.Location = new Point(378, 292);
      this.btClose.Name = "btClose";
      this.btClose.Size = new Size(108, 22);
      this.btClose.TabIndex = 0;
      this.btClose.Text = "Close";
      this.btClose.UseVisualStyleBackColor = false;
      this.tcInitialProgram.Controls.Add((Control) this.tpGeneralSettings);
      this.tcInitialProgram.Controls.Add((Control) this.tpPeripheralSettings);
      this.tcInitialProgram.Controls.Add((Control) this.tpMRImages);
      this.tcInitialProgram.Controls.Add((Control) this.tpLibraryReferences);
      this.tcInitialProgram.Controls.Add((Control) this.tpTrackList);
      this.tcInitialProgram.Location = new Point(12, 12);
      this.tcInitialProgram.Name = "tcInitialProgram";
      this.tcInitialProgram.SelectedIndex = 0;
      this.tcInitialProgram.Size = new Size(474, 274);
      this.tcInitialProgram.TabIndex = 0;
      this.tpGeneralSettings.BackColor = Color.Transparent;
      this.tpGeneralSettings.Controls.Add((Control) this.ipGeneralSettingsViewer);
      this.tpGeneralSettings.Cursor = Cursors.Default;
      this.tpGeneralSettings.Location = new Point(4, 22);
      this.tpGeneralSettings.Name = "tpGeneralSettings";
      this.tpGeneralSettings.Padding = new Padding(3);
      this.tpGeneralSettings.Size = new Size(466, 248);
      this.tpGeneralSettings.TabIndex = 0;
      this.tpGeneralSettings.Text = "General Settings";
      this.tpGeneralSettings.UseVisualStyleBackColor = true;
      this.ipGeneralSettingsViewer.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.ipGeneralSettingsViewer.AutoSize = true;
      this.ipGeneralSettingsViewer.BackColor = Color.Transparent;
      this.ipGeneralSettingsViewer.Location = new Point(3, 3);
      this.ipGeneralSettingsViewer.Name = "ipGeneralSettingsViewer";
      this.ipGeneralSettingsViewer.Size = new Size(460, 243);
      this.ipGeneralSettingsViewer.TabIndex = 12;
      this.tpPeripheralSettings.BackColor = Color.Transparent;
      this.tpPeripheralSettings.Controls.Add((Control) this.ipPeriperalsViewer);
      this.tpPeripheralSettings.Location = new Point(4, 22);
      this.tpPeripheralSettings.Name = "tpPeripheralSettings";
      this.tpPeripheralSettings.Padding = new Padding(3);
      this.tpPeripheralSettings.Size = new Size(466, 248);
      this.tpPeripheralSettings.TabIndex = 1;
      this.tpPeripheralSettings.Text = "Peripheral Settings";
      this.tpPeripheralSettings.UseVisualStyleBackColor = true;
      this.ipPeriperalsViewer.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.ipPeriperalsViewer.AutoSize = true;
      this.ipPeriperalsViewer.BackColor = Color.Transparent;
      this.ipPeriperalsViewer.Location = new Point(3, 3);
      this.ipPeriperalsViewer.Name = "ipPeriperalsViewer";
      this.ipPeriperalsViewer.Size = new Size(463, 242);
      this.ipPeriperalsViewer.TabIndex = 13;
      this.tpMRImages.BackColor = Color.Transparent;
      this.tpMRImages.Controls.Add((Control) this.ipImagesViewer);
      this.tpMRImages.Location = new Point(4, 22);
      this.tpMRImages.Name = "tpMRImages";
      this.tpMRImages.Padding = new Padding(3);
      this.tpMRImages.Size = new Size(466, 248);
      this.tpMRImages.TabIndex = 2;
      this.tpMRImages.Text = "MR Images";
      this.tpMRImages.UseVisualStyleBackColor = true;
      this.ipImagesViewer.BackColor = Color.Transparent;
      this.ipImagesViewer.Location = new Point(0, 6);
      this.ipImagesViewer.Name = "ipImagesViewer";
      this.ipImagesViewer.Size = new Size(463, 239);
      this.ipImagesViewer.TabIndex = 0;
      this.tpLibraryReferences.BackColor = Color.Transparent;
      this.tpLibraryReferences.Controls.Add((Control) this.ipLibraryReferences);
      this.tpLibraryReferences.Location = new Point(4, 22);
      this.tpLibraryReferences.Name = "tpLibraryReferences";
      this.tpLibraryReferences.Padding = new Padding(3);
      this.tpLibraryReferences.Size = new Size(466, 248);
      this.tpLibraryReferences.TabIndex = 3;
      this.tpLibraryReferences.Text = "Library References";
      this.tpLibraryReferences.UseVisualStyleBackColor = true;
      this.ipLibraryReferences.BackColor = Color.Transparent;
      this.ipLibraryReferences.Dock = DockStyle.Fill;
      this.ipLibraryReferences.Location = new Point(3, 3);
      this.ipLibraryReferences.Name = "ipLibraryReferences";
      this.ipLibraryReferences.Size = new Size(460, 242);
      this.ipLibraryReferences.TabIndex = 0;
      this.tpTrackList.Controls.Add((Control) this.ipTocViewer);
      this.tpTrackList.Location = new Point(4, 22);
      this.tpTrackList.Name = "tpTrackList";
      this.tpTrackList.Padding = new Padding(3);
      this.tpTrackList.Size = new Size(466, 248);
      this.tpTrackList.TabIndex = 4;
      this.tpTrackList.Text = "Track List";
      this.tpTrackList.UseVisualStyleBackColor = true;
      this.ipTocViewer.BackColor = Color.Transparent;
      this.ipTocViewer.Dock = DockStyle.Fill;
      this.ipTocViewer.Location = new Point(3, 3);
      this.ipTocViewer.Name = "ipTocViewer";
      this.ipTocViewer.Size = new Size(460, 242);
      this.ipTocViewer.TabIndex = 0;
      this.AcceptButton = (IButtonControl) this.btClose;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = SystemColors.Window;
      this.CancelButton = (IButtonControl) this.btClose;
      this.ClientSize = new Size(497, 321);
      this.Controls.Add((Control) this.tcInitialProgram);
      this.Controls.Add((Control) this.btClose);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (FormInitialProgram);
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "IP Viewer";
      this.Load += new EventHandler(this.FormIp_Load);
      this.tcInitialProgram.ResumeLayout(false);
      this.tpGeneralSettings.ResumeLayout(false);
      this.tpGeneralSettings.PerformLayout();
      this.tpPeripheralSettings.ResumeLayout(false);
      this.tpPeripheralSettings.PerformLayout();
      this.tpMRImages.ResumeLayout(false);
      this.tpLibraryReferences.ResumeLayout(false);
      this.tpTrackList.ResumeLayout(false);
      this.ResumeLayout(false);
    }
  }
}
