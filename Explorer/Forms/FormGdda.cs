﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Forms.FormGdda
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using SEGATools.Audio;
using SEGATools.DiscFileSystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace GDRomExplorer.Forms
{
  public class FormGdda : Form
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private IDiscSession discSession;
    private List<IDiscTrack> selectedDiscTracks;
    private IContainer components;
    private Button btConvertSelection;
    private GroupBox groupBox;
    private Button btUnselectAll;
    private Button btInverseSelection;
    private Button btSelectAll;
    private ListView listViewTracks;
    private ColumnHeader columnHeader1;
    private ColumnHeader columnHeader2;
    private Button btClose;
    private FolderBrowserDialog folderBrowserDialogForAudio;
    private SaveFileDialog saveFileDialogForAudio;
    private AudioConversionSettingsViewer audioConversionSettingsViewer;

    public List<IDiscTrack> SelectedDiscTracks => this.selectedDiscTracks.ToList<IDiscTrack>();

    public AudioConversionSettings AudioConversionSettings => this.audioConversionSettingsViewer.AudioConversionSettings;

    public FormGdda(IDiscSession discSession)
    {
      this.InitializeComponent();
      this.discSession = discSession;
      this.selectedDiscTracks = new List<IDiscTrack>();
    }

    private void frmGdda_Load(object sender, EventArgs e)
    {
      this.LoadTracks();
      this.UpdateConvertButton();
      this.listViewTracks.ItemCheck += new ItemCheckEventHandler(this.listViewTracks_ItemCheck);
      this.listViewTracks.ItemChecked += new ItemCheckedEventHandler(this.listViewTracks_ItemChecked);
      this.listViewTracks.ItemSelectionChanged += new ListViewItemSelectionChangedEventHandler(this.listViewTracks_ItemSelectionChanged);
    }

    private void listViewTracks_ItemCheck(object sender, ItemCheckEventArgs e) => this.UpdateConvertButton();

    private void listViewTracks_ItemChecked(object sender, ItemCheckedEventArgs e) => this.UpdateConvertButton();

    private void listViewTracks_ItemSelectionChanged(
      object sender,
      ListViewItemSelectionChangedEventArgs e)
    {
      this.UpdateConvertButton();
    }

    private void UpdateConvertButton() => this.btConvertSelection.Enabled = this.listViewTracks.CheckedItems.Count > 0;

    private void LoadTracks()
    {
      this.listViewTracks.BeginUpdate();
      this.listViewTracks.Items.Clear();
      foreach (IDiscTrack audioTrack in this.discSession.AudioTracks)
        this.listViewTracks.Items.Add(new ListViewItem()
        {
          SubItems = {
            audioTrack.ToString()
          },
          Tag = (object) audioTrack,
          Checked = true,
          ToolTipText = audioTrack.FileName
        });
      this.listViewTracks.EndUpdate();
    }

    private void EnableAll(bool isEnabled)
    {
      foreach (ListViewItem listViewItem in this.listViewTracks.Items)
        listViewItem.Checked = isEnabled;
    }

    private void InverseSelection()
    {
      foreach (ListViewItem listViewItem in this.listViewTracks.Items)
        listViewItem.Checked = !listViewItem.Checked;
    }

    private void btSelectAll_Click(object sender, EventArgs e)
    {
      this.EnableAll(true);
      this.UpdateConvertButton();
    }

    private void btUnselectAll_Click(object sender, EventArgs e)
    {
      this.EnableAll(false);
      this.UpdateConvertButton();
    }

    private void btInverseSelection_Click(object sender, EventArgs e)
    {
      this.InverseSelection();
      this.UpdateConvertButton();
    }

    private void btConvertSelection_Click(object sender, EventArgs e)
    {
      this.selectedDiscTracks.Clear();
      foreach (ListViewItem listViewItem in this.listViewTracks.Items)
      {
        if (listViewItem.Checked)
          this.selectedDiscTracks.Add(listViewItem.Tag as IDiscTrack);
      }
      if (this.selectedDiscTracks.Count > 0)
        this.DialogResult = DialogResult.OK;
      else
        this.DialogResult = DialogResult.None;
    }

    private void btClose_Click(object sender, EventArgs e) => this.DialogResult = DialogResult.Cancel;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormGdda));
      this.btConvertSelection = new Button();
      this.groupBox = new GroupBox();
      this.listViewTracks = new ListView();
      this.columnHeader1 = new ColumnHeader();
      this.columnHeader2 = new ColumnHeader();
      this.btUnselectAll = new Button();
      this.btInverseSelection = new Button();
      this.btSelectAll = new Button();
      this.btClose = new Button();
      this.folderBrowserDialogForAudio = new FolderBrowserDialog();
      this.saveFileDialogForAudio = new SaveFileDialog();
      this.audioConversionSettingsViewer = new AudioConversionSettingsViewer();
      this.groupBox.SuspendLayout();
      this.SuspendLayout();
      this.btConvertSelection.FlatStyle = FlatStyle.Popup;
      this.btConvertSelection.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btConvertSelection.ForeColor = Color.FromArgb(248, 48, 0);
      this.btConvertSelection.Location = new Point(248, 225);
      this.btConvertSelection.Name = "btConvertSelection";
      this.btConvertSelection.Size = new Size(108, 22);
      this.btConvertSelection.TabIndex = 5;
      this.btConvertSelection.Text = "Convert to CDDA";
      this.btConvertSelection.UseVisualStyleBackColor = false;
      this.btConvertSelection.Click += new EventHandler(this.btConvertSelection_Click);
      this.groupBox.Controls.Add((Control) this.listViewTracks);
      this.groupBox.Controls.Add((Control) this.btUnselectAll);
      this.groupBox.Controls.Add((Control) this.btInverseSelection);
      this.groupBox.Controls.Add((Control) this.btSelectAll);
      this.groupBox.Location = new Point(12, 12);
      this.groupBox.Name = "groupBox";
      this.groupBox.Size = new Size(344, 183);
      this.groupBox.TabIndex = 6;
      this.groupBox.TabStop = false;
      this.groupBox.Text = "Select GD-DA tracks to convert:";
      this.listViewTracks.CheckBoxes = true;
      this.listViewTracks.Columns.AddRange(new ColumnHeader[2]
      {
        this.columnHeader1,
        this.columnHeader2
      });
      this.listViewTracks.FullRowSelect = true;
      this.listViewTracks.HeaderStyle = ColumnHeaderStyle.None;
      this.listViewTracks.HideSelection = false;
      this.listViewTracks.Location = new Point(6, 47);
      this.listViewTracks.MultiSelect = false;
      this.listViewTracks.Name = "listViewTracks";
      this.listViewTracks.ShowItemToolTips = true;
      this.listViewTracks.Size = new Size(332, 130);
      this.listViewTracks.TabIndex = 3;
      this.listViewTracks.UseCompatibleStateImageBehavior = false;
      this.listViewTracks.View = View.Details;
      this.columnHeader1.Text = "";
      this.columnHeader1.Width = 20;
      this.columnHeader2.Text = "Track name";
      this.columnHeader2.Width = 280;
      this.btUnselectAll.FlatStyle = FlatStyle.Popup;
      this.btUnselectAll.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btUnselectAll.ForeColor = Color.FromArgb(248, 48, 0);
      this.btUnselectAll.Location = new Point(118, 19);
      this.btUnselectAll.Name = "btUnselectAll";
      this.btUnselectAll.Size = new Size(106, 22);
      this.btUnselectAll.TabIndex = 1;
      this.btUnselectAll.Text = "Unselect all";
      this.btUnselectAll.UseVisualStyleBackColor = false;
      this.btUnselectAll.Click += new EventHandler(this.btUnselectAll_Click);
      this.btInverseSelection.FlatStyle = FlatStyle.Popup;
      this.btInverseSelection.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btInverseSelection.ForeColor = Color.FromArgb(248, 48, 0);
      this.btInverseSelection.Location = new Point(230, 19);
      this.btInverseSelection.Name = "btInverseSelection";
      this.btInverseSelection.Size = new Size(106, 22);
      this.btInverseSelection.TabIndex = 2;
      this.btInverseSelection.Text = "Inverse selection";
      this.btInverseSelection.UseVisualStyleBackColor = false;
      this.btInverseSelection.Click += new EventHandler(this.btInverseSelection_Click);
      this.btSelectAll.FlatStyle = FlatStyle.Popup;
      this.btSelectAll.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btSelectAll.ForeColor = Color.FromArgb(248, 48, 0);
      this.btSelectAll.Location = new Point(6, 19);
      this.btSelectAll.Name = "btSelectAll";
      this.btSelectAll.Size = new Size(106, 22);
      this.btSelectAll.TabIndex = 0;
      this.btSelectAll.Text = "Select all";
      this.btSelectAll.UseVisualStyleBackColor = false;
      this.btSelectAll.Click += new EventHandler(this.btSelectAll_Click);
      this.btClose.DialogResult = DialogResult.Cancel;
      this.btClose.FlatStyle = FlatStyle.Popup;
      this.btClose.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btClose.ForeColor = Color.FromArgb(248, 48, 0);
      this.btClose.Location = new Point(12, 225);
      this.btClose.Name = "btClose";
      this.btClose.Size = new Size(108, 22);
      this.btClose.TabIndex = 4;
      this.btClose.Text = "Close";
      this.btClose.UseVisualStyleBackColor = false;
      this.btClose.Click += new EventHandler(this.btClose_Click);
      this.audioConversionSettingsViewer.AutoSize = true;
      this.audioConversionSettingsViewer.BackColor = Color.Transparent;
      this.audioConversionSettingsViewer.Location = new Point(13, 201);
      this.audioConversionSettingsViewer.Name = "audioConversionSettingsViewer";
      this.audioConversionSettingsViewer.Size = new Size(343, 23);
      this.audioConversionSettingsViewer.TabIndex = 7;
      this.AcceptButton = (IButtonControl) this.btConvertSelection;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = SystemColors.Window;
      this.CancelButton = (IButtonControl) this.btClose;
      this.ClientSize = new Size(368, 259);
      this.Controls.Add((Control) this.audioConversionSettingsViewer);
      this.Controls.Add((Control) this.btClose);
      this.Controls.Add((Control) this.groupBox);
      this.Controls.Add((Control) this.btConvertSelection);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (FormGdda);
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Convert GD-DA to CD-DA";
      this.Load += new EventHandler(this.frmGdda_Load);
      this.groupBox.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
