﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Forms.FormAbout
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.Properties;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace GDRomExplorer.Forms
{
  internal class FormAbout : Form
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private IContainer components;
    private TableLayoutPanel tableLayoutPanel;
    private PictureBox logoPictureBox;
    private TextBox textBoxDescription;
    private TableLayoutPanel tableLayoutPanelButton;
    private Button okButton;
    private PictureBox pictureBoxPaypal;
    private Panel panel1;
    private LinkLabel llblog;

    public FormAbout()
    {
      this.InitializeComponent();
      this.Text = string.Format("About {0}", (object) this.AssemblyTitle);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.AppendLine(string.Format("{0}", (object) this.AssemblyProduct));
      stringBuilder.AppendLine(string.Format("Version {0} build {1}", (object) this.AssemblyVersion, (object) this.AssemblyBuildSHA1));
      stringBuilder.Append(string.Format("Created by {0} ({1})", (object) this.AssemblyCompany, (object) this.AssemblyCopyright));
      this.textBoxDescription.Text = stringBuilder.ToString();
      this.llblog.Text = GDRomExplorer.Resources.Strings.GoToBlogLinkTitle;
    }

    public string AssemblyTitle
    {
      get
      {
        object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof (AssemblyTitleAttribute), false);
        if (customAttributes.Length > 0)
        {
          AssemblyTitleAttribute assemblyTitleAttribute = (AssemblyTitleAttribute) customAttributes[0];
          if (!string.IsNullOrEmpty(assemblyTitleAttribute.Title))
            return assemblyTitleAttribute.Title;
        }
        return Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
      }
    }

    public string AssemblyVersion
    {
      get
      {
        Version version = Assembly.GetExecutingAssembly().GetName().Version;
        return string.Format("{0}.{1}.{2}", (object) version.Major, (object) version.Minor, (object) version.Build);
      }
    }

    public string AssemblyBuildSHA1
    {
      get
      {
        try
        {
          return typeof (AssemblyGitBuildSHA1).Assembly.GetCustomAttributes(typeof (AssemblyGitBuildSHA1), false).Cast<AssemblyGitBuildSHA1>().First<AssemblyGitBuildSHA1>().GitBuildSHA1;
        }
        catch (Exception ex)
        {
          logger.Error(ex);
          return "unknown";
        }
      }
    }

    public string AssemblyBuildBranch
    {
      get
      {
        try
        {
          return typeof (AssemblyGitBuildBranch).Assembly.GetCustomAttributes(typeof (AssemblyGitBuildBranch), false).Cast<AssemblyGitBuildBranch>().First<AssemblyGitBuildBranch>().GitBuildBranch;
        }
        catch (Exception ex)
        {
          logger.Error(ex);
          return "unknown";
        }
      }
    }

    public string AssemblyDescription
    {
      get
      {
        object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof (AssemblyDescriptionAttribute), false);
        return customAttributes.Length == 0 ? "" : ((AssemblyDescriptionAttribute) customAttributes[0]).Description;
      }
    }

    public string AssemblyProduct
    {
      get
      {
        object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof (AssemblyProductAttribute), false);
        return customAttributes.Length == 0 ? "" : ((AssemblyProductAttribute) customAttributes[0]).Product;
      }
    }

    public string AssemblyCopyright
    {
      get
      {
        object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof (AssemblyCopyrightAttribute), false);
        return customAttributes.Length == 0 ? "" : ((AssemblyCopyrightAttribute) customAttributes[0]).Copyright;
      }
    }

    public string AssemblyCompany
    {
      get
      {
        object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof (AssemblyCompanyAttribute), false);
        return customAttributes.Length == 0 ? "" : ((AssemblyCompanyAttribute) customAttributes[0]).Company;
      }
    }

    private void paypalDonatePictureBox_Click(object sender, EventArgs e)
    {
      try
      {
        Process.Start(Settings.Default.PayPalDonateUrl);
      }
      catch
      {
      }
    }

    private void llblog_LinkClicked(object sender, EventArgs e)
    {
      try
      {
        Process.Start(Settings.Default.BlogUrl);
      }
      catch
      {
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormAbout));
      this.tableLayoutPanel = new TableLayoutPanel();
      this.logoPictureBox = new PictureBox();
      this.textBoxDescription = new TextBox();
      this.tableLayoutPanelButton = new TableLayoutPanel();
      this.pictureBoxPaypal = new PictureBox();
      this.panel1 = new Panel();
      this.llblog = new LinkLabel();
      this.okButton = new Button();
      this.tableLayoutPanel.SuspendLayout();
      ((ISupportInitialize) this.logoPictureBox).BeginInit();
      this.tableLayoutPanelButton.SuspendLayout();
      ((ISupportInitialize) this.pictureBoxPaypal).BeginInit();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      this.tableLayoutPanel.ColumnCount = 2;
      this.tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33f));
      this.tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 67f));
      this.tableLayoutPanel.Controls.Add((Control) this.logoPictureBox, 0, 0);
      this.tableLayoutPanel.Controls.Add((Control) this.textBoxDescription, 1, 0);
      this.tableLayoutPanel.Controls.Add((Control) this.tableLayoutPanelButton, 1, 1);
      this.tableLayoutPanel.Dock = DockStyle.Fill;
      this.tableLayoutPanel.Location = new Point(9, 9);
      this.tableLayoutPanel.Name = "tableLayoutPanel";
      this.tableLayoutPanel.RowCount = 2;
      this.tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 42.64706f));
      this.tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 57.35294f));
      this.tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanel.Size = new Size(417, 136);
      this.tableLayoutPanel.TabIndex = 0;
      this.logoPictureBox.Dock = DockStyle.Fill;
      this.logoPictureBox.Image = (Image) componentResourceManager.GetObject("logoPictureBox.Image");
      this.logoPictureBox.Location = new Point(3, 3);
      this.logoPictureBox.Name = "logoPictureBox";
      this.tableLayoutPanel.SetRowSpan((Control) this.logoPictureBox, 2);
      this.logoPictureBox.Size = new Size(131, 130);
      this.logoPictureBox.TabIndex = 12;
      this.logoPictureBox.TabStop = false;
      this.textBoxDescription.BackColor = SystemColors.Window;
      this.textBoxDescription.BorderStyle = BorderStyle.None;
      this.textBoxDescription.Dock = DockStyle.Fill;
      this.textBoxDescription.Location = new Point(143, 3);
      this.textBoxDescription.Margin = new Padding(6, 3, 3, 3);
      this.textBoxDescription.Multiline = true;
      this.textBoxDescription.Name = "textBoxDescription";
      this.textBoxDescription.ReadOnly = true;
      this.textBoxDescription.Size = new Size(271, 52);
      this.textBoxDescription.TabIndex = 23;
      this.textBoxDescription.TabStop = false;
      this.textBoxDescription.Text = "Description";
      this.tableLayoutPanelButton.ColumnCount = 2;
      this.tableLayoutPanelButton.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
      this.tableLayoutPanelButton.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
      this.tableLayoutPanelButton.Controls.Add((Control) this.pictureBoxPaypal, 1, 0);
      this.tableLayoutPanelButton.Controls.Add((Control) this.panel1, 0, 0);
      this.tableLayoutPanelButton.Controls.Add((Control) this.okButton, 1, 1);
      this.tableLayoutPanelButton.Dock = DockStyle.Fill;
      this.tableLayoutPanelButton.Location = new Point(140, 61);
      this.tableLayoutPanelButton.Name = "tableLayoutPanelButton";
      this.tableLayoutPanelButton.RowCount = 2;
      this.tableLayoutPanelButton.RowStyles.Add(new RowStyle(SizeType.Percent, 100f));
      this.tableLayoutPanelButton.RowStyles.Add(new RowStyle(SizeType.Absolute, 37f));
      this.tableLayoutPanelButton.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
      this.tableLayoutPanelButton.Size = new Size(274, 72);
      this.tableLayoutPanelButton.TabIndex = 27;
      this.pictureBoxPaypal.Image = (Image) GDRomExplorer.Properties.Resources.paypal_donate;
      this.pictureBoxPaypal.Location = new Point(140, 3);
      this.pictureBoxPaypal.Name = "pictureBoxPaypal";
      this.pictureBoxPaypal.Size = new Size(131, 29);
      this.pictureBoxPaypal.SizeMode = PictureBoxSizeMode.CenterImage;
      this.pictureBoxPaypal.TabIndex = 28;
      this.pictureBoxPaypal.TabStop = false;
      this.pictureBoxPaypal.Click += new EventHandler(this.paypalDonatePictureBox_Click);
      this.panel1.Controls.Add((Control) this.llblog);
      this.panel1.Dock = DockStyle.Fill;
      this.panel1.Location = new Point(3, 3);
      this.panel1.Name = "panel1";
      this.panel1.Size = new Size(131, 29);
      this.panel1.TabIndex = 33;
      this.llblog.ActiveLinkColor = Color.FromArgb(248, 48, 0);
      this.llblog.AutoSize = true;
      this.llblog.LinkBehavior = LinkBehavior.HoverUnderline;
      this.llblog.LinkColor = Color.FromArgb(248, 48, 0);
      this.llblog.Location = new Point(25, 8);
      this.llblog.Name = "llblog";
      this.llblog.Size = new Size(40, 13);
      this.llblog.TabIndex = 24;
      this.llblog.TabStop = true;
      this.llblog.Text = "blogUrl";
      this.llblog.TextAlign = ContentAlignment.MiddleCenter;
      this.llblog.Click += new EventHandler(this.llblog_LinkClicked);
      this.okButton.Anchor = AnchorStyles.None;
      this.okButton.DialogResult = DialogResult.OK;
      this.okButton.FlatStyle = FlatStyle.Popup;
      this.okButton.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.okButton.ForeColor = Color.FromArgb(248, 48, 0);
      this.okButton.Location = new Point(151, 42);
      this.okButton.Name = "okButton";
      this.okButton.Size = new Size(108, 22);
      this.okButton.TabIndex = 31;
      this.okButton.Text = "&Ok";
      this.okButton.UseVisualStyleBackColor = false;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = SystemColors.Window;
      this.ClientSize = new Size(435, 154);
      this.Controls.Add((Control) this.tableLayoutPanel);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (FormAbout);
      this.Padding = new Padding(9);
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "AboutBox";
      this.tableLayoutPanel.ResumeLayout(false);
      this.tableLayoutPanel.PerformLayout();
      ((ISupportInitialize) this.logoPictureBox).EndInit();
      this.tableLayoutPanelButton.ResumeLayout(false);
      ((ISupportInitialize) this.pictureBoxPaypal).EndInit();
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.ResumeLayout(false);
    }
  }
}
