﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Forms.FormAudioConversionSettings
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using SEGATools.Audio;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace GDRomExplorer.Forms
{
  public class FormAudioConversionSettings : Form
  {
    private IContainer components;
    private Button btOk;
    private Button btCancel;
    private AudioConversionSettingsViewer audioConversionSettingsViewer;

    public AudioConversionSettings AudioConversionSettings => this.audioConversionSettingsViewer.AudioConversionSettings;

    public FormAudioConversionSettings() => this.InitializeComponent();

    private void btCancel_Click(object sender, EventArgs e) => this.Close();

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.btOk = new Button();
      this.btCancel = new Button();
      this.audioConversionSettingsViewer = new AudioConversionSettingsViewer();
      this.SuspendLayout();
      this.btOk.DialogResult = DialogResult.OK;
      this.btOk.FlatStyle = FlatStyle.Popup;
      this.btOk.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btOk.ForeColor = Color.FromArgb(248, 48, 0);
      this.btOk.Location = new Point(195, 52);
      this.btOk.Name = "btOk";
      this.btOk.Size = new Size((int) sbyte.MaxValue, 22);
      this.btOk.TabIndex = 1;
      this.btOk.Text = "&OK";
      this.btOk.UseVisualStyleBackColor = false;
      this.btCancel.DialogResult = DialogResult.Cancel;
      this.btCancel.FlatStyle = FlatStyle.Popup;
      this.btCancel.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btCancel.ForeColor = Color.FromArgb(248, 48, 0);
      this.btCancel.Location = new Point(12, 52);
      this.btCancel.Name = "btCancel";
      this.btCancel.Size = new Size((int) sbyte.MaxValue, 22);
      this.btCancel.TabIndex = 2;
      this.btCancel.Text = "&Cancel";
      this.btCancel.UseVisualStyleBackColor = false;
      this.btCancel.Click += new EventHandler(this.btCancel_Click);
      this.audioConversionSettingsViewer.AutoSize = true;
      this.audioConversionSettingsViewer.BackColor = Color.Transparent;
      this.audioConversionSettingsViewer.Location = new Point(9, 12);
      this.audioConversionSettingsViewer.Name = "audioConversionSettingsViewer";
      this.audioConversionSettingsViewer.Size = new Size(325, 23);
      this.audioConversionSettingsViewer.TabIndex = 3;
      this.AcceptButton = (IButtonControl) this.btOk;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = SystemColors.Window;
      this.CancelButton = (IButtonControl) this.btCancel;
      this.ClientSize = new Size(334, 86);
      this.Controls.Add((Control) this.audioConversionSettingsViewer);
      this.Controls.Add((Control) this.btCancel);
      this.Controls.Add((Control) this.btOk);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (FormAudioConversionSettings);
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Audio Conversion Options";
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
