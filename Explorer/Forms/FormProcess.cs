﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Forms.FormProcess
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using GDRomExplorer.Others;
using GDRomExplorer.Resources;
using SEGATools.Audio;
using SEGATools.Disc;
using SEGATools.Encrypt;
using SEGATools.GDEmu;
using SEGATools.Scanner;
using SEGATools.UserProcess;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace GDRomExplorer.Forms
{
  public class FormProcess : Form
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private UserProcessBase process;
    private FormProcess.ProcessInfo processInfo;
    private Guid currentConversionTaskId;
    private ProgressBar progressBarUnique;
    private Label progressBarLabelUnique;
    private Point progressBarLabelUniqueInitialLocation;
    private Size progressBarLabelUniqueInitialSize;
    private bool NotifyAppStatusEvent;
    private IContainer components;
    private GroupBox groupBoxProgress;
    private ProgressBar progressBarTotal;
    private ProgressBar progressBarCurrent;
    private Label lbOutputTitle;
    private Label lbInputTitle;
    private Button btCancel;
    private Label lbOutput;
    private Label lbInput;
    private Label lbTotalPercentage;
    private Label lbCurrentPercentage;

    public static FormProcess createForDiscExtractor(
      DiscExtractor discExtractor,
      Guid TaskId)
    {
      FormProcess.ProcessInfo forDiscExtractor = FormProcess.ProcessInfo.GetProcessInfoForDiscExtractor();
      return new FormProcess((UserProcessBase) discExtractor, forDiscExtractor, TaskId, true);
    }

    public static FormProcess createForRawToWavConverter(
      Raw2WavConverter raw2WavConverter,
      Guid TaskId)
    {
      FormProcess.ProcessInfo rawToWavConverter = FormProcess.ProcessInfo.GetProcessInfoForRawToWavConverter();
      return new FormProcess((UserProcessBase) raw2WavConverter, rawToWavConverter, TaskId, true);
    }

    public static FormProcess createForDesEncryptor(
      DesEncryptDecryptTool desDecryptor,
      Guid TaskId)
    {
      FormProcess.ProcessInfo infoForDesEncryptor = FormProcess.ProcessInfo.GetProcessInfoForDesEncryptor();
      return new FormProcess((UserProcessBase) desDecryptor, infoForDesEncryptor, TaskId, true);
    }

    public static FormProcess createForDesDecryptor(
      DesEncryptDecryptTool desDecryptor,
      Guid TaskId)
    {
      FormProcess.ProcessInfo infoForDesDecryptor = FormProcess.ProcessInfo.GetProcessInfoForDesDecryptor();
      return new FormProcess((UserProcessBase) desDecryptor, infoForDesDecryptor, TaskId, true);
    }

    public static FormProcess createForFileScanner(FileScanner fileScanner, Guid TaskId)
    {
      FormProcess.ProcessInfo infoForFileScanner = FormProcess.ProcessInfo.GetProcessInfoForFileScanner();
      return new FormProcess((UserProcessBase) fileScanner, infoForFileScanner, TaskId, true);
    }

    public static FormProcess createForGDEmuConverter(
      GDEmuConverter gdEmuConverter,
      Guid TaskId)
    {
      FormProcess.ProcessInfo forGdEmuConverter = FormProcess.ProcessInfo.GetProcessInfoForGDEmuConverter();
      return new FormProcess((UserProcessBase) gdEmuConverter, forGdEmuConverter, TaskId, false);
    }

    private FormProcess(
      UserProcessBase Process,
      FormProcess.ProcessInfo ProcessInfo,
      Guid TaskId,
      bool NotifyAppStatusEvent)
    {
      this.InitializeComponent();
      this.process = Process;
      this.processInfo = ProcessInfo;
      this.Text = this.processInfo.ProcessTitle;
      this.groupBoxProgress.Text = this.processInfo.ProcessSubTitle + ":";
      this.currentConversionTaskId = TaskId;
      this.progressBarUnique = this.progressBarCurrent;
      this.progressBarLabelUnique = this.lbCurrentPercentage;
      this.progressBarLabelUniqueInitialLocation = this.progressBarLabelUnique.Location;
      this.progressBarLabelUniqueInitialSize = this.progressBarUnique.Size;
      this.NotifyAppStatusEvent = NotifyAppStatusEvent;
      this.AddEventListeners();
    }

    private void AddEventListeners()
    {
      this.process.AsyncOperationProgressChanged += new AsyncOperationProgressChangedEventHandler(this.ProgressChanged);
      this.process.AsyncOperationCompleted += new AsyncOperationCompletedEventHandler(this.ProcessCompleted);
      this.process.AsyncOperationWaitForUserConsent += new AsyncOperationProgressWaitingForUserConsentEventHandler(this.ProcessWaitForUserConsent);
      this.process.AsyncOperationUpdateUIView += new AsyncOperationProgressUpdateUIEventHandler(this.ProcessUpdateUIView);
    }

    private void RemoveEventListeners()
    {
      this.process.AsyncOperationProgressChanged -= new AsyncOperationProgressChangedEventHandler(this.ProgressChanged);
      this.process.AsyncOperationCompleted -= new AsyncOperationCompletedEventHandler(this.ProcessCompleted);
      this.process.AsyncOperationWaitForUserConsent -= new AsyncOperationProgressWaitingForUserConsentEventHandler(this.ProcessWaitForUserConsent);
      this.process.AsyncOperationUpdateUIView -= new AsyncOperationProgressUpdateUIEventHandler(this.ProcessUpdateUIView);
    }

    private void UpdateProgressBar(
      ProgressBar ProgressBar,
      Label ProgressPercentage,
      int Percentage)
    {
      if (Percentage == ProgressBar.Value)
        return;
      ProgressBar.Value = Percentage >= 100 ? 100 : Percentage;
      ProgressPercentage.Text = ProgressBar.Value.ToString() + "%";
    }

    private void NotifyNewAppStatus(string message)
    {
      if (!this.NotifyAppStatusEvent)
        return;
      AppStatus.NotifyNewAppStatus(message);
    }

    private void UpdateProgressBarsWithInitialValues()
    {
      this.progressBarUnique.Size = this.progressBarLabelUniqueInitialSize;
      this.progressBarLabelUnique.Location = this.progressBarLabelUniqueInitialLocation;
    }

    private void ResizeForOneProgressBarWithPercentage()
    {
      this.progressBarUnique.Height = this.progressBarTotal.Location.Y + this.progressBarTotal.Height - this.progressBarCurrent.Location.Y;
      this.progressBarLabelUnique.Location = new Point(this.lbCurrentPercentage.Location.X, this.progressBarCurrent.Location.Y + (this.progressBarCurrent.Height - this.lbCurrentPercentage.Height) / 2);
    }

    private void ResizeForOneProgressBarWithoutPercentage()
    {
      this.ResizeForOneProgressBarWithPercentage();
      this.progressBarUnique.Width = this.lbTotalPercentage.Location.X + this.lbTotalPercentage.Width - this.progressBarTotal.Location.X;
    }

    private void UpdateInputTitleAndText(string newTitle, bool showText)
    {
      this.lbInputTitle.Text = newTitle;
      this.lbInput.Visible = showText;
    }

    private void ProgressChanged(UserProcessProgressChangedEventArgs e)
    {
      if (!this.Visible)
      {
        int num = (int) this.ShowDialog();
      }
      this.UpdateProgressBar(this.progressBarCurrent, this.lbCurrentPercentage, e.ProgressPercentage);
      this.UpdateProgressBar(this.progressBarTotal, this.lbTotalPercentage, e.TotalProgressPercentage);
      this.lbInput.Text = e.Input;
      this.lbOutput.Text = e.Output;
      this.Update();
    }

    private void ProcessCompleted(object sender, UserProcessCompletedEventArgs e)
    {
      if (e.Error != null)
      {
        string str = string.Format("{0} error with \"{1}\":\n{2}", (object) this.processInfo.ProcessTypeName, (object) e.ResourceName, (object) e.Error.Message);
        FormProcess.logger.Error((object) str);
        int num = (int) MessageBox.Show((IWin32Window) this, str, this.processInfo.ProcessTitle, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        this.NotifyNewAppStatus(str);
      }
      else if (e.Cancelled)
      {
        string message = string.Format("{0} canceled!", (object) this.processInfo.ProcessTypeName);
        FormProcess.logger.Info((object) message);
        this.NotifyNewAppStatus(message);
      }
      else
      {
        this.UpdateProgressBar(this.progressBarCurrent, this.lbCurrentPercentage, 100);
        this.UpdateProgressBar(this.progressBarTotal, this.lbTotalPercentage, 100);
        string message = string.Format("{0} successfully completed!", (object) this.processInfo.ProcessTypeName);
        FormProcess.logger.Info((object) message);
        this.NotifyNewAppStatus(message);
      }
      this.RemoveEventListeners();
      this.Close();
    }

    private void ProcessWaitForUserConsent(UserProcessWaitingForUserConsentEventArgs e)
    {
      string caption = Strings.ResourceManager.GetString(e.QuestionTitleResourceName);
      if (MessageBox.Show((IWin32Window) this, UserProcessEventArgsConverter.ToFormatedString(e, Strings.ResourceManager), caption, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) != DialogResult.Yes)
        this.process.CancelAsync((object) this.currentConversionTaskId);
      e.ResetEvent.Set();
    }

    private void ProcessUpdateUIView(UserProcessUpdateUIViewEventArgs e)
    {
      this.UpdateProgressBarsWithInitialValues();
      switch (e.UIStyle)
      {
        case UserProcessUpdateUIViewEventArgs.UserProgressUIStyle.OneProgressBarWithPercentage:
          this.progressBarTotal.Visible = false;
          this.lbTotalPercentage.Visible = false;
          this.progressBarCurrent.Visible = true;
          this.lbCurrentPercentage.Visible = true;
          this.progressBarCurrent.Style = ProgressBarStyle.Continuous;
          this.ResizeForOneProgressBarWithPercentage();
          break;
        case UserProcessUpdateUIViewEventArgs.UserProgressUIStyle.OneProgressBarWithoutPercentage:
          this.progressBarTotal.Visible = false;
          this.lbTotalPercentage.Visible = false;
          this.progressBarCurrent.Visible = true;
          this.lbCurrentPercentage.Visible = false;
          this.progressBarCurrent.Style = ProgressBarStyle.Marquee;
          this.ResizeForOneProgressBarWithoutPercentage();
          break;
        case UserProcessUpdateUIViewEventArgs.UserProgressUIStyle.TwoProgressBarsWithPercentage:
          this.progressBarTotal.Visible = true;
          this.lbTotalPercentage.Visible = true;
          this.progressBarCurrent.Visible = true;
          this.lbCurrentPercentage.Visible = true;
          this.progressBarTotal.Style = ProgressBarStyle.Continuous;
          this.progressBarCurrent.Style = ProgressBarStyle.Continuous;
          break;
      }
      if (!e.UpdateInputTitle)
        return;
      this.UpdateInputTitleAndText(Strings.ResourceManager.GetString(e.InputTitleResourceName), e.ShowInputText);
    }

    private void btCancel_Click(object sender, EventArgs e) => this.process.CancelAsync((object) this.currentConversionTaskId);

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormProcess));
      this.groupBoxProgress = new GroupBox();
      this.lbCurrentPercentage = new Label();
      this.lbTotalPercentage = new Label();
      this.lbOutput = new Label();
      this.lbInput = new Label();
      this.btCancel = new Button();
      this.progressBarTotal = new ProgressBar();
      this.progressBarCurrent = new ProgressBar();
      this.lbOutputTitle = new Label();
      this.lbInputTitle = new Label();
      this.groupBoxProgress.SuspendLayout();
      this.SuspendLayout();
      this.groupBoxProgress.Controls.Add((Control) this.lbOutputTitle);
      this.groupBoxProgress.Controls.Add((Control) this.lbInputTitle);
      this.groupBoxProgress.Controls.Add((Control) this.lbCurrentPercentage);
      this.groupBoxProgress.Controls.Add((Control) this.lbTotalPercentage);
      this.groupBoxProgress.Controls.Add((Control) this.lbOutput);
      this.groupBoxProgress.Controls.Add((Control) this.lbInput);
      this.groupBoxProgress.Controls.Add((Control) this.btCancel);
      this.groupBoxProgress.Controls.Add((Control) this.progressBarTotal);
      this.groupBoxProgress.Controls.Add((Control) this.progressBarCurrent);
      this.groupBoxProgress.Location = new Point(12, 12);
      this.groupBoxProgress.Name = "groupBoxProgress";
      this.groupBoxProgress.Size = new Size(421, 137);
      this.groupBoxProgress.TabIndex = 9;
      this.groupBoxProgress.TabStop = false;
      this.groupBoxProgress.Text = "ProgressSubTitle";
      this.lbCurrentPercentage.AutoSize = true;
      this.lbCurrentPercentage.BackColor = Color.Transparent;
      this.lbCurrentPercentage.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.lbCurrentPercentage.ImageAlign = ContentAlignment.MiddleRight;
      this.lbCurrentPercentage.Location = new Point(370, 66);
      this.lbCurrentPercentage.Name = "lbCurrentPercentage";
      this.lbCurrentPercentage.Size = new Size(45, 16);
      this.lbCurrentPercentage.TabIndex = 9;
      this.lbCurrentPercentage.Text = "100%";
      this.lbCurrentPercentage.TextAlign = ContentAlignment.MiddleRight;
      this.lbTotalPercentage.AutoSize = true;
      this.lbTotalPercentage.BackColor = Color.Transparent;
      this.lbTotalPercentage.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.lbTotalPercentage.ImageAlign = ContentAlignment.MiddleRight;
      this.lbTotalPercentage.Location = new Point(370, 87);
      this.lbTotalPercentage.Name = "lbTotalPercentage";
      this.lbTotalPercentage.Size = new Size(45, 16);
      this.lbTotalPercentage.TabIndex = 10;
      this.lbTotalPercentage.Text = "100%";
      this.lbTotalPercentage.TextAlign = ContentAlignment.MiddleRight;
      this.lbOutput.AutoEllipsis = true;
      this.lbOutput.Location = new Point(51, 43);
      this.lbOutput.Name = "lbOutput";
      this.lbOutput.Size = new Size(358, 14);
      this.lbOutput.TabIndex = 8;
      this.lbInput.AutoEllipsis = true;
      this.lbInput.Location = new Point(51, 20);
      this.lbInput.Name = "lbInput";
      this.lbInput.Size = new Size(358, 14);
      this.lbInput.TabIndex = 7;
      this.btCancel.DialogResult = DialogResult.Cancel;
      this.btCancel.FlatStyle = FlatStyle.Popup;
      this.btCancel.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btCancel.ForeColor = Color.FromArgb(248, 48, 0);
      this.btCancel.Location = new Point(156, 108);
      this.btCancel.Name = "btCancel";
      this.btCancel.Size = new Size(108, 22);
      this.btCancel.TabIndex = 6;
      this.btCancel.Text = "Cancel";
      this.btCancel.UseVisualStyleBackColor = false;
      this.btCancel.Click += new EventHandler(this.btCancel_Click);
      this.progressBarTotal.Location = new Point(8, 87);
      this.progressBarTotal.Name = "progressBarTotal";
      this.progressBarTotal.Size = new Size(356, 15);
      this.progressBarTotal.TabIndex = 4;
      this.progressBarCurrent.Location = new Point(8, 66);
      this.progressBarCurrent.Name = "progressBarCurrent";
      this.progressBarCurrent.Size = new Size(356, 15);
      this.progressBarCurrent.TabIndex = 3;
      this.lbOutputTitle.AutoSize = true;
      this.lbOutputTitle.Location = new Point(6, 43);
      this.lbOutputTitle.Name = "lbOutputTitle";
      this.lbOutputTitle.Size = new Size(42, 13);
      this.lbOutputTitle.TabIndex = 1;
      this.lbOutputTitle.Text = "Output:";
      this.lbInputTitle.AutoSize = true;
      this.lbInputTitle.Location = new Point(6, 20);
      this.lbInputTitle.Name = "lbInputTitle";
      this.lbInputTitle.Size = new Size(34, 13);
      this.lbInputTitle.TabIndex = 0;
      this.lbInputTitle.Text = "Input:";
      this.lbInputTitle.TextAlign = ContentAlignment.MiddleLeft;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = SystemColors.Window;
      this.CancelButton = (IButtonControl) this.btCancel;
      this.ClientSize = new Size(445, 161);
      this.ControlBox = false;
      this.Controls.Add((Control) this.groupBoxProgress);
      this.DoubleBuffered = true;
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (FormProcess);
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "ProcessTitle";
      this.groupBoxProgress.ResumeLayout(false);
      this.groupBoxProgress.PerformLayout();
      this.ResumeLayout(false);
    }

    internal class ProcessInfo
    {
      internal string ProcessTitle;
      internal string ProcessSubTitle;
      internal string ProcessTypeName;

      internal static FormProcess.ProcessInfo GetProcessInfoForDesEncryptor() => new FormProcess.ProcessInfo()
      {
        ProcessTitle = Strings.DESEncryptorTitle,
        ProcessSubTitle = Strings.DESEncryptorSubTitle,
        ProcessTypeName = Strings.DESEncryptorTypeName
      };

      internal static FormProcess.ProcessInfo GetProcessInfoForDesDecryptor() => new FormProcess.ProcessInfo()
      {
        ProcessTitle = Strings.DESDecryptorTitle,
        ProcessSubTitle = Strings.DESDecryptorSubTitle,
        ProcessTypeName = Strings.DESDescriptorTypeName
      };

      internal static FormProcess.ProcessInfo GetProcessInfoForFileScanner() => new FormProcess.ProcessInfo()
      {
        ProcessTitle = Strings.FileScannerTitle,
        ProcessSubTitle = Strings.FileScannerSubTitle,
        ProcessTypeName = Strings.FileScannerTypeName
      };

      internal static FormProcess.ProcessInfo GetProcessInfoForDiscExtractor() => new FormProcess.ProcessInfo()
      {
        ProcessTitle = Strings.FileExtractorTitle,
        ProcessSubTitle = Strings.FileExtractorSubTitle,
        ProcessTypeName = Strings.FileExtractorTypeName
      };

      internal static FormProcess.ProcessInfo GetProcessInfoForRawToWavConverter() => new FormProcess.ProcessInfo()
      {
        ProcessTitle = Strings.Raw2WavConverterTitle,
        ProcessSubTitle = Strings.Raw2WavConverterSubTitle,
        ProcessTypeName = Strings.Raw2WavConverterTypeName
      };

      internal static FormProcess.ProcessInfo GetProcessInfoForGDEmuConverter() => new FormProcess.ProcessInfo()
      {
        ProcessTitle = Strings.GDEmuExporterTitle,
        ProcessSubTitle = Strings.GDEmuExporterSubTitle,
        ProcessTypeName = Strings.GDEmuExporterTypeName
      };
    }
  }
}
