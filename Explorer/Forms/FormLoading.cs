﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.Forms.FormLoading
// Assembly: GD-ROM Explorer, Version=1.6.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B7A7D10A-9A63-4E9E-9840-D297E5FC2219
// Assembly location: GD-ROM Explorer.exe

using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace GDRomExplorer.Forms
{
  public class FormLoading : Form
  {
    private IContainer components;
    private Label lbLoadingMessage;
    private ProgressBar progressBar;

    public FormLoading(string Title, string Message)
    {
      this.InitializeComponent();
      this.Text = Title;
      this.lbLoadingMessage.Text = Message;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.lbLoadingMessage = new Label();
      this.progressBar = new ProgressBar();
      this.SuspendLayout();
      this.lbLoadingMessage.Anchor = AnchorStyles.None;
      this.lbLoadingMessage.ImageAlign = ContentAlignment.TopCenter;
      this.lbLoadingMessage.Location = new Point(12, 9);
      this.lbLoadingMessage.Name = "lbLoadingMessage";
      this.lbLoadingMessage.Size = new Size(320, 20);
      this.lbLoadingMessage.TabIndex = 5;
      this.lbLoadingMessage.Text = "Message";
      this.lbLoadingMessage.TextAlign = ContentAlignment.MiddleLeft;
      this.progressBar.Location = new Point(12, 32);
      this.progressBar.MarqueeAnimationSpeed = 30;
      this.progressBar.Name = "progressBar";
      this.progressBar.Size = new Size(320, 23);
      this.progressBar.Style = ProgressBarStyle.Marquee;
      this.progressBar.TabIndex = 6;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = SystemColors.Window;
      this.ClientSize = new Size(344, 62);
      this.ControlBox = false;
      this.Controls.Add((Control) this.progressBar);
      this.Controls.Add((Control) this.lbLoadingMessage);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (FormLoading);
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Title";
      this.ResumeLayout(false);
    }
  }
}
