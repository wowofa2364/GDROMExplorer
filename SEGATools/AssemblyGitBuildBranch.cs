﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.AssemblyGitBuildBranch
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools
{
  [AttributeUsage(AttributeTargets.Assembly)]
  public class AssemblyGitBuildBranch : Attribute
  {
    public string GitBuildBranch { get; private set; }

    public AssemblyGitBuildBranch(string txt) => this.GitBuildBranch = txt;
  }
}
