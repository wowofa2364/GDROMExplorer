﻿using SEGATools;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: InternalsVisibleTo("CDI")]
[assembly: InternalsVisibleTo("GDI")]
[assembly: AssemblyCompany("Japanese Cake")]
[assembly: AssemblyGitBuildBranch("release_1.6.3")]
[assembly: InternalsVisibleTo("ISO9660")]
[assembly: AssemblyProduct("SEGA Tools")]
[assembly: AssemblyTitle("SEGA Tools")]
[assembly: AssemblyDescription("SEGATools.dll")]
[assembly: AssemblyConfiguration("")]
[assembly: InternalsVisibleTo("BIN")]
[assembly: ComVisible(true)]
[assembly: AssemblyGitBuildSHA1("fd2eb45")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCopyright("2012-2016")]
[assembly: Guid("c48aeffd-e41a-403e-9c32-f376d63a4ae2")]
[assembly: AssemblyFileVersion("1.0.3")]
[assembly: AssemblyVersion("1.0.3.0")]