﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Registry.FileAssociationInfo
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SEGATools.Registry
{
  public class FileAssociationInfo
  {
    private RegistryWrapper registryWrapper = new RegistryWrapper();
    private string extension;

    public static string[] GetExtensions()
    {
      RegistryKey classesRoot = Microsoft.Win32.Registry.ClassesRoot;
      List<string> stringList = new List<string>();
      foreach (string subKeyName in classesRoot.GetSubKeyNames())
      {
        if (subKeyName.StartsWith("."))
          stringList.Add(subKeyName);
      }
      return stringList.ToArray();
    }

    public string ContentType
    {
      get => this.GetContentType(this);
      set => this.SetContentType(this, value);
    }

    public bool Exists
    {
      get
      {
        RegistryKey classesRoot = Microsoft.Win32.Registry.ClassesRoot;
        try
        {
          if (classesRoot.OpenSubKey(this.extension) == null)
            return false;
        }
        catch (Exception ex)
        {
          Console.WriteLine(ex.ToString());
          return false;
        }
        return true;
      }
    }

    public string Extension
    {
      get => this.extension;
      set => this.extension = value;
    }

    public string[] OpenWithList
    {
      get => this.GetOpenWithList(this);
      set => this.SetOpenWithList(this, value);
    }

    public PerceivedTypes PerceivedType
    {
      get => this.GetPerceivedType(this);
      set => this.SetPerceivedType(this, value);
    }

    public Guid PersistentHandler
    {
      get => this.GetPersistentHandler(this);
      set => this.SetPersistentHandler(this, value);
    }

    [XmlAttribute]
    public string ProgID
    {
      get => this.GetProgID(this);
      set => this.SetProgID(this, value);
    }

    public void Create() => this.Create(this);

    public void Delete() => this.Delete(this);

    public bool IsValid(string extension, string progId)
    {
      FileAssociationInfo fileAssociationInfo = new FileAssociationInfo(extension);
      return fileAssociationInfo.Exists && !(progId != fileAssociationInfo.ProgID);
    }

    public FileAssociationInfo(string extension) => this.extension = extension;

    public FileAssociationInfo Create(string progId) => this.Create(progId, PerceivedTypes.None, string.Empty, (string[]) null);

    public FileAssociationInfo Create(
      string progId,
      PerceivedTypes perceivedType)
    {
      return this.Create(progId, perceivedType, string.Empty, (string[]) null);
    }

    public FileAssociationInfo Create(
      string progId,
      PerceivedTypes perceivedType,
      string contentType)
    {
      return this.Create(progId, PerceivedTypes.None, contentType, (string[]) null);
    }

    public FileAssociationInfo Create(
      string progId,
      PerceivedTypes perceivedType,
      string contentType,
      string[] openwithList)
    {
      FileAssociationInfo fileAssociationInfo = new FileAssociationInfo(this.extension);
      if (fileAssociationInfo.Exists)
        fileAssociationInfo.Delete();
      fileAssociationInfo.Create();
      fileAssociationInfo.ProgID = progId;
      if (perceivedType != PerceivedTypes.None)
        fileAssociationInfo.PerceivedType = perceivedType;
      if (contentType != string.Empty)
        fileAssociationInfo.ContentType = contentType;
      if (openwithList != null)
        fileAssociationInfo.OpenWithList = openwithList;
      return fileAssociationInfo;
    }

    protected string[] GetOpenWithList(FileAssociationInfo file)
    {
      if (!file.Exists)
        throw new Exception("Extension does not exist");
      RegistryKey registryKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(file.extension).OpenSubKey("OpenWithList");
      return registryKey == null ? new string[0] : registryKey.GetSubKeyNames();
    }

    protected void SetOpenWithList(FileAssociationInfo file, string[] programList)
    {
      if (!file.Exists)
        throw new Exception("Extension does not exist");
      RegistryKey registryKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(file.extension, true);
      if (registryKey.OpenSubKey("OpenWithList", true) != null)
        registryKey.DeleteSubKeyTree("OpenWithList");
      RegistryKey subKey = registryKey.CreateSubKey("OpenWithList");
      foreach (string program in programList)
        subKey.CreateSubKey(program);
      ShellNotification.NotifyOfChange();
    }

    protected PerceivedTypes GetPerceivedType(FileAssociationInfo file)
    {
      if (!file.Exists)
        throw new Exception("Extension does not exist");
      object obj = this.registryWrapper.Read(file.extension, "PerceivedType");
      PerceivedTypes perceivedTypes = PerceivedTypes.None;
      if (obj == null)
        return perceivedTypes;
      try
      {
        perceivedTypes = (PerceivedTypes) Enum.Parse(typeof (PerceivedTypes), obj.ToString(), true);
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.ToString());
      }
      return perceivedTypes;
    }

    protected void SetPerceivedType(FileAssociationInfo file, PerceivedTypes type)
    {
      if (!file.Exists)
        throw new Exception("Extension does not exist");
      this.registryWrapper.Write(file.extension, "PerceivedType", (object) type.ToString());
      ShellNotification.NotifyOfChange();
    }

    protected Guid GetPersistentHandler(FileAssociationInfo file)
    {
      if (!file.Exists)
        throw new Exception("Extension does not exist");
      object obj = this.registryWrapper.Read(file.extension + "\\PersistentHandler", string.Empty);
      return obj == null ? new Guid() : new Guid(obj.ToString());
    }

    protected void SetPersistentHandler(FileAssociationInfo file, Guid persistentHandler)
    {
      if (!file.Exists)
        throw new Exception("Extension does not exist");
      if (persistentHandler == Guid.Empty)
        return;
      this.registryWrapper.Write(file.extension + "\\" + (object) this.PersistentHandler, string.Empty, (object) persistentHandler);
      ShellNotification.NotifyOfChange();
    }

    protected string GetContentType(FileAssociationInfo file)
    {
      if (!file.Exists)
        throw new Exception("Extension does not exist");
      object obj = this.registryWrapper.Read(file.extension, "Content Type");
      return obj == null ? string.Empty : obj.ToString();
    }

    protected void SetContentType(FileAssociationInfo file, string type)
    {
      if (!file.Exists)
        throw new Exception("Extension does not exist");
      this.registryWrapper.Write(file.extension, "Content Type", (object) type);
      ShellNotification.NotifyOfChange();
    }

    protected string GetProgID(FileAssociationInfo file)
    {
      if (!file.Exists)
        throw new Exception("Extension does not exist");
      object obj = this.registryWrapper.Read(file.extension, string.Empty);
      return obj == null ? string.Empty : obj.ToString();
    }

    protected void SetProgID(FileAssociationInfo file, string progId)
    {
      if (!file.Exists)
        throw new Exception("Extension does not exist");
      this.registryWrapper.Write(file.extension, string.Empty, (object) progId);
      ShellNotification.NotifyOfChange();
    }

    protected void Create(FileAssociationInfo file)
    {
      if (file.Exists)
        file.Delete();
      Microsoft.Win32.Registry.ClassesRoot.CreateSubKey(file.extension);
    }

    protected void Delete(FileAssociationInfo file)
    {
      if (!file.Exists)
        throw new Exception("Key not found.");
      Microsoft.Win32.Registry.ClassesRoot.DeleteSubKeyTree(file.extension);
      ShellNotification.NotifyOfChange();
    }
  }
}
