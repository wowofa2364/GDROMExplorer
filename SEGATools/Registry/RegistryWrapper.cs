﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Registry.RegistryWrapper
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using Microsoft.Win32;
using System;

namespace SEGATools.Registry
{
  internal class RegistryWrapper
  {
    public object Read(string path, string valueName)
    {
      RegistryKey registryKey = Microsoft.Win32.Registry.ClassesRoot;
      string[] strArray = path.Split('\\');
      if (strArray == null || strArray.Length == 0)
        return (object) null;
      for (int index = 0; index < strArray.Length; ++index)
      {
        registryKey = registryKey.OpenSubKey(strArray[index]);
        if (registryKey == null)
          return (object) null;
        if (index == strArray.Length - 1)
          return registryKey.GetValue(valueName, (object) null, RegistryValueOptions.DoNotExpandEnvironmentNames);
      }
      return (object) null;
    }

    public void Write(string path, string valueName, object value)
    {
      RegistryKey registryKey1 = Microsoft.Win32.Registry.ClassesRoot;
      RegistryKey registryKey2 = registryKey1;
      string[] strArray = path.Split('\\');
      if (strArray == null || strArray.Length == 0)
        return;
      for (int index = 0; index < strArray.Length; ++index)
      {
        registryKey1 = registryKey1.OpenSubKey(strArray[index], true) ?? registryKey2.CreateSubKey(strArray[index]);
        if (index == strArray.Length - 1)
        {
          if (value is string)
            registryKey1.SetValue(valueName, (object) value.ToString());
          else if (value is uint || value.GetType().IsEnum)
          {
            if (registryKey1.GetValue(valueName, (object) null) == null)
            {
              registryKey1.SetValue(valueName, value, RegistryValueKind.DWord);
            }
            else
            {
              switch (registryKey1.GetValueKind(valueName))
              {
                case RegistryValueKind.String:
                  registryKey1.SetValue(valueName, (object) ("x" + ((uint) value).ToString("X8")));
                  break;
                case RegistryValueKind.Binary:
                  uint num = (uint) value;
                  byte[] numArray = new byte[4]
                  {
                    (byte) (num & (uint) byte.MaxValue),
                    (byte) ((num & 65280U) >> 1),
                    (byte) ((num & 16711680U) >> 2),
                    (byte) ((num & 4278190080U) >> 3)
                  };
                  numArray[0] = (byte) (num & (uint) byte.MaxValue);
                  numArray[1] = (byte) ((num & 65280U) >> 8);
                  numArray[2] = (byte) ((num & 16711680U) >> 16);
                  numArray[3] = (byte) ((num & 4278190080U) >> 24);
                  registryKey1.SetValue(valueName, (object) numArray, RegistryValueKind.Binary);
                  break;
                case RegistryValueKind.DWord:
                  registryKey1.SetValue(valueName, value, RegistryValueKind.DWord);
                  break;
              }
            }
          }
          else if (value is Guid guid)
            registryKey1.SetValue(valueName, (object) guid.ToString("B"));
        }
        registryKey2 = registryKey1;
      }
      registryKey1?.Close();
    }

    public void Delete(string path, string valueName)
    {
      RegistryKey registryKey = Microsoft.Win32.Registry.ClassesRoot;
      string[] strArray = path.Split('\\');
      if (strArray == null || strArray.Length == 0)
        return;
      for (int index = 0; index < strArray.Length; ++index)
      {
        registryKey = registryKey.OpenSubKey(strArray[index], true);
        if (registryKey == null)
          break;
        if (index == strArray.Length - 1)
          registryKey.DeleteValue(valueName, false);
      }
    }
  }
}
