﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Registry.ProgramVerb
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

namespace SEGATools.Registry
{
  public class ProgramVerb
  {
    private string command;
    private string name;

    public string Command => this.command;

    public string Name => this.name;

    public ProgramVerb(string name, string command)
    {
      this.name = name;
      this.command = command;
    }
  }
}
