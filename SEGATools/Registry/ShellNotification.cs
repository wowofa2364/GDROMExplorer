﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Registry.ShellNotification
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;
using System.Runtime.InteropServices;

namespace SEGATools.Registry
{
  internal class ShellNotification
  {
    [DllImport("shell32.dll")]
    private static extern void SHChangeNotify(
      uint wEventId,
      uint uFlags,
      IntPtr dwItem1,
      IntPtr dwItem2);

    public static void NotifyOfChange() => ShellNotification.SHChangeNotify(134217728U, 8192U, IntPtr.Zero, IntPtr.Zero);

    [Flags]
    private enum ShellChangeNotificationEvents : uint
    {
      SHCNE_RENAMEITEM = 1,
      SHCNE_CREATE = 2,
      SHCNE_DELETE = 4,
      SHCNE_MKDIR = 8,
      SHCNE_RMDIR = 16, // 0x00000010
      SHCNE_MEDIAINSERTED = 32, // 0x00000020
      SHCNE_MEDIAREMOVED = 64, // 0x00000040
      SHCNE_DRIVEREMOVED = 128, // 0x00000080
      SHCNE_DRIVEADD = 256, // 0x00000100
      SHCNE_NETSHARE = 512, // 0x00000200
      SHCNE_NETUNSHARE = 1024, // 0x00000400
      SHCNE_ATTRIBUTES = 2048, // 0x00000800
      SHCNE_UPDATEDIR = 4096, // 0x00001000
      SHCNE_UPDATEITEM = 8192, // 0x00002000
      SHCNE_SERVERDISCONNECT = 16384, // 0x00004000
      SHCNE_UPDATEIMAGE = 32768, // 0x00008000
      SHCNE_DRIVEADDGUI = 65536, // 0x00010000
      SHCNE_RENAMEFOLDER = 131072, // 0x00020000
      SHCNE_FREESPACE = 262144, // 0x00040000
      SHCNE_EXTENDED_EVENT = 67108864, // 0x04000000
      SHCNE_ASSOCCHANGED = 134217728, // 0x08000000
      SHCNE_DISKEVENTS = SHCNE_RENAMEFOLDER | SHCNE_UPDATEITEM | SHCNE_UPDATEDIR | SHCNE_ATTRIBUTES | SHCNE_RMDIR | SHCNE_MKDIR | SHCNE_DELETE | SHCNE_CREATE | SHCNE_RENAMEITEM, // 0x0002381F
      SHCNE_GLOBALEVENTS = SHCNE_ASSOCCHANGED | SHCNE_EXTENDED_EVENT | SHCNE_FREESPACE | SHCNE_DRIVEADDGUI | SHCNE_UPDATEIMAGE | SHCNE_DRIVEADD | SHCNE_DRIVEREMOVED | SHCNE_MEDIAREMOVED | SHCNE_MEDIAINSERTED, // 0x0C0581E0
      SHCNE_ALLEVENTS = 2147483647, // 0x7FFFFFFF
      SHCNE_INTERRUPT = 2147483648, // 0x80000000
    }

    private enum ShellChangeNotificationFlags
    {
      SHCNF_IDLIST = 0,
      SHCNF_PATHA = 1,
      SHCNF_PRINTERA = 2,
      SHCNF_DWORD = 3,
      SHCNF_PATHW = 5,
      SHCNF_PRINTERW = 6,
      SHCNF_TYPE = 255, // 0x000000FF
      SHCNF_FLUSH = 4096, // 0x00001000
      SHCNF_FLUSHNOWAIT = 8192, // 0x00002000
    }
  }
}
