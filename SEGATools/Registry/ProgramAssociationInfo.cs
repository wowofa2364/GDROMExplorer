﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Registry.ProgramAssociationInfo
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using Microsoft.Win32;
using System;
using System.Collections.Generic;

namespace SEGATools.Registry
{
  public class ProgramAssociationInfo
  {
    private RegistryWrapper registryWrapper = new RegistryWrapper();
    protected string progId;

    public bool AlwaysShowExtension
    {
      get => this.GetAlwaysShowExt();
      set => this.SetAlwaysShowExt(value);
    }

    public string Description
    {
      get => this.GetDescription();
      set => this.SetDescription(value);
    }

    public EditFlags EditFlags
    {
      get => this.GetEditFlags();
      set => this.SetEditFlags(value);
    }

    public ProgramIcon DefaultIcon
    {
      get => this.GetDefaultIcon();
      set => this.SetDefaultIcon(value);
    }

    public ProgramVerb[] Verbs
    {
      get => this.GetVerbs();
      set => this.SetVerbs(value);
    }

    public string ProgID => this.progId;

    public bool Exists
    {
      get
      {
        RegistryKey classesRoot = Microsoft.Win32.Registry.ClassesRoot;
        try
        {
          if (this.progId == string.Empty)
            return false;
          if (classesRoot.OpenSubKey(this.progId) == null)
            return false;
        }
        catch (Exception ex)
        {
          Console.WriteLine(ex.ToString());
          return false;
        }
        return true;
      }
    }

    public void Create()
    {
      if (this.Exists)
        return;
      Microsoft.Win32.Registry.ClassesRoot.CreateSubKey(this.progId);
    }

    public ProgramAssociationInfo Create(ProgramVerb verb) => this.Create(string.Empty, EditFlags.None, new ProgramVerb[1]
    {
      verb
    });

    public ProgramAssociationInfo Create(ProgramVerb[] verbs) => this.Create(string.Empty, EditFlags.None, verbs);

    public ProgramAssociationInfo Create(string description, ProgramVerb verb) => this.Create(description, EditFlags.None, new ProgramVerb[1]
    {
      verb
    });

    public ProgramAssociationInfo Create(
      string description,
      ProgramVerb[] verbs)
    {
      return this.Create(description, EditFlags.None, verbs);
    }

    public ProgramAssociationInfo Create(
      string description,
      EditFlags editFlags,
      ProgramVerb verb)
    {
      return this.Create(description, editFlags, new ProgramVerb[1]
      {
        verb
      });
    }

    public ProgramAssociationInfo Create(
      string description,
      EditFlags editFlags,
      ProgramVerb[] verbs)
    {
      if (this.Exists)
        this.Delete();
      this.Create();
      if (description != string.Empty)
        this.Description = description;
      if (editFlags != EditFlags.None)
        this.EditFlags = editFlags;
      this.Verbs = verbs;
      return this;
    }

    protected bool GetAlwaysShowExt()
    {
      if (!this.Exists)
        throw new Exception("Extension does not exist");
      return !(Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(this.progId).GetValue("AlwaysShowExt", (object) "ThisValueShouldNotExist").ToString() == "ThisValueShouldNotExist");
    }

    protected void SetAlwaysShowExt(bool value)
    {
      if (!this.Exists)
        throw new Exception("Extension does not exist");
      if (value)
        this.registryWrapper.Write(this.progId, "AlwaysShowExt", (object) string.Empty);
      else
        this.registryWrapper.Delete(this.progId, "AlwaysShowExt");
      ShellNotification.NotifyOfChange();
    }

    protected string GetDescription()
    {
      if (!this.Exists)
        throw new Exception("Extension does not exist");
      object obj = this.registryWrapper.Read(this.progId, string.Empty);
      return obj == null ? string.Empty : obj.ToString();
    }

    protected void SetDescription(string description)
    {
      if (!this.Exists)
        throw new Exception("Extension does not exist");
      this.registryWrapper.Write(this.progId, string.Empty, (object) description);
      ShellNotification.NotifyOfChange();
    }

    protected EditFlags GetEditFlags()
    {
      if (!this.Exists)
        throw new Exception("Extension does not exist");
      object obj = this.registryWrapper.Read(this.progId, "EditFlags");
      if (obj == null)
        return EditFlags.None;
      if (obj is byte[])
      {
        int val;
        if (!this.TryGetInt(obj as byte[], out val))
          return EditFlags.None;
        obj = (object) val;
      }
      try
      {
        return (EditFlags) Convert.ToUInt32(obj);
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.ToString());
      }
      return EditFlags.None;
    }

    protected void SetEditFlags(EditFlags flags)
    {
      if (!this.Exists)
        throw new Exception("Extension does not exist");
      this.registryWrapper.Write(this.progId, "EditFlags", (object) flags);
      ShellNotification.NotifyOfChange();
    }

    protected ProgramIcon GetDefaultIcon()
    {
      if (!this.Exists)
        throw new Exception("Extension does not exist");
      object obj = this.registryWrapper.Read(this.progId + "\\DefaultIcon", "");
      return obj == null ? ProgramIcon.None : ProgramIcon.Parse(obj.ToString());
    }

    protected void SetDefaultIcon(ProgramIcon icon)
    {
      if (!this.Exists)
        throw new Exception("Extension does not exist");
      if (!(icon != ProgramIcon.None))
        return;
      this.registryWrapper.Write(this.progId + "\\DefaultIcon", "", (object) icon.ToString());
      ShellNotification.NotifyOfChange();
    }

    protected ProgramVerb[] GetVerbs()
    {
      if (!this.Exists)
        throw new Exception("Extension does not exist");
      RegistryKey classesRoot = Microsoft.Win32.Registry.ClassesRoot;
      RegistryKey registryKey1 = classesRoot.OpenSubKey(this.progId);
      List<ProgramVerb> programVerbList = new List<ProgramVerb>();
      RegistryKey registryKey2 = registryKey1.OpenSubKey("shell", false);
      if (registryKey2 != null)
      {
        foreach (string subKeyName in registryKey2.GetSubKeyNames())
        {
          RegistryKey registryKey3 = registryKey2.OpenSubKey(subKeyName);
          if (registryKey3 != null)
          {
            RegistryKey registryKey4 = registryKey3.OpenSubKey("command");
            if (registryKey4 != null)
            {
              string command = (string) registryKey4.GetValue("", (object) "", RegistryValueOptions.DoNotExpandEnvironmentNames);
              programVerbList.Add(new ProgramVerb(subKeyName, command));
            }
          }
        }
        registryKey2.Close();
      }
      classesRoot.Close();
      return programVerbList.ToArray();
    }

    protected void SetVerbs(ProgramVerb[] verbs)
    {
      if (!this.Exists)
        throw new Exception("Extension does not exist");
      RegistryKey registryKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(this.progId, true);
      if (registryKey.OpenSubKey("shell", true) != null)
        registryKey.DeleteSubKeyTree("shell");
      RegistryKey subKey1 = registryKey.CreateSubKey("shell");
      foreach (ProgramVerb verb in verbs)
      {
        RegistryKey subKey2 = subKey1.CreateSubKey(verb.Name.ToLower());
        RegistryKey subKey3 = subKey2.CreateSubKey("command");
        subKey3.SetValue(string.Empty, (object) verb.Command, RegistryValueKind.ExpandString);
        subKey3.Close();
        subKey2.Close();
      }
      ShellNotification.NotifyOfChange();
    }

    protected void AddVerbInternal(ProgramVerb verb)
    {
      RegistryKey classesRoot = Microsoft.Win32.Registry.ClassesRoot;
      RegistryKey registryKey1 = classesRoot.OpenSubKey(this.progId).OpenSubKey("shell", true) ?? classesRoot.OpenSubKey(this.progId, true).CreateSubKey("shell");
      RegistryKey registryKey2 = registryKey1.OpenSubKey(verb.Name, true) ?? registryKey1.CreateSubKey(verb.Name);
      RegistryKey registryKey3 = registryKey2.OpenSubKey("command", true) ?? registryKey2.CreateSubKey("command");
      registryKey3.SetValue(string.Empty, (object) verb.Command, RegistryValueKind.ExpandString);
      registryKey3.Close();
      registryKey2.Close();
      classesRoot.Close();
      ShellNotification.NotifyOfChange();
    }

    protected void RemoveVerbInternal(string name)
    {
      RegistryKey classesRoot = Microsoft.Win32.Registry.ClassesRoot;
      RegistryKey registryKey = classesRoot.OpenSubKey(this.progId).OpenSubKey("shell", true);
      if (registryKey == null)
        throw new RegistryException("Shell key not found");
      foreach (string subKeyName in registryKey.GetSubKeyNames())
      {
        if (subKeyName == name)
        {
          registryKey.DeleteSubKeyTree(name);
          break;
        }
      }
      registryKey.Close();
      classesRoot.Close();
      ShellNotification.NotifyOfChange();
    }

    public void Delete()
    {
      if (!this.Exists)
        throw new Exception("Key not found.");
      Microsoft.Win32.Registry.ClassesRoot.DeleteSubKeyTree(this.progId);
    }

    public void AddVerb(ProgramVerb verb) => this.AddVerbInternal(verb);

    public void RemoveVerb(ProgramVerb verb)
    {
      if (verb == null)
        throw new NullReferenceException();
      this.RemoveVerb(verb.Name);
    }

    public void RemoveVerb(string name) => this.RemoveVerbInternal(name);

    public ProgramAssociationInfo(string progId) => this.progId = progId;

    private bool TryGetInt(byte[] arr, out int val)
    {
      try
      {
        if (arr.Length == 0)
        {
          val = -1;
          return false;
        }
        val = arr.Length != 1 ? BitConverter.ToInt32(arr, 0) : (int) arr[0];
        return true;
      }
      catch
      {
      }
      val = 0;
      return false;
    }
  }
}
