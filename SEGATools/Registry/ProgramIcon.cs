﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Registry.ProgramIcon
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

namespace SEGATools.Registry
{
  public class ProgramIcon
  {
    public static readonly ProgramIcon None = new ProgramIcon();
    private string path;
    private int index;

    public int Index
    {
      get => this.index;
      set => this.index = value;
    }

    public string Path
    {
      get => this.path;
      set => this.path = value;
    }

    public ProgramIcon(string path, int index)
    {
      this.path = path;
      this.index = index;
    }

    public ProgramIcon(string path)
    {
      this.path = path;
      this.index = 0;
    }

    public ProgramIcon()
    {
      this.path = string.Empty;
      this.index = 0;
    }

    public override string ToString() => this.path + "," + this.index.ToString();

    public static ProgramIcon Parse(string regString)
    {
      if (regString == string.Empty)
        return new ProgramIcon("");
      if (regString.StartsWith("\"") && regString.EndsWith("\"") && regString.Length > 3)
        regString = regString.Substring(1, regString.Length - 2);
      int index = 0;
      int length = regString.IndexOf(",");
      if (length == -1)
        length = regString.Length;
      else
        index = int.Parse(regString.Substring(length + 1));
      return new ProgramIcon(regString.Substring(0, length), index);
    }

    public static bool operator ==(ProgramIcon lv, ProgramIcon rv) => object.ReferenceEquals((object) lv, (object) null) && object.ReferenceEquals((object) rv, (object) null) || !object.ReferenceEquals((object) lv, (object) null) && !object.ReferenceEquals((object) rv, (object) null) && (lv.path == rv.path && lv.index == rv.index);

    public static bool operator !=(ProgramIcon lv, ProgramIcon rv) => !(lv == rv);

    public override bool Equals(object obj) => this == obj as ProgramIcon;

    public override int GetHashCode() => base.GetHashCode();
  }
}
