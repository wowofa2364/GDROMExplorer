﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Registry.EditFlags
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.Registry
{
  [Flags]
  public enum EditFlags : uint
  {
    None = 0,
    Exclude = 1,
    Show = 2,
    HasExtension = 4,
    NoEdit = 8,
    NoRemove = 16, // 0x00000010
    NoNewVerb = 32, // 0x00000020
    NoEditVerb = 64, // 0x00000040
    NoRemoveVerb = 128, // 0x00000080
    NoEditDesc = 256, // 0x00000100
    NoEditIcon = 512, // 0x00000200
    NoEditDflt = 1024, // 0x00000400
    NoEditVerbCmd = 2048, // 0x00000800
    NoEditVerbExe = 4096, // 0x00001000
    NoDDE = 8192, // 0x00002000
    NoEditMIME = 32768, // 0x00008000
    OpenIsSafe = 65536, // 0x00010000
    AlwaysUnsafe = 131072, // 0x00020000
    AlwaysShowExtension = 262144, // 0x00040000
    NoRecentDocuments = 1048576, // 0x00100000
  }
}
