﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Registry.RegistryException
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.Registry
{
  public class RegistryException : ApplicationException
  {
    public RegistryException()
    {
    }

    public RegistryException(string message)
      : base(message)
    {
    }

    public RegistryException(string message, Exception innerException)
      : base(message, innerException)
    {
    }
  }
}
