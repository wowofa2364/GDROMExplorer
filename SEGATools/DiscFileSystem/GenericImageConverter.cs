﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.DiscFileSystem.GenericImageConverter
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using ImageReader.DiscSectors;
using ImageReader.ImageReader;
using ImageReader.Stream;
using SEGATools.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace SEGATools.DiscFileSystem
{
  public class GenericImageConverter : IDiscFileSystemConverter
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private static readonly Regex FIND_NEXT_FILE = new Regex("^(?<prefix>.*)(?<index>[0-9]{2}?)$", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline);
    private static readonly int INVALID_FILE_INDEX = -1;
    private static readonly int DEFAULT_FILE_INDEX = 1;
    private IDiscSector discSector;

    public GenericImageConverter(IDiscSector discSector) => this.discSector = discSector;

    public IDiscFileSystem ToDiscFileSystem(string imageFileName)
    {
      InitialProgram initialProgram = this.GetInitialProgram(imageFileName, this.discSector);
      List<IDiscTrack> tracks = new List<IDiscTrack>();
      int index = this.GetIndexFromFileName(imageFileName);
      if (index == GenericImageConverter.INVALID_FILE_INDEX)
        index = GenericImageConverter.DEFAULT_FILE_INDEX;
      GenericImageConverter.TrackFile trackFile = new GenericImageConverter.TrackFile(index, imageFileName);
      uint num = 0;
      if (this.ContainsNonEmptyISO9660FileSystem(trackFile, num))
      {
        IDiscTrack discTrack = (IDiscTrack) new DiscTrack(trackFile.FileName, 0L, trackFile.Length, num, trackFile.Index, TrackModeType.Data, this.discSector);
        tracks.Add(discTrack);
      }
      else if (initialProgram != null && initialProgram.TableOfContent != null && this.ContainsNonEmptyISO9660FileSystem(trackFile, initialProgram.TableOfContent.FirstTrack.FrameAddress))
      {
        uint frameAddress = initialProgram.TableOfContent.FirstTrack.FrameAddress;
        IDiscTrack discTrack1 = (IDiscTrack) new DiscTrack(trackFile.FileName, 0L, trackFile.Length, frameAddress, trackFile.Index, TrackModeType.Data, this.discSector);
        tracks.Add(discTrack1);
        GenericImageConverter.TrackFile nextImageFile = this.FindNextImageFile(imageFileName);
        if (initialProgram.TableOfContent.HasSplitedDataTracks && nextImageFile != null)
        {
          IDiscTrack discTrack2 = (IDiscTrack) new DiscTrack(nextImageFile.FileName, 0L, nextImageFile.Length, initialProgram.TableOfContent.LastTrack.FrameAddress, nextImageFile.Index, TrackModeType.Data, this.discSector);
          tracks.Add(discTrack2);
        }
      }
      IDiscSession discSession = (IDiscSession) new DiscSession(1, string.Format(DiscSession.DEFAULT_SESSION_NAME_WITH_FORMAT, (object) 1), tracks);
      List<IDiscSession> sessions = new List<IDiscSession>();
      sessions.Add(discSession);
      bool supportCueSheet = tracks.Count == 1;
      return (IDiscFileSystem) new SEGATools.DiscFileSystem.DiscFileSystem(imageFileName, sessions, true, supportCueSheet);
    }

    private bool ContainsNonEmptyISO9660FileSystem(
      GenericImageConverter.TrackFile trackFile,
      uint lba)
    {
      IDiscTrack discTrack = (IDiscTrack) new DiscTrack(trackFile.FileName, 0L, trackFile.Length, lba, trackFile.Index, TrackModeType.Data, this.discSector);
      using (DiscImageReader discImageReader = new DiscImageReader())
      {
        try
        {
          discImageReader.Open(discTrack.FileInputStream, discTrack.LogicalBlockAddress, this.discSector);
          return discImageReader.RootDirectoryRecord.UsedSpace > 0U;
        }
        catch (DiscImageReaderException ex)
        {
          logger.Error(ex);
        }
      }
      return false;
    }

    private InitialProgram GetInitialProgram(string imageFileName, IDiscSector sector)
    {
      try
      {
        byte[] buffer;
        using (DiscSectorStream discSectorStream = new DiscSectorStream(imageFileName, sector, 0U, InitialProgram.IP_FILESIZE))
        {
          buffer = new byte[discSectorStream.Length];
          discSectorStream.Read(buffer, 0, buffer.Length);
        }
        return InitialProgramConverter.ToInitialProgram(buffer, 0);
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex);
      }
      finally
      {
      }
      return (InitialProgram) null;
    }

    private GenericImageConverter.TrackFile FindNextImageFile(string fileName)
    {
      int startIndex = this.GetIndexFromFileName(fileName);
      if (startIndex == GenericImageConverter.INVALID_FILE_INDEX)
        return (GenericImageConverter.TrackFile) null;
      string prefixFromFileName = this.GetFileNamePrefixFromFileName(fileName);
      if (string.IsNullOrEmpty(prefixFromFileName))
        return (GenericImageConverter.TrackFile) null;
      string[] array = ((IEnumerable<string>) Directory.GetFiles(Path.GetDirectoryName(fileName), prefixFromFileName + "*" + Path.GetExtension(fileName))).Where<string>((Func<string, bool>) (FilePath => this.GetIndexFromFileName(FilePath) > startIndex)).ToArray<string>();
      if (array.Length <= 0)
        return (GenericImageConverter.TrackFile) null;
      string fileName1 = array[0];
      return new GenericImageConverter.TrackFile(this.GetIndexFromFileName(fileName1), fileName1);
    }

    private string GetFileNamePrefixFromFileName(string fileName)
    {
      string withoutExtension = Path.GetFileNameWithoutExtension(fileName);
      Match match = GenericImageConverter.FIND_NEXT_FILE.Match(withoutExtension);
      return match.Success ? match.Groups["prefix"].Value : string.Empty;
    }

    private int GetIndexFromFileName(string fileName)
    {
      string withoutExtension = Path.GetFileNameWithoutExtension(fileName);
      Match match = GenericImageConverter.FIND_NEXT_FILE.Match(withoutExtension);
      return match.Success ? int.Parse(match.Groups["index"].Value) : GenericImageConverter.INVALID_FILE_INDEX;
    }

    private class TrackFile
    {
      public int Index { get; private set; }

      public long Length { get; private set; }

      public string FileName { get; private set; }

      public TrackFile(int index, string fileName)
      {
        this.Index = index;
        this.FileName = fileName;
        this.Length = new FileInfo(fileName).Length;
      }
    }
  }
}
