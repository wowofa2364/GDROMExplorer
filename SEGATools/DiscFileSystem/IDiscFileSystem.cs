﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.DiscFileSystem.IDiscFileSystem
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using ImageReader.ISO9660.DirectoryRecords;
using ImageReader.Stream;
using SEGATools.Binary;
using SEGATools.Encrypt;
using System.Collections.Generic;

namespace SEGATools.DiscFileSystem
{
  public interface IDiscFileSystem
  {
    bool CanExtractData { get; }

    bool CanBeExportedToCueSheet { get; }

    bool CanBeExportedToGdi { get; }

    List<IDiscSession> Sessions { get; }

    string FileName { get; }

    string DiscName { get; set; }

    DirectoryRecord MainBinary { get; set; }

    DiscImageType DiscType { get; set; }

    List<IDiscTrack> AllTracks { get; }

    DiscSectorStream GetDiscStreamForDirectoryRecord(DirectoryRecord directoryRecord);

    void RegisterLibraries(object file, List<SEGALibrary> Libraries);

    List<SEGALibrary> GetSEGALibraries(object file);

    DESKey NaomiDESKey { get; set; }

    void Close();
  }
}
