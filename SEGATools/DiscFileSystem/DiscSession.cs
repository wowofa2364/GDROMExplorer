﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.DiscFileSystem.DiscSession
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using ImageReader.ISO9660.DirectoryRecords;
using ImageReader.ISO9660.VolumeDescriptors;
using SEGATools.Security;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEGATools.DiscFileSystem
{
  internal sealed class DiscSession : IDiscSession
  {
    public static readonly string DEFAULT_SESSION_NAME_WITH_FORMAT = "Session {0}";
    private readonly string name;
    private int index;
    private InitialProgram ip;
    private IDiscFileSystem disc;
    private DirectoryRecord mainBinary;
    private PrimaryVolumeDescriptor primaryVolumeDescriptor;
    private DirectoryRecord rootDirectoryRecord;
    private List<IDiscTrack> tracks;

    public List<IDiscTrack> Tracks => this.tracks.ToList<IDiscTrack>();

    public List<IDiscTrack> DataTracks => this.tracks.FindAll((Predicate<IDiscTrack>) (track => track.TrackData == TrackModeType.Data));

    public IDiscTrack FirstDataTrack
    {
      get
      {
        List<IDiscTrack> all = this.tracks.FindAll((Predicate<IDiscTrack>) (track => track.TrackData == TrackModeType.Data));
        return all.Count <= 0 ? (IDiscTrack) null : all.OrderBy<IDiscTrack, int>((Func<IDiscTrack, int>) (track => track.Index)).First<IDiscTrack>();
      }
    }

    public IDiscTrack LastDataTrack
    {
      get
      {
        List<IDiscTrack> all = this.tracks.FindAll((Predicate<IDiscTrack>) (track => track.TrackData == TrackModeType.Data));
        return all.Count <= 0 ? (IDiscTrack) null : all.OrderBy<IDiscTrack, int>((Func<IDiscTrack, int>) (track => track.Index)).Last<IDiscTrack>();
      }
    }

    public List<IDiscTrack> AudioTracks => this.tracks.FindAll((Predicate<IDiscTrack>) (track => track.TrackData == TrackModeType.Audio));

    public int Index => this.index;

    public string Name => this.name;

    public DirectoryRecord MainBinary
    {
      get => this.mainBinary;
      set => this.mainBinary = value;
    }

    public DirectoryRecord RootDirectoryRecord
    {
      get => this.rootDirectoryRecord;
      set => this.rootDirectoryRecord = value;
    }

    public InitialProgram BootStrap
    {
      get => this.ip;
      set => this.ip = value;
    }

    public PrimaryVolumeDescriptor PrimaryVolumeDescriptor
    {
      get => this.primaryVolumeDescriptor;
      set => this.primaryVolumeDescriptor = value;
    }

    public IDiscFileSystem Disc
    {
      get => this.disc;
      set => this.disc = value;
    }

    public DiscSession(int index, string name, List<IDiscTrack> tracks)
    {
      this.index = index;
      this.name = name;
      this.tracks = tracks.OrderBy<IDiscTrack, uint>((Func<IDiscTrack, uint>) (track => track.LogicalBlockAddress)).ToList<IDiscTrack>();
      this.tracks.ForEach((Action<IDiscTrack>) (track => track.Session = (IDiscSession) this));
    }

    public void Close()
    {
      this.ip = (InitialProgram) null;
      this.mainBinary = (DirectoryRecord) null;
      this.rootDirectoryRecord = (DirectoryRecord) null;
      this.primaryVolumeDescriptor = (PrimaryVolumeDescriptor) null;
      if (this.tracks != null)
      {
        foreach (IDiscTrack track in this.tracks)
          track.Close();
        this.tracks.Clear();
        this.tracks = (List<IDiscTrack>) null;
      }
      this.disc = (IDiscFileSystem) null;
    }
  }
}
