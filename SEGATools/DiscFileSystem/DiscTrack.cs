﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.DiscFileSystem.DiscTrack
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using ImageReader.DiscSectors;
using SEGATools.Stream;
using SEGATools.VirtualFile;
using System;
using System.IO;

namespace SEGATools.DiscFileSystem
{
  internal sealed class DiscTrack : IDiscTrack, IVirtualFile, IDisposable
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private bool disposed;

    public string FileName { get; private set; }

    public string Name => string.Format("Track {0:00}", (object) this.Index);

    public int Index { get; private set; }

    public uint LogicalBlockAddress { get; private set; }

    public long Length { get; private set; }

    public long Offset { get; private set; }

    public TrackModeType TrackData { get; private set; }

    public IDiscSector TrackSector { get; private set; }

    public IDiscSession Session { get; set; }

    public static IDiscTrack CreateCopyFrom(IDiscTrack source, string newFileName) => (IDiscTrack) new DiscTrack(newFileName, source.Offset, source.Length, source.LogicalBlockAddress, source.Index, source.TrackData, source.TrackSector);

    internal DiscTrack(
      string fileName,
      long offset,
      long length,
      uint logicalBlockAdress,
      int index,
      TrackModeType trackData,
      IDiscSector trackSector)
    {
      this.disposed = false;
      this.LogicalBlockAddress = logicalBlockAdress;
      this.FileName = Path.GetFullPath(fileName);
      this.OriginalFileName = fileName;
      this.Index = index;
      this.TrackData = trackData;
      this.Offset = offset;
      this.Length = length;
      this.TrackSector = trackSector;
    }

    public void Close() => this.Session = (IDiscSession) null;

    public override string ToString() => this.Name;

    public System.IO.Stream FileInputStream => (System.IO.Stream) new SubStream((System.IO.Stream) File.Open(this.FileName, FileMode.Open, FileAccess.Read, FileShare.Read), this.Offset, this.Length, true);

    public string OriginalFileName { get; set; }

    public System.IO.Stream FileOutputStream => (System.IO.Stream) new SubStream((System.IO.Stream) File.Open(this.FileName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read), this.Offset, this.Length, true);

    public string VirtualName => Path.ChangeExtension(string.Format("track{0:00}", (object) this.Index), DiscFileUtils.GetExtensionForDiscTrack((IDiscTrack) this));

    public void Dispose()
    {
      this.Dispose(true);
      GC.SuppressFinalize((object) this);
    }

    private void Dispose(bool disposing)
    {
      if (this.disposed)
        return;
      if (disposing)
        this.Close();
      this.disposed = true;
    }
  }
}
