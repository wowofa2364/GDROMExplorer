﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.DiscFileSystem.IDiscTrack
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using ImageReader.DiscSectors;
using SEGATools.VirtualFile;
using System;

namespace SEGATools.DiscFileSystem
{
  public interface IDiscTrack : IVirtualFile, IDisposable
  {
    string FileName { get; }

    string Name { get; }

    int Index { get; }

    long Length { get; }

    uint LogicalBlockAddress { get; }

    long Offset { get; }

    TrackModeType TrackData { get; }

    IDiscSector TrackSector { get; }

    IDiscSession Session { get; set; }

    void Close();
  }
}
