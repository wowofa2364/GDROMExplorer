﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.DiscFileSystem.DiscFileSystem
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using ImageReader.ISO9660.DirectoryRecords;
using ImageReader.Stream;
using SEGATools.Binary;
using SEGATools.Encrypt;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEGATools.DiscFileSystem
{
  internal sealed class DiscFileSystem : IDiscFileSystem
  {
    private readonly Dictionary<object, List<SEGALibrary>> FileLibraries;

    public List<IDiscSession> Sessions { get; private set; }

    public string FileName { get; private set; }

    public bool CanExtractData { get; private set; }

    public bool CanBeExportedToCueSheet { get; private set; }

    public bool CanBeExportedToGdi { get; private set; }

    public string DiscName { get; set; }

    public DirectoryRecord MainBinary { get; set; }

    public DiscImageType DiscType { get; set; }

    public List<IDiscTrack> AllTracks
    {
      get
      {
        List<IDiscTrack> allTracks = new List<IDiscTrack>();
        this.Sessions.ForEach((Action<IDiscSession>) (session => allTracks.AddRange((IEnumerable<IDiscTrack>) session.Tracks)));
        return allTracks.OrderBy<IDiscTrack, uint>((Func<IDiscTrack, uint>) (track => track.LogicalBlockAddress)).ToList<IDiscTrack>();
      }
    }

    public DESKey NaomiDESKey { get; set; }

    internal DiscFileSystem(string filename, List<IDiscSession> sessions)
      : this(filename, sessions, false, false, false)
    {
    }

    internal DiscFileSystem(
      string filename,
      List<IDiscSession> sessions,
      bool supportDataExtraction)
      : this(filename, sessions, supportDataExtraction, false, false)
    {
    }

    internal DiscFileSystem(
      string filename,
      List<IDiscSession> sessions,
      bool supportDataExtraction,
      bool supportCueSheet)
      : this(filename, sessions, supportDataExtraction, supportCueSheet, false)
    {
    }

    internal DiscFileSystem(
      string filename,
      List<IDiscSession> sessions,
      bool supportDataExtraction,
      bool supportCueSheet,
      bool supportGdiExport)
    {
      this.FileName = filename;
      this.DiscName = "N/A";
      sessions.ForEach((Action<IDiscSession>) (discSession => discSession.Disc = (IDiscFileSystem) this));
      this.Sessions = sessions.OrderBy<IDiscSession, int>((Func<IDiscSession, int>) (session => session.Index)).ToList<IDiscSession>();
      this.DiscType = DiscImageType.Unknown;
      this.CanExtractData = supportDataExtraction;
      this.CanBeExportedToCueSheet = supportCueSheet;
      this.CanBeExportedToGdi = supportGdiExport;
      this.FileLibraries = new Dictionary<object, List<SEGALibrary>>();
    }

    public IDiscTrack GetTrackForLogicalBlockAddress(long lba) => this.AllTracks.LastOrDefault<IDiscTrack>((Func<IDiscTrack, bool>) (track => (long) track.LogicalBlockAddress <= lba));

    public DiscSectorStream GetDiscStreamForDirectoryRecord(
      DirectoryRecord directoryRecord)
    {
      IDiscTrack logicalBlockAddress = this.GetTrackForLogicalBlockAddress((long) directoryRecord.Extent);
      return logicalBlockAddress == null || directoryRecord.IsDirectory ? (DiscSectorStream) null : new DiscSectorStream(logicalBlockAddress.FileInputStream, logicalBlockAddress.TrackSector, directoryRecord.Extent - logicalBlockAddress.LogicalBlockAddress, directoryRecord.ExtentSize, true);
    }

    public void RegisterLibraries(object file, List<SEGALibrary> Libraries) => this.FileLibraries[file] = Libraries;

    public List<SEGALibrary> GetSEGALibraries(object file) => !this.FileLibraries.Keys.Contains<object>(file) ? (List<SEGALibrary>) null : this.FileLibraries[file];

    public void Close()
    {
      this.MainBinary = (DirectoryRecord) null;
      if (this.Sessions == null)
        return;
      foreach (IDiscSession session in this.Sessions)
        session.Close();
      this.Sessions.Clear();
      this.Sessions = (List<IDiscSession>) null;
    }
  }
}
