﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.DiscFileSystem.IDiscSession
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using ImageReader.ISO9660.DirectoryRecords;
using ImageReader.ISO9660.VolumeDescriptors;
using SEGATools.Security;
using System.Collections.Generic;

namespace SEGATools.DiscFileSystem
{
  public interface IDiscSession
  {
    List<IDiscTrack> Tracks { get; }

    List<IDiscTrack> DataTracks { get; }

    IDiscTrack FirstDataTrack { get; }

    IDiscTrack LastDataTrack { get; }

    List<IDiscTrack> AudioTracks { get; }

    int Index { get; }

    string Name { get; }

    DirectoryRecord MainBinary { get; set; }

    InitialProgram BootStrap { get; set; }

    PrimaryVolumeDescriptor PrimaryVolumeDescriptor { get; set; }

    DirectoryRecord RootDirectoryRecord { get; set; }

    IDiscFileSystem Disc { get; set; }

    void Close();
  }
}
