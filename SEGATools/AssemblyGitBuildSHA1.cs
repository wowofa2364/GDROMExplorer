﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.AssemblyGitBuildSHA1
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools
{
  [AttributeUsage(AttributeTargets.Assembly)]
  public class AssemblyGitBuildSHA1 : Attribute
  {
    public string GitBuildSHA1 { get; private set; }

    public AssemblyGitBuildSHA1(string txt) => this.GitBuildSHA1 = txt;
  }
}
