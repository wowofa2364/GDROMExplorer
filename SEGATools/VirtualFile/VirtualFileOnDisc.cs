﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.VirtualFile.VirtualFileOnDisc
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System.IO;

namespace SEGATools.VirtualFile
{
  internal class VirtualFileOnDisc : VirtualFileBase<string>
  {
    public override string WrappedObject { get; internal set; }

    public VirtualFileOnDisc(string FileName)
      : base(FileName, Path.GetFileName(FileName))
      => this.WrappedObject = FileName;

    public override System.IO.Stream FileInputStream => (System.IO.Stream) File.Open(this.OriginalFileName, FileMode.Open, FileAccess.Read, FileShare.Read);

    public override System.IO.Stream FileOutputStream => (System.IO.Stream) File.Open(this.OriginalFileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read);
  }
}
