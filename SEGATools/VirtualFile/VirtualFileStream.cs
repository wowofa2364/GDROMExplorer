﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.VirtualFile.VirtualFileStream
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System.IO;

namespace SEGATools.VirtualFile
{
  internal class VirtualFileStream : VirtualFileBase<System.IO.Stream>
  {
    public override System.IO.Stream WrappedObject { get; internal set; }

    public VirtualFileStream(System.IO.Stream FileStream, string FileName)
      : base(FileName, Path.GetFileName(FileName))
      => this.WrappedObject = FileStream;

    public override System.IO.Stream FileInputStream => this.WrappedObject;

    public override System.IO.Stream FileOutputStream => (System.IO.Stream) null;
  }
}
