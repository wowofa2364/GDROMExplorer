﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.VirtualFile.VirtualFileInitialProgramExtended
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using SEGATools.Security;
using System.IO;

namespace SEGATools.VirtualFile
{
  internal class VirtualFileInitialProgramExtended : VirtualFileBase<InitialProgramExtended>
  {
    public override InitialProgramExtended WrappedObject { get; internal set; }

    public VirtualFileInitialProgramExtended(InitialProgramExtended InitialProgram)
      : base(string.Empty, InitialProgram.FileName)
      => this.WrappedObject = InitialProgram;

    public override System.IO.Stream FileInputStream => this.WrappedObject.Stream;

    public override System.IO.Stream FileOutputStream => (System.IO.Stream) null;
  }
}
