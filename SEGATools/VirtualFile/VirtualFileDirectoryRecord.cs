﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.VirtualFile.VirtualFileDirectoryRecord
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using ImageReader.ISO9660.DirectoryRecords;
using SEGATools.DiscFileSystem;
using System.IO;

namespace SEGATools.VirtualFile
{
  internal class VirtualFileDirectoryRecord : VirtualFileBase<DirectoryRecord>
  {
    public override DirectoryRecord WrappedObject { get; internal set; }

    private IDiscFileSystem DiscFileSystem { get; set; }

    public VirtualFileDirectoryRecord(
      DirectoryRecord DirectoryRecord,
      IDiscFileSystem DiscFileSystem)
      : base(DiscFileSystem.FileName, DirectoryRecord.Name)
    {
      this.WrappedObject = DirectoryRecord;
      this.DiscFileSystem = DiscFileSystem;
    }

    public override System.IO.Stream FileInputStream => (System.IO.Stream) this.DiscFileSystem.GetDiscStreamForDirectoryRecord(this.WrappedObject);

    public override System.IO.Stream FileOutputStream => (System.IO.Stream) null;
  }
}
