﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.VirtualFile.VirtualFileFactory
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using ImageReader.ISO9660.DirectoryRecords;
using SEGATools.DiscFileSystem;
using SEGATools.Security;
using System.IO;

namespace SEGATools.VirtualFile
{
  public class VirtualFileFactory
  {
    public static IVirtualFile<string> createVirtualFile(string FileName) => (IVirtualFile<string>) new VirtualFileOnDisc(FileName);

    public static IVirtualFile<DirectoryRecord> createVirtualFile(
      DirectoryRecord DirectoryRecord,
      IDiscFileSystem DiscFileSystem)
    {
      return (IVirtualFile<DirectoryRecord>) new VirtualFileDirectoryRecord(DirectoryRecord, DiscFileSystem);
    }

    public static IVirtualFile<InitialProgramExtended> createVirtualFile(
      InitialProgramExtended InitialProgramExtended)
    {
      return (IVirtualFile<InitialProgramExtended>) new VirtualFileInitialProgramExtended(InitialProgramExtended);
    }

    public static IVirtualFile<System.IO.Stream> createVirtualFile(
      System.IO.Stream FileStream,
      string OriginalFileName)
    {
      return (IVirtualFile<System.IO.Stream>) new VirtualFileStream(FileStream, OriginalFileName);
    }
  }
}
