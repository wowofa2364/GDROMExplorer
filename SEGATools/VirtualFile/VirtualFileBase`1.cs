﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.VirtualFile.VirtualFileBase`1
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;
using System.IO;

namespace SEGATools.VirtualFile
{
  internal abstract class VirtualFileBase<T> : 
    IVirtualFile<T>,
    IVirtualFile,
    IEquatable<IVirtualFile>
  {
    public string OriginalFileName { get; set; }

    public string VirtualName { get; private set; }

    public abstract T WrappedObject { get; internal set; }

    public abstract System.IO.Stream FileInputStream { get; }

    public abstract System.IO.Stream FileOutputStream { get; }

    protected VirtualFileBase(string OriginalFileName, string VirtualName)
    {
      this.OriginalFileName = OriginalFileName;
      this.VirtualName = VirtualName;
    }

    public override int GetHashCode() => 1 + 17 * this.OriginalFileName.GetHashCode() + 31 * this.VirtualName.GetHashCode();

    public override bool Equals(object obj)
    {
      IVirtualFile virtualFileBase = obj as IVirtualFile;
      return !object.ReferenceEquals((object) virtualFileBase, (object) null) && this.Equals(virtualFileBase);
    }

    public bool Equals(IVirtualFile virtualFileBase) => string.Equals(this.VirtualName, virtualFileBase.VirtualName, StringComparison.Ordinal) && string.Equals(this.OriginalFileName, virtualFileBase.OriginalFileName, StringComparison.Ordinal);
  }
}
