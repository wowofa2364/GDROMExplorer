﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Properties.Resources
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SEGATools.Properties
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) SEGATools.Properties.Resources.resourceMan, (object) null))
          SEGATools.Properties.Resources.resourceMan = new ResourceManager("SEGATools.Properties.Resources", typeof (SEGATools.Properties.Resources).Assembly);
        return SEGATools.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get => SEGATools.Properties.Resources.resourceCulture;
      set => SEGATools.Properties.Resources.resourceCulture = value;
    }

    internal static string DiscFormatProviderAllExtensions => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (DiscFormatProviderAllExtensions), SEGATools.Properties.Resources.resourceCulture);

    internal static string LibRefColumnBuildDate => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (LibRefColumnBuildDate), SEGATools.Properties.Resources.resourceCulture);

    internal static string LibRefColumnName => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (LibRefColumnName), SEGATools.Properties.Resources.resourceCulture);

    internal static string LibRefColumnVersion => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (LibRefColumnVersion), SEGATools.Properties.Resources.resourceCulture);

    internal static string LibRefHint => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (LibRefHint), SEGATools.Properties.Resources.resourceCulture);

    internal static string LibRefNoLibFound => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (LibRefNoLibFound), SEGATools.Properties.Resources.resourceCulture);

    internal static string LibRefNumberPlurial => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (LibRefNumberPlurial), SEGATools.Properties.Resources.resourceCulture);

    internal static string LibRefNumberSingle => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (LibRefNumberSingle), SEGATools.Properties.Resources.resourceCulture);

    internal static string MRImageNoImageMessage => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (MRImageNoImageMessage), SEGATools.Properties.Resources.resourceCulture);

    internal static string MRIpImageSegaCustomBranding => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (MRIpImageSegaCustomBranding), SEGATools.Properties.Resources.resourceCulture);

    internal static string MRIpImageSegaLicense => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (MRIpImageSegaLicense), SEGATools.Properties.Resources.resourceCulture);

    internal static string MRIpImageSegaTM => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (MRIpImageSegaTM), SEGATools.Properties.Resources.resourceCulture);

    internal static string SfdMRImageTitle => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (SfdMRImageTitle), SEGATools.Properties.Resources.resourceCulture);

    internal static string TocColumnDataType => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (TocColumnDataType), SEGATools.Properties.Resources.resourceCulture);

    internal static string TocColumnEnd => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (TocColumnEnd), SEGATools.Properties.Resources.resourceCulture);

    internal static string TocColumnName => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (TocColumnName), SEGATools.Properties.Resources.resourceCulture);

    internal static string TocColumnSize => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (TocColumnSize), SEGATools.Properties.Resources.resourceCulture);

    internal static string TocColumnSizeInBytes => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (TocColumnSizeInBytes), SEGATools.Properties.Resources.resourceCulture);

    internal static string TocColumnSizeInSectors => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (TocColumnSizeInSectors), SEGATools.Properties.Resources.resourceCulture);

    internal static string TocColumnStart => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (TocColumnStart), SEGATools.Properties.Resources.resourceCulture);

    internal static string TocHint => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (TocHint), SEGATools.Properties.Resources.resourceCulture);

    internal static string TocNoTrackFound => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (TocNoTrackFound), SEGATools.Properties.Resources.resourceCulture);

    internal static string TocTrackNumberPlurial => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (TocTrackNumberPlurial), SEGATools.Properties.Resources.resourceCulture);

    internal static string TocTrackNumberSingle => SEGATools.Properties.Resources.ResourceManager.GetString(nameof (TocTrackNumberSingle), SEGATools.Properties.Resources.resourceCulture);
  }
}
