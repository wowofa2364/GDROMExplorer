﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Disc.DiscExtractorException
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.Disc
{
  public class DiscExtractorException : Exception
  {
    public DiscExtractorException(string message)
      : base(string.Format("Disc Extractor Error: {0}", (object) message))
    {
    }

    public DiscExtractorException(string message, Exception innerException)
      : base(string.Format("Disc Extractor Error: {0}", (object) message), innerException)
    {
    }
  }
}
