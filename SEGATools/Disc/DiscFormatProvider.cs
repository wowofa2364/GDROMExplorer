﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Disc.DiscFormatProvider
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using SEGATools.FileFormat;
using SEGATools.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;

namespace SEGATools.Disc
{
  public class DiscFormatProvider : Component
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private static readonly string FILEFORMAT_NAMESPACE = "GDRomExplorer.ImageFileFormat";
    private static readonly string FILEFORMAT_PLUGIN_FOLDER = "Formats";
    private static List<IImageFileFormat> fileFormats;
    private IContainer components;

    public List<IImageFileFormat> SupportedFileFormat => DiscFormatProvider.fileFormats.ToList<IImageFileFormat>();

    public DiscFormatProvider()
    {
      this.InitializeComponent();
      if (DiscFormatProvider.fileFormats != null)
        return;
      DiscFormatProvider.LoadImageFileFormatAssemblies();
    }

    public DiscFormatProvider(IContainer container)
    {
      container.Add((IComponent) this);
      this.InitializeComponent();
      if (DiscFormatProvider.fileFormats != null)
        return;
      DiscFormatProvider.LoadImageFileFormatAssemblies();
    }

    public string GetFileDialogFilters(params string[] fileExtensionFilters)
    {
      int num = 0;
      List<string> stringList1 = new List<string>();
      List<string> stringList2 = new List<string>();
      foreach (IImageFileFormat fileFormat in DiscFormatProvider.fileFormats)
      {
        string[] strArray = fileFormat.FileExtentions;
        if (fileExtensionFilters != null && fileExtensionFilters.Length > 0)
          strArray = ((IEnumerable<string>) fileFormat.FileExtentions).Where<string>((Func<string, bool>) (extension => ((IEnumerable<string>) fileExtensionFilters).Contains<string>(extension))).ToArray<string>();
        for (int index = 0; index < strArray.Length; ++index)
        {
          stringList1.Add(fileFormat.FileExtentionDescriptions[index]);
          string str = string.Format("*{0}", (object) fileFormat.FileExtentions[index]);
          stringList1.Add(str);
          stringList2.Add(str);
          ++num;
        }
      }
      if (num > 1)
      {
        stringList1.Insert(0, string.Join(";", stringList2.ToArray()));
        stringList1.Insert(0, Resources.DiscFormatProviderAllExtensions);
      }
      return string.Join("|", stringList1.ToArray());
    }

    public int GetFileFormatIndex(IImageFileFormat imageFileFormat) => DiscFormatProvider.fileFormats.IndexOf(imageFileFormat) + 2;

    public bool IsFileExtensionSupported(string fileExtension) => this.FindImageFileFormatForFileExtension(fileExtension) != null;

    public IImageFileFormat FindImageFileFormatForFileExtension(string fileExtension) => string.IsNullOrEmpty(fileExtension) ? (IImageFileFormat) null : DiscFormatProvider.fileFormats.Find((Predicate<IImageFileFormat>) (imageFileFormat => new List<string>((IEnumerable<string>) imageFileFormat.FileExtentions).Contains<string>(fileExtension, (IEqualityComparer<string>) StringComparer.InvariantCultureIgnoreCase)));

    public string GetDescriptionForFileExtension(string extension, IImageFileFormat imageFileFormat)
    {
      for (int index = 0; index < imageFileFormat.FileExtentions.Length; ++index)
      {
        if (imageFileFormat.FileExtentions[index].Equals(extension, StringComparison.InvariantCultureIgnoreCase))
          return imageFileFormat.FileExtentionDescriptions[index];
      }
      return string.Empty;
    }

    private static void LoadImageFileFormatAssemblies()
    {
      string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), DiscFormatProvider.FILEFORMAT_PLUGIN_FOLDER);
      try
      {
        DiscFormatProvider.LoadImageFileFormatAssembliesFromPluginPath(path);
      }
      catch (DirectoryNotFoundException ex)
      {
        DiscFormatProvider.logger.ErrorFormat("Unable to find the plugin folder: {0}", (object) path);
        logger.Error(ex);
      }
    }

    private static void LoadImageFileFormatAssembliesFromPluginPath(string path)
    {
      DiscFormatProvider.fileFormats = new List<IImageFileFormat>();
      List<Type> typeList = new List<Type>();
      Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
      foreach (string file in Directory.GetFiles(path, "*.dll"))
      {
        try
        {
          List<Type> list = ((IEnumerable<Type>) Assembly.LoadFile(file).GetTypes()).Where<Type>((Func<Type, bool>) (t => t.IsClass && t.Namespace != null && t.Namespace.StartsWith("GDRomExplorer.ImageFileFormat") && typeof (IImageFileFormat).IsAssignableFrom(t))).ToList<Type>();
          typeList.AddRange((IEnumerable<Type>) list);
        }
        catch (ReflectionTypeLoadException ex)
        {
          DiscFormatProvider.logger.ErrorFormat("Unable to load types in namespace \"{0}\"", (object) DiscFormatProvider.FILEFORMAT_NAMESPACE);
          logger.Error(ex);
          return;
        }
      }
      DiscFormatProvider.logger.InfoFormat("Found {0} file formats to load", (object) typeList.Count);
      foreach (Type type in typeList)
      {
        try
        {
          IImageFileFormat instance = (IImageFileFormat) Activator.CreateInstance(type);
          if (DiscFormatProvider.fileFormats.Contains(instance))
          {
            DiscFormatProvider.logger.WarnFormat("File format {0} already loaded", (object) type.Name);
          }
          else
          {
            instance.PluginAssemblyFileName = type.Assembly.CodeBase;
            DiscFormatProvider.fileFormats.Add(instance);
            DiscFormatProvider.logger.InfoFormat("File format {0} loaded", (object) type.Name);
          }
        }
        catch (Exception ex)
        {
          DiscFormatProvider.logger.ErrorFormat("Unable to load file format {0}: {1}", (object) type.Name, (object) ex);
        }
      }
      DiscFormatProvider.fileFormats = DiscFormatProvider.fileFormats.Distinct<IImageFileFormat>().ToList<IImageFileFormat>();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent() => this.components = (IContainer) new Container();
  }
}
