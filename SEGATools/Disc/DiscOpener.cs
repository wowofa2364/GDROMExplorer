﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Disc.DiscOpener
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using SEGATools.Binary;
using SEGATools.DiscFileSystem;
using SEGATools.Scanner;
using SEGATools.UserProcess;
using SEGATools.VirtualFile;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;

namespace SEGATools.Disc
{
  public class DiscOpener : UserProcessBase
  {
    private IDiscFileSystem disc;
    private bool computePathTable;

    public event AsyncOperationProgressChangedEventHandler FileOpenerProgressChanged
    {
      add => this.AsyncOperationProgressChanged += value;
      remove => this.AsyncOperationProgressChanged -= value;
    }

    public event AsyncOperationCompletedEventHandler FileOpenerCompleted
    {
      add => this.AsyncOperationCompleted += value;
      remove => this.AsyncOperationCompleted -= value;
    }

    public IDiscFileSystem DiscFileSystem => this.disc;

    public DiscOpener()
    {
    }

    public DiscOpener(IContainer container)
      : base(container)
    {
    }

    public void OpenImageAsync(
      string Filename,
      IDiscFileSystemConverter ImageFileConverter,
      bool ComputePathTable)
    {
      AsyncOperation asyncOperation = this.CreateAsyncOperation((object) Guid.NewGuid());
      this.computePathTable = ComputePathTable;
      new DiscOpener.FileOpenerWorkerEventHandler(this.FileOpenerWorker).BeginInvoke(Filename, ImageFileConverter, asyncOperation, (AsyncCallback) null, (object) null);
    }

    public void OpenImage(
      string Filename,
      IDiscFileSystemConverter ImageFileConverter,
      bool ComputePathTable)
    {
      this.computePathTable = ComputePathTable;
      this.FileOpenerWorker(Filename, ImageFileConverter, (AsyncOperation) null);
    }

    public void CloseImage()
    {
      if (this.disc == null)
        return;
      this.disc.Close();
      this.disc = (IDiscFileSystem) null;
    }

    private void FileOpenerWorker(
      string Filename,
      IDiscFileSystemConverter ImageFileConverter,
      AsyncOperation asyncOp)
    {
      Exception exception = (Exception) null;
      if (asyncOp != null)
        this.ReportProgress(new UserProcessProgressChangedEventArgs(Filename, 0, asyncOp.UserSuppliedState), asyncOp);
      if (this.disc != null)
      {
        this.disc.Close();
        this.disc = (IDiscFileSystem) null;
      }
      try
      {
        this.disc = DiscFileSystemBuilder.ToDisc(Filename, ImageFileConverter, this.computePathTable);
        foreach (IDiscSession discSession in this.disc.Sessions.FindAll((Predicate<IDiscSession>) (session => session.BootStrap != null)))
        {
          using (FileScanner fileScanner = new FileScanner())
          {
            IVirtualFile<System.IO.Stream> virtualFile = VirtualFileFactory.createVirtualFile(discSession.BootStrap.Stream, this.disc.FileName);
            List<SEGALibrary> Libraries = fileScanner.ScanFile<SEGALibrary>((IVirtualFile) virtualFile, FileScannerPattern.aPatternForSEGALibraries(), (IFileScannerResultConverter<SEGALibrary>) new FileScannerResultConverterForSEGALibrary());
            this.disc.RegisterLibraries((object) discSession.BootStrap, Libraries);
          }
        }
      }
      catch (Exception ex)
      {
        exception = ex;
        this.disc = (IDiscFileSystem) null;
        UserProcessBase.logger.DebugFormat("Error while reading the image file: {0}", (object) ex);
      }
      if ((this.TaskCanceled(asyncOp) || exception != null) && this.disc != null)
      {
        this.disc.Close();
        this.disc = (IDiscFileSystem) null;
      }
      this.ReportCompletion(Filename, exception, asyncOp);
    }

    private delegate void FileOpenerWorkerEventHandler(
      string filename,
      IDiscFileSystemConverter imageFileConverter,
      AsyncOperation asyncOp);
  }
}
