﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Disc.DiscSectorEncoder
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using ImageReader.DiscSectors;
using SEGATools.HashAlgorithm;
using SEGATools.UserProcess;
using System;
using System.ComponentModel;
using System.IO;

namespace SEGATools.Disc
{
  public class DiscSectorEncoder : UserProcessBase
  {
    private EDC edcEncoder = new EDC();
    private ECC.ECCParityVectorAlgorithm eccPParityEncoder = (ECC.ECCParityVectorAlgorithm) new ECC.PParity();
    private ECC.ECCParityVectorAlgorithm eccQParityEncoder = (ECC.ECCParityVectorAlgorithm) new ECC.QParity();

    public DiscSectorEncoder()
    {
    }

    public DiscSectorEncoder(IContainer container)
      : base(container)
    {
    }

    public void EncodeMode1Sectors(System.IO.Stream stream, int[] sectors)
    {
      byte[] numArray = new byte[DiscSectorCommon.RawSectorSize];
      for (int index = 0; index < sectors.Length; ++index)
      {
        UserProcessBase.logger.DebugFormat("Patching EDC and ECC data of sector {0}", (object) sectors[index]);
        stream.Seek((long) (sectors[index] * DiscSectorCommon.RawSectorSize), SeekOrigin.Begin);
        stream.Read(numArray, 0, DiscSectorCommon.RawSectorSize);
        this.ClearErrorDetectionAndCorrectionData(numArray);
        this.GenerateEDC(numArray);
        this.eccPParityEncoder.ComputeVectors(numArray);
        this.eccQParityEncoder.ComputeVectors(numArray);
        stream.Seek((long) -DiscSectorCommon.RawSectorSize, SeekOrigin.Current);
        stream.Write(numArray, 0, numArray.Length);
      }
    }

    public void GenerateEDC(byte[] sector)
    {
      byte[] hash = this.edcEncoder.ComputeHash(sector, 0, EDC.SECTOR_DATA_LENGTH_FOR_EDC_COMPUTATION);
      Buffer.BlockCopy((Array) hash, 0, (Array) sector, EDC.EDC_DATA_OFFSET, hash.Length);
    }

    public void ClearErrorDetectionAndCorrectionData(byte[] sector)
    {
      for (int edcDataOffset = EDC.EDC_DATA_OFFSET; edcDataOffset < sector.Length; ++edcDataOffset)
        sector[edcDataOffset] = (byte) 0;
    }
  }
}
