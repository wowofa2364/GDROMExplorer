﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Formater.SizeFormater
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.Formater
{
  public static class SizeFormater
  {
    public static string ToHumanReadableSize(long Size)
    {
      double num = Convert.ToDouble(Size);
      if (num >= Math.Pow(1024.0, 4.0))
        return Math.Round(num / Math.Pow(1024.0, 4.0), 2).ToString() + " TB";
      if (num >= Math.Pow(1024.0, 3.0))
        return Math.Round(num / Math.Pow(1024.0, 3.0), 2).ToString() + " GB";
      if (num >= Math.Pow(1024.0, 2.0))
        return Math.Round(num / Math.Pow(1024.0, 2.0), 2).ToString() + " MB";
      if (num >= 1024.0)
        return Math.Round(num / 1024.0, 2).ToString() + " KB";
      return num == 0.0 ? num.ToString() + " Byte" : num.ToString() + " Bytes";
    }
  }
}
