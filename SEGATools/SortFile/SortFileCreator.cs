﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.SortFile.SortFileCreator
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using ImageReader.ISO9660.DirectoryRecords;
using SEGATools.DiscFileSystem;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SEGATools.SortFile
{
  public class SortFileCreator
  {
    public static readonly string DefaultFileName = "sortfile";

    public static void CreateSortFile(
      IDiscSession Session,
      string PathPrefix,
      int lowestFileWeight,
      string OutputSortFilePath)
    {
      List<DirectoryRecord> list = Session.RootDirectoryRecord.GetAllSubDirectories().FindAll((Predicate<DirectoryRecord>) (directoryRecord => !directoryRecord.IsDirectory)).OrderByDescending<DirectoryRecord, uint>((Func<DirectoryRecord, uint>) (directoryRecord => directoryRecord.Extent)).ToList<DirectoryRecord>();
      using (StreamWriter streamWriter = new StreamWriter(OutputSortFilePath))
      {
        for (int index = 0; index < list.Count; ++index)
        {
          string str = Path.Combine(PathPrefix, list[index].FullPath.Substring(1));
          streamWriter.WriteLine(str.Replace("\\", "/") + " " + (object) (index + lowestFileWeight));
        }
      }
    }
  }
}
