﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.InitialProgramToc
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;
using System.Collections.Generic;
using System.Linq;

namespace SEGATools.Security
{
  public class InitialProgramToc
  {
    public InitialProgramTrackInfo FirstTrack => this.Tracks.Aggregate<InitialProgramTrackInfo>((Func<InitialProgramTrackInfo, InitialProgramTrackInfo, InitialProgramTrackInfo>) ((a, b) => a.Index > b.Index ? b : a));

    public InitialProgramTrackInfo LastTrack => this.Tracks.Aggregate<InitialProgramTrackInfo>((Func<InitialProgramTrackInfo, InitialProgramTrackInfo, InitialProgramTrackInfo>) ((a, b) => a.Index <= b.Index ? b : a));

    public bool HasSplitedDataTracks => this.FirstTrack != null && this.LastTrack != null && this.FirstTrack.Index != this.LastTrack.Index;

    public List<InitialProgramTrackInfo> Tracks { get; private set; }

    internal InitialProgramToc(InitialProgramTrackInfo[] tracks) => this.Tracks = ((IEnumerable<InitialProgramTrackInfo>) tracks).OrderBy<InitialProgramTrackInfo, int>((Func<InitialProgramTrackInfo, int>) (track => track.Index)).ToList<InitialProgramTrackInfo>();
  }
}
