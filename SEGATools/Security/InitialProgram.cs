﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.InitialProgram
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using SEGATools.Graphics;
using System;
using System.IO;

namespace SEGATools.Security
{
  public class InitialProgram
  {
    public static readonly uint IP_FILESIZE = 32768;
    private static readonly string IP_FILENAME = "IP.BIN";
    private static readonly string IP0000_FILENAME = "IP0000.BIN";

    public byte[] RawData { get; internal set; }

    public string RawPeripherals { get; internal set; }

    public string HardwareID { get; internal set; }

    public string MakerID { get; internal set; }

    public string CRC { get; internal set; }

    public string ComputedCRC { get; internal set; }

    public bool IsCRCValid => this.CRC.Equals(this.ComputedCRC);

    public string DeviceInformation { get; internal set; }

    public string AreaSymbols { get; internal set; }

    public string ProductID { get; internal set; }

    public string ProductVersion { get; internal set; }

    public string MainBinary { get; internal set; }

    public string CompanyName { get; internal set; }

    public string SoftwareName { get; internal set; }

    public DateTime ReleaseDate { get; internal set; }

    public InitialProgramToc TableOfContent { get; internal set; }

    public MRImage SegaLicenseImage { get; internal set; }

    public MRImage SegaTradeMarkImage { get; internal set; }

    public MRImage CustomBrandingImage { get; internal set; }

    public bool SupportVga => InitialProgramUtils.ConvertHexStringToBytes(this.RawPeripherals.Substring(5, 1))[0] == (byte) 1;

    public bool UseWindowsCE => InitialProgramUtils.ConvertHexStringToBytes(this.RawPeripherals.Substring(6, 1))[0] == (byte) 1;

    public bool SupportJapanArea => this.SupportArea(SupportedAreas.Japan);

    public bool SupportUsaArea => this.SupportArea(SupportedAreas.Usa);

    public bool SupportEuropeArea => this.SupportArea(SupportedAreas.Europe);

    public bool SupportGun => this.SupportPeripheral(SupportedPeripherals.Gun);

    public bool SupportKeyboard => this.SupportPeripheral(SupportedPeripherals.Keyboard);

    public bool SupportMouse => this.SupportPeripheral(SupportedPeripherals.Mouse);

    public bool SupportMemoryCard => this.SupportExpandedPeripheral(SupportedExpandedPeripherals.MemoryCard);

    public bool SupportSoundInputPeripheral => this.SupportExpandedPeripheral(SupportedExpandedPeripherals.SoundInputPeripheral);

    public bool SupportVibrator => this.SupportExpandedPeripheral(SupportedExpandedPeripherals.Vibrator);

    public bool SupportOtherDevices => this.SupportExpandedPeripheral(SupportedExpandedPeripherals.OtherDevices);

    public bool SupportStartABDirections => this.SupportButtons(SupportedButtons.StartABDirections);

    public bool SupportCButton => this.SupportButtons(SupportedButtons.CButton);

    public bool SupportDButton => this.SupportButtons(SupportedButtons.DButton);

    public bool SupportXButton => this.SupportButtons(SupportedButtons.XButton);

    public bool SupportYButton => this.SupportButtons(SupportedButtons.YButton);

    public bool SupportZButton => this.SupportButtons(SupportedButtons.ZButton);

    public bool SupportExpandedDirectionButtons => this.SupportButtons(SupportedButtons.ExpandedDirectionButtons);

    public bool SupportAnalogRTrigger => this.SupportButtons(SupportedButtons.AnalogRTrigger);

    public bool SupportAnalogLTrigger => this.SupportButtons(SupportedButtons.AnalogLTrigger);

    public bool SupportAnalogHorizontalController => this.SupportButtons(SupportedButtons.AnalogHorizontalController);

    public bool SupportAnalogVerticalController => this.SupportButtons(SupportedButtons.AnalogVerticalController);

    public bool SupportExpandedAnalogHorizontal => this.SupportButtons(SupportedButtons.ExpandedAnalogHorizontal);

    public bool SupportExpandedAnalogVertical => this.SupportButtons(SupportedButtons.ExpandedAnalogVertical);

    public bool SupportArea(SupportedAreas supportedAreas)
    {
      SupportedAreas supportedAreas1 = SupportedAreas.None;
      if (this.AreaSymbols[0] == 'J')
        supportedAreas1 |= SupportedAreas.Japan;
      if (this.AreaSymbols[1] == 'U')
        supportedAreas1 |= SupportedAreas.Usa;
      if (this.AreaSymbols[2] == 'E')
        supportedAreas1 |= SupportedAreas.Europe;
      return (supportedAreas1 & supportedAreas) == supportedAreas;
    }

    public bool SupportPeripheral(SupportedPeripherals supportedPeripherals) => ((SupportedPeripherals) InitialProgramUtils.ConvertHexStringToBytes(this.RawPeripherals.Substring(0, 1))[0] & supportedPeripherals) == supportedPeripherals;

    public bool SupportExpandedPeripheral(
      SupportedExpandedPeripherals supportedExpandedPeripherals)
    {
      return ((SupportedExpandedPeripherals) InitialProgramUtils.ConvertHexStringToBytes(this.RawPeripherals.Substring(4, 1))[0] & supportedExpandedPeripherals) == supportedExpandedPeripherals;
    }

    public bool SupportButtons(SupportedButtons supportedButtons) => ((SupportedButtons) BitConverter.ToUInt32(InitialProgramUtils.ConvertHexStringToBytes(this.RawPeripherals.Substring(0, 4)), 0) & supportedButtons) == supportedButtons;

    public System.IO.Stream Stream => (System.IO.Stream) new MemoryStream(this.RawData, 0, this.RawData.Length, false);

    public string FileName => this.SegaLicenseImage != null ? InitialProgram.IP_FILENAME : InitialProgram.IP0000_FILENAME;
  }
}
