﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.InitialProgramTocViewer
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using ImageReader.DiscSectors;
using SEGATools.Formater;
using SEGATools.Properties;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SEGATools.Security
{
  public class InitialProgramTocViewer : UserControl
  {
    private IContainer components;
    private Label lbTocHint;
    private TableLayoutPanel tableLayoutPanel;
    private ListView listViewToc;

    public InitialProgramTocViewer()
    {
      this.InitializeComponent();
      this.InitializeHintText(0);
      this.InitializeListViewColumns();
    }

    public void LoadInitialProgram(InitialProgramExtended ip)
    {
      if (ip == null)
        return;
      this.LoadToc((InitialProgram) ip);
    }

    private void InitializeHintText(int NumberOfTracks)
    {
      string str;
      switch (NumberOfTracks)
      {
        case 0:
          str = Resources.TocNoTrackFound;
          break;
        case 1:
          str = Resources.TocTrackNumberSingle;
          break;
        default:
          str = string.Format(Resources.TocTrackNumberPlurial, (object) NumberOfTracks);
          break;
      }
      this.lbTocHint.Text = string.Format(Resources.TocHint, (object) str);
    }

    private void InitializeListViewColumns()
    {
      this.listViewToc.Columns.Add(InitialProgramTocViewer.createColumn(Resources.TocColumnName, HorizontalAlignment.Center));
      this.listViewToc.Columns.Add(InitialProgramTocViewer.createColumn(Resources.TocColumnDataType, HorizontalAlignment.Center));
      this.listViewToc.Columns.Add(InitialProgramTocViewer.createColumn(Resources.TocColumnStart, HorizontalAlignment.Right));
      this.listViewToc.Columns.Add(InitialProgramTocViewer.createColumn(Resources.TocColumnEnd, HorizontalAlignment.Right));
      this.listViewToc.Columns.Add(InitialProgramTocViewer.createColumn(Resources.TocColumnSizeInSectors, HorizontalAlignment.Right));
      this.listViewToc.Columns.Add(InitialProgramTocViewer.createColumn(Resources.TocColumnSize, HorizontalAlignment.Right));
      this.listViewToc.Columns.Add(InitialProgramTocViewer.createColumn(Resources.TocColumnSizeInBytes, HorizontalAlignment.Right));
      this.ResizeListViewColumns();
    }

    private static ColumnHeader createColumn(string Name, HorizontalAlignment alignment) => new ColumnHeader()
    {
      Text = Name,
      TextAlign = alignment
    };

    private void ResizeListViewColumns() => this.listViewToc.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

    private void LoadToc(InitialProgram ip)
    {
      if (ip.TableOfContent == null)
      {
        this.InitializeHintText(0);
        this.ResizeListViewColumns();
      }
      else
      {
        foreach (InitialProgramTrackInfo track in ip.TableOfContent.Tracks)
        {
          long Size = (long) track.Size * (long) DiscSectorCommon.RawSectorSize;
          this.listViewToc.Items.Add(new ListViewItem(new string[7]
          {
            string.Format("{0:00}", (object) track.Index),
            track.Ctrl.ToString().ToLower(),
            track.FrameAddress.ToString(),
            track.LastFrameAddress.ToString(),
            track.Size.ToString(),
            SizeFormater.ToHumanReadableSize(Size),
            Size.ToString()
          }));
        }
        this.InitializeHintText(ip.TableOfContent.Tracks.Count);
        this.ResizeListViewColumns();
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.lbTocHint = new Label();
      this.tableLayoutPanel = new TableLayoutPanel();
      this.listViewToc = new ListView();
      this.tableLayoutPanel.SuspendLayout();
      this.SuspendLayout();
      this.lbTocHint.BackColor = Color.Transparent;
      this.lbTocHint.Dock = DockStyle.Fill;
      this.lbTocHint.Location = new Point(3, 0);
      this.lbTocHint.Name = "lbTocHint";
      this.lbTocHint.Size = new Size(422, 24);
      this.lbTocHint.TabIndex = 0;
      this.lbTocHint.Text = "...";
      this.lbTocHint.TextAlign = ContentAlignment.MiddleLeft;
      this.tableLayoutPanel.BackColor = Color.Transparent;
      this.tableLayoutPanel.ColumnCount = 1;
      this.tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100f));
      this.tableLayoutPanel.Controls.Add((Control) this.lbTocHint, 0, 0);
      this.tableLayoutPanel.Controls.Add((Control) this.listViewToc, 0, 1);
      this.tableLayoutPanel.Dock = DockStyle.Fill;
      this.tableLayoutPanel.Location = new Point(0, 0);
      this.tableLayoutPanel.Name = "tableLayoutPanel";
      this.tableLayoutPanel.RowCount = 2;
      this.tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 24f));
      this.tableLayoutPanel.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel.Size = new Size(428, 150);
      this.tableLayoutPanel.TabIndex = 1;
      this.listViewToc.BackColor = SystemColors.Window;
      this.listViewToc.CausesValidation = false;
      this.listViewToc.Dock = DockStyle.Fill;
      this.listViewToc.FullRowSelect = true;
      this.listViewToc.HeaderStyle = ColumnHeaderStyle.Nonclickable;
      this.listViewToc.Location = new Point(0, 24);
      this.listViewToc.Margin = new Padding(0);
      this.listViewToc.Name = "listViewToc";
      this.listViewToc.ShowGroups = false;
      this.listViewToc.Size = new Size(428, 126);
      this.listViewToc.TabIndex = 1;
      this.listViewToc.UseCompatibleStateImageBehavior = false;
      this.listViewToc.View = View.Details;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = Color.Transparent;
      this.Controls.Add((Control) this.tableLayoutPanel);
      this.Name = nameof (InitialProgramTocViewer);
      this.Size = new Size(428, 150);
      this.tableLayoutPanel.ResumeLayout(false);
      this.ResumeLayout(false);
    }
  }
}
