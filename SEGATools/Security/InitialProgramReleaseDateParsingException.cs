﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.InitialProgramReleaseDateParsingException
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.Security
{
  public class InitialProgramReleaseDateParsingException : InitialProgramException
  {
    public InitialProgramReleaseDateParsingException(string releaseDate, Exception innerException)
      : base(string.Format("invalid release date \"{0}\"", (object) releaseDate), innerException)
    {
    }
  }
}
