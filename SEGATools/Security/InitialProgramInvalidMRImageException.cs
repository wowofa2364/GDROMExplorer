﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.InitialProgramInvalidMRImageException
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.Security
{
  public class InitialProgramInvalidMRImageException : InitialProgramException
  {
    public InitialProgramInvalidMRImageException(int offset, Exception innerException)
      : base(string.Format("data at offset 0x{0:X8} is not a valid MR image", (object) offset), innerException)
    {
    }
  }
}
