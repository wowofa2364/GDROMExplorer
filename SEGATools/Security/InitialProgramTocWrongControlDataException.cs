﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.InitialProgramTocWrongControlDataException
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

namespace SEGATools.Security
{
  internal class InitialProgramTocWrongControlDataException : InitialProgramTocException
  {
    public int TrackNumber { get; private set; }

    public InitialProgramTrackInfo.ControlData ControlData { get; private set; }

    public InitialProgramTocWrongControlDataException(
      int trackNumber,
      InitialProgramTrackInfo.ControlData wrongCtrlValue)
      : base(string.Format("Track {0} control data value is wrong: {1}", (object) trackNumber, (object) wrongCtrlValue))
    {
      this.TrackNumber = trackNumber;
      this.ControlData = wrongCtrlValue;
    }
  }
}
