﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.InitialProgramTocTrackBuilder
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

namespace SEGATools.Security
{
  public class InitialProgramTocTrackBuilder
  {
    private int Index;
    private uint StartFAD;
    private uint Size;
    private InitialProgramTrackInfo.ControlData Ctrl;

    private InitialProgramTocTrackBuilder()
    {
    }

    public InitialProgramTocTrackBuilder WithIndex(int Index)
    {
      this.Index = Index;
      return this;
    }

    public InitialProgramTocTrackBuilder WithStartFAD(uint StartFAD)
    {
      this.StartFAD = StartFAD;
      return this;
    }

    public InitialProgramTocTrackBuilder WithCtrl(
      InitialProgramTrackInfo.ControlData Ctrl)
    {
      this.Ctrl = Ctrl;
      return this;
    }

    public InitialProgramTocTrackBuilder WithLastFAD(uint LastFAD)
    {
      this.Size = LastFAD + 1U - this.StartFAD;
      return this;
    }

    public InitialProgramTrackInfo Build() => new InitialProgramTrackInfo(this.Index, this.StartFAD, this.Size, this.Ctrl);

    public static InitialProgramTocTrackBuilder aTrackWithIndex(
      int Index)
    {
      return new InitialProgramTocTrackBuilder().WithIndex(Index);
    }
  }
}
