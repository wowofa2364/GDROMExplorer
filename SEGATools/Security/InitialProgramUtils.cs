﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.InitialProgramUtils
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.Security
{
  public class InitialProgramUtils
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();

    public static int ComputeCRC(string productId, string productVersion)
    {
      int num = (int) ushort.MaxValue;
      foreach (char ch in productId + productVersion)
      {
        num ^= (int) ch << 8;
        for (int index = 0; index < 8; ++index)
        {
          if ((num & 32768) > 1)
            num = num << 1 ^ 4129;
          else
            num <<= 1;
        }
      }
      return num & (int) ushort.MaxValue;
    }

    public static byte[] ConvertHexStringToBytes(string hexString)
    {
      byte[] numArray = new byte[hexString.Length];
      for (int index = 0; index < numArray.Length; ++index)
      {
        try
        {
          numArray[index] = Convert.ToByte(hexString[index].ToString(), 16);
        }
        catch (Exception ex)
        {
          logger.Error(ex);
          numArray[index] = (byte) 0;
        }
      }
      if (BitConverter.IsLittleEndian)
        Array.Reverse((Array) numArray);
      return numArray;
    }
  }
}
