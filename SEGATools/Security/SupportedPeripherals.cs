﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.SupportedPeripherals
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.Security
{
  [Flags]
  public enum SupportedPeripherals
  {
    None = 0,
    Unused1 = 1,
    Keyboard = 2,
    Gun = 4,
    Mouse = 8,
    Unused2 = 16, // 0x00000010
    Unused3 = 32, // 0x00000020
    Unused4 = 64, // 0x00000040
    Unused5 = 128, // 0x00000080
  }
}
