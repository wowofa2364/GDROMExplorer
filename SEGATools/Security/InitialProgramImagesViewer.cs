﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.InitialProgramImagesViewer
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using SEGATools.Graphics;
using SEGATools.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SEGATools.Security
{
  public class InitialProgramImagesViewer : UserControl
  {
    private List<InitialProgramImagesViewer.MRImageItem> mrImageItems = new List<InitialProgramImagesViewer.MRImageItem>();
    private IContainer components;
    private ComboBox cbMRImages;
    private Label lbMRImage;
    private MRImageViewer mrImageViewer;

    public InitialProgramImagesViewer()
    {
      this.InitializeComponent();
      this.cbMRImages.SelectionChangeCommitted += new EventHandler(this.cbMRImages_SelectionChangeCommitted);
    }

    public void LoadInitialProgram(InitialProgramExtended ip, string basePath)
    {
      this.mrImageViewer.BasePath = basePath;
      this.cbMRImages.Enabled = false;
      this.mrImageItems.Clear();
      this.addMRImage(ip.SegaLicenseImage, Resources.MRIpImageSegaLicense);
      this.addMRImage(ip.CustomBrandingImage, Resources.MRIpImageSegaCustomBranding);
      this.addMRImage(ip.SegaTradeMarkImage, Resources.MRIpImageSegaTM);
      this.cbMRImages.DataSource = (object) this.mrImageItems;
      this.cbMRImages.DisplayMember = InitialProgramImagesViewer.MRImageItem.DisplayMember;
      this.cbMRImages.ValueMember = InitialProgramImagesViewer.MRImageItem.ValueMember;
      if (this.mrImageItems.Count <= 0)
        return;
      this.cbMRImages.Enabled = true;
      this.cbMRImages.SelectedIndex = 0;
      this.mrImageViewer.LoadMRImage(this.mrImageItems[0].Image, this.mrImageItems[0].Name);
    }

    private void addMRImage(MRImage mrImage, string displayName)
    {
      if (mrImage == null)
        return;
      this.mrImageItems.Add(new InitialProgramImagesViewer.MRImageItem(displayName, mrImage));
    }

    private void cbMRImages_SelectionChangeCommitted(object sender, EventArgs e)
    {
      if (this.cbMRImages.SelectedItem == null)
        return;
      InitialProgramImagesViewer.MRImageItem selectedItem = (InitialProgramImagesViewer.MRImageItem) this.cbMRImages.SelectedItem;
      this.mrImageViewer.LoadMRImage(selectedItem.Image, selectedItem.Name);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.cbMRImages = new ComboBox();
      this.lbMRImage = new Label();
      this.mrImageViewer = new MRImageViewer();
      this.SuspendLayout();
      this.cbMRImages.DropDownStyle = ComboBoxStyle.DropDownList;
      this.cbMRImages.FlatStyle = FlatStyle.Popup;
      this.cbMRImages.FormattingEnabled = true;
      this.cbMRImages.Location = new Point(76, 9);
      this.cbMRImages.Name = "cbMRImages";
      this.cbMRImages.Size = new Size(380, 21);
      this.cbMRImages.TabIndex = 0;
      this.lbMRImage.AutoSize = true;
      this.lbMRImage.Location = new Point(11, 12);
      this.lbMRImage.Name = "lbMRImage";
      this.lbMRImage.Size = new Size(59, 13);
      this.lbMRImage.TabIndex = 0;
      this.lbMRImage.Text = "MR Image:";
      this.mrImageViewer.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.mrImageViewer.Location = new Point(2, 35);
      this.mrImageViewer.Name = "mrImageViewer";
      this.mrImageViewer.Size = new Size(460, 205);
      this.mrImageViewer.TabIndex = 0;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.cbMRImages);
      this.Controls.Add((Control) this.lbMRImage);
      this.Controls.Add((Control) this.mrImageViewer);
      this.Name = "MRInitialProgramImagesViewer";
      this.Size = new Size(463, 240);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    internal sealed class MRImageItem
    {
      public static string DisplayMember => "Name";

      public static string ValueMember => "Image";

      public string Name { get; set; }

      public MRImage Image { get; set; }

      public MRImageItem(string name, MRImage image)
      {
        this.Name = name;
        this.Image = image;
      }
    }
  }
}
