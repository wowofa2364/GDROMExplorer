﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.InitialProgramPeripheralsViewer
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SEGATools.Security
{
  public class InitialProgramPeripheralsViewer : UserControl
  {
    private IContainer components;
    private GroupBox gbSupportedPeripherals;
    private FlowLayoutPanel fplSupportedPeripherals;
    private CheckBox cbMouse;
    private CheckBox cbKeyboard;
    private CheckBox cbGunController;
    private GroupBox gbSupportedExpandedPeripherals;
    private FlowLayoutPanel flpSupportedExpandedPeripherals;
    private CheckBox cbMemoryCard;
    private CheckBox cbVibrator;
    private CheckBox cbSoundInputPeripheral;
    private CheckBox cbOtherDevices;
    private GroupBox gbMinimalSet;
    private FlowLayoutPanel flpMinimalSet;
    private CheckBox cbStartABDirButtons;
    private CheckBox cbCButton;
    private CheckBox cbDButton;
    private CheckBox cbXButton;
    private CheckBox cbYButton;
    private CheckBox cbZButton;
    private CheckBox cbAnalogLTrigger;
    private CheckBox cbAnalogRTrigger;
    private CheckBox cbStandardAnalogX;
    private CheckBox cbStandardAnalogY;
    private CheckBox cbExpandedDirectionButtons;
    private CheckBox cbExpandedAnalogX;
    private CheckBox cbExpandedAnalogY;

    public InitialProgramPeripheralsViewer() => this.InitializeComponent();

    public void LoadInitialProgram(InitialProgramExtended ip)
    {
      if (ip == null)
        return;
      this.SetSupportedPeripherals((InitialProgram) ip);
      this.SetSupportedExpandedPeripherals((InitialProgram) ip);
      this.SetSupportedButtons((InitialProgram) ip);
    }

    private void SetSupportedPeripherals(InitialProgram ip)
    {
      this.cbGunController.Checked = ip.SupportGun;
      this.cbKeyboard.Checked = ip.SupportKeyboard;
      this.cbMouse.Checked = ip.SupportMouse;
    }

    private void SetSupportedExpandedPeripherals(InitialProgram ip)
    {
      this.cbMemoryCard.Checked = ip.SupportMemoryCard;
      this.cbVibrator.Checked = ip.SupportVibrator;
      this.cbSoundInputPeripheral.Checked = ip.SupportSoundInputPeripheral;
      this.cbOtherDevices.Checked = ip.SupportOtherDevices;
    }

    private void SetSupportedButtons(InitialProgram ip)
    {
      this.cbStartABDirButtons.Checked = ip.SupportStartABDirections;
      this.cbCButton.Checked = ip.SupportCButton;
      this.cbDButton.Checked = ip.SupportDButton;
      this.cbXButton.Checked = ip.SupportXButton;
      this.cbYButton.Checked = ip.SupportYButton;
      this.cbZButton.Checked = ip.SupportZButton;
      this.cbExpandedDirectionButtons.Checked = ip.SupportExpandedDirectionButtons;
      this.cbAnalogLTrigger.Checked = ip.SupportAnalogLTrigger;
      this.cbAnalogRTrigger.Checked = ip.SupportAnalogRTrigger;
      this.cbStandardAnalogX.Checked = ip.SupportAnalogHorizontalController;
      this.cbStandardAnalogY.Checked = ip.SupportAnalogVerticalController;
      this.cbExpandedAnalogX.Checked = ip.SupportExpandedAnalogHorizontal;
      this.cbExpandedAnalogY.Checked = ip.SupportExpandedAnalogVertical;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.gbSupportedPeripherals = new GroupBox();
      this.fplSupportedPeripherals = new FlowLayoutPanel();
      this.cbMouse = new CheckBox();
      this.cbKeyboard = new CheckBox();
      this.cbGunController = new CheckBox();
      this.gbSupportedExpandedPeripherals = new GroupBox();
      this.flpSupportedExpandedPeripherals = new FlowLayoutPanel();
      this.cbMemoryCard = new CheckBox();
      this.cbVibrator = new CheckBox();
      this.cbSoundInputPeripheral = new CheckBox();
      this.cbOtherDevices = new CheckBox();
      this.gbMinimalSet = new GroupBox();
      this.flpMinimalSet = new FlowLayoutPanel();
      this.cbStartABDirButtons = new CheckBox();
      this.cbCButton = new CheckBox();
      this.cbDButton = new CheckBox();
      this.cbXButton = new CheckBox();
      this.cbYButton = new CheckBox();
      this.cbZButton = new CheckBox();
      this.cbAnalogLTrigger = new CheckBox();
      this.cbAnalogRTrigger = new CheckBox();
      this.cbStandardAnalogX = new CheckBox();
      this.cbStandardAnalogY = new CheckBox();
      this.cbExpandedDirectionButtons = new CheckBox();
      this.cbExpandedAnalogX = new CheckBox();
      this.cbExpandedAnalogY = new CheckBox();
      this.gbSupportedPeripherals.SuspendLayout();
      this.fplSupportedPeripherals.SuspendLayout();
      this.gbSupportedExpandedPeripherals.SuspendLayout();
      this.flpSupportedExpandedPeripherals.SuspendLayout();
      this.gbMinimalSet.SuspendLayout();
      this.flpMinimalSet.SuspendLayout();
      this.SuspendLayout();
      this.gbSupportedPeripherals.Controls.Add((Control) this.fplSupportedPeripherals);
      this.gbSupportedPeripherals.Location = new Point(6, 148);
      this.gbSupportedPeripherals.Name = "gbSupportedPeripherals";
      this.gbSupportedPeripherals.Size = new Size(147, 91);
      this.gbSupportedPeripherals.TabIndex = 0;
      this.gbSupportedPeripherals.TabStop = false;
      this.gbSupportedPeripherals.Text = "Supported peripherals:";
      this.fplSupportedPeripherals.AutoSize = true;
      this.fplSupportedPeripherals.Controls.Add((Control) this.cbMouse);
      this.fplSupportedPeripherals.Controls.Add((Control) this.cbKeyboard);
      this.fplSupportedPeripherals.Controls.Add((Control) this.cbGunController);
      this.fplSupportedPeripherals.Dock = DockStyle.Fill;
      this.fplSupportedPeripherals.FlowDirection = FlowDirection.TopDown;
      this.fplSupportedPeripherals.Location = new Point(3, 16);
      this.fplSupportedPeripherals.Name = "fplSupportedPeripherals";
      this.fplSupportedPeripherals.Size = new Size(141, 72);
      this.fplSupportedPeripherals.TabIndex = 0;
      this.cbMouse.AutoCheck = false;
      this.cbMouse.AutoSize = true;
      this.cbMouse.Location = new Point(3, 3);
      this.cbMouse.Name = "cbMouse";
      this.cbMouse.Size = new Size(58, 17);
      this.cbMouse.TabIndex = 14;
      this.cbMouse.Text = "Mouse";
      this.cbMouse.UseVisualStyleBackColor = true;
      this.cbKeyboard.AutoCheck = false;
      this.cbKeyboard.AutoSize = true;
      this.cbKeyboard.Location = new Point(3, 26);
      this.cbKeyboard.Name = "cbKeyboard";
      this.cbKeyboard.Size = new Size(71, 17);
      this.cbKeyboard.TabIndex = 15;
      this.cbKeyboard.Text = "Keyboard";
      this.cbKeyboard.UseVisualStyleBackColor = true;
      this.cbGunController.AutoCheck = false;
      this.cbGunController.AutoSize = true;
      this.cbGunController.Location = new Point(3, 49);
      this.cbGunController.Name = "cbGunController";
      this.cbGunController.Size = new Size(93, 17);
      this.cbGunController.TabIndex = 16;
      this.cbGunController.Text = "Gun Controller";
      this.cbGunController.UseVisualStyleBackColor = true;
      this.gbSupportedExpandedPeripherals.Controls.Add((Control) this.flpSupportedExpandedPeripherals);
      this.gbSupportedExpandedPeripherals.Location = new Point(163, 148);
      this.gbSupportedExpandedPeripherals.Name = "gbSupportedExpandedPeripherals";
      this.gbSupportedExpandedPeripherals.Size = new Size(292, 91);
      this.gbSupportedExpandedPeripherals.TabIndex = 0;
      this.gbSupportedExpandedPeripherals.TabStop = false;
      this.gbSupportedExpandedPeripherals.Text = "Supported Expanded Peripherals:";
      this.flpSupportedExpandedPeripherals.Controls.Add((Control) this.cbMemoryCard);
      this.flpSupportedExpandedPeripherals.Controls.Add((Control) this.cbVibrator);
      this.flpSupportedExpandedPeripherals.Controls.Add((Control) this.cbSoundInputPeripheral);
      this.flpSupportedExpandedPeripherals.Controls.Add((Control) this.cbOtherDevices);
      this.flpSupportedExpandedPeripherals.Dock = DockStyle.Fill;
      this.flpSupportedExpandedPeripherals.FlowDirection = FlowDirection.TopDown;
      this.flpSupportedExpandedPeripherals.Location = new Point(3, 16);
      this.flpSupportedExpandedPeripherals.Name = "flpSupportedExpandedPeripherals";
      this.flpSupportedExpandedPeripherals.Size = new Size(286, 72);
      this.flpSupportedExpandedPeripherals.TabIndex = 0;
      this.cbMemoryCard.AutoCheck = false;
      this.cbMemoryCard.AutoSize = true;
      this.cbMemoryCard.Location = new Point(3, 3);
      this.cbMemoryCard.Name = "cbMemoryCard";
      this.cbMemoryCard.Size = new Size(88, 17);
      this.cbMemoryCard.TabIndex = 17;
      this.cbMemoryCard.Text = "Memory Card";
      this.cbMemoryCard.UseVisualStyleBackColor = true;
      this.cbVibrator.AutoCheck = false;
      this.cbVibrator.AutoSize = true;
      this.cbVibrator.Location = new Point(3, 26);
      this.cbVibrator.Name = "cbVibrator";
      this.cbVibrator.Size = new Size(62, 17);
      this.cbVibrator.TabIndex = 18;
      this.cbVibrator.Text = "Vibrator";
      this.cbVibrator.UseVisualStyleBackColor = true;
      this.cbSoundInputPeripheral.AutoCheck = false;
      this.cbSoundInputPeripheral.AutoSize = true;
      this.cbSoundInputPeripheral.Location = new Point(3, 49);
      this.cbSoundInputPeripheral.Name = "cbSoundInputPeripheral";
      this.cbSoundInputPeripheral.Size = new Size(134, 17);
      this.cbSoundInputPeripheral.TabIndex = 19;
      this.cbSoundInputPeripheral.Text = "Sound Input Peripheral";
      this.cbSoundInputPeripheral.UseVisualStyleBackColor = true;
      this.cbOtherDevices.AutoCheck = false;
      this.cbOtherDevices.AutoSize = true;
      this.cbOtherDevices.Location = new Point(143, 3);
      this.cbOtherDevices.Name = "cbOtherDevices";
      this.cbOtherDevices.Size = new Size(94, 17);
      this.cbOtherDevices.TabIndex = 20;
      this.cbOtherDevices.Text = "Other Devices";
      this.cbOtherDevices.UseVisualStyleBackColor = true;
      this.gbMinimalSet.AutoSize = true;
      this.gbMinimalSet.BackColor = Color.Transparent;
      this.gbMinimalSet.Controls.Add((Control) this.flpMinimalSet);
      this.gbMinimalSet.Location = new Point(6, 3);
      this.gbMinimalSet.Name = "gbMinimalSet";
      this.gbMinimalSet.Size = new Size(449, 139);
      this.gbMinimalSet.TabIndex = 0;
      this.gbMinimalSet.TabStop = false;
      this.gbMinimalSet.Text = "Minimal set of buttons required by application:";
      this.flpMinimalSet.Controls.Add((Control) this.cbStartABDirButtons);
      this.flpMinimalSet.Controls.Add((Control) this.cbCButton);
      this.flpMinimalSet.Controls.Add((Control) this.cbDButton);
      this.flpMinimalSet.Controls.Add((Control) this.cbXButton);
      this.flpMinimalSet.Controls.Add((Control) this.cbYButton);
      this.flpMinimalSet.Controls.Add((Control) this.cbZButton);
      this.flpMinimalSet.Controls.Add((Control) this.cbAnalogLTrigger);
      this.flpMinimalSet.Controls.Add((Control) this.cbAnalogRTrigger);
      this.flpMinimalSet.Controls.Add((Control) this.cbStandardAnalogX);
      this.flpMinimalSet.Controls.Add((Control) this.cbStandardAnalogY);
      this.flpMinimalSet.Controls.Add((Control) this.cbExpandedDirectionButtons);
      this.flpMinimalSet.Controls.Add((Control) this.cbExpandedAnalogX);
      this.flpMinimalSet.Controls.Add((Control) this.cbExpandedAnalogY);
      this.flpMinimalSet.Dock = DockStyle.Fill;
      this.flpMinimalSet.FlowDirection = FlowDirection.TopDown;
      this.flpMinimalSet.Location = new Point(3, 16);
      this.flpMinimalSet.Name = "flpMinimalSet";
      this.flpMinimalSet.Size = new Size(443, 120);
      this.flpMinimalSet.TabIndex = 0;
      this.cbStartABDirButtons.AutoCheck = false;
      this.cbStartABDirButtons.AutoSize = true;
      this.cbStartABDirButtons.Location = new Point(3, 3);
      this.cbStartABDirButtons.Name = "cbStartABDirButtons";
      this.cbStartABDirButtons.Size = new Size(161, 17);
      this.cbStartABDirButtons.TabIndex = 1;
      this.cbStartABDirButtons.Text = "Start, A, B, Direction Buttons";
      this.cbStartABDirButtons.UseVisualStyleBackColor = true;
      this.cbCButton.AutoCheck = false;
      this.cbCButton.AutoSize = true;
      this.cbCButton.Location = new Point(3, 26);
      this.cbCButton.Name = "cbCButton";
      this.cbCButton.Size = new Size(67, 17);
      this.cbCButton.TabIndex = 2;
      this.cbCButton.Text = "C Button";
      this.cbCButton.UseVisualStyleBackColor = true;
      this.cbDButton.AutoCheck = false;
      this.cbDButton.AutoSize = true;
      this.cbDButton.Location = new Point(3, 49);
      this.cbDButton.Name = "cbDButton";
      this.cbDButton.Size = new Size(68, 17);
      this.cbDButton.TabIndex = 3;
      this.cbDButton.Text = "D Button";
      this.cbDButton.UseVisualStyleBackColor = true;
      this.cbXButton.AutoCheck = false;
      this.cbXButton.AutoSize = true;
      this.cbXButton.Location = new Point(3, 72);
      this.cbXButton.Name = "cbXButton";
      this.cbXButton.Size = new Size(67, 17);
      this.cbXButton.TabIndex = 4;
      this.cbXButton.Text = "X Button";
      this.cbXButton.UseVisualStyleBackColor = true;
      this.cbYButton.AutoCheck = false;
      this.cbYButton.AutoSize = true;
      this.cbYButton.Location = new Point(3, 95);
      this.cbYButton.Name = "cbYButton";
      this.cbYButton.Size = new Size(67, 17);
      this.cbYButton.TabIndex = 5;
      this.cbYButton.Text = "Y Button";
      this.cbYButton.UseVisualStyleBackColor = true;
      this.cbZButton.AutoCheck = false;
      this.cbZButton.AutoSize = true;
      this.cbZButton.Location = new Point(170, 3);
      this.cbZButton.Name = "cbZButton";
      this.cbZButton.Size = new Size(67, 17);
      this.cbZButton.TabIndex = 6;
      this.cbZButton.Text = "Z Button";
      this.cbZButton.UseVisualStyleBackColor = true;
      this.cbAnalogLTrigger.AutoCheck = false;
      this.cbAnalogLTrigger.AutoSize = true;
      this.cbAnalogLTrigger.Location = new Point(170, 26);
      this.cbAnalogLTrigger.Name = "cbAnalogLTrigger";
      this.cbAnalogLTrigger.Size = new Size(104, 17);
      this.cbAnalogLTrigger.TabIndex = 7;
      this.cbAnalogLTrigger.Text = "Analog L Trigger";
      this.cbAnalogLTrigger.UseVisualStyleBackColor = true;
      this.cbAnalogRTrigger.AutoCheck = false;
      this.cbAnalogRTrigger.AutoSize = true;
      this.cbAnalogRTrigger.Location = new Point(170, 49);
      this.cbAnalogRTrigger.Name = "cbAnalogRTrigger";
      this.cbAnalogRTrigger.Size = new Size(106, 17);
      this.cbAnalogRTrigger.TabIndex = 8;
      this.cbAnalogRTrigger.Text = "Analog R Trigger";
      this.cbAnalogRTrigger.UseVisualStyleBackColor = true;
      this.cbStandardAnalogX.AutoCheck = false;
      this.cbStandardAnalogX.AutoSize = true;
      this.cbStandardAnalogX.Location = new Point(170, 72);
      this.cbStandardAnalogX.Name = "cbStandardAnalogX";
      this.cbStandardAnalogX.Size = new Size(115, 17);
      this.cbStandardAnalogX.TabIndex = 9;
      this.cbStandardAnalogX.Text = "Standard Analog X";
      this.cbStandardAnalogX.UseVisualStyleBackColor = true;
      this.cbStandardAnalogY.AutoCheck = false;
      this.cbStandardAnalogY.AutoSize = true;
      this.cbStandardAnalogY.Location = new Point(170, 95);
      this.cbStandardAnalogY.Name = "cbStandardAnalogY";
      this.cbStandardAnalogY.Size = new Size(115, 17);
      this.cbStandardAnalogY.TabIndex = 10;
      this.cbStandardAnalogY.Text = "Standard Analog Y";
      this.cbStandardAnalogY.UseVisualStyleBackColor = true;
      this.cbExpandedDirectionButtons.AutoCheck = false;
      this.cbExpandedDirectionButtons.AutoSize = true;
      this.cbExpandedDirectionButtons.Location = new Point(291, 3);
      this.cbExpandedDirectionButtons.Name = "cbExpandedDirectionButtons";
      this.cbExpandedDirectionButtons.Size = new Size(158, 17);
      this.cbExpandedDirectionButtons.TabIndex = 11;
      this.cbExpandedDirectionButtons.Text = "Expanded Direction Buttons";
      this.cbExpandedDirectionButtons.UseVisualStyleBackColor = true;
      this.cbExpandedAnalogX.AutoCheck = false;
      this.cbExpandedAnalogX.AutoSize = true;
      this.cbExpandedAnalogX.Location = new Point(291, 26);
      this.cbExpandedAnalogX.Name = "cbExpandedAnalogX";
      this.cbExpandedAnalogX.Size = new Size(120, 17);
      this.cbExpandedAnalogX.TabIndex = 12;
      this.cbExpandedAnalogX.Text = "Expanded Analog X";
      this.cbExpandedAnalogX.UseVisualStyleBackColor = true;
      this.cbExpandedAnalogY.AutoCheck = false;
      this.cbExpandedAnalogY.AutoSize = true;
      this.cbExpandedAnalogY.Location = new Point(291, 49);
      this.cbExpandedAnalogY.Name = "cbExpandedAnalogY";
      this.cbExpandedAnalogY.Size = new Size(120, 17);
      this.cbExpandedAnalogY.TabIndex = 13;
      this.cbExpandedAnalogY.Text = "Expanded Analog Y";
      this.cbExpandedAnalogY.UseVisualStyleBackColor = true;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoSize = true;
      this.BackColor = Color.Transparent;
      this.Controls.Add((Control) this.gbSupportedPeripherals);
      this.Controls.Add((Control) this.gbSupportedExpandedPeripherals);
      this.Controls.Add((Control) this.gbMinimalSet);
      this.Name = nameof (InitialProgramPeripheralsViewer);
      this.Size = new Size(460, 246);
      this.gbSupportedPeripherals.ResumeLayout(false);
      this.gbSupportedPeripherals.PerformLayout();
      this.fplSupportedPeripherals.ResumeLayout(false);
      this.fplSupportedPeripherals.PerformLayout();
      this.gbSupportedExpandedPeripherals.ResumeLayout(false);
      this.flpSupportedExpandedPeripherals.ResumeLayout(false);
      this.flpSupportedExpandedPeripherals.PerformLayout();
      this.gbMinimalSet.ResumeLayout(false);
      this.flpMinimalSet.ResumeLayout(false);
      this.flpMinimalSet.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
