﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.InitialProgramTrackInfo
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

namespace SEGATools.Security
{
  public class InitialProgramTrackInfo
  {
    public int Index { get; private set; }

    public uint FrameAddress { get; private set; }

    public uint LastFrameAddress => (uint) ((int) this.FrameAddress + (int) this.Size - 1);

    public InitialProgramTrackInfo.ControlData Ctrl { get; private set; }

    public uint Size { get; private set; }

    internal InitialProgramTrackInfo(
      int index,
      uint StartFAD,
      uint Size,
      InitialProgramTrackInfo.ControlData Ctrl)
    {
      this.Index = index;
      this.FrameAddress = StartFAD;
      this.Size = Size;
      this.Ctrl = Ctrl;
    }

    public override string ToString() => string.Format("Track {0} [CTLR = {1}, FAD = 0x{2:X}, SIZE = 0x{3:X}]", (object) this.Index, (object) this.Ctrl, (object) this.FrameAddress, (object) this.Size);

    public enum ControlData
    {
      Audio = 0,
      Data = 4,
    }
  }
}
