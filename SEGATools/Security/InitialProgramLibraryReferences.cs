﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.InitialProgramLibraryReferences
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using SEGATools.Binary;
using SEGATools.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SEGATools.Security
{
  public class InitialProgramLibraryReferences : UserControl
  {
    private IContainer components;
    private TableLayoutPanel tableLayoutPanel;
    private Label lbLibraryHint;
    private ListView listViewLibRef;

    public InitialProgramLibraryReferences()
    {
      this.InitializeComponent();
      this.InitializeHintText(0);
      this.InitializeListViewColumns();
    }

    public void LoadInitialProgram(InitialProgramExtended ip)
    {
      if (ip == null)
        return;
      this.LoadLibraryReferences(ip);
    }

    private void InitializeHintText(int NumberOfLibraries)
    {
      string str;
      switch (NumberOfLibraries)
      {
        case 0:
          str = Resources.LibRefNoLibFound;
          break;
        case 1:
          str = Resources.LibRefNumberSingle;
          break;
        default:
          str = string.Format(Resources.LibRefNumberPlurial, (object) NumberOfLibraries);
          break;
      }
      this.lbLibraryHint.Text = string.Format(Resources.LibRefHint, (object) str);
    }

    private void InitializeListViewColumns()
    {
      this.listViewLibRef.Columns.Add(InitialProgramLibraryReferences.createColumn(Resources.LibRefColumnName, HorizontalAlignment.Left));
      this.listViewLibRef.Columns.Add(InitialProgramLibraryReferences.createColumn(Resources.LibRefColumnVersion, HorizontalAlignment.Right));
      this.listViewLibRef.Columns.Add(InitialProgramLibraryReferences.createColumn(Resources.LibRefColumnBuildDate, HorizontalAlignment.Right));
      this.ResizeListViewColumns();
    }

    private void LoadLibraryReferences(InitialProgramExtended ip)
    {
      foreach (SEGALibrary segaLibrary in (IEnumerable<SEGALibrary>) ip.Libraries.OrderBy<SEGALibrary, string>((Func<SEGALibrary, string>) (lib => lib.Name)))
        this.listViewLibRef.Items.Add(new ListViewItem(new string[3]
        {
          segaLibrary.Name,
          segaLibrary.Version.ToString(),
          segaLibrary.BuildDate.ToString()
        }));
      this.InitializeHintText(ip.Libraries.Count);
      this.ResizeListViewColumns();
    }

    private void ResizeListViewColumns() => this.listViewLibRef.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

    private static ColumnHeader createColumn(string Name, HorizontalAlignment alignment) => new ColumnHeader()
    {
      Text = Name,
      TextAlign = alignment
    };

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.tableLayoutPanel = new TableLayoutPanel();
      this.lbLibraryHint = new Label();
      this.listViewLibRef = new ListView();
      this.tableLayoutPanel.SuspendLayout();
      this.SuspendLayout();
      this.tableLayoutPanel.BackColor = Color.Transparent;
      this.tableLayoutPanel.ColumnCount = 1;
      this.tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100f));
      this.tableLayoutPanel.Controls.Add((Control) this.lbLibraryHint, 0, 0);
      this.tableLayoutPanel.Controls.Add((Control) this.listViewLibRef, 0, 1);
      this.tableLayoutPanel.Dock = DockStyle.Fill;
      this.tableLayoutPanel.Location = new Point(0, 0);
      this.tableLayoutPanel.Name = "tableLayoutPanel";
      this.tableLayoutPanel.RowCount = 2;
      this.tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 24f));
      this.tableLayoutPanel.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel.Size = new Size(428, 150);
      this.tableLayoutPanel.TabIndex = 0;
      this.lbLibraryHint.BackColor = Color.Transparent;
      this.lbLibraryHint.Dock = DockStyle.Fill;
      this.lbLibraryHint.Location = new Point(3, 0);
      this.lbLibraryHint.Name = "lbLibraryHint";
      this.lbLibraryHint.Size = new Size(422, 24);
      this.lbLibraryHint.TabIndex = 0;
      this.lbLibraryHint.Text = "...";
      this.lbLibraryHint.TextAlign = ContentAlignment.MiddleLeft;
      this.listViewLibRef.BackColor = SystemColors.Window;
      this.listViewLibRef.Dock = DockStyle.Fill;
      this.listViewLibRef.FullRowSelect = true;
      this.listViewLibRef.HeaderStyle = ColumnHeaderStyle.Nonclickable;
      this.listViewLibRef.Location = new Point(0, 24);
      this.listViewLibRef.Margin = new Padding(0);
      this.listViewLibRef.MultiSelect = false;
      this.listViewLibRef.Name = "listViewLibRef";
      this.listViewLibRef.ShowGroups = false;
      this.listViewLibRef.Size = new Size(428, 126);
      this.listViewLibRef.TabIndex = 1;
      this.listViewLibRef.UseCompatibleStateImageBehavior = false;
      this.listViewLibRef.View = View.Details;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = SystemColors.Control;
      this.Controls.Add((Control) this.tableLayoutPanel);
      this.Name = nameof (InitialProgramLibraryReferences);
      this.Size = new Size(428, 150);
      this.tableLayoutPanel.ResumeLayout(false);
      this.ResumeLayout(false);
    }
  }
}
