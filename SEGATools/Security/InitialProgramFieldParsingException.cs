﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.InitialProgramFieldParsingException
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.Security
{
  public class InitialProgramFieldParsingException : InitialProgramException
  {
    public InitialProgramFieldParsingException(
      string fieldName,
      int offset,
      Exception innerException)
      : base(string.Format("the field {0} at offset 0x{1:X8} is not valid", (object) fieldName, (object) offset), innerException)
    {
    }
  }
}
