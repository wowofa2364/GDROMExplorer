﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.SupportedAreas
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.Security
{
  [Flags]
  public enum SupportedAreas
  {
    None = 0,
    Japan = 1,
    Usa = 2,
    Europe = 4,
  }
}
