﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.SupportedButtons
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.Security
{
  [Flags]
  public enum SupportedButtons
  {
    None = 0,
    StartABDirections = 1,
    CButton = 2,
    DButton = 4,
    XButton = 8,
    Unused1 = 16, // 0x00000010
    Unused2 = 32, // 0x00000020
    Unused3 = 64, // 0x00000040
    Unused4 = 128, // 0x00000080
    YButton = 256, // 0x00000100
    ZButton = 512, // 0x00000200
    ExpandedDirectionButtons = 1024, // 0x00000400
    AnalogRTrigger = 2048, // 0x00000800
    Unused5 = 4096, // 0x00001000
    Unused6 = 8192, // 0x00002000
    Unused7 = 16384, // 0x00004000
    Unused8 = 32768, // 0x00008000
    AnalogLTrigger = 65536, // 0x00010000
    AnalogHorizontalController = 131072, // 0x00020000
    AnalogVerticalController = 262144, // 0x00040000
    ExpandedAnalogHorizontal = 524288, // 0x00080000
    Unused9 = 1048576, // 0x00100000
    Unused10 = 2097152, // 0x00200000
    Unused11 = 4194304, // 0x00400000
    Unused12 = 8388608, // 0x00800000
    ExpandedAnalogVertical = 16777216, // 0x01000000
    Unused13 = 33554432, // 0x02000000
    Unused14 = 67108864, // 0x04000000
    Unused15 = 134217728, // 0x08000000
    Unused16 = 268435456, // 0x10000000
    Unused17 = 536870912, // 0x20000000
    Unused18 = 1073741824, // 0x40000000
    Unused19 = -2147483648, // 0x80000000
  }
}
