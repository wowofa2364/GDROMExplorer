﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.SupportedExpandedPeripherals
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.Security
{
  [Flags]
  public enum SupportedExpandedPeripherals
  {
    None = 0,
    OtherDevices = 1,
    Vibrator = 2,
    SoundInputPeripheral = 4,
    MemoryCard = 8,
    Unused1 = 16, // 0x00000010
    Unused2 = 32, // 0x00000020
    Unused3 = 64, // 0x00000040
    Unused4 = 128, // 0x00000080
  }
}
