﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.InitialProgramGeneralSettingsViewer
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using SEGATools.Binary;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SEGATools.Security
{
  public class InitialProgramGeneralSettingsViewer : UserControl
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private IContainer components;
    private ToolTip toolTip;
    private Label lbHardwareId;
    private Label lbMakerId;
    private Label lbCrc;
    private Label lbReleaseDate;
    private Label lbMedia;
    private Label lbProductId;
    private FlowLayoutPanel fplArea;
    private CheckBox cbAreaJapan;
    private CheckBox cbAreaUs;
    private CheckBox cbAreaEurope;
    private GroupBox gbSupportedAreas;
    private Label lbFirstReadFilename;
    private Label lbCompagnyName;
    private Label lbApplicationTitle;
    private TextBox tbHardwareId;
    private TextBox tbMakerId;
    private TextBox tbProductId;
    private TextBox tbProductVersion;
    private TextBox tbMedia;
    private TextBox tbCrc;
    private TextBox tbReleaseDate;
    private TextBox tbFirstFileName;
    private FlowLayoutPanel flpSegaLibrary;
    private GroupBox gbSegaAIPLibrary;
    private TextBox tbCompagnyName;
    private TextBox tbApplicationName;
    private FlowLayoutPanel flpMediaOptions;
    private CheckBox cbMediaOptionsVgaSupport;
    private CheckBox cbMediaOptionsUseWindowsCe;
    private GroupBox gbMediaOptions;
    private RadioButton rbSegaLibraryRoundZero;
    private RadioButton rbSegaLibraryRoundNearest;

    public InitialProgramGeneralSettingsViewer() => this.InitializeComponent();

    public void LoadInitialProgram(InitialProgramExtended ip)
    {
      if (ip == null)
        return;
      this.tbApplicationName.Text = ip.SoftwareName.Trim();
      this.tbHardwareId.Text = ip.HardwareID;
      this.tbMakerId.Text = ip.MakerID;
      this.tbFirstFileName.Text = ip.MainBinary.Trim();
      this.tbCompagnyName.Text = ip.CompanyName.Trim();
      this.tbProductId.Text = ip.ProductID.Trim();
      this.tbProductVersion.Text = ip.ProductVersion.Trim();
      this.tbReleaseDate.Text = ip.ReleaseDate.Date.ToString("yyyy/MM/dd");
      this.tbMedia.Text = ip.DeviceInformation.Trim();
      this.SetCRC((InitialProgram) ip);
      this.SetAreaSymbols((InitialProgram) ip);
      this.SetOther((InitialProgram) ip);
      if (ip.Libraries == null)
        return;
      this.SetAIPLibrary(ip);
    }

    private void SetCRC(InitialProgram ip)
    {
      this.tbCrc.Text = ip.CRC;
      this.tbCrc.ForeColor = Color.White;
      if (!ip.IsCRCValid)
      {
        this.tbCrc.BackColor = Color.Red;
        this.toolTip.SetToolTip((Control) this.tbCrc, string.Format("Incorrect CRC! Should be {0}", (object) ip.ComputedCRC));
      }
      else
        this.tbCrc.BackColor = Color.Green;
    }

    private void SetAreaSymbols(InitialProgram ip)
    {
      this.cbAreaJapan.Checked = ip.SupportJapanArea;
      this.cbAreaUs.Checked = ip.SupportUsaArea;
      this.cbAreaEurope.Checked = ip.SupportEuropeArea;
    }

    private void SetOther(InitialProgram ip)
    {
      this.cbMediaOptionsVgaSupport.Checked = ip.SupportVga;
      this.cbMediaOptionsUseWindowsCe.Checked = ip.UseWindowsCE;
    }

    private void SetAIPLibrary(InitialProgramExtended ip)
    {
      this.rbSegaLibraryRoundZero.Enabled = this.rbSegaLibraryRoundNearest.Enabled = false;
      this.rbSegaLibraryRoundZero.Checked = this.rbSegaLibraryRoundNearest.Checked = false;
      SEGALibrary segaLibrary1 = ip.Libraries.Find((Predicate<SEGALibrary>) (library => library.GetLibraryType() == SEGALibraryType.AIP));
      if (segaLibrary1 == null)
        return;
      SEGALibrary segaLibrary2 = ip.Libraries.Find((Predicate<SEGALibrary>) (library => library.GetLibraryType() == SEGALibraryType.FLASH_MEMORY_MANAGER));
      SEGALibraryVersion version = SEGALibrary.AIP_103.Version;
      if (segaLibrary2 != null)
        version = SEGALibrary.AIPf_105.Version;
      this.rbSegaLibraryRoundZero.Enabled = this.rbSegaLibraryRoundNearest.Enabled = false;
      this.rbSegaLibraryRoundZero.Checked = this.rbSegaLibraryRoundNearest.Checked = false;
      if (segaLibrary1.Version.CompareTo(version) <= 0)
        this.rbSegaLibraryRoundZero.Checked = true;
      else
        this.rbSegaLibraryRoundNearest.Checked = true;
      if (!ip.UseWindowsCE)
        this.rbSegaLibraryRoundZero.Enabled = this.rbSegaLibraryRoundNearest.Enabled = true;
      this.gbSegaAIPLibrary.Text = "SEGA AIP Library: v" + (object) segaLibrary1.Version;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      this.toolTip = new ToolTip(this.components);
      this.lbHardwareId = new Label();
      this.lbMakerId = new Label();
      this.lbCrc = new Label();
      this.lbReleaseDate = new Label();
      this.lbMedia = new Label();
      this.lbProductId = new Label();
      this.fplArea = new FlowLayoutPanel();
      this.cbAreaJapan = new CheckBox();
      this.cbAreaUs = new CheckBox();
      this.cbAreaEurope = new CheckBox();
      this.gbSupportedAreas = new GroupBox();
      this.lbFirstReadFilename = new Label();
      this.lbCompagnyName = new Label();
      this.lbApplicationTitle = new Label();
      this.tbHardwareId = new TextBox();
      this.tbMakerId = new TextBox();
      this.tbProductId = new TextBox();
      this.tbProductVersion = new TextBox();
      this.tbMedia = new TextBox();
      this.tbCrc = new TextBox();
      this.tbReleaseDate = new TextBox();
      this.tbFirstFileName = new TextBox();
      this.flpSegaLibrary = new FlowLayoutPanel();
      this.rbSegaLibraryRoundZero = new RadioButton();
      this.rbSegaLibraryRoundNearest = new RadioButton();
      this.gbSegaAIPLibrary = new GroupBox();
      this.tbCompagnyName = new TextBox();
      this.tbApplicationName = new TextBox();
      this.flpMediaOptions = new FlowLayoutPanel();
      this.cbMediaOptionsVgaSupport = new CheckBox();
      this.cbMediaOptionsUseWindowsCe = new CheckBox();
      this.gbMediaOptions = new GroupBox();
      this.fplArea.SuspendLayout();
      this.gbSupportedAreas.SuspendLayout();
      this.flpSegaLibrary.SuspendLayout();
      this.gbSegaAIPLibrary.SuspendLayout();
      this.flpMediaOptions.SuspendLayout();
      this.gbMediaOptions.SuspendLayout();
      this.SuspendLayout();
      this.lbHardwareId.AutoSize = true;
      this.lbHardwareId.Location = new Point(3, 55);
      this.lbHardwareId.Name = "lbHardwareId";
      this.lbHardwareId.Size = new Size(70, 13);
      this.lbHardwareId.TabIndex = 0;
      this.lbHardwareId.Text = "Hardware ID:";
      this.lbMakerId.AutoSize = true;
      this.lbMakerId.Location = new Point(3, 94);
      this.lbMakerId.Name = "lbMakerId";
      this.lbMakerId.Size = new Size(54, 13);
      this.lbMakerId.TabIndex = 0;
      this.lbMakerId.Text = "Maker ID:";
      this.lbCrc.AutoSize = true;
      this.lbCrc.Location = new Point(340, 152);
      this.lbCrc.Name = "lbCrc";
      this.lbCrc.Size = new Size(32, 13);
      this.lbCrc.TabIndex = 0;
      this.lbCrc.Text = "CRC:";
      this.lbReleaseDate.AutoSize = true;
      this.lbReleaseDate.Location = new Point(155, 133);
      this.lbReleaseDate.Name = "lbReleaseDate";
      this.lbReleaseDate.Size = new Size(73, 13);
      this.lbReleaseDate.TabIndex = 0;
      this.lbReleaseDate.Text = "Release date:";
      this.lbMedia.AutoSize = true;
      this.lbMedia.Location = new Point(245, 133);
      this.lbMedia.Name = "lbMedia";
      this.lbMedia.Size = new Size(39, 13);
      this.lbMedia.TabIndex = 0;
      this.lbMedia.Text = "Media:";
      this.lbProductId.AutoSize = true;
      this.lbProductId.Location = new Point(155, 94);
      this.lbProductId.Name = "lbProductId";
      this.lbProductId.Size = new Size(61, 13);
      this.lbProductId.TabIndex = 0;
      this.lbProductId.Text = "Product ID:";
      this.fplArea.AutoSize = true;
      this.fplArea.Controls.Add((Control) this.cbAreaJapan);
      this.fplArea.Controls.Add((Control) this.cbAreaUs);
      this.fplArea.Controls.Add((Control) this.cbAreaEurope);
      this.fplArea.Dock = DockStyle.Fill;
      this.fplArea.FlowDirection = FlowDirection.TopDown;
      this.fplArea.Location = new Point(3, 16);
      this.fplArea.Name = "fplArea";
      this.fplArea.Size = new Size(112, 69);
      this.fplArea.TabIndex = 0;
      this.cbAreaJapan.AutoCheck = false;
      this.cbAreaJapan.AutoSize = true;
      this.cbAreaJapan.Location = new Point(3, 3);
      this.cbAreaJapan.Name = "cbAreaJapan";
      this.cbAreaJapan.Size = new Size(55, 17);
      this.cbAreaJapan.TabIndex = 11;
      this.cbAreaJapan.Text = "Japan";
      this.cbAreaJapan.UseVisualStyleBackColor = true;
      this.cbAreaUs.AutoCheck = false;
      this.cbAreaUs.AutoSize = true;
      this.cbAreaUs.Location = new Point(3, 26);
      this.cbAreaUs.Name = "cbAreaUs";
      this.cbAreaUs.Size = new Size(48, 17);
      this.cbAreaUs.TabIndex = 12;
      this.cbAreaUs.Text = "USA";
      this.cbAreaUs.UseVisualStyleBackColor = true;
      this.cbAreaEurope.AutoCheck = false;
      this.cbAreaEurope.AutoSize = true;
      this.cbAreaEurope.Location = new Point(3, 49);
      this.cbAreaEurope.Name = "cbAreaEurope";
      this.cbAreaEurope.Size = new Size(60, 17);
      this.cbAreaEurope.TabIndex = 13;
      this.cbAreaEurope.Text = "Europe";
      this.cbAreaEurope.UseVisualStyleBackColor = true;
      this.gbSupportedAreas.AutoSize = true;
      this.gbSupportedAreas.Controls.Add((Control) this.fplArea);
      this.gbSupportedAreas.Location = new Point(336, 58);
      this.gbSupportedAreas.Name = "gbSupportedAreas";
      this.gbSupportedAreas.Size = new Size(118, 88);
      this.gbSupportedAreas.TabIndex = 0;
      this.gbSupportedAreas.TabStop = false;
      this.gbSupportedAreas.Text = "Supported area(s):";
      this.lbFirstReadFilename.AutoSize = true;
      this.lbFirstReadFilename.Location = new Point(3, 133);
      this.lbFirstReadFilename.Name = "lbFirstReadFilename";
      this.lbFirstReadFilename.Size = new Size(95, 13);
      this.lbFirstReadFilename.TabIndex = 0;
      this.lbFirstReadFilename.Text = "First read filename:";
      this.lbCompagnyName.AutoSize = true;
      this.lbCompagnyName.Location = new Point(155, 55);
      this.lbCompagnyName.Name = "lbCompagnyName";
      this.lbCompagnyName.Size = new Size(89, 13);
      this.lbCompagnyName.TabIndex = 0;
      this.lbCompagnyName.Text = "Compagny name:";
      this.lbApplicationTitle.AutoSize = true;
      this.lbApplicationTitle.Location = new Point(3, 0);
      this.lbApplicationTitle.Name = "lbApplicationTitle";
      this.lbApplicationTitle.Size = new Size(81, 13);
      this.lbApplicationTitle.TabIndex = 0;
      this.lbApplicationTitle.Text = "Application title:";
      this.tbHardwareId.BackColor = Color.White;
      this.tbHardwareId.Location = new Point(6, 71);
      this.tbHardwareId.MaxLength = 15;
      this.tbHardwareId.Name = "tbHardwareId";
      this.tbHardwareId.ReadOnly = true;
      this.tbHardwareId.Size = new Size(143, 20);
      this.tbHardwareId.TabIndex = 2;
      this.tbHardwareId.Text = "SEGA SEGAKATANA";
      this.tbHardwareId.TextAlign = HorizontalAlignment.Center;
      this.tbMakerId.BackColor = Color.White;
      this.tbMakerId.Location = new Point(6, 110);
      this.tbMakerId.Name = "tbMakerId";
      this.tbMakerId.ReadOnly = true;
      this.tbMakerId.Size = new Size(143, 20);
      this.tbMakerId.TabIndex = 4;
      this.tbMakerId.Text = "SEGA ENTERPRISES";
      this.tbMakerId.TextAlign = HorizontalAlignment.Center;
      this.tbProductId.BackColor = Color.White;
      this.tbProductId.Location = new Point(158, 110);
      this.tbProductId.Name = "tbProductId";
      this.tbProductId.ReadOnly = true;
      this.tbProductId.Size = new Size(81, 20);
      this.tbProductId.TabIndex = 5;
      this.tbProductId.TextAlign = HorizontalAlignment.Center;
      this.tbProductVersion.BackColor = Color.White;
      this.tbProductVersion.Location = new Point(248, 110);
      this.tbProductVersion.Name = "tbProductVersion";
      this.tbProductVersion.ReadOnly = true;
      this.tbProductVersion.Size = new Size(81, 20);
      this.tbProductVersion.TabIndex = 6;
      this.tbProductVersion.TextAlign = HorizontalAlignment.Center;
      this.tbMedia.BackColor = Color.White;
      this.tbMedia.Location = new Point(248, 149);
      this.tbMedia.Name = "tbMedia";
      this.tbMedia.ReadOnly = true;
      this.tbMedia.Size = new Size(81, 20);
      this.tbMedia.TabIndex = 9;
      this.tbMedia.TextAlign = HorizontalAlignment.Center;
      this.tbCrc.BackColor = Color.White;
      this.tbCrc.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.tbCrc.ForeColor = Color.White;
      this.tbCrc.Location = new Point(378, 149);
      this.tbCrc.Name = "tbCrc";
      this.tbCrc.ReadOnly = true;
      this.tbCrc.Size = new Size(76, 20);
      this.tbCrc.TabIndex = 10;
      this.tbCrc.Text = "FFFF";
      this.tbCrc.TextAlign = HorizontalAlignment.Center;
      this.tbReleaseDate.BackColor = Color.White;
      this.tbReleaseDate.Location = new Point(158, 149);
      this.tbReleaseDate.Name = "tbReleaseDate";
      this.tbReleaseDate.ReadOnly = true;
      this.tbReleaseDate.Size = new Size(81, 20);
      this.tbReleaseDate.TabIndex = 8;
      this.tbReleaseDate.TextAlign = HorizontalAlignment.Center;
      this.tbFirstFileName.BackColor = Color.White;
      this.tbFirstFileName.Location = new Point(6, 149);
      this.tbFirstFileName.Name = "tbFirstFileName";
      this.tbFirstFileName.ReadOnly = true;
      this.tbFirstFileName.Size = new Size(143, 20);
      this.tbFirstFileName.TabIndex = 7;
      this.tbFirstFileName.TextAlign = HorizontalAlignment.Center;
      this.flpSegaLibrary.AutoSize = true;
      this.flpSegaLibrary.Controls.Add((Control) this.rbSegaLibraryRoundZero);
      this.flpSegaLibrary.Controls.Add((Control) this.rbSegaLibraryRoundNearest);
      this.flpSegaLibrary.Dock = DockStyle.Fill;
      this.flpSegaLibrary.FlowDirection = FlowDirection.TopDown;
      this.flpSegaLibrary.Location = new Point(3, 16);
      this.flpSegaLibrary.Name = "flpSegaLibrary";
      this.flpSegaLibrary.Size = new Size(227, 46);
      this.flpSegaLibrary.TabIndex = 0;
      this.rbSegaLibraryRoundZero.AutoCheck = false;
      this.rbSegaLibraryRoundZero.AutoSize = true;
      this.rbSegaLibraryRoundZero.Location = new Point(3, 3);
      this.rbSegaLibraryRoundZero.Name = "rbSegaLibraryRoundZero";
      this.rbSegaLibraryRoundZero.Size = new Size(152, 17);
      this.rbSegaLibraryRoundZero.TabIndex = 23;
      this.rbSegaLibraryRoundZero.TabStop = true;
      this.rbSegaLibraryRoundZero.Text = "Floating Point: Round Zero";
      this.rbSegaLibraryRoundZero.UseVisualStyleBackColor = true;
      this.rbSegaLibraryRoundNearest.AutoCheck = false;
      this.rbSegaLibraryRoundNearest.AutoSize = true;
      this.rbSegaLibraryRoundNearest.Location = new Point(3, 26);
      this.rbSegaLibraryRoundNearest.Name = "rbSegaLibraryRoundNearest";
      this.rbSegaLibraryRoundNearest.Size = new Size(167, 17);
      this.rbSegaLibraryRoundNearest.TabIndex = 24;
      this.rbSegaLibraryRoundNearest.TabStop = true;
      this.rbSegaLibraryRoundNearest.Text = "Floating Point: Round Nearest";
      this.rbSegaLibraryRoundNearest.UseVisualStyleBackColor = true;
      this.gbSegaAIPLibrary.AutoSize = true;
      this.gbSegaAIPLibrary.Controls.Add((Control) this.flpSegaLibrary);
      this.gbSegaAIPLibrary.Location = new Point(6, 175);
      this.gbSegaAIPLibrary.Name = "gbSegaAIPLibrary";
      this.gbSegaAIPLibrary.Size = new Size(233, 65);
      this.gbSegaAIPLibrary.TabIndex = 0;
      this.gbSegaAIPLibrary.TabStop = false;
      this.gbSegaAIPLibrary.Text = "SEGA AIP Library:";
      this.tbCompagnyName.BackColor = Color.White;
      this.tbCompagnyName.Location = new Point(158, 71);
      this.tbCompagnyName.Name = "tbCompagnyName";
      this.tbCompagnyName.ReadOnly = true;
      this.tbCompagnyName.Size = new Size(171, 20);
      this.tbCompagnyName.TabIndex = 3;
      this.tbCompagnyName.TextAlign = HorizontalAlignment.Center;
      this.tbApplicationName.BackColor = Color.White;
      this.tbApplicationName.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.tbApplicationName.Location = new Point(6, 16);
      this.tbApplicationName.Multiline = true;
      this.tbApplicationName.Name = "tbApplicationName";
      this.tbApplicationName.ReadOnly = true;
      this.tbApplicationName.Size = new Size(448, 36);
      this.tbApplicationName.TabIndex = 1;
      this.tbApplicationName.Text = "THIS IS A SAMPLE DISK FOR USE INTHE OFFICE ONLY / DO NOT SELL / DO NOT CARRY OUT FROM THE OFFICE";
      this.tbApplicationName.TextAlign = HorizontalAlignment.Center;
      this.flpMediaOptions.AutoSize = true;
      this.flpMediaOptions.Controls.Add((Control) this.cbMediaOptionsVgaSupport);
      this.flpMediaOptions.Controls.Add((Control) this.cbMediaOptionsUseWindowsCe);
      this.flpMediaOptions.Dock = DockStyle.Fill;
      this.flpMediaOptions.FlowDirection = FlowDirection.TopDown;
      this.flpMediaOptions.Location = new Point(3, 16);
      this.flpMediaOptions.Name = "flpMediaOptions";
      this.flpMediaOptions.Size = new Size(200, 46);
      this.flpMediaOptions.TabIndex = 22;
      this.cbMediaOptionsVgaSupport.AutoCheck = false;
      this.cbMediaOptionsVgaSupport.AutoSize = true;
      this.cbMediaOptionsVgaSupport.Location = new Point(3, 3);
      this.cbMediaOptionsVgaSupport.Name = "cbMediaOptionsVgaSupport";
      this.cbMediaOptionsVgaSupport.Size = new Size(118, 17);
      this.cbMediaOptionsVgaSupport.TabIndex = 16;
      this.cbMediaOptionsVgaSupport.Text = "VGA box supported";
      this.cbMediaOptionsVgaSupport.UseVisualStyleBackColor = true;
      this.cbMediaOptionsUseWindowsCe.AutoCheck = false;
      this.cbMediaOptionsUseWindowsCe.AutoSize = true;
      this.cbMediaOptionsUseWindowsCe.Location = new Point(3, 26);
      this.cbMediaOptionsUseWindowsCe.Name = "cbMediaOptionsUseWindowsCe";
      this.cbMediaOptionsUseWindowsCe.Size = new Size(106, 17);
      this.cbMediaOptionsUseWindowsCe.TabIndex = 17;
      this.cbMediaOptionsUseWindowsCe.Text = "Use WindowsCE";
      this.cbMediaOptionsUseWindowsCe.UseVisualStyleBackColor = true;
      this.gbMediaOptions.AutoSize = true;
      this.gbMediaOptions.Controls.Add((Control) this.flpMediaOptions);
      this.gbMediaOptions.Location = new Point(248, 175);
      this.gbMediaOptions.Name = "gbMediaOptions";
      this.gbMediaOptions.Size = new Size(206, 65);
      this.gbMediaOptions.TabIndex = 0;
      this.gbMediaOptions.TabStop = false;
      this.gbMediaOptions.Text = "Other:";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.gbMediaOptions);
      this.Controls.Add((Control) this.gbSegaAIPLibrary);
      this.Controls.Add((Control) this.tbApplicationName);
      this.Controls.Add((Control) this.tbCompagnyName);
      this.Controls.Add((Control) this.tbFirstFileName);
      this.Controls.Add((Control) this.tbReleaseDate);
      this.Controls.Add((Control) this.tbCrc);
      this.Controls.Add((Control) this.tbMedia);
      this.Controls.Add((Control) this.tbProductVersion);
      this.Controls.Add((Control) this.tbProductId);
      this.Controls.Add((Control) this.tbMakerId);
      this.Controls.Add((Control) this.tbHardwareId);
      this.Controls.Add((Control) this.lbApplicationTitle);
      this.Controls.Add((Control) this.lbCompagnyName);
      this.Controls.Add((Control) this.lbFirstReadFilename);
      this.Controls.Add((Control) this.gbSupportedAreas);
      this.Controls.Add((Control) this.lbProductId);
      this.Controls.Add((Control) this.lbMedia);
      this.Controls.Add((Control) this.lbReleaseDate);
      this.Controls.Add((Control) this.lbCrc);
      this.Controls.Add((Control) this.lbMakerId);
      this.Controls.Add((Control) this.lbHardwareId);
      this.Name = nameof (InitialProgramGeneralSettingsViewer);
      this.Size = new Size(460, 246);
      this.fplArea.ResumeLayout(false);
      this.fplArea.PerformLayout();
      this.gbSupportedAreas.ResumeLayout(false);
      this.gbSupportedAreas.PerformLayout();
      this.flpSegaLibrary.ResumeLayout(false);
      this.flpSegaLibrary.PerformLayout();
      this.gbSegaAIPLibrary.ResumeLayout(false);
      this.gbSegaAIPLibrary.PerformLayout();
      this.flpMediaOptions.ResumeLayout(false);
      this.flpMediaOptions.PerformLayout();
      this.gbMediaOptions.ResumeLayout(false);
      this.gbMediaOptions.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
