﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Security.InitialProgramExtended
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using SEGATools.Binary;
using System.Collections.Generic;

namespace SEGATools.Security
{
  public class InitialProgramExtended : InitialProgram
  {
    public List<SEGALibrary> Libraries;

    private InitialProgramExtended(InitialProgram ip, List<SEGALibrary> libraries)
    {
      this.AreaSymbols = ip.AreaSymbols;
      this.CompanyName = ip.CompanyName;
      this.ComputedCRC = ip.ComputedCRC;
      this.CRC = ip.CRC;
      this.CustomBrandingImage = ip.CustomBrandingImage;
      this.DeviceInformation = ip.DeviceInformation;
      this.HardwareID = ip.HardwareID;
      this.MainBinary = ip.MainBinary;
      this.MakerID = ip.MakerID;
      this.ProductID = ip.ProductID;
      this.ProductVersion = ip.ProductVersion;
      this.RawData = ip.RawData;
      this.RawPeripherals = ip.RawPeripherals;
      this.ReleaseDate = ip.ReleaseDate;
      this.SegaLicenseImage = ip.SegaLicenseImage;
      this.SegaTradeMarkImage = ip.SegaTradeMarkImage;
      this.SoftwareName = ip.SoftwareName;
      this.TableOfContent = ip.TableOfContent;
      this.Libraries = libraries;
    }

    public static InitialProgramExtended create(
      InitialProgram ip,
      List<SEGALibrary> libraries)
    {
      return new InitialProgramExtended(ip, libraries);
    }
  }
}
