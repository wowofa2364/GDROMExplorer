﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Scanner.FileScannerWrongArgumentsException
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.Scanner
{
  public class FileScannerWrongArgumentsException : FileScannerException
  {
    private FileScannerWrongArgumentsException(string message)
      : base(string.Format("String Scanner Wrong Argument Error: {0}", (object) message))
    {
    }

    private FileScannerWrongArgumentsException(string message, Exception innerException)
      : base(string.Format("String Scanner Wrong Argument Error: {0}", (object) message), innerException)
    {
    }

    public static FileScannerWrongArgumentsException aNotValidArgument(
      string argumentName)
    {
      return new FileScannerWrongArgumentsException(string.Format("the argument \"{0}\" is not valid", (object) argumentName));
    }
  }
}
