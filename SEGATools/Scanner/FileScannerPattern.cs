﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Scanner.FileScannerPattern
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using SEGATools.Binary;
using System;
using System.Text.RegularExpressions;

namespace SEGATools.Scanner
{
  public sealed class FileScannerPattern
  {
    private static readonly string SEGA_IP_LIBRARIES = "^(?<library>\\w+)\\s(ver|version:)\\s(?<version_major>\\d+)(\\.)(?<version_minor>\\d+)(\\s|\\x00\\k<library>\\s)(build:)(?<date>\\w{3}\\s\\d{2}\\s\\d{4})\\s?(?<time>\\d{2}:\\d{2}:\\d{2})";

    public string CapturedString { get; private set; }

    public string Pattern { get; private set; }

    public Regex RegularExpression { get; private set; }

    public System.Text.RegularExpressions.Match MatchResult { get; private set; }

    public Type ResultType { get; private set; }

    public static FileScannerPattern aPatternForSEGALibraries() => new FileScannerPattern(FileScannerPattern.SEGA_IP_LIBRARIES, true, typeof (SEGALibrary));

    private FileScannerPattern(string Pattern, bool IgnoreCase, Type ResultType)
    {
      RegexOptions options = RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.CultureInvariant;
      if (IgnoreCase)
        options |= RegexOptions.IgnoreCase;
      this.Pattern = Pattern;
      this.RegularExpression = new Regex(Pattern, options);
      this.ResultType = ResultType;
    }

    public bool Match(string input)
    {
      this.MatchResult = this.RegularExpression.Match(input);
      this.CapturedString = this.MatchResult.Value.Replace(char.MinValue, ' ').Trim();
      return this.MatchResult.Success;
    }
  }
}
