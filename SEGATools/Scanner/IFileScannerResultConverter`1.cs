﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Scanner.IFileScannerResultConverter`1
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System.Text.RegularExpressions;

namespace SEGATools.Scanner
{
  public interface IFileScannerResultConverter<T>
  {
    T ToResult(Match MatchResults);
  }
}
