﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Scanner.FileScannerResultConverterForSEGALibrary
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using SEGATools.Binary;
using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace SEGATools.Scanner
{
  public class FileScannerResultConverterForSEGALibrary : IFileScannerResultConverter<SEGALibrary>
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();

    public SEGALibrary ToResult(Match match)
    {
      try
      {
        DateTime exact = DateTime.ParseExact(match.Groups["date"].Value + " " + match.Groups["time"].Value, "MMM dd yyyy HH:mm:ss", (IFormatProvider) CultureInfo.CreateSpecificCulture("en-US").DateTimeFormat);
        SEGALibraryVersion Version = new SEGALibraryVersion(match.Groups["version_major"].Value, match.Groups["version_minor"].Value);
        return new SEGALibrary(match.Groups["library"].Value, Version, exact);
      }
      catch (Exception ex)
      {
        throw FileScannerResultConverterException.aParsingException(ex);
      }
    }
  }
}
