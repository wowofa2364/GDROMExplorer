﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Audio.CanonicalWaveHeader
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.Audio
{
  internal class CanonicalWaveHeader
  {
    private const short HEADERSIZE = 44;
    private const int _chunkID = 1179011410;
    private const int _remainingSize = 20;
    private const int _format = 1163280727;
    private const int _subChunk1ID = 544501094;
    private const int _subChunk1Size = 16;
    private const short _audioFormat = 1;
    private const int _subChunk2ID = 1635017060;
    private int _subChunk2Size;
    private bool sampleDataInitialized;

    public short Size => 44;

    public int ChunkID => 1179011410;

    public int ChunkSize => 36 + this.SubChunk2Size;

    public int Format => 1163280727;

    public int SubChunk1ID => 544501094;

    public int SubChunk1Size => 16;

    public short AudioFormat => 1;

    public short NumberOfChannels { get; private set; }

    public int SampleRate { get; private set; }

    public int ByteRate => this.SampleRate * (int) this.NumberOfChannels * ((int) this.BitsPerSample / 8);

    public short BlockAlign => (short) ((int) this.NumberOfChannels * ((int) this.BitsPerSample / 8));

    public short BitsPerSample { get; private set; }

    public int SubChunk2ID => 1635017060;

    public int SubChunk2Size
    {
      get => this._subChunk2Size;
      set
      {
        this._subChunk2Size = value;
        this.sampleDataInitialized = true;
      }
    }

    public CanonicalWaveHeader(
      short numberOfChannels,
      int sampleRate,
      short bitsPerSample,
      int? sampleDataSize)
    {
      this.NumberOfChannels = numberOfChannels;
      this.SampleRate = sampleRate;
      this.BitsPerSample = bitsPerSample;
      if (!sampleDataSize.HasValue)
        return;
      this.sampleDataInitialized = true;
      this.SubChunk2Size = sampleDataSize.Value;
    }

    public byte[] ToByteArray()
    {
      if (!this.sampleDataInitialized)
        throw new UninitialiedSampleDataSizeException("Sample data size (SubChunk2Size property) has not been initialied.");
      byte[] numArray = new byte[44];
      BitConverter.GetBytes(this.ChunkID).CopyTo((Array) numArray, 0);
      BitConverter.GetBytes(this.ChunkSize).CopyTo((Array) numArray, 4);
      BitConverter.GetBytes(this.Format).CopyTo((Array) numArray, 8);
      BitConverter.GetBytes(this.SubChunk1ID).CopyTo((Array) numArray, 12);
      BitConverter.GetBytes(this.SubChunk1Size).CopyTo((Array) numArray, 16);
      BitConverter.GetBytes(this.AudioFormat).CopyTo((Array) numArray, 20);
      BitConverter.GetBytes(this.NumberOfChannels).CopyTo((Array) numArray, 22);
      BitConverter.GetBytes(this.SampleRate).CopyTo((Array) numArray, 24);
      BitConverter.GetBytes(this.ByteRate).CopyTo((Array) numArray, 28);
      BitConverter.GetBytes(this.BlockAlign).CopyTo((Array) numArray, 32);
      BitConverter.GetBytes(this.BitsPerSample).CopyTo((Array) numArray, 34);
      BitConverter.GetBytes(this.SubChunk2ID).CopyTo((Array) numArray, 36);
      BitConverter.GetBytes(this.SubChunk2Size).CopyTo((Array) numArray, 40);
      return numArray;
    }

    public static CanonicalWaveHeader FromByteArray(byte[] headerArray) => new CanonicalWaveHeader(BitConverter.ToInt16(headerArray, 22), BitConverter.ToInt32(headerArray, 24), BitConverter.ToInt16(headerArray, 34), new int?(BitConverter.ToInt32(headerArray, 40)));
  }
}
