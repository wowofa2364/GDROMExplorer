﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Audio.AudioConversionSettings
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

namespace SEGATools.Audio
{
  public class AudioConversionSettings
  {
    public bool RemovePause { get; private set; }

    public short PauseDurationInSeconds => this.RemovePause ? (short) 2 : (short) 0;

    public int SamplingRate { get; private set; }

    public short BitsPerSample => 16;

    public short NumberOfChannels { get; private set; }

    private AudioConversionSettings(int SamplingRate, short NumberOfChannels, bool RemovePause)
    {
      this.SamplingRate = SamplingRate;
      this.NumberOfChannels = NumberOfChannels;
      this.RemovePause = RemovePause;
    }

    public static AudioConversionSettings defaultAudioConvOptions() => new AudioConversionSettings(44100, (short) 2, false);

    public static AudioConversionSettings newStereoPCMSettings(
      int SamplingRate,
      bool RemovePause)
    {
      return new AudioConversionSettings(SamplingRate, (short) 2, RemovePause);
    }

    public static AudioConversionSettings newMonoPCMSettings(
      int SamplingRate,
      bool RemovePause)
    {
      return new AudioConversionSettings(SamplingRate, (short) 1, RemovePause);
    }
  }
}
