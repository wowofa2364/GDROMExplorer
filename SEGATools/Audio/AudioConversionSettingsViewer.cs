﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Audio.AudioConversionSettingsViewer
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SEGATools.Audio
{
  public class AudioConversionSettingsViewer : UserControl
  {
    private List<AudioConversionSettingsViewer.SamplingRateItem> samplingRates = new List<AudioConversionSettingsViewer.SamplingRateItem>();
    private IContainer components;
    private FlowLayoutPanel flowLayoutPanel;
    private Label lbSamplingRate;
    private ComboBox cbSamplingRate;
    private CheckBox cbRemovePause;

    [RefreshProperties(RefreshProperties.Repaint)]
    [NotifyParentProperty(true)]
    [Category("Appearance")]
    [Description("Specifies whether the remove pause checkbox is visible.")]
    [DefaultValue(typeof (bool), "true")]
    public bool ShowRemovePause
    {
      get => this.cbRemovePause.Visible;
      set => this.cbRemovePause.Visible = value;
    }

    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public bool RemovePause => this.cbRemovePause.Checked;

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    [Browsable(false)]
    public int SamplingRate => int.Parse(this.cbSamplingRate.SelectedItem.ToString());

    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public AudioConversionSettings AudioConversionSettings
    {
      get => AudioConversionSettings.newStereoPCMSettings((int) this.cbSamplingRate.SelectedValue, this.cbRemovePause.Checked);
      set
      {
        this.cbSamplingRate.SelectedItem = (object) this.samplingRates.Find((Predicate<AudioConversionSettingsViewer.SamplingRateItem>) (samplingRate => samplingRate.Value == value.SamplingRate));
        this.cbRemovePause.Checked = value.RemovePause;
      }
    }

    public AudioConversionSettingsViewer()
    {
      this.InitializeComponent();
      this.samplingRates.Add(new AudioConversionSettingsViewer.SamplingRateItem(16000));
      this.samplingRates.Add(new AudioConversionSettingsViewer.SamplingRateItem(44100));
      this.samplingRates.Add(new AudioConversionSettingsViewer.SamplingRateItem(48000));
      this.cbSamplingRate.DataSource = (object) this.samplingRates;
      this.cbSamplingRate.DisplayMember = AudioConversionSettingsViewer.SamplingRateItem.DisplayMember;
      this.cbSamplingRate.ValueMember = AudioConversionSettingsViewer.SamplingRateItem.ValueMember;
      this.cbSamplingRate.SelectedItem = (object) this.samplingRates.Find((Predicate<AudioConversionSettingsViewer.SamplingRateItem>) (samplingRate => samplingRate.Value == 44100));
      this.cbSamplingRate.Enabled = false;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.flowLayoutPanel = new FlowLayoutPanel();
      this.lbSamplingRate = new Label();
      this.cbSamplingRate = new ComboBox();
      this.cbRemovePause = new CheckBox();
      this.flowLayoutPanel.SuspendLayout();
      this.SuspendLayout();
      this.flowLayoutPanel.AutoScroll = true;
      this.flowLayoutPanel.AutoSize = true;
      this.flowLayoutPanel.BackColor = Color.Transparent;
      this.flowLayoutPanel.Controls.Add((Control) this.lbSamplingRate);
      this.flowLayoutPanel.Controls.Add((Control) this.cbSamplingRate);
      this.flowLayoutPanel.Controls.Add((Control) this.cbRemovePause);
      this.flowLayoutPanel.Dock = DockStyle.Fill;
      this.flowLayoutPanel.Location = new Point(0, 0);
      this.flowLayoutPanel.Name = "flowLayoutPanel";
      this.flowLayoutPanel.Size = new Size(325, 28);
      this.flowLayoutPanel.TabIndex = 0;
      this.lbSamplingRate.AutoSize = true;
      this.lbSamplingRate.Location = new Point(3, 3);
      this.lbSamplingRate.Margin = new Padding(3);
      this.lbSamplingRate.Name = "lbSamplingRate";
      this.lbSamplingRate.Size = new Size(79, 13);
      this.lbSamplingRate.TabIndex = 9;
      this.lbSamplingRate.Text = "Sampling Rate:";
      this.lbSamplingRate.TextAlign = ContentAlignment.BottomLeft;
      this.cbSamplingRate.FormattingEnabled = true;
      this.cbSamplingRate.Items.AddRange(new object[2]
      {
        (object) "44100",
        (object) "48000"
      });
      this.cbSamplingRate.Location = new Point(85, 0);
      this.cbSamplingRate.Margin = new Padding(0);
      this.cbSamplingRate.Name = "cbSamplingRate";
      this.cbSamplingRate.Size = new Size(75, 21);
      this.cbSamplingRate.TabIndex = 10;
      this.cbRemovePause.AutoSize = true;
      this.cbRemovePause.CheckAlign = ContentAlignment.MiddleRight;
      this.cbRemovePause.Enabled = false;
      this.cbRemovePause.Location = new Point(163, 3);
      this.cbRemovePause.Name = "cbRemovePause";
      this.cbRemovePause.Size = new Size(150, 17);
      this.cbRemovePause.TabIndex = 11;
      this.cbRemovePause.Text = "Remove 2-seconds pause";
      this.cbRemovePause.UseVisualStyleBackColor = true;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = Color.Transparent;
      this.Controls.Add((Control) this.flowLayoutPanel);
      this.Name = nameof (AudioConversionSettingsViewer);
      this.Size = new Size(325, 28);
      this.flowLayoutPanel.ResumeLayout(false);
      this.flowLayoutPanel.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    internal sealed class SamplingRateItem
    {
      public static string DisplayMember => "Name";

      public static string ValueMember => "Value";

      public int Value { get; set; }

      public string Name { get; set; }

      public SamplingRateItem(int SamplingRate)
      {
        this.Name = SamplingRate.ToString() + "Hz";
        this.Value = SamplingRate;
      }
    }
  }
}
