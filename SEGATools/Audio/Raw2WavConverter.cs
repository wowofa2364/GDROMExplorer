﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Audio.Raw2WavConverter
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using SEGATools.UserProcess;
using SEGATools.VirtualFile;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;

namespace SEGATools.Audio
{
  public class Raw2WavConverter : UserProcessBase
  {
    private static readonly int BUFFER_SIZE = 524288;

    public AudioConversionSettings AudioConversionSettings { get; set; }

    public event AsyncOperationProgressChangedEventHandler ConversionProgressChanged
    {
      add => this.AsyncOperationProgressChanged += value;
      remove => this.AsyncOperationProgressChanged -= value;
    }

    public event AsyncOperationCompletedEventHandler ConversionCompleted
    {
      add => this.AsyncOperationCompleted += value;
      remove => this.AsyncOperationCompleted -= value;
    }

    public Raw2WavConverter() => this.AudioConversionSettings = AudioConversionSettings.defaultAudioConvOptions();

    public Raw2WavConverter(IContainer container)
      : base(container)
      => this.AudioConversionSettings = AudioConversionSettings.defaultAudioConvOptions();

    public void ConvertAsync(IList<IVirtualFile> InputFiles, string outputPath, object taskId)
    {
      AsyncOperation asyncOperation = this.CreateAsyncOperation(taskId);
      new Raw2WavConverter.WorkerEventHandler(this.ConvertRaw2WavWorker).BeginInvoke(InputFiles, outputPath, asyncOperation, (AsyncCallback) null, (object) null);
    }

    private void ConvertRaw2WavWorker(
      IList<IVirtualFile> inputFiles,
      string outputPath,
      AsyncOperation asyncOp)
    {
      long TotalNumberOfBytesToWrite = 0;
      Exception exception = (Exception) null;
      foreach (IVirtualFile inputFile in (IEnumerable<IVirtualFile>) inputFiles)
        TotalNumberOfBytesToWrite += this.ComputeAudioStreamLength(inputFile.FileInputStream, this.AudioConversionSettings.PauseDurationInSeconds);
      long TotalNumberOfBytesRemainingToWrite = TotalNumberOfBytesToWrite;
      try
      {
        foreach (IVirtualFile inputFile in (IEnumerable<IVirtualFile>) inputFiles)
        {
          if (!this.TaskCanceled(asyncOp))
          {
            IVirtualFile OutputFile = inputFiles.Count != 1 ? (IVirtualFile) VirtualFileFactory.createVirtualFile(Path.Combine(outputPath, Path.GetFileName(Path.ChangeExtension(inputFile.VirtualName, ".wav")))) : (IVirtualFile) VirtualFileFactory.createVirtualFile(outputPath);
            TotalNumberOfBytesRemainingToWrite = this.ConvertStream(inputFile, OutputFile, TotalNumberOfBytesRemainingToWrite, TotalNumberOfBytesToWrite, asyncOp);
          }
          else
            break;
        }
      }
      catch (Exception ex)
      {
        exception = ex;
      }
      this.ReportCompletion(outputPath, exception, asyncOp);
    }

    private long ComputeAudioStreamLength(System.IO.Stream AudioStream, short NumberOfSecondToSubtrack) => AudioStream.Length - (long) ((int) this.AudioConversionSettings.NumberOfChannels * this.AudioConversionSettings.SamplingRate * ((int) this.AudioConversionSettings.BitsPerSample / 8) * (int) NumberOfSecondToSubtrack);

    private void GenerateWaveHeader(
      BinaryReader InputFileReader,
      BinaryWriter OutputFileWriter,
      out long BytesToWrite)
    {
      BytesToWrite = this.ComputeAudioStreamLength(InputFileReader.BaseStream, this.AudioConversionSettings.PauseDurationInSeconds);
      CanonicalWaveHeader canonicalWaveHeader = new CanonicalWaveHeader(this.AudioConversionSettings.NumberOfChannels, this.AudioConversionSettings.SamplingRate, this.AudioConversionSettings.BitsPerSample, new int?((int) BytesToWrite));
      OutputFileWriter.Write(canonicalWaveHeader.ToByteArray());
    }

    private long ConvertStream(
      IVirtualFile InputFile,
      IVirtualFile OutputFile,
      long TotalNumberOfBytesRemainingToWrite,
      long TotalNumberOfBytesToWrite,
      AsyncOperation asyncOp)
    {
      long num = 0;
      long BytesToWrite = 0;
      using (BinaryReader InputFileReader = new BinaryReader(InputFile.FileInputStream))
      {
        using (BinaryWriter OutputFileWriter = new BinaryWriter(OutputFile.FileOutputStream))
        {
          this.GenerateWaveHeader(InputFileReader, OutputFileWriter, out BytesToWrite);
          num = BytesToWrite;
          while (num > 0L)
          {
            if (!this.TaskCanceled(asyncOp))
            {
              byte[] buffer = num <= (long) Raw2WavConverter.BUFFER_SIZE ? InputFileReader.ReadBytes((int) num) : InputFileReader.ReadBytes(Raw2WavConverter.BUFFER_SIZE);
              OutputFileWriter.Write(buffer);
              num -= (long) buffer.Length;
              TotalNumberOfBytesRemainingToWrite -= (long) buffer.Length;
              int progressPercentage = (int) ((double) (BytesToWrite - num) / (double) BytesToWrite * 100.0);
              int totalProgressPercentage = (int) ((double) (TotalNumberOfBytesToWrite - TotalNumberOfBytesRemainingToWrite) / (double) TotalNumberOfBytesToWrite * 100.0);
              this.ReportProgress(new UserProcessProgressChangedEventArgs(InputFile.VirtualName, OutputFile.OriginalFileName, progressPercentage, totalProgressPercentage, asyncOp.UserSuppliedState), asyncOp);
            }
            else
              break;
          }
        }
      }
      if (num == 0L)
      {
        if (!this.TaskCanceled(asyncOp))
          goto label_16;
      }
      try
      {
        File.Delete(OutputFile.OriginalFileName);
      }
      catch (Exception ex)
      {
        UserProcessBase.logger.ErrorFormat("Unable to delete the file {0}: {1}", (object) OutputFile, (object) ex.Message);
      }
label_16:
      return TotalNumberOfBytesRemainingToWrite;
    }

    private delegate void WorkerEventHandler(
      IList<IVirtualFile> InputFiles,
      string OutputPath,
      AsyncOperation asyncOp);
  }
}
