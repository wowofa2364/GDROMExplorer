﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.UserProcess.UserProcessUpdateUIViewEventArgs
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.UserProcess
{
  public class UserProcessUpdateUIViewEventArgs : EventArgs
  {
    public UserProcessUpdateUIViewEventArgs.UserProgressUIStyle UIStyle { get; private set; }

    public string InputTitleResourceName { get; private set; }

    public bool UpdateInputTitle { get; private set; }

    public bool ShowInputText { get; private set; }

    public UserProcessUpdateUIViewEventArgs(
      UserProcessUpdateUIViewEventArgs.UserProgressUIStyle userProgressUIStyle,
      bool showInputText)
    {
      this.UIStyle = userProgressUIStyle;
      this.UpdateInputTitle = false;
      this.ShowInputText = showInputText;
    }

    public UserProcessUpdateUIViewEventArgs(
      UserProcessUpdateUIViewEventArgs.UserProgressUIStyle userProgressUIStyle,
      string newInputTitleResourceName,
      bool showInputText)
    {
      this.UIStyle = userProgressUIStyle;
      this.InputTitleResourceName = newInputTitleResourceName;
      this.UpdateInputTitle = true;
      this.ShowInputText = showInputText;
    }

    public static UserProcessUpdateUIViewEventArgs OneProgressBarWithoutPercentage(
      string newInputTitleResourceName,
      bool showInputText)
    {
      return new UserProcessUpdateUIViewEventArgs(UserProcessUpdateUIViewEventArgs.UserProgressUIStyle.OneProgressBarWithoutPercentage, newInputTitleResourceName, showInputText);
    }

    public enum UserProgressUIStyle
    {
      OneProgressBarWithPercentage,
      OneProgressBarWithoutPercentage,
      TwoProgressBarsWithPercentage,
    }
  }
}
