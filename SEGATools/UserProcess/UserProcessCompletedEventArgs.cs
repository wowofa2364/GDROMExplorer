﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.UserProcess.UserProcessCompletedEventArgs
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;
using System.ComponentModel;

namespace SEGATools.UserProcess
{
  public class UserProcessCompletedEventArgs : AsyncCompletedEventArgs
  {
    public string ResourceName { get; private set; }

    public UserProcessCompletedEventArgs(
      string resourceName,
      Exception e,
      bool canceled,
      object state)
      : base(e, canceled, state)
    {
      this.ResourceName = resourceName;
    }
  }
}
