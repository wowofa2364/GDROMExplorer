﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.UserProcess.UserProcessWaitingForUserConsentFileConflictEventArgs
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

namespace SEGATools.UserProcess
{
  public class UserProcessWaitingForUserConsentFileConflictEventArgs : 
    UserProcessWaitingForUserConsentEventArgs
  {
    public UserProcessWaitingForUserConsentFileConflictEventArgs(
      string questionTitleResourceName,
      string questionContentResourceName,
      string[] questionContentArgs,
      object state)
      : base(questionTitleResourceName, questionContentResourceName, questionContentArgs, state)
    {
    }
  }
}
