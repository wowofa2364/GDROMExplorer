﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.UserProcess.UserProcessBase
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Threading;

namespace SEGATools.UserProcess
{
  public class UserProcessBase : Component
  {
    protected static readonly Logger.ILog logger = Logger.CreateLog();
    private HybridDictionary userStateToLifetime = new HybridDictionary();
    private Container components;
    private SendOrPostCallback onProgressReportDelegate;
    private SendOrPostCallback onCompletedDelegate;
    private SendOrPostCallback onProgressWaitingForUserConsentDelegate;
    private SendOrPostCallback onUpdateUIViewDelegate;

    public event AsyncOperationProgressChangedEventHandler AsyncOperationProgressChanged;

    public event AsyncOperationCompletedEventHandler AsyncOperationCompleted;

    public event AsyncOperationProgressWaitingForUserConsentEventHandler AsyncOperationWaitForUserConsent;

    public event AsyncOperationProgressUpdateUIEventHandler AsyncOperationUpdateUIView;

    public UserProcessBase(IContainer container)
      : this()
      => container.Add((IComponent) this);

    public UserProcessBase()
    {
      this.InitializeComponent();
      this.InitializeDelegates();
    }

    private void InitializeComponent() => this.components = new Container();

    protected void InitializeDelegates()
    {
      this.onProgressReportDelegate = new SendOrPostCallback(this.ProgressReport);
      this.onCompletedDelegate = new SendOrPostCallback(this.ProcessCompleted);
      this.onProgressWaitingForUserConsentDelegate = new SendOrPostCallback(this.ProcessWaitingForUserConsent);
      this.onUpdateUIViewDelegate = new SendOrPostCallback(this.ProcessUpdateUIView);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    public void CancelAsync(object taskId)
    {
      if (!(this.userStateToLifetime[taskId] is AsyncOperation))
        return;
      lock (this.userStateToLifetime.SyncRoot)
        this.userStateToLifetime.Remove(taskId);
    }

    protected AsyncOperation CreateAsyncOperation(object taskId)
    {
      AsyncOperation operation = AsyncOperationManager.CreateOperation(taskId);
      lock (this.userStateToLifetime.SyncRoot)
      {
        if (this.userStateToLifetime.Contains(taskId))
          throw new ArgumentException("Task ID parameter must be unique", nameof (taskId));
        this.userStateToLifetime[taskId] = (object) operation;
      }
      return operation;
    }

    protected bool TaskCanceled(AsyncOperation asyncOp) => asyncOp != null && this.userStateToLifetime[asyncOp.UserSuppliedState] == null;

    protected void ReportProgress(UserProcessProgressChangedEventArgs e, AsyncOperation asyncOp)
    {
      asyncOp.Post(this.onProgressReportDelegate, (object) e);
      Thread.Sleep(0);
    }

    protected void ReportCompletion(string Filename, Exception exception, AsyncOperation asyncOp)
    {
      if (asyncOp == null)
        return;
      bool canceled = this.RemoveTask(asyncOp);
      UserProcessCompletedEventArgs completedEventArgs = new UserProcessCompletedEventArgs(Filename, exception, canceled, asyncOp.UserSuppliedState);
      this.EndTask(asyncOp, (object) completedEventArgs);
    }

    protected bool AskForUserConsent(
      UserProcessWaitingForUserConsentEventArgs e,
      AsyncOperation asyncOp)
    {
      asyncOp.Post(this.onProgressWaitingForUserConsentDelegate, (object) e);
      e.ResetEvent.WaitOne(-1, true);
      return this.TaskCanceled(asyncOp);
    }

    protected void UpdateUIView(UserProcessUpdateUIViewEventArgs e, AsyncOperation asyncOp) => asyncOp?.Post(this.onUpdateUIViewDelegate, (object) e);

    private bool RemoveTask(AsyncOperation asyncOp)
    {
      bool flag = this.TaskCanceled(asyncOp);
      if (!flag)
      {
        lock (this.userStateToLifetime.SyncRoot)
          this.userStateToLifetime.Remove(asyncOp.UserSuppliedState);
      }
      return flag;
    }

    private void EndTask(AsyncOperation asyncOp, object SendOrPostCallbackArg) => asyncOp.PostOperationCompleted(this.onCompletedDelegate, SendOrPostCallbackArg);

    private void ProgressReport(object operationState) => this.OnProgressChanged(operationState as UserProcessProgressChangedEventArgs);

    private void ProcessCompleted(object operationState) => this.OnProgressCompleted(operationState as UserProcessCompletedEventArgs);

    private void ProcessWaitingForUserConsent(object operationState) => this.OnProgressWaitingForUserConsent(operationState as UserProcessWaitingForUserConsentEventArgs);

    private void ProcessUpdateUIView(object operationState) => this.OnProgressUpdateUIView(operationState as UserProcessUpdateUIViewEventArgs);

    private void OnProgressChanged(UserProcessProgressChangedEventArgs e)
    {
      if (this.AsyncOperationProgressChanged == null)
        return;
      this.AsyncOperationProgressChanged(e);
    }

    private void OnProgressCompleted(UserProcessCompletedEventArgs e)
    {
      if (this.AsyncOperationCompleted == null)
        return;
      this.AsyncOperationCompleted((object) this, e);
    }

    private void OnProgressWaitingForUserConsent(UserProcessWaitingForUserConsentEventArgs e)
    {
      if (this.AsyncOperationWaitForUserConsent == null)
        return;
      this.AsyncOperationWaitForUserConsent(e);
    }

    private void OnProgressUpdateUIView(UserProcessUpdateUIViewEventArgs e)
    {
      if (this.AsyncOperationUpdateUIView == null)
        return;
      this.AsyncOperationUpdateUIView(e);
    }
  }
}
