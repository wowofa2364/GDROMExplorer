﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.UserProcess.UserProcessWaitingForUserConsentEventArgs
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;
using System.Threading;

namespace SEGATools.UserProcess
{
  public abstract class UserProcessWaitingForUserConsentEventArgs : EventArgs
  {
    public string QuestionTitleResourceName { get; private set; }

    public string QuestionContentResourceName { get; private set; }

    public string[] QuestionContentArgs { get; private set; }

    public bool HasStringArguments => this.QuestionContentArgs != null && this.QuestionContentArgs.Length > 0;

    public object UserState { get; private set; }

    public ManualResetEvent ResetEvent { get; private set; }

    public UserProcessWaitingForUserConsentEventArgs(
      string questionTitleResourceName,
      string questionContentResourceName,
      string[] questionContentArgs,
      object state)
    {
      this.QuestionTitleResourceName = questionTitleResourceName;
      this.QuestionContentResourceName = questionContentResourceName;
      this.QuestionContentArgs = questionContentArgs;
      this.UserState = state;
      this.ResetEvent = new ManualResetEvent(false);
    }
  }
}
