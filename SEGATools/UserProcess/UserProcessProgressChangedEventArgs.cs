﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.UserProcess.UserProcessProgressChangedEventArgs
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System.ComponentModel;

namespace SEGATools.UserProcess
{
  public class UserProcessProgressChangedEventArgs : ProgressChangedEventArgs
  {
    public int TotalProgressPercentage { get; private set; }

    public string Input { get; private set; }

    public string Output { get; private set; }

    public UserProcessProgressChangedEventArgs(
      string inputOrOutput,
      int progressPercentage,
      object userToken)
      : this(inputOrOutput, inputOrOutput, progressPercentage, progressPercentage, userToken)
    {
    }

    public UserProcessProgressChangedEventArgs(
      string input,
      string output,
      int progressPercentage,
      object userToken)
      : this(input, output, progressPercentage, progressPercentage, userToken)
    {
    }

    public UserProcessProgressChangedEventArgs(
      string input,
      string output,
      int progressPercentage,
      int totalProgressPercentage,
      object userToken)
      : base(progressPercentage, userToken)
    {
      this.Input = input;
      this.Output = output;
      this.TotalProgressPercentage = totalProgressPercentage;
    }
  }
}
