﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.HashAlgorithm.EDC
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.HashAlgorithm
{
  public class EDC : System.Security.Cryptography.HashAlgorithm
  {
    public static readonly int SECTOR_DATA_LENGTH_FOR_EDC_COMPUTATION = 2064;
    public static readonly int EDC_DATA_OFFSET = EDC.SECTOR_DATA_LENGTH_FOR_EDC_COMPUTATION;
    public static readonly int EDC_DATA_LENGTH = 4;
    private static readonly uint EDC_POLYNOMIAL = 3623976961;
    private static readonly uint[] EDC_CRC_LOOKUP_TABLE = new uint[256]
    {
      0U,
      2425422081U,
      2434859521U,
      28312320U,
      2453734401U,
      47187200U,
      56624640U,
      2482046721U,
      2491484161U,
      68159744U,
      94374400U,
      2503019265U,
      113249280U,
      2521894145U,
      2548108801U,
      124784384U,
      2566983681U,
      160436480U,
      136319488U,
      2561741569U,
      188748800U,
      2614170881U,
      2590053889U,
      183506688U,
      226498560U,
      2635143425U,
      2627803649U,
      204479232U,
      2680232961U,
      256908544U,
      249568768U,
      2658213633U,
      2181111809U,
      311435520U,
      320872960U,
      2209424129U,
      272638976U,
      2161190145U,
      2170627585U,
      300951296U,
      377497600U,
      2249271553U,
      2275486209U,
      389032704U,
      2227252225U,
      340798720U,
      367013376U,
      2238787329U,
      452997120U,
      2341548289U,
      2317431297U,
      447755008U,
      2302751745U,
      433075456U,
      408958464U,
      2297509633U,
      2407610369U,
      521156864U,
      513817088U,
      2385591041U,
      499137536U,
      2370911489U,
      2363571713U,
      477118208U,
      3019980801U,
      613433600U,
      622871040U,
      3048293121U,
      641745920U,
      3067168001U,
      3076605441U,
      670058240U,
      545277952U,
      2953922817U,
      2980137473U,
      556813056U,
      2999012353U,
      575687936U,
      601902592U,
      3010547457U,
      754995200U,
      3180417281U,
      3156300289U,
      749753088U,
      3208729601U,
      802182400U,
      778065408U,
      3203487489U,
      3112261633U,
      688937216U,
      681597440U,
      3090242305U,
      734026752U,
      3142671617U,
      3135331841U,
      712007424U,
      905994240U,
      2794545409U,
      2803982849U,
      934306560U,
      2755748865U,
      886072576U,
      895510016U,
      2784061185U,
      2726389761U,
      839936256U,
      866150912U,
      2737924865U,
      817916928U,
      2689690881U,
      2715905537U,
      829452032U,
      2936107009U,
      1066430720U,
      1042313728U,
      2930864897U,
      1027634176U,
      2916185345U,
      2892068353U,
      1022392064U,
      998275072U,
      2870049025U,
      2862709249U,
      976255744U,
      2848029697U,
      961576192U,
      954236416U,
      2826010369U,
      3623976961U,
      1217429760U,
      1226867200U,
      3652289281U,
      1245742080U,
      3671164161U,
      3680601601U,
      1274054400U,
      1283491840U,
      3692136705U,
      3718351361U,
      1295026944U,
      3737226241U,
      1313901824U,
      1340116480U,
      3748761345U,
      1090555904U,
      3515977985U,
      3491860993U,
      1085313792U,
      3544290305U,
      1137743104U,
      1113626112U,
      3539048193U,
      3582040065U,
      1158715648U,
      1151375872U,
      3560020737U,
      1203805184U,
      3612450049U,
      3605110273U,
      1181785856U,
      1509990400U,
      3398541569U,
      3407979009U,
      1538302720U,
      3359745025U,
      1490068736U,
      1499506176U,
      3388057345U,
      3464603649U,
      1578150144U,
      1604364800U,
      3476138753U,
      1556130816U,
      3427904769U,
      3454119425U,
      1567665920U,
      3271667713U,
      1401991424U,
      1377874432U,
      3266425601U,
      1363194880U,
      3251746049U,
      3227629057U,
      1357952768U,
      1468053504U,
      3339827457U,
      3332487681U,
      1446034176U,
      3317808129U,
      1431354624U,
      1424014848U,
      3295788801U,
      1811988480U,
      4237410561U,
      4246848001U,
      1840300800U,
      4265722881U,
      1859175680U,
      1868613120U,
      4294035201U,
      4169254913U,
      1745930496U,
      1772145152U,
      4180790017U,
      1791020032U,
      4199664897U,
      4225879553U,
      1802555136U,
      4110536705U,
      1703989504U,
      1679872512U,
      4105294593U,
      1732301824U,
      4157723905U,
      4133606913U,
      1727059712U,
      1635833856U,
      4044478721U,
      4037138945U,
      1613814528U,
      4089568257U,
      1666243840U,
      1658904064U,
      4067548929U,
      3993100289U,
      2123424000U,
      2132861440U,
      4021412609U,
      2084627456U,
      3973178625U,
      3982616065U,
      2112939776U,
      2055268352U,
      3927042305U,
      3953256961U,
      2066803456U,
      3905022977U,
      2018569472U,
      2044784128U,
      3916558081U,
      1996550144U,
      3885101313U,
      3860984321U,
      1991308032U,
      3846304769U,
      1976628480U,
      1952511488U,
      3841062657U,
      3816945665U,
      1930492160U,
      1923152384U,
      3794926337U,
      1908472832U,
      3780246785U,
      3772907009U,
      1886453504U
    };
    private uint hash;

    protected override void HashCore(byte[] array, int ibStart, int cbSize)
    {
      EDC.CheckBufferSize(array, ibStart, cbSize);
      uint num1 = 0;
      for (int index = ibStart; index < ibStart + cbSize; ++index)
      {
        uint num2 = (uint) (((int) num1 ^ (int) array[ibStart + index]) & (int) byte.MaxValue);
        num1 = EDC.EDC_CRC_LOOKUP_TABLE[(int) num2] ^ num1 >> 8;
      }
      this.hash = num1;
    }

    protected override byte[] HashFinal() => BitConverter.GetBytes(this.hash);

    public override void Initialize()
    {
    }

    private static uint[] GenerateCRCLookUpTable()
    {
      uint[] numArray = new uint[256];
      for (uint index1 = 0; (long) index1 < (long) numArray.Length; ++index1)
      {
        uint num = index1;
        for (int index2 = 8; index2 > 0; --index2)
        {
          if (((int) num & 1) != 0)
            num = num >> 1 ^ EDC.EDC_POLYNOMIAL;
          else
            num >>= 1;
        }
        numArray[(int) index1] = num;
      }
      return numArray;
    }

    private static void CheckBufferSize(byte[] buffer, int ibStart, int cbSize)
    {
      if (buffer == null)
        throw new ArgumentNullException(nameof (buffer));
      if (buffer.Length - ibStart < cbSize)
        throw new ArgumentException("buffer too small");
    }
  }
}
