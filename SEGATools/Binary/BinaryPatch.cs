﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Binary.BinaryPatch
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;
using System.Security.Cryptography;

namespace SEGATools.Binary
{
  public class BinaryPatch
  {
    private static readonly System.Security.Cryptography.HashAlgorithm HashAlgorithm = (System.Security.Cryptography.HashAlgorithm) SHA1.Create();

    public long Offset { get; private set; }

    public byte[] Data { get; private set; }

    public string Hash { get; private set; }

    internal BinaryPatch(long offset, byte[] data)
    {
      this.Offset = offset;
      this.Data = data;
      this.Hash = BitConverter.ToString(BinaryPatch.HashAlgorithm.ComputeHash(data));
    }

    public BinaryPatch Translate(long OffsetDelta) => new BinaryPatch(this.Offset + OffsetDelta, this.Data);

    public override string ToString() => string.Format("BinaryPatch [Offset=0x{0:X}, Hash=0x{1}]", (object) this.Offset, (object) this.Hash);

    public override int GetHashCode() => 1 + 17 * this.Offset.GetHashCode() + 31 * this.Hash.GetHashCode();

    public override bool Equals(object obj)
    {
      if (obj == null || this.GetType() != obj.GetType())
        return false;
      BinaryPatch binaryPatch = (BinaryPatch) obj;
      return this.Offset == binaryPatch.Offset && this.Hash.Equals(binaryPatch.Hash);
    }
  }
}
