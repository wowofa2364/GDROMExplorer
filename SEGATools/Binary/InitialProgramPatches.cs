﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Binary.InitialProgramPatches
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using ImageReader.DiscSectors;

namespace SEGATools.Binary
{
  public class InitialProgramPatches
  {
    private static int IP0000_REPEATED_TIME = 16;

    public static BinaryPatch[] RegionFreePatchesForTrack1
    {
      get
      {
        BinaryPatch[] binaryPatchArray = new BinaryPatch[InitialProgramPatches.IP0000_REPEATED_TIME];
        for (int index = 0; index < binaryPatchArray.Length; ++index)
          binaryPatchArray[index] = InitialProgramPatches.RegionFreeFlagsPatch.Translate((long) (index * DiscSectorCommon.LogicalSectorSize));
        return binaryPatchArray;
      }
    }

    public static BinaryPatch[] RegionFreePatchesForTrack3 => new BinaryPatch[4]
    {
      InitialProgramPatches.RegionFreeFlagsPatch,
      InitialProgramPatches.RegionJapanAreaProtectionSymbolsPatch,
      InitialProgramPatches.RegionUsaAreaProtectionSymbolsPatch,
      InitialProgramPatches.RegionEuropeAreaProtectionSymbolsPatch
    };

    public static BinaryPatch[] VGAFlagPatchesForTrack1
    {
      get
      {
        BinaryPatch[] binaryPatchArray = new BinaryPatch[InitialProgramPatches.IP0000_REPEATED_TIME];
        for (int index = 0; index < binaryPatchArray.Length; ++index)
          binaryPatchArray[index] = InitialProgramPatches.VGAFlagPatch.Translate((long) (index * DiscSectorCommon.LogicalSectorSize));
        return binaryPatchArray;
      }
    }

    public static BinaryPatch[] VGAFlagPatchesForTrack3 => new BinaryPatch[1]
    {
      InitialProgramPatches.VGAFlagPatch
    };

    public static BinaryPatch VGAFlagPatch => new BinaryPatch(61L, new byte[1]
    {
      (byte) 49
    });

    public static BinaryPatch RegionFreeFlagsPatch => new BinaryPatch(48L, new byte[3]
    {
      (byte) 74,
      (byte) 85,
      (byte) 69
    });

    public static BinaryPatch RegionJapanAreaProtectionSymbolsPatch => new BinaryPatch(14080L, new byte[32]
    {
      (byte) 14,
      (byte) 160,
      (byte) 9,
      (byte) 0,
      (byte) 70,
      (byte) 111,
      (byte) 114,
      (byte) 32,
      (byte) 74,
      (byte) 65,
      (byte) 80,
      (byte) 65,
      (byte) 78,
      (byte) 44,
      (byte) 84,
      (byte) 65,
      (byte) 73,
      (byte) 87,
      (byte) 65,
      (byte) 78,
      (byte) 44,
      (byte) 80,
      (byte) 72,
      (byte) 73,
      (byte) 76,
      (byte) 73,
      (byte) 80,
      (byte) 73,
      (byte) 78,
      (byte) 69,
      (byte) 83,
      (byte) 46
    });

    public static BinaryPatch RegionUsaAreaProtectionSymbolsPatch => new BinaryPatch(14112L, new byte[32]
    {
      (byte) 14,
      (byte) 160,
      (byte) 9,
      (byte) 0,
      (byte) 70,
      (byte) 111,
      (byte) 114,
      (byte) 32,
      (byte) 85,
      (byte) 83,
      (byte) 65,
      (byte) 32,
      (byte) 97,
      (byte) 110,
      (byte) 100,
      (byte) 32,
      (byte) 67,
      (byte) 65,
      (byte) 78,
      (byte) 65,
      (byte) 68,
      (byte) 65,
      (byte) 46,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32
    });

    public static BinaryPatch RegionEuropeAreaProtectionSymbolsPatch => new BinaryPatch(14144L, new byte[32]
    {
      (byte) 14,
      (byte) 160,
      (byte) 9,
      (byte) 0,
      (byte) 70,
      (byte) 111,
      (byte) 114,
      (byte) 32,
      (byte) 69,
      (byte) 85,
      (byte) 82,
      (byte) 79,
      (byte) 80,
      (byte) 69,
      (byte) 46,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32,
      (byte) 32
    });
  }
}
