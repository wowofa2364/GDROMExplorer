﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Binary.BinaryPatcher
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using SEGATools.UserProcess;
using System.ComponentModel;
using System.IO;

namespace SEGATools.Binary
{
  public class BinaryPatcher : UserProcessBase
  {
    public BinaryPatcher()
    {
    }

    public BinaryPatcher(IContainer container)
      : base(container)
    {
    }

    public void ApplyPatch(System.IO.Stream stream, BinaryPatch Patch)
    {
      UserProcessBase.logger.DebugFormat("Applying {0}", (object) Patch);
      stream.Seek(Patch.Offset, SeekOrigin.Begin);
      stream.Write(Patch.Data, 0, Patch.Data.Length);
    }

    public void ApplyPatches(System.IO.Stream stream, BinaryPatch[] Patches)
    {
      foreach (BinaryPatch patch in Patches)
        this.ApplyPatch(stream, patch);
    }
  }
}
