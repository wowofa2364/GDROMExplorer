﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Binary.SEGALibraryVersion
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.Binary
{
  public class SEGALibraryVersion : IComparable<SEGALibraryVersion>
  {
    public string Major { get; private set; }

    public string Minor { get; private set; }

    public SEGALibraryVersion(string major, string minor)
    {
      this.Major = major;
      this.Minor = minor;
    }

    public override bool Equals(object obj) => obj != null && this.GetType() == obj.GetType() && this.CompareTo((SEGALibraryVersion) obj) == 0;

    public override string ToString() => string.Format("{0}.{1}", (object) this.Major, (object) this.Minor);

    public override int GetHashCode() => 1 + 17 * this.Major.GetHashCode() + 31 * this.Minor.GetHashCode();

    public int CompareTo(SEGALibraryVersion other)
    {
      int num = this.Major.CompareTo(other.Major);
      return num != 0 ? num : this.Minor.CompareTo(other.Minor);
    }
  }
}
