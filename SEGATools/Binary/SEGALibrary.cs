﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Binary.SEGALibrary
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace SEGATools.Binary
{
  public class SEGALibrary
  {
    public static readonly SEGALibrary AIP_103 = new SEGALibrary("AIP", new SEGALibraryVersion("1", "03"));
    public static readonly SEGALibrary AIPf_105 = new SEGALibrary("AIPf", new SEGALibraryVersion("1", "05"));
    public static readonly SEGALibrary SYTMR_053 = new SEGALibrary("syTmr", new SEGALibraryVersion("0", "53"));
    public static readonly SEGALibrary FMMNG_103 = new SEGALibrary("fmMng", new SEGALibraryVersion("1", "03"));

    public string Name { get; private set; }

    public SEGALibraryVersion Version { get; private set; }

    public DateTime BuildDate { get; private set; }

    public SEGALibraryType GetLibraryType() => SEGALibrary.GetTypeFromName(this.Name);

    public SEGALibrary(string Name, SEGALibraryVersion Version)
      : this(Name, Version, new DateTime())
    {
    }

    public SEGALibrary(string Name, SEGALibraryVersion Version, DateTime BuildOn)
    {
      this.Name = Name;
      this.Version = Version;
      this.BuildDate = BuildOn;
    }

    public override string ToString() => string.Format("[{0} v{1} build: {2}]", (object) this.Name, (object) this.Version, (object) this.BuildDate);

    public override bool Equals(object obj)
    {
      if (obj == null || this.GetType() != obj.GetType())
        return false;
      SEGALibrary segaLibrary = (SEGALibrary) obj;
      return this.GetLibraryType() == segaLibrary.GetLibraryType() && this.Version.Equals((object) segaLibrary.Version);
    }

    public override int GetHashCode() => 1 + 17 * this.Name.GetHashCode() + 31 * this.Version.GetHashCode();

    private static SEGALibraryType GetTypeFromName(string Name)
    {
      if (SEGALibrary.AIP_103.Name.Equals(Name, StringComparison.InvariantCultureIgnoreCase) || SEGALibrary.AIPf_105.Name.Equals(Name, StringComparison.InvariantCultureIgnoreCase))
        return SEGALibraryType.AIP;
      if (SEGALibrary.SYTMR_053.Name.Equals(Name, StringComparison.InvariantCultureIgnoreCase))
        return SEGALibraryType.SYS_TIMER;
      return SEGALibrary.FMMNG_103.Name.Equals(Name, StringComparison.InvariantCultureIgnoreCase) ? SEGALibraryType.FLASH_MEMORY_MANAGER : SEGALibraryType.UNKNOWN;
    }
  }
}
