﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.CueSheet.CueSheetCreator
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using ImageReader.DiscSectors;
using SEGATools.DiscFileSystem;
using System;
using System.IO;
using System.Linq;

namespace SEGATools.CueSheet
{
  public class CueSheetCreator
  {
    public static void CreateFromDiscSession(IDiscSession session, string FileName, string Title)
    {
      using (StreamWriter streamWriter = new StreamWriter(FileName))
      {
        streamWriter.WriteLine(string.Format("TITLE \"{0}\"\n", (object) Title.ToUpper()));
        if (session.Tracks.Count == 0)
          return;
        foreach (IDiscTrack track1 in session.Tracks)
        {
          IDiscTrack Track = track1;
          streamWriter.WriteLine(string.Format("FILE \"{0}\" BINARY", (object) Path.GetFileName(Track.FileName)));
          streamWriter.WriteLine(string.Format("TRACK {0:00} {1}", (object) Track.Index, (object) CueSheetCreator.GetCueSheetTrackModeFromTrack(Track)));
          if (Track == session.Tracks[0])
            streamWriter.WriteLine("PREGAP " + CueSheetCreator.LogicalBlockAddressToMmSsFf(Track.LogicalBlockAddress));
          streamWriter.WriteLine("INDEX 01 00:00:00");
          if (!CueSheetCreator.IsTheLastTrack(Track, session) && CueSheetCreator.IsPostGapRequired(Track, session.Tracks.First<IDiscTrack>((Func<IDiscTrack, bool>) (track => track.Index == Track.Index + 1))))
            streamWriter.WriteLine("POSTGAP 00:02:00");
          streamWriter.WriteLine("");
        }
      }
    }

    private static string LogicalBlockAddressToMmSsFf(uint lba)
    {
      uint num1 = lba / 75U;
      uint num2 = num1 / 60U;
      uint num3 = num1 % 60U;
      uint num4 = lba - (uint) (((int) num2 * 60 + (int) num3) * 75);
      return string.Format("{0:00}:{1:00}:{2:00}", (object) num2, (object) num3, (object) num4);
    }

    private static string GetCueSheetTrackModeFromTrack(IDiscTrack Track)
    {
      if (Track.TrackData == TrackModeType.Audio)
        return "AUDIO";
      if (Track.TrackSector is ISO9660Sector)
        return "MODE1/2048";
      if (Track.TrackSector is CDROMMode1RawSector)
        return "MODE1/2352";
      throw new FormatException(string.Format("Not supported track mode for {0}", (object) Track));
    }

    private static bool IsTheLastTrack(IDiscTrack Track, IDiscSession Session) => Track.Index == Session.Tracks[Session.Tracks.Count - 1].Index;

    private static bool IsPostGapRequired(IDiscTrack FirstTrack, IDiscTrack SecondTrack) => FirstTrack.Index + 1 == SecondTrack.Index && FirstTrack.TrackData != SecondTrack.TrackData;
  }
}
