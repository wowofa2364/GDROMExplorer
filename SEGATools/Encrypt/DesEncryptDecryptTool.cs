﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Encrypt.DesEncryptDecryptTool
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using SEGATools.UserProcess;
using SEGATools.VirtualFile;
using System;
using System.ComponentModel;
using System.IO;
using System.Security.Cryptography;

namespace SEGATools.Encrypt
{
  public class DesEncryptDecryptTool : UserProcessBase
  {
    private const int BUFFER_SIZE = 65536;

    public event AsyncOperationProgressChangedEventHandler DecryptionProgressChanged
    {
      add => this.AsyncOperationProgressChanged += value;
      remove => this.AsyncOperationProgressChanged -= value;
    }

    public event AsyncOperationCompletedEventHandler DecryptionCompleted
    {
      add => this.AsyncOperationCompleted += value;
      remove => this.AsyncOperationCompleted -= value;
    }

    public void EncryptAsync(
      IVirtualFile inputFile,
      IVirtualFile outputFile,
      DESKey Key,
      object taskId)
    {
      AsyncOperation asyncOperation = this.CreateAsyncOperation(taskId);
      new DesEncryptDecryptTool.WorkerEventHandler(this.EncryptOrDecryptWorker).BeginInvoke(inputFile, outputFile, Key.EncryptionCryptoTransform, asyncOperation, (AsyncCallback) null, (object) null);
    }

    public void DecryptAsync(
      IVirtualFile inputFile,
      IVirtualFile outputFile,
      DESKey Key,
      object taskId)
    {
      AsyncOperation asyncOperation = this.CreateAsyncOperation(taskId);
      new DesEncryptDecryptTool.WorkerEventHandler(this.EncryptOrDecryptWorker).BeginInvoke(inputFile, outputFile, Key.DecryptionCryptoTransform, asyncOperation, (AsyncCallback) null, (object) null);
    }

    private void EncryptOrDecryptWorker(
      IVirtualFile inputFile,
      IVirtualFile outputFile,
      ICryptoTransform CryptoTransform,
      AsyncOperation asyncOp)
    {
      byte[] numArray = new byte[65536];
      Exception exception = (Exception) null;
      try
      {
        using (System.IO.Stream fileInputStream = inputFile.FileInputStream)
        {
          using (System.IO.Stream fileOutputStream = outputFile.FileOutputStream)
          {
            fileInputStream.Seek(0L, SeekOrigin.Begin);
            while (fileInputStream.Position < fileInputStream.Length)
            {
              if (!this.TaskCanceled(asyncOp))
              {
                using (MemoryStream memoryStream = new MemoryStream(numArray))
                {
                  using (CryptoStream CryptoStream = new CryptoStream((System.IO.Stream) memoryStream, CryptoTransform, CryptoStreamMode.Write))
                    this.EncryptOrDecryptBuffer(numArray, CryptoStream, fileInputStream, fileOutputStream);
                }
                int num = (int) ((double) fileInputStream.Position / (double) fileInputStream.Length * 100.0);
                this.ReportProgress(new UserProcessProgressChangedEventArgs(inputFile.OriginalFileName, outputFile.OriginalFileName, num, num, asyncOp.UserSuppliedState), asyncOp);
              }
              else
                break;
            }
          }
        }
      }
      catch (Exception ex)
      {
        exception = ex;
      }
      if (!this.TaskCanceled(asyncOp))
      {
        if (exception == null)
          goto label_27;
      }
      try
      {
        File.Delete(outputFile.OriginalFileName);
      }
      catch (Exception ex)
      {
        UserProcessBase.logger.ErrorFormat("Unable to delete the file {0}: {1}", (object) outputFile.OriginalFileName, (object) ex.Message);
      }
label_27:
      this.ReportCompletion(outputFile.OriginalFileName, exception, asyncOp);
    }

    private void EncryptOrDecryptBuffer(
      byte[] Buffer,
      CryptoStream CryptoStream,
      System.IO.Stream InputStream,
      System.IO.Stream OutputStream)
    {
      int num1 = 0;
      int num2 = Buffer.Length / 8;
      InputStream.Read(Buffer, 0, Buffer.Length);
      while (num1 < num2)
      {
        int num3 = num1++ * 8;
        if (BitConverter.IsLittleEndian)
          Array.Reverse((Array) Buffer, num3, 8);
        CryptoStream.Write(Buffer, num3, 8);
        if (BitConverter.IsLittleEndian)
          Array.Reverse((Array) Buffer, num3, 8);
      }
      OutputStream.Write(Buffer, 0, Buffer.Length);
    }

    private delegate void WorkerEventHandler(
      IVirtualFile inputFile,
      IVirtualFile outputFile,
      ICryptoTransform CryptoTransform,
      AsyncOperation asyncOp);
  }
}
