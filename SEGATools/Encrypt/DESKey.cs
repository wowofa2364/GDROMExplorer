﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Encrypt.DESKey
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace SEGATools.Encrypt
{
  public class DESKey
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();

    private static readonly byte[] DEFAULT_NAOMI_IV = new byte[8];
    private static readonly Regex VALID_KEY_STRING = new Regex("[a-f0-9]{16}", RegexOptions.IgnoreCase | RegexOptions.Compiled);

    public byte[] Key { get; private set; }

    public byte[] IV => DESKey.DEFAULT_NAOMI_IV;

    public ICryptoTransform DecryptionCryptoTransform { get; private set; }

    public ICryptoTransform EncryptionCryptoTransform { get; private set; }

    private DESKey(
      byte[] Key,
      ICryptoTransform encryptionCryptoTransform,
      ICryptoTransform decryptionCryptoTransform)
    {
      this.Key = Key;
      this.EncryptionCryptoTransform = encryptionCryptoTransform;
      this.DecryptionCryptoTransform = decryptionCryptoTransform;
    }

    public string KeyString => string.Format("{7:X}{6:X}{5:X}{4:X}{3:X}{2:X}{1:X}{0:X}", (object) this.Key[0], (object) this.Key[1], (object) this.Key[2], (object) this.Key[3], (object) this.Key[4], (object) this.Key[5], (object) this.Key[6], (object) this.Key[7]);

    public override string ToString() => string.Format("DESKey: 0x{0}", (object) this.KeyString);

    public static DESKey Parse(string key)
    {
      byte[] numArray = new byte[key.Length / 2];
      for (int index = 0; index < numArray.Length; ++index)
      {
        numArray[index] = (byte) ((key[2 * index] > 'F' ? (int) key[2 * index] - 87 : (key[2 * index] > '9' ? (int) key[2 * index] - 55 : (int) key[2 * index] - 48)) << 4);
        numArray[index] |= key[2 * index + 1] > 'F' ? (byte) ((int) key[2 * index + 1] - 87) : (key[2 * index + 1] > '9' ? (byte) ((int) key[2 * index + 1] - 55) : (byte) ((int) key[2 * index + 1] - 48));
      }
      if (BitConverter.IsLittleEndian)
        Array.Reverse((Array) numArray);
      DES des = (DES) new DESCryptoServiceProvider();
      des.KeySize = 64;
      des.Padding = PaddingMode.None;
      des.Mode = CipherMode.ECB;
      ICryptoTransform encryptor = des.CreateEncryptor(numArray, DESKey.DEFAULT_NAOMI_IV);
      ICryptoTransform decryptor = des.CreateDecryptor(numArray, DESKey.DEFAULT_NAOMI_IV);
      return new DESKey(numArray, encryptor, decryptor);
    }

    public static bool TryParse(string key)
    {
      if (string.IsNullOrEmpty(key))
        return false;
      string str = key.Trim();
      if (!DESKey.VALID_KEY_STRING.Match(str).Success)
        return false;
      try
      {
        DESKey.Parse(str);
        return true;
      }
      catch (CryptographicException ex)
      {
        logger.Error(ex);
        return false;
      }
    }
  }
}
