﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.GDEmu.DiscTrackCopyInfo
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using ImageReader.DiscSectors;
using SEGATools.Binary;
using SEGATools.DiscFileSystem;
using System.Collections.Generic;
using System.Linq;

namespace SEGATools.GDEmu
{
  internal class DiscTrackCopyInfo
  {
    private HashSet<BinaryPatch> patchesApplied;

    public IDiscTrack SourceTrack { get; private set; }

    public IDiscTrack DestinationTrack { get; private set; }

    public int[] ModifiedSectors
    {
      get
      {
        HashSet<int> source = new HashSet<int>();
        foreach (BinaryPatch binaryPatch in this.patchesApplied)
        {
          int num1 = (int) binaryPatch.Offset / DiscSectorCommon.LogicalSectorSize;
          int num2 = (int) (binaryPatch.Offset + (long) binaryPatch.Data.Length) / DiscSectorCommon.LogicalSectorSize;
          for (int index = num1; index <= num2; ++index)
            source.Add(index);
        }
        return source.ToArray<int>();
      }
    }

    public static DiscTrackCopyInfo CreateFrom(
      IDiscTrack source,
      string newFileName)
    {
      return new DiscTrackCopyInfo(source, DiscTrack.CreateCopyFrom(source, newFileName));
    }

    private DiscTrackCopyInfo(IDiscTrack source, IDiscTrack destination)
    {
      this.SourceTrack = source;
      this.DestinationTrack = destination;
      this.patchesApplied = new HashSet<BinaryPatch>();
    }

    public void AddPatches(params BinaryPatch[] patches) => this.patchesApplied.UnionWith((IEnumerable<BinaryPatch>) patches);
  }
}
