﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.GDEmu.GDICreator
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using SEGATools.DiscFileSystem;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SEGATools.GDEmu
{
  public class GDICreator
  {
    public static void CreateGDIFile(List<IDiscTrack> Tracks, string OutputGDIFile)
    {
      List<IDiscTrack> list = Tracks.OrderBy<IDiscTrack, uint>((Func<IDiscTrack, uint>) (track => track.LogicalBlockAddress)).ToList<IDiscTrack>();
      using (StreamWriter streamWriter = new StreamWriter(OutputGDIFile))
      {
        streamWriter.WriteLine(list.Count);
        for (int index = 0; index < list.Count; ++index)
        {
          IDiscTrack discTrack = list[index];
          streamWriter.WriteLine("{0} {1} {2} {3} {4} {5}", (object) (index + 1), (object) discTrack.LogicalBlockAddress, (object) (short) discTrack.TrackData, (object) discTrack.TrackSector.Size, (object) discTrack.VirtualName, (object) discTrack.Offset);
        }
      }
    }
  }
}
