﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.GDEmu.GDEmuExportOptions
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System.IO;

namespace SEGATools.GDEmu
{
  public class GDEmuExportOptions
  {
    private static readonly string GDEMU_DEFAULT_DISC_IMAGE_FILE_NAME = "disc.gdi";
    public bool RegionFree;
    public bool ForceVGA;
    public string OutputPath;

    public string GDIFileName => GDEmuExportOptions.GDEMU_DEFAULT_DISC_IMAGE_FILE_NAME;

    public string GetOutputGDIFilePath() => Path.Combine(this.OutputPath, this.GDIFileName);
  }
}
