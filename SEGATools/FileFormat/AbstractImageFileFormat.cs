﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.FileFormat.AbstractImageFileFormat
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using SEGATools.DiscFileSystem;
using System.Collections.Generic;
using System.Linq;

namespace SEGATools.FileFormat
{
  public abstract class AbstractImageFileFormat : IImageFileFormat
  {
    public abstract string[] FileExtentions { get; }

    public abstract string[] FileExtentionDescriptions { get; }

    public abstract IDiscFileSystemConverter ImageFileConverter { get; }

    public string PluginAssemblyFileName { get; set; }

    public override bool Equals(object obj)
    {
      if (obj == null || !typeof (IImageFileFormat).IsAssignableFrom(obj.GetType()))
        return false;
      IImageFileFormat imageFileFormat = (IImageFileFormat) obj;
      return ((IEnumerable<string>) this.FileExtentions).SequenceEqual<string>((IEnumerable<string>) imageFileFormat.FileExtentions) && ((IEnumerable<string>) this.FileExtentionDescriptions).SequenceEqual<string>((IEnumerable<string>) imageFileFormat.FileExtentionDescriptions);
    }

    public override int GetHashCode() => 1 + 17 * this.FileExtentions.GetHashCode() + 31 * this.FileExtentionDescriptions.GetHashCode();
  }
}
