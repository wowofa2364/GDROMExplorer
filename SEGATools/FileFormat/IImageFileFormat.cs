﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.FileFormat.IImageFileFormat
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using SEGATools.DiscFileSystem;

namespace SEGATools.FileFormat
{
  public interface IImageFileFormat
  {
    string[] FileExtentions { get; }

    string[] FileExtentionDescriptions { get; }

    IDiscFileSystemConverter ImageFileConverter { get; }

    string PluginAssemblyFileName { get; set; }
  }
}
