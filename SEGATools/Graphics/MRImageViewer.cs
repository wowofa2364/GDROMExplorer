﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Graphics.MRImageViewer
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using SEGATools.Formater;
using SEGATools.Properties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SEGATools.Graphics
{
  public class MRImageViewer : UserControl
  {
    private MRImageExporter mrImageExporter;
    private string basePath;
    private IContainer components;
    private PictureBox pictureBox;
    private GroupBox gbMRImageInfo;
    private Label lbColors;
    private Label lbImageSize;
    private Label lbNumberOfColorsValue;
    private Label lbImageSizeValue;
    private Label lbFileSizeValue;
    private Label lbFileSize;
    private Label lbCompressedSizeValue;
    private Label lbSize;
    private Label lbUncompressedSizeValue;
    private Label lbUncompressedSize;
    private Label lbCompressionRatioValue;
    private Label lbCompressionRatio;
    private Button btExportMRImage;
    private SaveFileDialog saveFileDialog;
    private Label lbNoMrImage;

    public MRImage MRImage { get; private set; }

    public string BasePath
    {
      get => this.basePath;
      set
      {
        if (string.IsNullOrEmpty(value))
          return;
        this.basePath = value;
        this.saveFileDialog.InitialDirectory = this.basePath;
      }
    }

    private bool DisplayNoImageMessage => true;

    public MRImageViewer()
    {
      this.InitializeComponent();
      this.pictureBox.Resize += new EventHandler(this.pictureBox_Resize);
      this.mrImageExporter = new MRImageExporter();
      this.saveFileDialog.Title = Resources.SfdMRImageTitle;
      this.saveFileDialog.Filter = this.mrImageExporter.CreateSaveFileDialogFilter();
      this.lbNoMrImage.Text = Resources.MRImageNoImageMessage;
      this.CenterImageInDisplayArea((Control) this.lbNoMrImage);
      this.UpdateViewAndDisplayNoImage();
    }

    public void LoadMRImage(MRImage mrImage, string imageName)
    {
      this.MRImage = mrImage;
      this.saveFileDialog.FileName = imageName;
      this.SetImageProperties(this.MRImage);
      this.UpdateViewAndDisplayImages();
    }

    private void SetImageProperties(MRImage mrImage)
    {
      this.lbImageSizeValue.Text = string.Format("{0}x{1}", (object) mrImage.ImageSize.Width, (object) mrImage.ImageSize.Height);
      this.lbNumberOfColorsValue.Text = string.Format("{0}", (object) mrImage.ColorPalette.Length);
      this.lbFileSizeValue.Text = string.Format("{0} ({1} bytes)", (object) SizeFormater.ToHumanReadableSize((long) mrImage.FileSize), (object) mrImage.FileSize);
      this.lbCompressedSizeValue.Text = string.Format("{0} bytes", (object) mrImage.CompressedSize);
      this.lbUncompressedSizeValue.Text = string.Format("{0} bytes", (object) mrImage.UncompressedSize);
      this.lbCompressionRatioValue.Text = string.Format("{0:0.0}:1", (object) mrImage.CompressionRatio);
      this.pictureBox.Image = (Image) mrImage.ToBitmap();
    }

    private void CenterImageInDisplayArea(Control control)
    {
      int x = (this.Width - control.Size.Width) / 2;
      int y = (this.gbMRImageInfo.Location.Y - control.Size.Height) / 2;
      control.Location = new Point(x, y);
    }

    private void OpenFileDialogAndExportImage(Image image)
    {
      if (this.saveFileDialog.ShowDialog((IWin32Window) this) != DialogResult.OK)
        return;
      this.mrImageExporter.Save(this.MRImage, this.saveFileDialog.FileName, this.mrImageExporter.GetMRImageFormatFromFilterIndex(this.saveFileDialog.FilterIndex));
      this.saveFileDialog.InitialDirectory = Path.GetDirectoryName(this.saveFileDialog.FileName);
      this.saveFileDialog.FileName = Path.GetFileName(this.saveFileDialog.FileName);
    }

    private void UpdateViewAndDisplayNoImage()
    {
      this.lbNoMrImage.Visible = this.DisplayNoImageMessage;
      this.gbMRImageInfo.Visible = false;
      this.btExportMRImage.Visible = false;
      this.pictureBox.Visible = false;
    }

    private void UpdateViewAndDisplayImages()
    {
      this.lbNoMrImage.Visible = false;
      this.gbMRImageInfo.Visible = true;
      this.btExportMRImage.Visible = true;
      this.pictureBox.Visible = true;
    }

    private void pictureBox_Resize(object sender, EventArgs e) => this.CenterImageInDisplayArea((Control) this.pictureBox);

    private void btExportMRImage_Click(object sender, EventArgs e) => this.OpenFileDialogAndExportImage(this.pictureBox.Image);

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.pictureBox = new PictureBox();
      this.gbMRImageInfo = new GroupBox();
      this.lbCompressionRatioValue = new Label();
      this.lbCompressionRatio = new Label();
      this.lbUncompressedSizeValue = new Label();
      this.lbUncompressedSize = new Label();
      this.lbCompressedSizeValue = new Label();
      this.lbSize = new Label();
      this.lbFileSizeValue = new Label();
      this.lbFileSize = new Label();
      this.lbNumberOfColorsValue = new Label();
      this.lbImageSizeValue = new Label();
      this.lbColors = new Label();
      this.lbImageSize = new Label();
      this.btExportMRImage = new Button();
      this.saveFileDialog = new SaveFileDialog();
      this.lbNoMrImage = new Label();
      ((ISupportInitialize) this.pictureBox).BeginInit();
      this.gbMRImageInfo.SuspendLayout();
      this.SuspendLayout();
      this.pictureBox.Anchor = AnchorStyles.Top | AnchorStyles.Bottom;
      this.pictureBox.BorderStyle = BorderStyle.FixedSingle;
      this.pictureBox.Location = new Point(6, 3);
      this.pictureBox.Name = "pictureBox";
      this.pictureBox.Size = new Size(448, 90);
      this.pictureBox.SizeMode = PictureBoxSizeMode.AutoSize;
      this.pictureBox.TabIndex = 0;
      this.pictureBox.TabStop = false;
      this.gbMRImageInfo.Controls.Add((Control) this.lbCompressionRatioValue);
      this.gbMRImageInfo.Controls.Add((Control) this.lbCompressionRatio);
      this.gbMRImageInfo.Controls.Add((Control) this.lbUncompressedSizeValue);
      this.gbMRImageInfo.Controls.Add((Control) this.lbUncompressedSize);
      this.gbMRImageInfo.Controls.Add((Control) this.lbCompressedSizeValue);
      this.gbMRImageInfo.Controls.Add((Control) this.lbSize);
      this.gbMRImageInfo.Controls.Add((Control) this.lbFileSizeValue);
      this.gbMRImageInfo.Controls.Add((Control) this.lbFileSize);
      this.gbMRImageInfo.Controls.Add((Control) this.lbNumberOfColorsValue);
      this.gbMRImageInfo.Controls.Add((Control) this.lbImageSizeValue);
      this.gbMRImageInfo.Controls.Add((Control) this.lbColors);
      this.gbMRImageInfo.Controls.Add((Control) this.lbImageSize);
      this.gbMRImageInfo.Location = new Point(6, 99);
      this.gbMRImageInfo.Name = "gbMRImageInfo";
      this.gbMRImageInfo.Size = new Size(448, 75);
      this.gbMRImageInfo.TabIndex = 0;
      this.gbMRImageInfo.TabStop = false;
      this.gbMRImageInfo.Text = "Properties:";
      this.lbCompressionRatioValue.AutoSize = true;
      this.lbCompressionRatioValue.Location = new Point(345, 54);
      this.lbCompressionRatioValue.Name = "lbCompressionRatioValue";
      this.lbCompressionRatioValue.Size = new Size(89, 13);
      this.lbCompressionRatioValue.TabIndex = 0;
      this.lbCompressionRatioValue.Text = "compression ratio";
      this.lbCompressionRatio.AutoSize = true;
      this.lbCompressionRatio.Location = new Point(235, 54);
      this.lbCompressionRatio.Name = "lbCompressionRatio";
      this.lbCompressionRatio.Size = new Size(98, 13);
      this.lbCompressionRatio.TabIndex = 0;
      this.lbCompressionRatio.Text = "Compression Ratio:";
      this.lbUncompressedSizeValue.AutoSize = true;
      this.lbUncompressedSizeValue.Location = new Point(345, 37);
      this.lbUncompressedSizeValue.Name = "lbUncompressedSizeValue";
      this.lbUncompressedSizeValue.Size = new Size(97, 13);
      this.lbUncompressedSizeValue.TabIndex = 0;
      this.lbUncompressedSizeValue.Text = "uncompressed size";
      this.lbUncompressedSize.AutoSize = true;
      this.lbUncompressedSize.Location = new Point(234, 37);
      this.lbUncompressedSize.Name = "lbUncompressedSize";
      this.lbUncompressedSize.Size = new Size(104, 13);
      this.lbUncompressedSize.TabIndex = 0;
      this.lbUncompressedSize.Text = "Uncompressed Size:";
      this.lbCompressedSizeValue.AutoSize = true;
      this.lbCompressedSizeValue.Location = new Point(345, 20);
      this.lbCompressedSizeValue.Name = "lbCompressedSizeValue";
      this.lbCompressedSizeValue.Size = new Size(85, 13);
      this.lbCompressedSizeValue.TabIndex = 0;
      this.lbCompressedSizeValue.Text = "compressed size";
      this.lbSize.AutoSize = true;
      this.lbSize.Location = new Point(235, 20);
      this.lbSize.Name = "lbSize";
      this.lbSize.Size = new Size(91, 13);
      this.lbSize.TabIndex = 0;
      this.lbSize.Text = "Compressed Size:";
      this.lbFileSizeValue.AutoSize = true;
      this.lbFileSizeValue.Location = new Point(106, 54);
      this.lbFileSizeValue.Name = "lbFileSizeValue";
      this.lbFileSizeValue.Size = new Size(64, 13);
      this.lbFileSizeValue.TabIndex = 0;
      this.lbFileSizeValue.Text = "size in bytes";
      this.lbFileSize.AutoSize = true;
      this.lbFileSize.Location = new Point(7, 54);
      this.lbFileSize.Name = "lbFileSize";
      this.lbFileSize.Size = new Size(49, 13);
      this.lbFileSize.TabIndex = 0;
      this.lbFileSize.Text = "File Size:";
      this.lbNumberOfColorsValue.AutoSize = true;
      this.lbNumberOfColorsValue.Location = new Point(103, 37);
      this.lbNumberOfColorsValue.Name = "lbNumberOfColorsValue";
      this.lbNumberOfColorsValue.Size = new Size(13, 13);
      this.lbNumberOfColorsValue.TabIndex = 0;
      this.lbNumberOfColorsValue.Text = "0";
      this.lbImageSizeValue.AutoSize = true;
      this.lbImageSizeValue.Location = new Point(103, 20);
      this.lbImageSizeValue.Name = "lbImageSizeValue";
      this.lbImageSizeValue.Size = new Size(54, 13);
      this.lbImageSizeValue.TabIndex = 0;
      this.lbImageSizeValue.Text = "XXXxYYY";
      this.lbColors.AutoSize = true;
      this.lbColors.Location = new Point(7, 37);
      this.lbColors.Name = "lbColors";
      this.lbColors.Size = new Size(90, 13);
      this.lbColors.TabIndex = 0;
      this.lbColors.Text = "Number of colors:";
      this.lbImageSize.AutoSize = true;
      this.lbImageSize.Location = new Point(7, 20);
      this.lbImageSize.Name = "lbImageSize";
      this.lbImageSize.Size = new Size(62, 13);
      this.lbImageSize.TabIndex = 0;
      this.lbImageSize.Text = "Image Size:";
      this.btExportMRImage.CausesValidation = false;
      this.btExportMRImage.FlatStyle = FlatStyle.Popup;
      this.btExportMRImage.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.btExportMRImage.ForeColor = Color.FromArgb(248, 48, 0);
      this.btExportMRImage.Location = new Point(6, 180);
      this.btExportMRImage.Name = "btExportMRImage";
      this.btExportMRImage.Size = new Size(108, 22);
      this.btExportMRImage.TabIndex = 1;
      this.btExportMRImage.Text = "Export Image...";
      this.btExportMRImage.UseVisualStyleBackColor = false;
      this.btExportMRImage.Click += new EventHandler(this.btExportMRImage_Click);
      this.lbNoMrImage.AutoSize = true;
      this.lbNoMrImage.Font = new Font("Microsoft Sans Serif", 15.75f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.lbNoMrImage.Location = new Point(117, 35);
      this.lbNoMrImage.Name = "lbNoMrImage";
      this.lbNoMrImage.Size = new Size(226, 25);
      this.lbNoMrImage.TabIndex = 2;
      this.lbNoMrImage.Text = "No MR Image found!";
      this.lbNoMrImage.TextAlign = ContentAlignment.MiddleCenter;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.lbNoMrImage);
      this.Controls.Add((Control) this.btExportMRImage);
      this.Controls.Add((Control) this.gbMRImageInfo);
      this.Controls.Add((Control) this.pictureBox);
      this.Name = nameof (MRImageViewer);
      this.Size = new Size(460, 209);
      ((ISupportInitialize) this.pictureBox).EndInit();
      this.gbMRImageInfo.ResumeLayout(false);
      this.gbMRImageInfo.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
