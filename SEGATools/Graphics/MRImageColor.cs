﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Graphics.MRImageColor
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System.Runtime.InteropServices;

namespace SEGATools.Graphics
{
  [StructLayout(LayoutKind.Explicit, Size = 4, Pack = 1)]
  internal struct MRImageColor
  {
    [FieldOffset(0)]
    internal byte Blue;
    [FieldOffset(1)]
    internal byte Green;
    [FieldOffset(2)]
    internal byte Red;
    [FieldOffset(3)]
    internal byte Alpha;
  }
}
