﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Graphics.MRImage
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace SEGATools.Graphics
{
  public class MRImage
  {
    private MRImageHeader header;
    private Bitmap imageBitmap;

    public Size ImageSize => new Size(this.header.Width, this.header.Height);

    public int UncompressedSize => this.Pixels.Length;

    public int FileSize => this.header.Size;

    public int CompressedSize => this.header.Size - this.header.Offset;

    public float CompressionRatio => (float) this.UncompressedSize / (float) this.CompressedSize;

    public int Offset { get; private set; }

    public byte[] Pixels { get; private set; }

    public Color[] ColorPalette { get; private set; }

    private byte[] RawData { get; set; }

    public System.IO.Stream FileStream => (System.IO.Stream) new MemoryStream(this.RawData, 0, this.RawData.Length, false);

    internal MRImage(
      MRImageHeader header,
      Color[] colorPalette,
      byte[] pixels,
      int offset,
      byte[] rawData)
    {
      this.header = header;
      this.ColorPalette = colorPalette;
      this.Pixels = pixels;
      this.Offset = offset;
      this.RawData = rawData;
      this.ToBitmap();
    }

    private byte GetPixel(int x, int y) => this.Pixels[y * this.header.Width + x];

    private Color GetColorForPixel(int x, int y) => this.ColorPalette[(int) this.GetPixel(x, y)];

    public Bitmap ToBitmap()
    {
      if (this.imageBitmap != null)
        return this.imageBitmap;
      Bitmap bitmap = new Bitmap(this.ImageSize.Width, this.ImageSize.Height, PixelFormat.Format24bppRgb);
      for (int y = 0; y < bitmap.Height; ++y)
      {
        for (int x = 0; x < bitmap.Width; ++x)
        {
          Color colorForPixel = this.GetColorForPixel(x, y);
          bitmap.SetPixel(x, y, colorForPixel);
        }
      }
      this.imageBitmap = bitmap;
      return bitmap;
    }
  }
}
