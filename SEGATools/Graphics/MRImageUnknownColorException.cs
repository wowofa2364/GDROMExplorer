﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Graphics.MRImageUnknownColorException
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

namespace SEGATools.Graphics
{
  internal class MRImageUnknownColorException : MRImageReadingException
  {
    public MRImageUnknownColorException(string message)
      : base(string.Format("MR Image Unknown Color Error: {0}", (object) message))
    {
    }

    public static MRImageUnknownColorException anUnknownColor(
      byte colorNumber)
    {
      return new MRImageUnknownColorException(string.Format("color number {0} does not exist", (object) colorNumber));
    }
  }
}
