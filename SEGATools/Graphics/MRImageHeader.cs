﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Graphics.MRImageHeader
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System.Runtime.InteropServices;

namespace SEGATools.Graphics
{
  [StructLayout(LayoutKind.Explicit, Size = 28, Pack = 1)]
  internal struct MRImageHeader
  {
    [FieldOffset(0)]
    internal int Size;
    [FieldOffset(8)]
    internal int Offset;
    [FieldOffset(12)]
    internal int Width;
    [FieldOffset(16)]
    internal int Height;
    [FieldOffset(24)]
    internal uint NumberOfColors;
  }
}
