﻿// Decompiled with JetBrains decompiler
// Type: SEGATools.Graphics.MRImageExporter
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;

namespace SEGATools.Graphics
{
  public class MRImageExporter
  {
    private static List<MRImageExporter.MRImageFormat> supportedMRImageFormats;

    public MRImageExporter()
    {
      if (MRImageExporter.supportedMRImageFormats != null)
        return;
      MRImageExporter.supportedMRImageFormats = new List<MRImageExporter.MRImageFormat>();
      MRImageExporter.supportedMRImageFormats.Add(new MRImageExporter.MRImageFormat(".mr", "MR image"));
      MRImageExporter.supportedMRImageFormats.Add(new MRImageExporter.MRImageFormat(".bmp", "Bitmap image", ImageFormat.Bmp));
      MRImageExporter.supportedMRImageFormats.Add(new MRImageExporter.MRImageFormat(".png", "PNG image", ImageFormat.Png));
      MRImageExporter.supportedMRImageFormats.Add(new MRImageExporter.MRImageFormat(".jpeg", "JPEG image", ImageFormat.Jpeg));
      MRImageExporter.supportedMRImageFormats.Add(new MRImageExporter.MRImageFormat(".gif", "GIF image", ImageFormat.Gif));
    }

    public string CreateSaveFileDialogFilter()
    {
      string str = string.Empty;
      foreach (MRImageExporter.MRImageFormat supportedMrImageFormat in MRImageExporter.supportedMRImageFormats)
        str = str + "|" + supportedMrImageFormat.Description + "|*" + supportedMrImageFormat.Extension;
      if (!string.IsNullOrEmpty(str))
        str = str.Substring(1);
      return str;
    }

    public MRImageExporter.MRImageFormat GetMRImageFormatFromFilterIndex(int index) => index < 1 || index > MRImageExporter.supportedMRImageFormats.Count ? (MRImageExporter.MRImageFormat) null : MRImageExporter.supportedMRImageFormats[index - 1];

    public MRImageExporter.MRImageFormat FindMRImageFileFormatForFileExtension(
      string fileExtension)
    {
      return string.IsNullOrEmpty(fileExtension) ? (MRImageExporter.MRImageFormat) null : MRImageExporter.supportedMRImageFormats.Find((Predicate<MRImageExporter.MRImageFormat>) (mrImageFileFormat => mrImageFileFormat.Extension.Equals(fileExtension, StringComparison.InvariantCultureIgnoreCase)));
    }

    public bool IsImageExtensionSupported(string fileExtension) => this.FindMRImageFileFormatForFileExtension(fileExtension) != null;

    public void Save(
      MRImage mrImage,
      string outputFileName,
      MRImageExporter.MRImageFormat imageFormat)
    {
      if (imageFormat == null)
        throw new ArgumentException(nameof (imageFormat));
      if (imageFormat.IsRawMR)
      {
        using (System.IO.Stream fileStream1 = mrImage.FileStream)
        {
          byte[] buffer = new byte[fileStream1.Length];
          using (FileStream fileStream2 = new FileStream(outputFileName, FileMode.OpenOrCreate))
          {
            fileStream1.Read(buffer, 0, buffer.Length);
            fileStream2.Write(buffer, 0, buffer.Length);
          }
        }
      }
      else
        mrImage.ToBitmap().Save(outputFileName, imageFormat.Format);
    }

    public class MRImageFormat
    {
      public bool IsRawMR { get; set; }

      public string Extension { get; set; }

      public string Description { get; set; }

      public ImageFormat Format { get; set; }

      public MRImageFormat(string extension, string description, ImageFormat imageFormat)
      {
        this.Extension = extension;
        this.Description = description;
        this.Format = imageFormat;
        this.IsRawMR = false;
      }

      public MRImageFormat(string extension, string description)
      {
        this.Extension = extension;
        this.Description = description;
        this.IsRawMR = true;
      }
    }
  }
}
