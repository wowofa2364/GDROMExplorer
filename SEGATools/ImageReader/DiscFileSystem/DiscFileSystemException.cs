﻿// Decompiled with JetBrains decompiler
// Type: ImageReader.DiscFileSystem.DiscFileSystemException
// Assembly: SEGATools, Version=1.0.3.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: D631183F-57B1-40A1-B502-5364D288307A
// Assembly location: SEGATools.dll

using System;

namespace ImageReader.DiscFileSystem
{
  public class DiscFileSystemException : Exception
  {
    public DiscFileSystemException(string message)
      : base(string.Format("Disc File-System Error: {0}", (object) message))
    {
    }

    public DiscFileSystemException(string message, Exception innerException)
      : base(string.Format("Disc File-System Error: {0}", (object) message), innerException)
    {
    }

    public static DiscFileSystemException noFileSystemFoundException() => new DiscFileSystemException("no file-system found!");
  }
}
