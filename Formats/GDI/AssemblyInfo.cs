﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyCopyright("2014 - 2016")]
[assembly: AssemblyTitle("GD-ROM Explorer GDI File Format")]
[assembly: AssemblyDescription("GDI File Format Plugin")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Japanese Cake")]
[assembly: AssemblyProduct("GD-ROM Explorer - GDI File Format Plugin")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(true)]
[assembly: Guid("d7bec06d-acf2-4d01-a319-8978bb4c951a")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyVersion("1.0.0.0")]
