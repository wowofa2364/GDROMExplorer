﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.ImageFileFormat.GDI.GDImageFormat
// Assembly: GDI, Version=1.0.0.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: F4295E7C-8421-4324-B5B1-F38932DD6235
// Assembly location: Formats\GDI.dll

using SEGATools.DiscFileSystem;
using SEGATools.FileFormat;

namespace GDRomExplorer.ImageFileFormat.GDI
{
  internal class GDImageFormat : AbstractImageFileFormat
  {
    public override string[] FileExtentions => new string[1]
    {
      ".gdi"
    };

    public override string[] FileExtentionDescriptions => new string[1]
    {
      "GDROM image file"
    };

    public override IDiscFileSystemConverter ImageFileConverter => (IDiscFileSystemConverter) new GDImageBuilder();
  }
}
