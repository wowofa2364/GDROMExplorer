﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.ImageFileFormat.GDI.IGDIFileSystemValidator
// Assembly: GDI, Version=1.0.0.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: F4295E7C-8421-4324-B5B1-F38932DD6235
// Assembly location: Formats\GDI.dll

using SEGATools.DiscFileSystem;

namespace GDRomExplorer.ImageFileFormat.GDI
{
  public interface IGDIFileSystemValidator
  {
    void CheckDiscTrackSize(IDiscTrack track);

    void CheckDiscFileSystem(IDiscFileSystem disc);
  }
}
