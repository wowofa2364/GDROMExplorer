﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.ImageFileFormat.GDI.InitialProgramProvider
// Assembly: GDI, Version=1.0.0.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: F4295E7C-8421-4324-B5B1-F38932DD6235
// Assembly location: Formats\GDI.dll

using ImageReader.Stream;
using SEGATools.DiscFileSystem;
using SEGATools.Security;
using System;

namespace GDRomExplorer.ImageFileFormat.GDI
{
  public class InitialProgramProvider : IInitialProgramProvider
  {
    public InitialProgram GetInitialProgram(IDiscTrack track)
    {
      try
      {
        byte[] buffer;
        using (DiscSectorStream discSectorStream = new DiscSectorStream(track.FileInputStream, track.TrackSector))
        {
          buffer = new byte[(int) InitialProgram.IP_FILESIZE];
          discSectorStream.Read(buffer, 0, buffer.Length);
        }
        return InitialProgramConverter.ToInitialProgram(buffer, 0);
      }
      catch (Exception ex)
      {
        throw new DiscFormatException(string.Format("Invalid boot sector: {0} does not contain a valid boot sector (IP)", (object) track), ex);
      }
    }
  }
}
