﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.ImageFileFormat.ISO9660.ISO9660ImageFormat
// Assembly: ISO9660, Version=1.0.0.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: 05C13092-F2D8-4D43-9750-90851EBE12FD
// Assembly location: Formats\ISO9660.dll

using ImageReader.DiscSectors;
using SEGATools.DiscFileSystem;
using SEGATools.FileFormat;

namespace GDRomExplorer.ImageFileFormat.ISO9660
{
  internal class ISO9660ImageFormat : AbstractImageFileFormat
  {
    public override string[] FileExtentions => new string[1]
    {
      ".iso"
    };

    public override string[] FileExtentionDescriptions => new string[1]
    {
      "ISO9660 image file"
    };

    public override IDiscFileSystemConverter ImageFileConverter => (IDiscFileSystemConverter) new GenericImageConverter((IDiscSector) new ISO9660Sector());
  }
}
