﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("GD-ROM Explorer ISO9660 File Format")]
[assembly: AssemblyDescription("ISO9660 File Format Plugin")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Japanese Cake")]
[assembly: AssemblyProduct("GD-ROM Explorer - ISO9660 File Format Plugin")]
[assembly: AssemblyCopyright("2014 - 2016")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(true)]
[assembly: Guid("f024f756-98ed-4ffa-b06b-baf386b755a5")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyVersion("1.0.0.0")]
