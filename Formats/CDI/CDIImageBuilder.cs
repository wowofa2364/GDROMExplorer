﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.ImageFileFormat.CDI.CDIImageBuilder
// Assembly: CDI, Version=1.0.1.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B71D3BB5-2FC4-43C2-853E-907E3A458120
// Assembly location: Formats\CDI.dll

using SEGATools.DiscFileSystem;
using System.IO;

namespace GDRomExplorer.ImageFileFormat.CDI
{
  public class CDIImageBuilder : IDiscFileSystemConverter
  {
    public IDiscFileSystem ToDiscFileSystem(string imageFileName)
    {
      CDIHeader cdiHeader;
      using (FileStream fileStream = File.Open(imageFileName, FileMode.Open, FileAccess.Read, FileShare.Read))
        cdiHeader = CDIHeaderConverter.ToCDIHeader((Stream) fileStream);
      if (cdiHeader.cdiVersion == CDIVersion.CDI_VERSION_UNKNOWN)
        throw new DiscFormatException(string.Format("Unsupported CDI version 0x{0:X}", (object) cdiHeader.rawCdiVersion));
      CDIToc cdiToc = (CDIToc) null;
      using (FileStream fileStream = File.Open(imageFileName, FileMode.Open, FileAccess.Read, FileShare.Read))
        cdiToc = CDITocConverter.ToCdiToc(cdiHeader, (Stream) fileStream, imageFileName);
      return (IDiscFileSystem) new SEGATools.DiscFileSystem.DiscFileSystem(imageFileName, cdiToc.sessions, true, false);
    }
  }
}
