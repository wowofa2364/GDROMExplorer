﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.ImageFileFormat.CDI.CDIToc
// Assembly: CDI, Version=1.0.1.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B71D3BB5-2FC4-43C2-853E-907E3A458120
// Assembly location: Formats\CDI.dll

using SEGATools.DiscFileSystem;
using System.Collections.Generic;

namespace GDRomExplorer.ImageFileFormat.CDI
{
  internal class CDIToc
  {
    internal CDIHeader cdiHeader;
    internal ushort numberOfSessions;
    internal ushort[] numberOfTracks;
    internal List<IDiscSession> sessions;

    internal CDIToc(CDIHeader cdiHeader)
    {
      this.cdiHeader = cdiHeader;
      this.sessions = new List<IDiscSession>();
    }
  }
}
