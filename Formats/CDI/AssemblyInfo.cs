﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyCopyright("2014 - 2016")]
[assembly: AssemblyTitle("GD-ROM Explorer CDI File Format")]
[assembly: AssemblyDescription("CDI File Format Plugin")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Japanese Cake")]
[assembly: AssemblyProduct("GD-ROM Explorer - CDI File Format Plugin")]
[assembly: Guid("0dae2017-8888-4e40-a4aa-798856425b20")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(true)]
[assembly: AssemblyFileVersion("1.0.1.0")]
[assembly: AssemblyVersion("1.0.1.0")]
