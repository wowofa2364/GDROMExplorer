﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.ImageFileFormat.CDI.CDIImageFormat
// Assembly: CDI, Version=1.0.1.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B71D3BB5-2FC4-43C2-853E-907E3A458120
// Assembly location: Formats\CDI.dll

using SEGATools.DiscFileSystem;
using SEGATools.FileFormat;

namespace GDRomExplorer.ImageFileFormat.CDI
{
  internal class CDIImageFormat : AbstractImageFileFormat
  {
    public override string[] FileExtentions => new string[1]
    {
      ".cdi"
    };

    public override string[] FileExtentionDescriptions => new string[1]
    {
      "DiscJuggler image file"
    };

    public override IDiscFileSystemConverter ImageFileConverter => (IDiscFileSystemConverter) new CDIImageBuilder();
  }
}
