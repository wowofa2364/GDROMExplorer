﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.ImageFileFormat.CDI.CDIHeaderConverter
// Assembly: CDI, Version=1.0.1.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B71D3BB5-2FC4-43C2-853E-907E3A458120
// Assembly location: Formats\CDI.dll

using SEGATools.DiscFileSystem;
using System;
using System.IO;

namespace GDRomExplorer.ImageFileFormat.CDI
{
  internal class CDIHeaderConverter
  {
    private static readonly byte HEADER_MIN_SIZE = 8;
    private static readonly short HEADER_OFFSET = -8;
    private static readonly short HEADER_SIZE = 8;

    internal static CDIHeader ToCDIHeader(Stream imageStream)
    {
      if (imageStream.Length < (long) CDIHeaderConverter.HEADER_MIN_SIZE)
        throw new DiscFormatException("The stream is too small");
      CDIHeader cdiHeader = new CDIHeader();
      byte[] buffer = new byte[(int) CDIHeaderConverter.HEADER_SIZE];
      imageStream.Seek((long) CDIHeaderConverter.HEADER_OFFSET, SeekOrigin.End);
      imageStream.Read(buffer, 0, (int) CDIHeaderConverter.HEADER_SIZE);
      cdiHeader.rawCdiVersion = BitConverter.ToUInt32(buffer, 0);
      cdiHeader.cdiVersion = cdiHeader.rawCdiVersion == 2147483652U || cdiHeader.rawCdiVersion == 2147483653U || cdiHeader.rawCdiVersion == 2147483654U ? (CDIVersion) cdiHeader.rawCdiVersion : CDIVersion.CDI_VERSION_UNKNOWN;
      cdiHeader.headerOffset = BitConverter.ToUInt32(buffer, 4);
      if (cdiHeader.cdiVersion > CDIVersion.CDI_VERSION_3)
        cdiHeader.headerOffset = (uint) imageStream.Length - cdiHeader.headerOffset;
      return cdiHeader;
    }
  }
}
