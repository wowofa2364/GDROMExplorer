﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.ImageFileFormat.CDI.CDIHeader
// Assembly: CDI, Version=1.0.1.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B71D3BB5-2FC4-43C2-853E-907E3A458120
// Assembly location: Formats\CDI.dll

namespace GDRomExplorer.ImageFileFormat.CDI
{
  internal class CDIHeader
  {
    internal CDIVersion cdiVersion;
    internal uint rawCdiVersion;
    internal uint headerOffset;
  }
}
