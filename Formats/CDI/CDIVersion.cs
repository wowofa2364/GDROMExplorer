﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.ImageFileFormat.CDI.CDIVersion
// Assembly: CDI, Version=1.0.1.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: B71D3BB5-2FC4-43C2-853E-907E3A458120
// Assembly location: Formats\CDI.dll

namespace GDRomExplorer.ImageFileFormat.CDI
{
  internal enum CDIVersion : uint
  {
    CDI_VERSION_UNKNOWN = 0,
    CDI_VERSION_2 = 2147483652, // 0x80000004
    CDI_VERSION_3 = 2147483653, // 0x80000005
    CDI_VERSION_35 = 2147483654, // 0x80000006
  }
}
