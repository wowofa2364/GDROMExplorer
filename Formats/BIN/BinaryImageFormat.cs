﻿// Decompiled with JetBrains decompiler
// Type: GDRomExplorer.ImageFileFormat.BIN.BinaryImageFormat
// Assembly: BIN, Version=1.0.0.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: 6DE81A29-0420-4189-8D61-5DDF40D77BD6
// Assembly location: C:\Games\gdrom\Formats\BIN.dll

using ImageReader.DiscSectors;
using SEGATools.DiscFileSystem;
using SEGATools.FileFormat;

namespace GDRomExplorer.ImageFileFormat.BIN
{
  internal class BinaryImageFormat : AbstractImageFileFormat
  {
    public override string[] FileExtentions => new string[1]
    {
      ".bin"
    };

    public override string[] FileExtentionDescriptions => new string[1]
    {
      "Binary image file"
    };

    public override IDiscFileSystemConverter ImageFileConverter => (IDiscFileSystemConverter) new GenericImageConverter((IDiscSector) new CDROMMode1RawSector());
  }
}
