﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("GD-ROM Explorer BIN File Format")]
[assembly: AssemblyDescription("BIN File Format Plugin")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Japanese Cake")]
[assembly: AssemblyProduct("GD-ROM Explorer - BIN File Format Plugin")]
[assembly: AssemblyCopyright("2014 - 2016")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(true)]
[assembly: Guid("47b79a6e-d9b7-483d-8013-ffcdab185e81")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyVersion("1.0.0.0")]
