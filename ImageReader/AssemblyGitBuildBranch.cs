﻿// Decompiled with JetBrains decompiler
// Type: ImageReader.AssemblyGitBuildBranch
// Assembly: ImageReader, Version=1.5.2.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: E0717604-B50B-4CB4-B85C-C17F43D5C04B
// Assembly location: ImageReader.dll

using System;

namespace ImageReader
{
  [AttributeUsage(AttributeTargets.Assembly)]
  public class AssemblyGitBuildBranch : Attribute
  {
    public string GitBuildBranch { get; private set; }

    public AssemblyGitBuildBranch(string txt) => this.GitBuildBranch = txt;
  }
}
