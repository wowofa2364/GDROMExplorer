﻿// Decompiled with JetBrains decompiler
// Type: ImageReader.ImageReader.DiscImageReader
// Assembly: ImageReader, Version=1.5.2.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: E0717604-B50B-4CB4-B85C-C17F43D5C04B
// Assembly location: ImageReader.dll

using ImageReader.DiscSectors;
using ImageReader.ISO9660.DirectoryRecords;
using ImageReader.ISO9660.VolumeDescriptors;
using ImageReader.Stream;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace ImageReader.ImageReader
{
  public class DiscImageReader : IDisposable
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private bool disposed;
    private IDiscSector discSector;
    private System.IO.Stream image;
    private uint numberOfFiles;
    private Dictionary<string, DirectoryRecord> pathTable;
    private uint preGap;
    private PrimaryVolumeDescriptor primaryVolumeDescriptor;
    private DiscImageReaderStatus status;
    private uint totalFileSize;

    public uint NumberOfFiles
    {
      get
      {
        this.ValidateStatus(DiscImageReaderStatus.OPENED);
        return this.numberOfFiles;
      }
    }

    public List<string> Paths
    {
      get
      {
        this.ValidateStatus(DiscImageReaderStatus.OPENED);
        return new List<string>((IEnumerable<string>) this.pathTable.Keys);
      }
    }

    public uint TotalFileSize
    {
      get
      {
        this.ValidateStatus(DiscImageReaderStatus.OPENED);
        return this.totalFileSize;
      }
    }

    public DiscImageReaderStatus Status => this.status;

    public PrimaryVolumeDescriptor PrimaryVolumeDescriptor
    {
      get
      {
        this.ValidateStatus(DiscImageReaderStatus.OPENED);
        return this.primaryVolumeDescriptor;
      }
    }

    public DirectoryRecord RootDirectoryRecord
    {
      get
      {
        this.ValidateStatus(DiscImageReaderStatus.OPENED);
        return this.primaryVolumeDescriptor.RootDirectoryRecord;
      }
    }

    public bool ParsePathTable { get; set; }

    public DiscImageReader()
    {
      this.disposed = false;
      this.ParsePathTable = false;
      this.status = DiscImageReaderStatus.CLOSED;
    }

    public void Open(System.IO.Stream imageStream, uint imagePreGap, IDiscSector discSector)
    {
      this.ValidateStatus(DiscImageReaderStatus.CLOSED);
      this.discSector = discSector;
      this.image = this.ValidateImage(imageStream);
      this.preGap = this.ValidatePreGap(imagePreGap);
      this.ReadImageContent();
    }

    public void Close()
    {
      if (this.image != null)
      {
        this.image.Dispose();
        this.image = (System.IO.Stream) null;
      }
      this.discSector = (IDiscSector) null;
      this.pathTable = (Dictionary<string, DirectoryRecord>) null;
      this.primaryVolumeDescriptor = (PrimaryVolumeDescriptor) null;
      this.numberOfFiles = 0U;
      this.totalFileSize = 0U;
      this.status = DiscImageReaderStatus.CLOSED;
    }

    public bool PathExist(string Path)
    {
      this.ValidateStatus(DiscImageReaderStatus.OPENED);
      return this.GetDirectoryRecord(Path) != null;
    }

    public DirectoryRecord GetDirectoryRecord(string path)
    {
      this.ValidateStatus(DiscImageReaderStatus.OPENED);
      if (string.IsNullOrEmpty(path))
        return (DirectoryRecord) null;
      if (!Path.IsPathRooted(path))
        throw new NotSupportedException("relative path are not supported");
      if (DirectoryRecord.ROOT_DIRECTORY_NAME.Equals(path))
        return this.primaryVolumeDescriptor.RootDirectoryRecord;
      if (this.pathTable.ContainsKey(path))
        return this.pathTable[path];
      string directoryName = Path.GetDirectoryName(path);
      if (!this.pathTable.ContainsKey(directoryName))
        return (DirectoryRecord) null;
      string FileName = Path.GetFileName(path);
      return this.pathTable[directoryName].children.Find((Predicate<DirectoryRecord>) (directoryRecord => directoryRecord.Name.Equals(FileName)));
    }

    private System.IO.Stream ValidateImage(System.IO.Stream imageStream)
    {
      if (imageStream == null || !imageStream.CanRead || !imageStream.CanSeek)
        throw new DiscImageReaderException(string.Format("invalid image stream: {0}", (object) imageStream));
      if (imageStream.Length < (long) ((int) VolumeDescriptor.VolumeDescriptorStartLba * this.discSector.Size + (int) VolumeDescriptor.VolumeDescriptorSize))
        throw new DiscImageReaderException(string.Format("image stream too small: {0}", (object) imageStream));
      return imageStream;
    }

    private uint ValidatePreGap(uint preGap) => preGap >= 0U ? preGap : throw new ArgumentOutOfRangeException(nameof (preGap), "must be positive");

    private void ValidateStatus(DiscImageReaderStatus expectedStatus)
    {
      if (this.status != expectedStatus)
        throw new ObjectDisposedException(this.GetType().FullName);
    }

    private void ReadImageContent()
    {
      try
      {
        this.primaryVolumeDescriptor = this.ReadPrimaryVolumeDescriptor();
        this.numberOfFiles = 0U;
        this.totalFileSize = 0U;
        Dictionary<string, List<DirectoryRecord>> directories = this.ReadDirectories(this.primaryVolumeDescriptor, this.pathTable);
        DiscImageReader.logger.DebugFormat("{0} {1} found in the file table", (object) this.numberOfFiles, this.numberOfFiles > 1U ? (object) "files" : (object) "file");
        this.pathTable = !this.ParsePathTable ? this.ComputePathTable(directories) : this.ReadPathTable(this.primaryVolumeDescriptor);
        DiscImageReader.logger.DebugFormat("{0} {1} found in the path table", (object) this.pathTable.Count, this.pathTable.Count > 1 ? (object) "paths" : (object) "path");
        this.status = DiscImageReaderStatus.OPENED;
        DiscImageReader.logger.Debug((object) "Image content successfully read");
      }
      catch (Exception ex)
      {
        DiscImageReader.logger.DebugFormat("Unable to read the image content: {0}", (object) ex);
        this.Close();
        throw new DiscImageReaderException("the image content is not valid", ex);
      }
    }

    private PrimaryVolumeDescriptor ReadPrimaryVolumeDescriptor()
    {
      byte[] buffer = new byte[(int) VolumeDescriptor.VolumeDescriptorSize];
      using (DiscSectorStream discSectorStream = new DiscSectorStream(this.image, this.discSector, (uint) VolumeDescriptor.VolumeDescriptorStartLba, (uint) VolumeDescriptor.VolumeDescriptorSize, false))
      {
        discSectorStream.Read(buffer, 0, buffer.Length);
        GCHandle gcHandle = GCHandle.Alloc((object) buffer, GCHandleType.Pinned);
        VolumeDescriptor structure = (VolumeDescriptor) Marshal.PtrToStructure(gcHandle.AddrOfPinnedObject(), typeof (VolumeDescriptor));
        gcHandle.Free();
        string str;
        try
        {
          str = Encoding.ASCII.GetString(structure.StandardIdentifier);
        }
        catch (DecoderFallbackException ex)
        {
          logger.Error("wrong standard identifer");
          logger.Error(ex);
          throw new DiscImageReaderException("wrong standard identifer");
        }
        if ("CD001".Equals(str) && (int) VolumeDescriptor.VolumeDescriptorVersion == (int) structure.Version)
        {
          if (structure.Type == VolumeDescriptorType.PRIMARY_VOLUME_DESCRIPTOR)
            goto label_10;
        }
        throw new DiscImageReaderException("primary volume descriptor not found");
      }
label_10:
      PrimaryVolumeDescriptor volumeDescriptor = VolumeDescriptorConverter.ToPrimaryVolumeDescriptor(buffer, 0);
      if (volumeDescriptor.RootDirectoryRecord.Extent < this.preGap)
        throw new DiscImageReaderException("the root directory record extent must be positive");
      return volumeDescriptor;
    }

    private Dictionary<string, DirectoryRecord> ReadPathTable(
      PrimaryVolumeDescriptor PrimVolDesc)
    {
      int index = 0;
      string empty = string.Empty;
      Dictionary<string, DirectoryRecord> dictionary = new Dictionary<string, DirectoryRecord>();
      List<string> stringList = new List<string>();
      int pathTableLocation = (int) PrimVolDesc.PathTableLocation;
      int logicalBlockSize = (int) PrimVolDesc.LogicalBlockSize;
      byte[] numArray = new byte[(int) PrimVolDesc.PathTableSize];
      new DiscSectorStream(this.image, this.discSector, PrimVolDesc.PathTableLocation - this.preGap, PrimVolDesc.PathTableSize, false).Read(numArray, 0, numArray.Length);
      while ((long) index < (long) PrimVolDesc.PathTableSize)
      {
        int count = (int) numArray[index];
        ushort num = 0;
        if (count > 0)
        {
          uint uint32 = BitConverter.ToUInt32(numArray, index + 2);
          num = BitConverter.ToUInt16(numArray, index + 6);
          string rootDirectoryName = Encoding.Default.GetString(numArray, index + 8, count);
          if (!string.IsNullOrEmpty(rootDirectoryName))
          {
            if ((int) DirectoryRecord.ROOT_OR_SELF_DIRECTORY_FILE_IDENTIFIER == (int) (byte) rootDirectoryName[0])
              rootDirectoryName = DirectoryRecord.ROOT_DIRECTORY_NAME;
            string key = Path.Combine(num <= (ushort) 1 ? DirectoryRecord.ROOT_DIRECTORY_NAME : stringList[(int) num - 1], rootDirectoryName);
            dictionary.Add(key, (DirectoryRecord) null);
            stringList.Add(key);
            DiscImageReader.logger.DebugFormat("Path {0} located at LBA {1}", (object) key, (object) uint32);
          }
        }
        index += count + 8;
        if (count % 2 > 0)
          ++index;
        if (count == 0)
          ++index;
        if (num == (ushort) 0)
          ++index;
      }
      if (dictionary.Count == 0)
        dictionary.Add(DirectoryRecord.ROOT_DIRECTORY_NAME, (DirectoryRecord) null);
      return dictionary;
    }

    private Dictionary<string, List<DirectoryRecord>> ReadDirectories(
      PrimaryVolumeDescriptor pvd,
      Dictionary<string, DirectoryRecord> pathTable)
    {
      Dictionary<string, List<DirectoryRecord>> directories = new Dictionary<string, List<DirectoryRecord>>();
      this.ReadDirectoriesRec(this.primaryVolumeDescriptor, this.primaryVolumeDescriptor.RootDirectoryRecord.Name, pvd.RootDirectoryRecord, directories);
      return directories;
    }

    private void ReadDirectoriesRec(
      PrimaryVolumeDescriptor Pvd,
      string parentPath,
      DirectoryRecord directoryRecord,
      Dictionary<string, List<DirectoryRecord>> directories)
    {
      byte[] buffer = new byte[(int) directoryRecord.ExtentSize];
      List<DirectoryRecord> directoryRecordList = new List<DirectoryRecord>();
      new DiscSectorStream(this.image, this.discSector, directoryRecord.Extent - this.preGap, directoryRecord.ExtentSize, false).Read(buffer, 0, (int) directoryRecord.ExtentSize);
      int startIndex = 0;
      while ((long) startIndex < (long) directoryRecord.ExtentSize)
      {
        ushort num = (ushort) buffer[startIndex];
        if (num > (ushort) 0)
        {
          DirectoryRecord directoryRecord1 = DirectoryRecordConverter.ToDirectoryRecord(buffer, startIndex);
          if (!DirectoryRecord.SELF_DIRECTORY_NAME.Equals(directoryRecord1.Name) && !DirectoryRecord.PARENT_DIRECTORY_NAME.Equals(directoryRecord1.Name))
          {
            if (directoryRecord1.IsDirectory)
            {
              string key = Path.Combine(parentPath, directoryRecord1.Name);
              directoryRecordList.Add(directoryRecord1);
              if (!directories.ContainsKey(key))
                directories.Add(key, new List<DirectoryRecord>());
            }
            else
            {
              this.totalFileSize += directoryRecord1.ExtentSize;
              ++this.numberOfFiles;
            }
            if (!directories.ContainsKey(parentPath))
              directories.Add(parentPath, new List<DirectoryRecord>());
            directories[parentPath].Add(directoryRecord1);
            directoryRecord1.parent = directoryRecord;
            directoryRecord.children.Add(directoryRecord1);
          }
          startIndex += (int) num;
        }
        else
          ++startIndex;
      }
      foreach (DirectoryRecord directoryRecord1 in directoryRecordList)
      {
        string parentPath1 = Path.Combine(parentPath, directoryRecord1.Name);
        this.ReadDirectoriesRec(Pvd, parentPath1, directoryRecord1, directories);
      }
    }

    private Dictionary<string, DirectoryRecord> ComputePathTable(
      Dictionary<string, List<DirectoryRecord>> directories)
    {
      Dictionary<string, DirectoryRecord> dictionary = new Dictionary<string, DirectoryRecord>();
      foreach (KeyValuePair<string, List<DirectoryRecord>> directory in directories)
      {
        foreach (DirectoryRecord directoryRecord in directory.Value.FindAll((Predicate<DirectoryRecord>) (dr => dr.IsDirectory)))
        {
          if (!dictionary.Keys.Contains<string>(directoryRecord.ParentDirectory.FullPath))
            dictionary.Add(directoryRecord.ParentDirectory.FullPath, directoryRecord.ParentDirectory);
          if (!dictionary.Keys.Contains<string>(directoryRecord.FullPath))
            dictionary.Add(directoryRecord.FullPath, directoryRecord);
        }
      }
      return dictionary;
    }

    public virtual void Dispose()
    {
      this.Dispose(true);
      GC.SuppressFinalize((object) this);
    }

    protected virtual void Dispose(bool disposing)
    {
      if (this.disposed)
        return;
      if (disposing)
        this.Close();
      this.disposed = true;
    }
  }
}
