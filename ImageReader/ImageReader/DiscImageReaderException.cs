﻿// Decompiled with JetBrains decompiler
// Type: ImageReader.ImageReader.DiscImageReaderException
// Assembly: ImageReader, Version=1.5.2.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: E0717604-B50B-4CB4-B85C-C17F43D5C04B
// Assembly location: ImageReader.dll

using System;

namespace ImageReader.ImageReader
{
  public class DiscImageReaderException : Exception
  {
    public DiscImageReaderException(string message)
      : base(string.Format("ISOReader Error: {0}", (object) message))
    {
    }

    public DiscImageReaderException(string message, Exception innerException)
      : base(string.Format("ISOReader Error: {0}", (object) message), innerException)
    {
    }
  }
}
