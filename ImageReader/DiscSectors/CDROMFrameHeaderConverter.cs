﻿// Decompiled with JetBrains decompiler
// Type: ImageReader.DiscSectors.CDROMFrameHeaderConverter
// Assembly: ImageReader, Version=1.5.2.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: E0717604-B50B-4CB4-B85C-C17F43D5C04B
// Assembly location: ImageReader.dll

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace ImageReader.DiscSectors
{
  public class CDROMFrameHeaderConverter
  {
    public static readonly ushort CDROM_FRAME_HEADER_SIZE = 16;
    private static readonly byte[] CDROM_FRAME_HEADER_SYNC = new byte[12]
    {
      (byte) 0,
      byte.MaxValue,
      byte.MaxValue,
      byte.MaxValue,
      byte.MaxValue,
      byte.MaxValue,
      byte.MaxValue,
      byte.MaxValue,
      byte.MaxValue,
      byte.MaxValue,
      byte.MaxValue,
      (byte) 0
    };

    public static CDROMDataModeType ToCDROMFrameHeader(
      byte[] buffer,
      int startIndex)
    {
      if (buffer.Length < startIndex + (int) CDROMFrameHeaderConverter.CDROM_FRAME_HEADER_SIZE)
        throw new ArgumentOutOfRangeException();
      GCHandle gcHandle = GCHandle.Alloc((object) buffer, GCHandleType.Pinned);
      CDROMFrameHeaderConverter.CDROMFrameHeader structure = (CDROMFrameHeaderConverter.CDROMFrameHeader) Marshal.PtrToStructure(gcHandle.AddrOfPinnedObject(), typeof (CDROMFrameHeaderConverter.CDROMFrameHeader));
      gcHandle.Free();
      if (((IEnumerable<byte>) CDROMFrameHeaderConverter.CDROM_FRAME_HEADER_SYNC).Where<byte>((Func<byte, int, bool>) ((t, i) => (int) t != (int) buffer[i])).Any<byte>())
        throw new FormatException();
      return Enum.IsDefined(typeof (CDROMDataModeType), (object) structure.Mode) ? structure.Mode : throw new FormatException();
    }

    [StructLayout(LayoutKind.Explicit, Size = 16)]
    private struct CDROMFrameHeader
    {
      [FieldOffset(0)]
      public byte Sync;
      [FieldOffset(12)]
      public byte Address;
      [FieldOffset(15)]
      public CDROMDataModeType Mode;
    }
  }
}
