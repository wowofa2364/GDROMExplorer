﻿// Decompiled with JetBrains decompiler
// Type: ImageReader.DiscSectors.CDROMXAMode2Form1Sector
// Assembly: ImageReader, Version=1.5.2.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: E0717604-B50B-4CB4-B85C-C17F43D5C04B
// Assembly location: ImageReader.dll

namespace ImageReader.DiscSectors
{
  public class CDROMXAMode2Form1Sector : DiscSectorBase
  {
    public override int Size => 2336;

    public override int DataOffset => 8;

    public override int DataLength => 2048;
  }
}
