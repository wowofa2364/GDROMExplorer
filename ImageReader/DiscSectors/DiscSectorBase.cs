﻿// Decompiled with JetBrains decompiler
// Type: ImageReader.DiscSectors.DiscSectorBase
// Assembly: ImageReader, Version=1.5.2.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: E0717604-B50B-4CB4-B85C-C17F43D5C04B
// Assembly location: ImageReader.dll

using System;

namespace ImageReader.DiscSectors
{
  public abstract class DiscSectorBase : IDiscSector
  {
    public override bool Equals(object obj)
    {
      if (obj == null)
        return false;
      if (this == obj)
        return true;
      if (!(obj is IDiscSector))
        return false;
      IDiscSector discSector = obj as IDiscSector;
      return this.Size == discSector.Size && this.DataOffset == discSector.DataOffset && this.DataLength == discSector.DataLength;
    }

    public override int GetHashCode() => ((17 * 23 + this.Size) * 23 + this.DataOffset) * 23 + this.DataLength;

    public virtual int Size => throw new NotImplementedException();

    public virtual int DataOffset => throw new NotImplementedException();

    public virtual int DataLength => throw new NotImplementedException();
  }
}
