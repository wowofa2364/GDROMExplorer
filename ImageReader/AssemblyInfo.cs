﻿using ImageReader;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyGitBuildBranch("release_1.6.3")]
[assembly: AssemblyFileVersion("1.5.2")]
[assembly: AssemblyGitBuildSHA1("fd2eb45")]
[assembly: AssemblyTitle("Image Reader")]
[assembly: AssemblyDescription("ImageReader.dll")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Japanese Cake")]
[assembly: AssemblyProduct("Image Reader")]
[assembly: AssemblyCopyright("2012-2016")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(true)]
[assembly: Guid("d23954c0-4b76-497a-bcac-66de624f695f")]
[assembly: AssemblyVersion("1.5.2.0")]