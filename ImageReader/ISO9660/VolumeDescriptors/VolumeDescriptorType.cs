﻿// Decompiled with JetBrains decompiler
// Type: ImageReader.ISO9660.VolumeDescriptors.VolumeDescriptorType
// Assembly: ImageReader, Version=1.5.2.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: E0717604-B50B-4CB4-B85C-C17F43D5C04B
// Assembly location: ImageReader.dll

namespace ImageReader.ISO9660.VolumeDescriptors
{
  public enum VolumeDescriptorType : byte
  {
    BOOT_RECORD = 0,
    PRIMARY_VOLUME_DESCRIPTOR = 1,
    SUPPLEMENTARY_VOLUME_DESCRIPTOR = 2,
    VOLUME_PARTITION_DESCRIPTOR = 3,
    VOLUME_DESCRIPTION_SET_TERMINATOR = 255, // 0xFF
  }
}
