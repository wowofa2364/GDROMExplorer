﻿// Decompiled with JetBrains decompiler
// Type: ImageReader.ISO9660.VolumeDescriptors.PrimaryVolumeDescriptor
// Assembly: ImageReader, Version=1.5.2.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: E0717604-B50B-4CB4-B85C-C17F43D5C04B
// Assembly location: ImageReader.dll

using ImageReader.ISO9660.DirectoryRecords;
using System;

namespace ImageReader.ISO9660.VolumeDescriptors
{
  public class PrimaryVolumeDescriptor
  {
    internal uint volumeSpaceSizeTypeL;
    internal uint volumeSpaceSizeTypeM;
    internal ushort volumeSetSizeTypeL;
    internal ushort VolumeSetSizeTypeM;
    internal ushort volumeSequenceNumberTypeL;
    internal ushort volumeSequenceNumberTypeM;
    internal ushort logicalBlockSizeTypeL;
    internal ushort logicalBlockSizeTypeM;
    internal uint pathTableSizeTypeL;
    internal uint pathTableSizeTypeM;
    internal uint pathTableLocationTypeL;
    internal uint optionalPathTableLocationTypeL;
    internal uint pathTableLocationTypeM;
    internal uint optionalPathTableLocationTypeM;
    internal DirectoryRecord rootDirectoryRecord;

    public VolumeDescriptorType Type { get; internal set; }

    public string StandardIdentifier { get; internal set; }

    public sbyte Version { get; internal set; }

    public string SystemIdentifier { get; internal set; }

    public string Identifier { get; internal set; }

    public uint SpaceSize => !BitConverter.IsLittleEndian ? this.volumeSpaceSizeTypeM : this.volumeSpaceSizeTypeL;

    public ushort SetSize => !BitConverter.IsLittleEndian ? this.VolumeSetSizeTypeM : this.volumeSetSizeTypeL;

    public ushort SequenceNumber => !BitConverter.IsLittleEndian ? this.volumeSequenceNumberTypeM : this.volumeSequenceNumberTypeL;

    public ushort LogicalBlockSize => !BitConverter.IsLittleEndian ? this.logicalBlockSizeTypeM : this.logicalBlockSizeTypeL;

    public uint PathTableSize => !BitConverter.IsLittleEndian ? this.pathTableSizeTypeM : this.pathTableSizeTypeL;

    public uint PathTableLocation => !BitConverter.IsLittleEndian ? this.pathTableLocationTypeM : this.pathTableLocationTypeL;

    public bool HasOptionalPathTable => (BitConverter.IsLittleEndian ? (int) this.optionalPathTableLocationTypeL : (int) this.optionalPathTableLocationTypeM) != 0;

    public uint OptionalPathTableLocation => !BitConverter.IsLittleEndian ? this.optionalPathTableLocationTypeM : this.optionalPathTableLocationTypeL;

    public DirectoryRecord RootDirectoryRecord => this.rootDirectoryRecord;

    public string SetIdentifier { get; internal set; }

    public string PublisherIdentifier { get; internal set; }

    public string PreparerIdentifier { get; internal set; }

    public string ApplicationIdentifier { get; internal set; }

    public string CopyrightFileIdentifier { get; internal set; }

    public string AbstractFileIdentifier { get; internal set; }

    public string BibliographicFileIdentifier { get; internal set; }

    public DateTime? CreationDateTime { get; internal set; }

    public DateTime? ModificationDateTime { get; internal set; }

    public DateTime? ExpirationDateTime { get; internal set; }

    public DateTime? EffectiveDateTime { get; internal set; }

    public sbyte FileStructureVersion { get; internal set; }
  }
}
