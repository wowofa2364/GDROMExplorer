﻿// Decompiled with JetBrains decompiler
// Type: ImageReader.ISO9660.VolumeDescriptors.VolumeDescriptor
// Assembly: ImageReader, Version=1.5.2.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: E0717604-B50B-4CB4-B85C-C17F43D5C04B
// Assembly location: ImageReader.dll

using System.Runtime.InteropServices;

namespace ImageReader.ISO9660.VolumeDescriptors
{
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public struct VolumeDescriptor
  {
    public const string VolumeDescriptorIdentifier = "CD001";
    public static readonly ushort VolumeDescriptorStartLba = 16;
    public static readonly ushort VolumeDescriptorSize = 2048;
    public static readonly byte VolumeDescriptorVersion = 1;
    public VolumeDescriptorType Type;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
    public byte[] StandardIdentifier;
    public byte Version;
  }
}
