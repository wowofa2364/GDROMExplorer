﻿// Decompiled with JetBrains decompiler
// Type: ImageReader.ISO9660.PathTable.PathTableEntry
// Assembly: ImageReader, Version=1.5.2.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: E0717604-B50B-4CB4-B85C-C17F43D5C04B
// Assembly location: ImageReader.dll

namespace ImageReader.ISO9660.PathTable
{
  public class PathTableEntry
  {
    private byte extentAttributeRecordLength;
    private uint extent;
    private ushort parentDirectoryIndex;
    private string directoryIdentifier;

    public byte ExtentAttributeRecordLength
    {
      get => this.extentAttributeRecordLength;
      internal set => this.extentAttributeRecordLength = value;
    }

    public uint Extent
    {
      get => this.extent;
      internal set => this.extent = value;
    }

    public ushort ParentDirectoryIndex
    {
      get => this.parentDirectoryIndex;
      internal set => this.parentDirectoryIndex = value;
    }

    public string DirectoryIdentifier
    {
      get => this.directoryIdentifier;
      internal set => this.directoryIdentifier = value;
    }
  }
}
