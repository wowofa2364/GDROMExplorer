﻿// Decompiled with JetBrains decompiler
// Type: ImageReader.ISO9660.DirectoryRecords.DirectoryRecord
// Assembly: ImageReader, Version=1.5.2.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: E0717604-B50B-4CB4-B85C-C17F43D5C04B
// Assembly location: ImageReader.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ImageReader.ISO9660.DirectoryRecords
{
  public class DirectoryRecord
  {
    public static readonly char DIRECTORY_SEPARATOR_CHAR = Path.DirectorySeparatorChar;
    public static readonly string DIRECTORY_SEPARATOR = DirectoryRecord.DIRECTORY_SEPARATOR_CHAR.ToString();
    public static readonly string ROOT_DIRECTORY_NAME = DirectoryRecord.DIRECTORY_SEPARATOR_CHAR.ToString();
    public static readonly string SELF_DIRECTORY_NAME = ".";
    public static readonly string PARENT_DIRECTORY_NAME = "..";
    internal static readonly byte ROOT_OR_SELF_DIRECTORY_FILE_IDENTIFIER = 0;
    internal static readonly byte PARENT_DIRECTORY_FILE_IDENTIFIER = 1;
    internal ushort extendAttributeLength;
    internal uint extentTypeL;
    internal uint extentTypeM;
    internal uint extentSizeTypeL;
    internal uint extentSizeTypeM;
    internal ushort fileUnitSize;
    internal ushort fileGapSize;
    internal ushort volumeSequenceNumberTypeL;
    internal ushort volumeSequenceNumberTypeM;
    internal DirectoryRecord parent;
    internal List<DirectoryRecord> children;

    public static string FindCommonPathPrefix(List<DirectoryRecord> directoryRecords)
    {
      if (directoryRecords.Count < 1)
        return (string) null;
      Dictionary<DirectoryRecord, string> source = new Dictionary<DirectoryRecord, string>();
      foreach (DirectoryRecord directoryRecord in directoryRecords)
        source[directoryRecord] = directoryRecord.IsDirectory ? directoryRecord.FullPath : Path.GetDirectoryName(directoryRecord.FullPath);
      int minDepth = source.Min<KeyValuePair<DirectoryRecord, string>>((Func<KeyValuePair<DirectoryRecord, string>, int>) (pkv => pkv.Value.Split(new char[1]
      {
        DirectoryRecord.DIRECTORY_SEPARATOR_CHAR
      }, StringSplitOptions.RemoveEmptyEntries).Length));
      string pathPrefix = source.First<KeyValuePair<DirectoryRecord, string>>((Func<KeyValuePair<DirectoryRecord, string>, bool>) (pkv => pkv.Value.Split(new char[1]
      {
        DirectoryRecord.DIRECTORY_SEPARATOR_CHAR
      }, StringSplitOptions.RemoveEmptyEntries).Length == minDepth)).Value;
      for (int index = 0; index < minDepth && !directoryRecords.TrueForAll((Predicate<DirectoryRecord>) (dr => dr.FullPath.StartsWith(pathPrefix))); ++index)
        pathPrefix = Path.GetDirectoryName(pathPrefix);
      if (directoryRecords.Count == 1 && directoryRecords[0].IsDirectory && !directoryRecords[0].IsRoot)
        pathPrefix = Path.GetDirectoryName(pathPrefix);
      return pathPrefix;
    }

    internal DirectoryRecord() => this.children = new List<DirectoryRecord>();

    public byte RecordLength { get; internal set; }

    public string Name { get; internal set; }

    public uint Extent => !BitConverter.IsLittleEndian ? this.extentTypeM : this.extentTypeL;

    public uint ExtentSize => !BitConverter.IsLittleEndian ? this.extentSizeTypeM : this.extentSizeTypeL;

    public DateTime? RecordingDateTime { get; internal set; }

    public DirectoryRecordFlags Flags { get; internal set; }

    public bool HasValidFileIdentifier { get; internal set; }

    public ushort VolumeSequenceNumber => !BitConverter.IsLittleEndian ? this.volumeSequenceNumberTypeM : this.volumeSequenceNumberTypeL;

    public int Depth
    {
      get
      {
        if (this.IsRoot)
          return 0;
        return this.FullPath.Split(new char[1]
        {
          Path.DirectorySeparatorChar
        }, StringSplitOptions.RemoveEmptyEntries).Length;
      }
    }

    public bool IsHidden => (this.Flags & DirectoryRecordFlags.HIDDEN_FILE) != DirectoryRecordFlags.NONE;

    public bool IsDirectory => (this.Flags & DirectoryRecordFlags.DIRECTORY) != DirectoryRecordFlags.NONE;

    public bool IsAssociated => (this.Flags & DirectoryRecordFlags.ASSOCIATED_FILE) != DirectoryRecordFlags.NONE;

    public bool HasExtentedAttributeRecord => (this.Flags & DirectoryRecordFlags.EXTENDED_ATTRIBUTE_RECORD) != DirectoryRecordFlags.NONE;

    public bool HasOwnerAndGroupPermissions => (this.Flags & DirectoryRecordFlags.OWNER_AND_GROUP_PERMISSIONS) != DirectoryRecordFlags.NONE;

    public bool HasReservedFlags => (this.Flags & DirectoryRecordFlags.RESERVED_FLAGS) != DirectoryRecordFlags.NONE;

    public bool IsNotFinalRecord => (this.Flags & DirectoryRecordFlags.NOT_FINAL_RECORD) != DirectoryRecordFlags.NONE;

    public bool IsFlagSet(DirectoryRecordFlags flags) => (this.Flags & flags) != DirectoryRecordFlags.NONE;

    public bool IsRoot => this.parent == null;

    public DirectoryRecord RootDirectory => this.IsRoot ? this : this.parent.RootDirectory;

    public DirectoryRecord ParentDirectory => this.parent;

    public List<DirectoryRecord> SubDirectories => this.children;

    public List<DirectoryRecord> GetAllSubFolder()
    {
      List<DirectoryRecord> list = new List<DirectoryRecord>();
      this.GetAllSubDirectoriesRec(list, true, false);
      return list;
    }

    public List<DirectoryRecord> GetAllSubFiles()
    {
      List<DirectoryRecord> list = new List<DirectoryRecord>();
      this.GetAllSubDirectoriesRec(list, false, true);
      return list;
    }

    public List<DirectoryRecord> GetAllSubDirectories()
    {
      List<DirectoryRecord> list = new List<DirectoryRecord>();
      this.GetAllSubDirectoriesRec(list, true, true);
      return list;
    }

    private void GetAllSubDirectoriesRec(
      List<DirectoryRecord> list,
      bool includeFolders,
      bool includeFiles)
    {
      foreach (DirectoryRecord child in this.children)
      {
        if (includeFolders && child.IsDirectory)
          list.Add(child);
        if (includeFiles && !child.IsDirectory)
          list.Add(child);
        child.GetAllSubDirectoriesRec(list, includeFolders, includeFiles);
      }
    }

    public bool Contains(string directoryRecordName) => this.children.Find((Predicate<DirectoryRecord>) (directoryRecord => directoryRecord.Name.Equals(directoryRecordName))) != null;

    public DirectoryRecord Find(DirectoryRecord directoryRecord)
    {
      if (directoryRecord == this)
        return this;
      DirectoryRecord directoryRecord1 = (DirectoryRecord) null;
      if (directoryRecord.FullPath.StartsWith(this.FullPath))
      {
        foreach (DirectoryRecord child in this.children)
        {
          directoryRecord1 = child.Find(directoryRecord);
          if (directoryRecord1 != null)
            break;
        }
      }
      return directoryRecord1;
    }

    public DirectoryRecord Find(string path)
    {
      if (this.FullPath.Equals(path))
        return this;
      DirectoryRecord directoryRecord = (DirectoryRecord) null;
      if (path.StartsWith(this.FullPath))
      {
        foreach (DirectoryRecord child in this.children)
        {
          directoryRecord = child.Find(path);
          if (directoryRecord != null)
            break;
        }
      }
      return directoryRecord;
    }

    public string FullPath => this.parent != null ? Path.Combine(this.parent.FullPath, this.Name) : this.Name;

    public uint UsedSpace
    {
      get
      {
        if (!this.IsDirectory)
          return this.ExtentSize;
        uint num = 0;
        foreach (DirectoryRecord child in this.children)
          num += child.UsedSpace;
        return num;
      }
    }
  }
}
