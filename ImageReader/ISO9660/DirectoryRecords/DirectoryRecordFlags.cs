﻿// Decompiled with JetBrains decompiler
// Type: ImageReader.ISO9660.DirectoryRecords.DirectoryRecordFlags
// Assembly: ImageReader, Version=1.5.2.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: E0717604-B50B-4CB4-B85C-C17F43D5C04B
// Assembly location: ImageReader.dll

using System;

namespace ImageReader.ISO9660.DirectoryRecords
{
  [Flags]
  public enum DirectoryRecordFlags : byte
  {
    NONE = 0,
    HIDDEN_FILE = 1,
    DIRECTORY = 2,
    ASSOCIATED_FILE = 4,
    EXTENDED_ATTRIBUTE_RECORD = 8,
    OWNER_AND_GROUP_PERMISSIONS = EXTENDED_ATTRIBUTE_RECORD | ASSOCIATED_FILE | DIRECTORY | HIDDEN_FILE, // 0x0F
    RESERVED_FLAGS = 48, // 0x30
    NOT_FINAL_RECORD = 64, // 0x40
    ANY = 255, // 0xFF
  }
}
