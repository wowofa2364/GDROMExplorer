﻿// Decompiled with JetBrains decompiler
// Type: ImageReader.ISO9660.DirectoryRecords.DirectoryRecordConverter
// Assembly: ImageReader, Version=1.5.2.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: E0717604-B50B-4CB4-B85C-C17F43D5C04B
// Assembly location: ImageReader.dll

using System;
using System.Text;
using System.Text.RegularExpressions;

namespace ImageReader.ISO9660.DirectoryRecords
{
  public class DirectoryRecordConverter
  {
    private static readonly Logger.ILog logger = Logger.CreateLog();
    private static readonly byte DIRECTORY_RECORD_MIN_SIZE = 34;
    private static readonly byte OFFSET_RECORD_LENGTH = 0;
    private static readonly byte OFFSET_EXTENDED_ATTRIBUTE_LENGTH = 1;
    private static readonly byte OFFSET_TYPE_L_EXTENT_LOCATION = 2;
    private static readonly byte OFFSET_TYPE_M_EXTENT_LOCATION = 6;
    private static readonly byte OFFSET_TYPE_L_EXTENT_SIZE = 10;
    private static readonly byte OFFSET_TYPE_M_EXTENT_SIZE = 14;
    private static readonly byte OFFSET_DATETIME = 18;
    private static readonly byte OFFSET_FLAGS = 25;
    private static readonly byte OFFSET_INTERLEAVED_FILE_UNIT_SIZE = 26;
    private static readonly byte OFFSET_INTERLEAVED_FILE_GAP_SIZE = 27;
    private static readonly byte OFFSET_TYPE_L_VOLUME_SEQUENCE_NUMBER = 28;
    private static readonly byte OFFSET_TYPE_M_VOLUME_SEQUENCE_NUMBER = 30;
    private static readonly byte OFFSET_FILE_IDENTIFIER_LENGTH = 32;
    private static readonly byte OFFSET_FILE_IDENTIFIER = 33;
    private static readonly Regex VALID_FILENAME = new Regex("[^A-Z0-9._]+", RegexOptions.IgnoreCase);

    public static DirectoryRecord ToDirectoryRecord(byte[] buffer, int startIndex) => DirectoryRecordConverter.ToDirectoryRecord(buffer, startIndex, false);

    public static DirectoryRecord ToRootDirectoryRecord(
      byte[] buffer,
      int startIndex)
    {
      return DirectoryRecordConverter.ToDirectoryRecord(buffer, startIndex, true);
    }

    private static DirectoryRecord ToDirectoryRecord(
      byte[] buffer,
      int startIndex,
      bool isForRootDirectory)
    {
      byte num1 = buffer[startIndex + (int) DirectoryRecordConverter.OFFSET_RECORD_LENGTH];
      if (startIndex < 0 || buffer.Length - startIndex < (int) num1 || (int) num1 < (int) DirectoryRecordConverter.DIRECTORY_RECORD_MIN_SIZE)
        throw new ArgumentOutOfRangeException();
      DirectoryRecord directoryRecord = new DirectoryRecord();
      directoryRecord.RecordLength = num1;
      directoryRecord.extendAttributeLength = (ushort) buffer[startIndex + (int) DirectoryRecordConverter.OFFSET_EXTENDED_ATTRIBUTE_LENGTH];
      directoryRecord.extentTypeL = BitConverter.ToUInt32(buffer, startIndex + (int) DirectoryRecordConverter.OFFSET_TYPE_L_EXTENT_LOCATION);
      directoryRecord.extentTypeM = BitConverter.ToUInt32(buffer, startIndex + (int) DirectoryRecordConverter.OFFSET_TYPE_M_EXTENT_LOCATION);
      directoryRecord.extentSizeTypeL = BitConverter.ToUInt32(buffer, startIndex + (int) DirectoryRecordConverter.OFFSET_TYPE_L_EXTENT_SIZE);
      directoryRecord.extentSizeTypeM = BitConverter.ToUInt32(buffer, startIndex + (int) DirectoryRecordConverter.OFFSET_TYPE_M_EXTENT_SIZE);
      directoryRecord.RecordingDateTime = DirectoryRecordConverter.DirectoryRecordDateTimeConverter.ToDateTime(buffer, startIndex + (int) DirectoryRecordConverter.OFFSET_DATETIME);
      directoryRecord.Flags = (DirectoryRecordFlags) buffer[startIndex + (int) DirectoryRecordConverter.OFFSET_FLAGS];
      directoryRecord.fileUnitSize = (ushort) buffer[startIndex + (int) DirectoryRecordConverter.OFFSET_INTERLEAVED_FILE_UNIT_SIZE];
      directoryRecord.fileGapSize = (ushort) buffer[startIndex + (int) DirectoryRecordConverter.OFFSET_INTERLEAVED_FILE_GAP_SIZE];
      directoryRecord.volumeSequenceNumberTypeL = BitConverter.ToUInt16(buffer, startIndex + (int) DirectoryRecordConverter.OFFSET_TYPE_L_VOLUME_SEQUENCE_NUMBER);
      directoryRecord.volumeSequenceNumberTypeM = BitConverter.ToUInt16(buffer, startIndex + (int) DirectoryRecordConverter.OFFSET_TYPE_M_VOLUME_SEQUENCE_NUMBER);
      byte num2 = buffer[startIndex + (int) DirectoryRecordConverter.OFFSET_FILE_IDENTIFIER_LENGTH];
      if ((int) buffer[startIndex + (int) DirectoryRecordConverter.OFFSET_FILE_IDENTIFIER] == (int) DirectoryRecord.ROOT_OR_SELF_DIRECTORY_FILE_IDENTIFIER)
        directoryRecord.Name = !isForRootDirectory ? DirectoryRecord.SELF_DIRECTORY_NAME : DirectoryRecord.ROOT_DIRECTORY_NAME;
      else if ((int) buffer[startIndex + (int) DirectoryRecordConverter.OFFSET_FILE_IDENTIFIER] == (int) DirectoryRecord.PARENT_DIRECTORY_FILE_IDENTIFIER)
      {
        directoryRecord.Name = DirectoryRecord.PARENT_DIRECTORY_NAME;
      }
      else
      {
        if (!directoryRecord.IsDirectory)
          num2 -= (byte) 2;
        string input = Encoding.Default.GetString(buffer, startIndex + (int) DirectoryRecordConverter.OFFSET_FILE_IDENTIFIER, (int) num2);
        directoryRecord.Name = DirectoryRecordConverter.VALID_FILENAME.Replace(input, "_invalid_");
        directoryRecord.HasValidFileIdentifier = directoryRecord.Name.Equals(input);
      }
      return directoryRecord;
    }

    private sealed class DirectoryRecordDateTimeConverter
    {
      private static readonly byte DATETIME_SIZE = 7;
      private static readonly byte OFFSET_YEAR = 0;
      private static readonly byte OFFSET_MONTH = 1;
      private static readonly byte OFFSET_DAY = 2;
      private static readonly byte OFFSET_HOUR = 3;
      private static readonly byte OFFSET_MINUTE = 4;
      private static readonly byte OFFSET_SECOND = 5;
      private static readonly byte OFFSET_TIME_ZONE = 6;

      internal static DateTime? ToDateTime(byte[] buffer, int startIndex)
      {
        if (startIndex < 0 || buffer.Length - startIndex < (int) DirectoryRecordConverter.DirectoryRecordDateTimeConverter.DATETIME_SIZE)
          throw new ArgumentOutOfRangeException();
        byte num1 = buffer[startIndex + (int) DirectoryRecordConverter.DirectoryRecordDateTimeConverter.OFFSET_YEAR];
        byte num2 = buffer[startIndex + (int) DirectoryRecordConverter.DirectoryRecordDateTimeConverter.OFFSET_MONTH];
        byte num3 = buffer[startIndex + (int) DirectoryRecordConverter.DirectoryRecordDateTimeConverter.OFFSET_DAY];
        byte num4 = buffer[startIndex + (int) DirectoryRecordConverter.DirectoryRecordDateTimeConverter.OFFSET_HOUR];
        byte num5 = buffer[startIndex + (int) DirectoryRecordConverter.DirectoryRecordDateTimeConverter.OFFSET_MINUTE];
        byte num6 = buffer[startIndex + (int) DirectoryRecordConverter.DirectoryRecordDateTimeConverter.OFFSET_SECOND];
        try
        {
          return new DateTime?(new DateTime(1900 + (int) num1, (int) num2, (int) num3, (int) num4, (int) num5, (int) num6, DateTimeKind.Local));
        }
        catch (Exception ex)
        {
          logger.Error(ex);
          return new DateTime?();
        }
      }
    }
  }
}
