﻿// Decompiled with JetBrains decompiler
// Type: ImageReader.AssemblyGitBuildSHA1
// Assembly: ImageReader, Version=1.5.2.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: E0717604-B50B-4CB4-B85C-C17F43D5C04B
// Assembly location: ImageReader.dll

using System;

namespace ImageReader
{
  [AttributeUsage(AttributeTargets.Assembly)]
  public class AssemblyGitBuildSHA1 : Attribute
  {
    public string GitBuildSHA1 { get; private set; }

    public AssemblyGitBuildSHA1(string txt) => this.GitBuildSHA1 = txt;
  }
}
