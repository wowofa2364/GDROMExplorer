﻿// Decompiled with JetBrains decompiler
// Type: ImageReader.Logger
// Assembly: ImageReader, Version=1.5.2.0, Culture=neutral, PublicKeyToken=611be24fdeb07e08
// MVID: E0717604-B50B-4CB4-B85C-C17F43D5C04B
// Assembly location: ImageReader.dll

using log4net;
using log4net.Core;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;

namespace ImageReader
{
  internal static class Logger
  {
    private static readonly bool loggingIsOff = true;

    static Logger()
    {
      try
      {
        //Assembly.Load("log4net").GetType("log4net.Config.XmlConfigurator").GetMethod("Configure", new Type[0]).Invoke((object) null, (object[]) null);
        Assembly.Load("log4net");
        //log4net.Config.XmlConfigurator.Configure();
        Logger.loggingIsOff = false;
      }
      catch
      {
      }
    }

    public static Logger.ILog CreateLog()
    {
      Type declaringType = new StackFrame(1, false).GetMethod().DeclaringType;
      return !Logger.loggingIsOff ? (Logger.ILog) new Logger.Log4NetLogger(declaringType) : (Logger.ILog) new Logger.NoLog();
    }

    internal interface ILog
    {
      bool IsDebugEnabled { get; }

      bool IsInfoEnabled { get; }

      bool IsWarnEnabled { get; }

      bool IsErrorEnabled { get; }

      bool IsFatalEnabled { get; }

      void Debug(object message);

      void Debug(object message, Exception exception);

      void DebugFormat(string format, params object[] args);

      void DebugFormat(string format, object arg0);

      void DebugFormat(string format, object arg0, object arg1);

      void DebugFormat(string format, object arg0, object arg1, object arg2);

      void DebugFormat(IFormatProvider provider, string format, params object[] args);

      void Info(object message);

      void Info(object message, Exception exception);

      void InfoFormat(string format, params object[] args);

      void InfoFormat(string format, object arg0);

      void InfoFormat(string format, object arg0, object arg1);

      void InfoFormat(string format, object arg0, object arg1, object arg2);

      void InfoFormat(IFormatProvider provider, string format, params object[] args);

      void Warn(object message);

      void Warn(object message, Exception exception);

      void WarnFormat(string format, params object[] args);

      void WarnFormat(string format, object arg0);

      void WarnFormat(string format, object arg0, object arg1);

      void WarnFormat(string format, object arg0, object arg1, object arg2);

      void WarnFormat(IFormatProvider provider, string format, params object[] args);

      void Error(object message);

      void Error(object message, Exception exception);

      void ErrorFormat(string format, params object[] args);

      void ErrorFormat(string format, object arg0);

      void ErrorFormat(string format, object arg0, object arg1);

      void ErrorFormat(string format, object arg0, object arg1, object arg2);

      void ErrorFormat(IFormatProvider provider, string format, params object[] args);

      void Fatal(object message);

      void Fatal(object message, Exception exception);

      void FatalFormat(string format, params object[] args);

      void FatalFormat(string format, object arg0);

      void FatalFormat(string format, object arg0, object arg1);

      void FatalFormat(string format, object arg0, object arg1, object arg2);

      void FatalFormat(IFormatProvider provider, string format, params object[] args);
    }

    private class NoLog : Logger.ILog
    {
      public bool IsDebugEnabled => false;

      public bool IsInfoEnabled => false;

      public bool IsWarnEnabled => false;

      public bool IsErrorEnabled => false;

      public bool IsFatalEnabled => false;

      public void Debug(object message)
      {
      }

      public void Debug(object message, Exception exception)
      {
      }

      public void DebugFormat(string format, params object[] args)
      {
      }

      public void DebugFormat(string format, object arg0)
      {
      }

      public void DebugFormat(string format, object arg0, object arg1)
      {
      }

      public void DebugFormat(string format, object arg0, object arg1, object arg2)
      {
      }

      public void DebugFormat(IFormatProvider provider, string format, params object[] args)
      {
      }

      public void Info(object message)
      {
      }

      public void Info(object message, Exception exception)
      {
      }

      public void InfoFormat(string format, params object[] args)
      {
      }

      public void InfoFormat(string format, object arg0)
      {
      }

      public void InfoFormat(string format, object arg0, object arg1)
      {
      }

      public void InfoFormat(string format, object arg0, object arg1, object arg2)
      {
      }

      public void InfoFormat(IFormatProvider provider, string format, params object[] args)
      {
      }

      public void Warn(object message)
      {
      }

      public void Warn(object message, Exception exception)
      {
      }

      public void WarnFormat(string format, params object[] args)
      {
      }

      public void WarnFormat(string format, object arg0)
      {
      }

      public void WarnFormat(string format, object arg0, object arg1)
      {
      }

      public void WarnFormat(string format, object arg0, object arg1, object arg2)
      {
      }

      public void WarnFormat(IFormatProvider provider, string format, params object[] args)
      {
      }

      public void Error(object message)
      {
      }

      public void Error(object message, Exception exception)
      {
      }

      public void ErrorFormat(string format, params object[] args)
      {
      }

      public void ErrorFormat(string format, object arg0)
      {
      }

      public void ErrorFormat(string format, object arg0, object arg1)
      {
      }

      public void ErrorFormat(string format, object arg0, object arg1, object arg2)
      {
      }

      public void ErrorFormat(IFormatProvider provider, string format, params object[] args)
      {
      }

      public void Fatal(object message)
      {
      }

      public void Fatal(object message, Exception exception)
      {
      }

      public void FatalFormat(string format, params object[] args)
      {
      }

      public void FatalFormat(string format, object arg0)
      {
      }

      public void FatalFormat(string format, object arg0, object arg1)
      {
      }

      public void FatalFormat(string format, object arg0, object arg1, object arg2)
      {
      }

      public void FatalFormat(IFormatProvider provider, string format, params object[] args)
      {
      }
    }

    private class Log4NetLogger : Logger.ILog
    {
      private readonly log4net.ILog rootLogger;
      private readonly Type loggingType;
      private readonly ILogger logger;

      public Log4NetLogger(Type type)
      {
        this.loggingType = type;
        this.rootLogger = LogManager.GetLogger(this.loggingType);
        this.logger = ((ILoggerWrapper) this.rootLogger).Logger;
      }

      public bool IsDebugEnabled => this.rootLogger.IsDebugEnabled;

      public bool IsInfoEnabled => this.rootLogger.IsInfoEnabled;

      public bool IsWarnEnabled => this.rootLogger.IsWarnEnabled;

      public bool IsErrorEnabled => this.rootLogger.IsErrorEnabled;

      public bool IsFatalEnabled => this.rootLogger.IsFatalEnabled;

      public void Debug(object message)
      {
        if (!this.IsDebugEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Debug, message, (Exception) null);
      }

      public void Debug(object message, Exception exception)
      {
        if (!this.IsDebugEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Debug, message, exception);
      }

      public void DebugFormat(string format, params object[] args)
      {
        if (!this.IsDebugEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Debug, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, args), (Exception) null);
      }

      public void DebugFormat(string format, object arg0)
      {
        if (!this.IsDebugEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Debug, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, arg0), (Exception) null);
      }

      public void DebugFormat(string format, object arg0, object arg1)
      {
        if (!this.IsDebugEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Debug, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, arg0, arg1), (Exception) null);
      }

      public void DebugFormat(string format, object arg0, object arg1, object arg2)
      {
        if (!this.IsDebugEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Debug, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, arg0, arg1, arg2), (Exception) null);
      }

      public void DebugFormat(IFormatProvider provider, string format, params object[] args)
      {
        if (!this.IsDebugEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Debug, (object) string.Format(provider, format, args), (Exception) null);
      }

      public void Info(object message)
      {
        if (!this.IsInfoEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Info, message, (Exception) null);
      }

      public void Info(object message, Exception exception)
      {
        if (!this.IsInfoEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Info, message, exception);
      }

      public void InfoFormat(string format, params object[] args)
      {
        if (!this.IsInfoEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Info, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, args), (Exception) null);
      }

      public void InfoFormat(string format, object arg0)
      {
        if (!this.IsInfoEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Info, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, arg0), (Exception) null);
      }

      public void InfoFormat(string format, object arg0, object arg1)
      {
        if (!this.IsInfoEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Info, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, arg0, arg1), (Exception) null);
      }

      public void InfoFormat(string format, object arg0, object arg1, object arg2)
      {
        if (!this.IsInfoEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Info, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, arg0, arg1, arg2), (Exception) null);
      }

      public void InfoFormat(IFormatProvider provider, string format, params object[] args)
      {
        if (!this.IsInfoEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Info, (object) string.Format(provider, format, args), (Exception) null);
      }

      public void Warn(object message)
      {
        if (!this.IsWarnEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Warn, message, (Exception) null);
      }

      public void Warn(object message, Exception exception)
      {
        if (!this.IsWarnEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Warn, message, exception);
      }

      public void WarnFormat(string format, params object[] args)
      {
        if (!this.IsWarnEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Warn, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, args), (Exception) null);
      }

      public void WarnFormat(string format, object arg0)
      {
        if (!this.IsWarnEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Warn, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, arg0), (Exception) null);
      }

      public void WarnFormat(string format, object arg0, object arg1)
      {
        if (!this.IsWarnEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Warn, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, arg0, arg1), (Exception) null);
      }

      public void WarnFormat(string format, object arg0, object arg1, object arg2)
      {
        if (!this.IsWarnEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Warn, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, arg0, arg1, arg2), (Exception) null);
      }

      public void WarnFormat(IFormatProvider provider, string format, params object[] args)
      {
        if (!this.IsWarnEnabled)
          return;
        this.logger.Log(this.loggingType, (Level) Level.Warn, (object) string.Format(provider, format, args), (Exception) null);
      }

      public void Error(object message) => this.logger.Log(this.loggingType, (Level) Level.Error, message, (Exception) null);

      public void Error(object message, Exception exception) => this.logger.Log(this.loggingType, (Level) Level.Error, message, exception);

      public void ErrorFormat(string format, params object[] args) => this.logger.Log(this.loggingType, (Level) Level.Error, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, args), (Exception) null);

      public void ErrorFormat(string format, object arg0) => this.logger.Log(this.loggingType, (Level) Level.Error, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, arg0), (Exception) null);

      public void ErrorFormat(string format, object arg0, object arg1) => this.logger.Log(this.loggingType, (Level) Level.Error, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, arg0, arg1), (Exception) null);

      public void ErrorFormat(string format, object arg0, object arg1, object arg2) => this.logger.Log(this.loggingType, (Level) Level.Error, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, arg0, arg1, arg2), (Exception) null);

      public void ErrorFormat(IFormatProvider provider, string format, params object[] args) => this.logger.Log(this.loggingType, (Level) Level.Error, (object) string.Format(provider, format, args), (Exception) null);

      public void Fatal(object message) => this.logger.Log(this.loggingType, (Level) Level.Fatal, message, (Exception) null);

      public void Fatal(object message, Exception exception) => this.logger.Log(this.loggingType, (Level) Level.Fatal, message, exception);

      public void FatalFormat(string format, params object[] args) => this.logger.Log(this.loggingType, (Level) Level.Fatal, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, args), (Exception) null);

      public void FatalFormat(string format, object arg0) => this.logger.Log(this.loggingType, (Level) Level.Fatal, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, arg0), (Exception) null);

      public void FatalFormat(string format, object arg0, object arg1) => this.logger.Log(this.loggingType, (Level) Level.Fatal, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, arg0, arg1), (Exception) null);

      public void FatalFormat(string format, object arg0, object arg1, object arg2) => this.logger.Log(this.loggingType, (Level) Level.Fatal, (object) string.Format((IFormatProvider) CultureInfo.InvariantCulture, format, arg0, arg1, arg2), (Exception) null);

      public void FatalFormat(IFormatProvider provider, string format, params object[] args) => this.logger.Log(this.loggingType, (Level) Level.Fatal, (object) string.Format(provider, format, args), (Exception) null);
    }
  }
}
